$(function() {
    function emptyField(fieldName)
    {
        $("#"+fieldName).empty().append("<option value=''>Select " + titleCase(fieldName) + "</option>");
    }

    function disableField(fieldName)
    {
        $("#"+fieldName).prop('disabled', 'disabled');
    }

    function titleCase(string)
    {
        return string.charAt(0).toUpperCase() + string.slice(1);
    }
    
    $("#country").on('change',function() {
        if($(this).val() === '')
        {
            emptyField('state');
            disableField('state');

            emptyField('city');
            disableField('city');
        }
        else
        {
            $.ajax({
                url: "/register/getStates",
                type: "get",
                data: {
                    strCountryCode: $(this).val()
                },
                dataType: "json",
                success: function(response)
                {
                    emptyField('state');
                    $.each(response, function(i, item) {
                        $("#state").prop('disabled', false);

                        if ($('#state').attr('old-value') == item.state_id)
                        {
                            $("#state").append("<option value='" + item.state_id + "' selected='selected'>"+ item.state_name +" </option>");
                        }
                        else
                        {
                            $("#state").append("<option value='" + item.state_id + "' >"+ item.state_name +" </option>");
                        }
                    });

                    if ($('#state').attr('old-value'))
                    {
                        $('#state').trigger('change');
                    }
                },
                error: function()
                {
                    emptyField('state');
                    disableField('state');
                }
            });
        }
    });

    $("#state").on('change',function() {
        if($(this).val() === '')
        {
            emptyField('city');
            disableField('city');
        }
        else
        {
            $.ajax({
                url: "/register/getCities",
                type: "get",
                data:{
                    intStateID: $(this).val()
                },
                dataType: "json",
                success: function(response)
                {
                    emptyField('city');
                    $.each(response, function(i, item) {
                        $("#city").prop('disabled', false);

                        if ($('#city').attr('old-value') == item.city_id)
                        {
                            $("#city").append("<option value='" + item.city_id + "' selected='selected'>"+ item.city_name +" </option>");
                        }
                        else
                        {
                            $("#city").append("<option value='" + item.city_id + "' >"+ item.city_name +" </option>");
                        }
                    })
                },
                error: function(xhr)
                {
                    emptyField('city');
                    disableField('city');
                }
            });
        }
    });
    
    if($("#country").val() === '')
    {
        emptyField('state');
        disableField('state');

        emptyField('city');
        disableField('city');
    }
    else 
    {
        $('#country').trigger('change');
    }

    if($("#state").val() === '')
    {
        emptyField('city');
        disableField('city');
    }
});