(function(angular) {
    "use strict";
    
    angular.module('Product', 
        [
            'Http',
            'ngRoute',
            'User',
            'Auth',
            'Paginator',
            'Campaign'
        ]);
    
})(window.angular);