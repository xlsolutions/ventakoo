(function(angular) {
    "use strict";

    angular.module('Product')
        .value('ProductAttributeEntity', {
            handle: null,
            description: null,
            value: null
        })
        .value('ProductAttributeEntitySet', [])
        .value('ProductAttributeDictionary', {});
})(window.angular);