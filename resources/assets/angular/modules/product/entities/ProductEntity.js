(function(angular) {
    "use strict";

    angular.module('Product')
        .value('ProductEntity', {
            handle: null,
            ownerId: null,
            owner: null,
            quantity: 0,
            price: 0,
            datePosted: null,
            category: null,
            visibility: 'PUBLIC',
            name: null,
            description: null,
            imageOriginal: null,
            imageThumbnail: null,
            campaignState: null, 
            campaignId: 0,
            timeStart: 0,
            timeEnd: 0,
            timeDuration: 0
        })
        .value('ProductEntitySet', []);
})(window.angular);