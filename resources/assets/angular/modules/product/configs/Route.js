(function(angular) {
    "use strict";

    angular.module('Product')
        .config([
            '$routeProvider',
            '$locationProvider',
            RouteConfig
        ]);

    function RouteConfig($routeProvider, $locationProvider) {
        $locationProvider.html5Mode({
            enabled: false,
            requireBase: false
        });

        $routeProvider
            .when('/user/:userId/product', {
                templateUrl: '/angular/modules/product/views/pages/index.html'
            })
            .when('/product', {
                templateUrl: '/angular/modules/product/views/pages/index.html'
            })
            //@todo: convert to component
            .when('/product/campaign', {
                templateUrl: '/angular/modules/product/views/pages/campaign.html',
                controller: 'ProductCampaignController',
                controllerAs: 'productCampaign'
            })
            .when('/product/sell/:handle?', {
                templateUrl: '/angular/modules/product/views/pages/sell.html'
            })
            //@todo: convert to component
            .when('/preview/product/:handle', {
                templateUrl: '/angular/modules/product/views/pages/view.html',
                controller: 'ProductViewController',
                controllerAs: 'productView'
            })
            //@todo: convert to component
            .when('/product/:handle?', {
                templateUrl: '/angular/modules/product/views/pages/view.html',
                controller: 'ProductCampaignViewController',
                controllerAs: 'productView'
            });
    }

})(window.angular);