(function(angular, _) {
    "use strict";

    angular.module('Product')
        .controller('ProductCampaignController', [
            '$location',
            'AuthService',
            'PaginatorService',
            'ProductService',
            'CampaignService',
            ProductCampaignController
        ]);

    function ProductCampaignController($location,
                                       AuthService,
                                       PaginatorService,
                                       ProductService,
                                       CampaignService
    ) {

        var self = this;

        self.defaultFilterState = $location.search().state || 'DRAFT';

        var reset = function() {
            self.products = {
                DRAFT: [],
                FOR_REVIEW: [],
                REVIEWING: [],
                APPROVED: [],
                START: [],
                PAUSED: [],
                COMPLETED: [],
                CANCELLED: [],
                DECLINED: []
            };
        };

        var refresh = function() {
            reset();
            
            //@todo: get cache key and remove it if needed only instead of always getting fresh data
            var bolCache = false;
            
            ProductService.GetAllByUser(AuthService.GetData('user_id'), PaginatorService.getParam(), bolCache)
                .then(function(response) {
                    _.forEach(response, function (objProduct) {
                        self.products[objProduct.getCampaignState()].push(objProduct);
                    });
                });
        };

        var init = function() {
            PaginatorService.setLimit(25);
            
            reset();

            AuthService.RefreshAuth()
                .then(function(response) {
                    if (!response.user_id) {
                        window.location = '/login';
                    } else {
                        ProductService.CountAllByUser(AuthService.GetData('user_id'))
                            .then(function(response){
                                PaginatorService.setRecordCount(response);
                            });
                        
                        refresh();
                    }
                });
        };

        self.prevPage = function() {
            PaginatorService.prevPage(refresh);
        };
        self.nextPage = function() {
            PaginatorService.nextPage(refresh);
        };
        self.hidePrevBtn = PaginatorService.hidePrevIndicator;
        self.hideNextBtn = PaginatorService.hideNextIndicator;
        self.currentPage = PaginatorService.getPage;
        self.totalPage = PaginatorService.getTotalPage;
        
        self.isCampaignStateBy = function(strState) {
            return self.defaultFilterState == strState;    
        };
        
        self.getTotalRecordByCampaignState = function(strState) {
            return self.products[strState].length;
        };
        
        self.getProductList = function() {
            return self.products[self.defaultFilterState];    
        };

        self.showButtonActionsFor = function(strBtnState) {            
            switch(strBtnState) {
                case 'FOR_REVIEW':
                    return _.includes(['DRAFT','DECLINED','CANCELLED'], self.defaultFilterState);

                case 'START':
                    return _.includes(['APPROVED', 'PAUSED'], self.defaultFilterState);

                case 'PAUSE':
                    return _.includes(['START'], self.defaultFilterState);

                case 'TRASH':
                    return _.includes(['DRAFT', 'FOR_REVIEW', 'DECLINED', 'CANCELLED'], self.defaultFilterState);

                case 'CANCEL':
                    return _.includes(['FOR_REVIEW', 'APPROVED'], self.defaultFilterState);

                case 'EDIT':
                    return _.includes(['DRAFT', 'DECLINED', 'CANCELLED'], self.defaultFilterState);
            }
        };
        
        self.moveToTrash = function(intProductIndex, strHandle) {
            if (self.loading) {
                return false;
            }

            if (confirm('Warning: Are you sure do you want to delete this?')) {
                self.loading = true;

                ProductService.GetData().handle = strHandle;
                
                ProductService.Delete()
                    .then(function() {
                        self.products[self.defaultFilterState].splice(intProductIndex, 1);
                        
                        alert('Campaign has been moved to trash');
                    })
                    .finally(function() {
                        self.loading = false;
                    });
            }
        };
        
        self.submitCampaignFor = function(intIndex, objProduct, postCampaignState) {
            var intAutoPublish = 0;
            
            if (self.loading) {
                return false;
            }

            var strStateMessage = '';

            switch(postCampaignState)
            {
                case 'FOR_REVIEW':
                    strStateMessage = 'submit';
                    break;

                case 'CANCEL':
                    strStateMessage = 'cancel';
                    break;

                case 'START':
                    strStateMessage = 'start';
                    break;

                case 'PAUSED':
                    strStateMessage = 'pause';
                    break;
            }

            if (confirm('Are you sure do you want to ' + strStateMessage + ' this campaign?')) {
                self.loading = true;

                var objCampaign = CampaignService.GetData();
                objCampaign.productId = objProduct.getHandle();
                objCampaign.campaignId = objProduct.getCampaignId();
                objCampaign.post_status = postCampaignState;

                if (postCampaignState === 'FOR_REVIEW') {
                    intAutoPublish = confirm('Do you want to auto publish your campaign upon approval?') ? 1 : 0;

                    objCampaign.auto_publish = intAutoPublish;
                }

                var strCurrentCampaignState = objProduct.getCampaignState();

                CampaignService.Save(objCampaign)
                    .then(function(response) {
                        objProduct.setCampaignId(response.campaignId);
                        objProduct.setCampaignState(postCampaignState);
                        
                        self.products[strCurrentCampaignState].splice(intIndex, 1);
                        self.products[postCampaignState].push(objProduct);
                        
                        alert('Request submitted');
                    })
                    .finally(function() {
                        self.loading = false;
                    });
            }
        };
        
        init();
    }
})(window.angular, window._);