(function(angular, _) {
    "use strict";

    angular.module('Product')
        .controller('ProductCampaignViewController', [
            '$routeParams',
            'ProductEntity',
            'ProductCampaignService',
            'ProductAttributeService',
            'UserService',
            'CategoryService',
            ProductCampaignViewController
        ]);

    function ProductCampaignViewController($routeParams,
                                   ProductEntity,
                                   ProductCampaignService,
                                   ProductAttributeService,
                                   UserService,
                                   CategoryService) {

        var self = this;

        self.model = {
            item: {},
            attributes: {},
            categories: [],
            seller: {}
        };

        ProductEntity.handle = $routeParams.handle;

        self.isImage = function(strHandle) {
            return _.includes(['IMAGE_GALLERY', 'IMAGE_THUMBNAIL', 'IMAGE_ORIGINAL'], strHandle);
        };

        (function() {
            ProductCampaignService.GetByID()
                .then(function(objProduct) {
                    self.model.item = ProductCampaignService.NormalizeDataSet(objProduct);

                    self.photoView = self.model.item.getPhotoOriginal();

                    CategoryService.GetAll()
                        .then(function(response) {

                            if (response.length > 0) {
                                var arrCategory = CategoryService.GetParentHandle(self.model.item.getCategory());

                                self.model.categories = _.map(arrCategory.reverse(), function(strHandle) {
                                    return CategoryService.GetHandleFromDictionary(strHandle);
                                });
                            }
                        });

                    UserService.GetByHandle(self.model.item.getUserId())
                        .then(function(objUser) {
                            self.model.seller = objUser;
                        });

                    ProductAttributeService.GetByID()
                        .then(function (objAttributeSet) {
                            angular.forEach(objAttributeSet, function (objAttribute) {

                                if (!self.model.attributes[objAttribute.handle]) {
                                    self.model.attributes[objAttribute.handle] = [];
                                }

                                self.model.attributes[objAttribute.handle].push(objAttribute);
                            });
                        });

                    ProductCampaignService.GetAllByUser(self.model.item.getUserId(), {
                        limit: 3,
                        page: 1,
                        excludeProductHandle: ProductEntity.handle,
                        campaignState: 'START'
                    })
                        .then(function(response) {
                            self.campaignProductList = response;

                            angular.forEach(self.campaignProductList, function (objProductSet) {
                                CategoryService.GetAll()
                                    .then(function(response) {

                                        if (response.length > 0) {
                                            var arrCategory = CategoryService.GetParentHandle(objProductSet.getCategory());

                                            objProductSet.categories = _.map(arrCategory.reverse(), function(strHandle) {
                                                return CategoryService.GetHandleFromDictionary(strHandle);
                                            });
                                        }
                                    });
                            });
                        });
                });
        })();
    }
})(window.angular, window._);