(function(angular, _) {
    "use strict";

    angular.module('Product')
        .controller('ProductViewController', [
            '$routeParams',
            '$location',
            'ProductEntity',
            'ProductService',
            'ProductAttributeService',
            'UserService',
            'CategoryService',
            'AuthService',
            ProductViewController
        ]);

    function ProductViewController($routeParams,
                                   $location,
                                   ProductEntity,
                                   ProductService,
                                   ProductAttributeService,
                                   UserService,
                                   CategoryService,
                                   AuthService) {

        var self = this;

        self.model = {
            item: {},
            attributes: {},
            categories: [],
            seller: {}
        };

        ProductEntity.handle = $routeParams.handle;

        self.isImage = function(strHandle) {
            return _.includes(['IMAGE_GALLERY', 'IMAGE_THUMBNAIL', 'IMAGE_ORIGINAL'], strHandle);
        };
        
        (function() {
            ProductService.GetByID()
                .then(function(objProduct) {
                    self.model.item = ProductService.NormalizeDataSet(objProduct);

                    //@readme: preview state only, meaning only the owner can view this unless the campaign state is START then other people can view this
                    if ((self.model.item.getUserId() != AuthService.GetData('user_id'))
                        && self.model.item.getTimeStart() != null
                        && self.model.item.getTimeDuration() == 0
                    ) {
                        $location.path('/404');

                        return false;
                    }

                    self.photoView = self.model.item.getPhotoOriginal();
                    
                    CategoryService.GetAll()
                        .then(function(response) {

                            if (response.length > 0) {
                                var arrCategory = CategoryService.GetParentHandle(self.model.item.getCategory());

                                self.model.categories = _.map(arrCategory.reverse(), function(strHandle) {
                                    return CategoryService.GetHandleFromDictionary(strHandle);
                                });
                            }
                        });

                    UserService.GetByHandle(self.model.item.getUserId())
                        .then(function(objUser) {
                            self.model.seller = objUser;
                        });

                    ProductAttributeService.GetByID()
                        .then(function (objAttributeSet) {
                            angular.forEach(objAttributeSet, function (objAttribute) {

                                if (!self.model.attributes[objAttribute.handle]) {
                                    self.model.attributes[objAttribute.handle] = [];
                                }

                                self.model.attributes[objAttribute.handle].push(objAttribute);
                            });
                        });

                    ProductService.GetAllByUser(self.model.item.getUserId(), {
                        limit: 3,
                        page: 1,
                        excludeProductHandle: ProductEntity.handle,
                        campaignState: 'START'
                    })
                        .then(function(response) {
                            self.campaignProductList = response;
                            
                            angular.forEach(self.campaignProductList, function (objProductSet) {
                                CategoryService.GetAll()
                                    .then(function(response) {

                                        if (response.length > 0) {
                                            var arrCategory = CategoryService.GetParentHandle(objProductSet.getCategory());

                                            objProductSet.categories = _.map(arrCategory.reverse(), function(strHandle) {
                                                return CategoryService.GetHandleFromDictionary(strHandle);
                                            });
                                        }
                                    });
                            });
                        });
                });
        })();
    }
})(window.angular, window._);