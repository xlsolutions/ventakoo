(function(angular) {
    "use strict";

    angular.module('Product')
        .controller('ProductSellController', [
            '$q',
            '$routeParams',
            '$location',
            'WatchService',
            'ProductAttributeService',
            'ProductService',
            'AuthService',
            ProductSellController
        ]);

    function ProductSellController($q,
                                   $routeParams,
                                   $location,
                                   WatchService,
                                   ProductAttributeService,
                                   ProductService,
                                   AuthService
    ) {

        var self = this;
        var bolSubmitting = false;
        var bolSellProductEditPage = !_.isUndefined($routeParams.handle);

        var addAttribute = function(unkObject) {
            unkObject['id'] = ProductAttributeService.GetClientID();

            self.item.attributes.push(unkObject);

            refreshDomSelectAttribute();
        };
        
        var refreshDomSelectAttribute = function() {
            self.item.domSelectAttributes = self.item.domSelectAttributes || [];

            angular.forEach(_.unionBy(self.item.attributes, 'handle'), function (objAttribute) {
                
                if (!_.findKey(self.item.domSelectAttributes, { handle: objAttribute.handle })) {
                    self.item.domSelectAttributes.push({
                        handle: objAttribute.handle,
                        description: objAttribute.description
                    });
                }
            });
        };

        var shouldDeleteAttributes = function() {
            var defer = $q.defer();

            if (self.productAttributeTrashBin) {
                var intCountDeleted = 0;

                angular.forEach(self.productAttributeTrashBin, function (strHandle) {
                    ProductAttributeService.DeleteAttribute(strHandle)
                        .finally(function () {
                            intCountDeleted++;
                        });
                });

                var objInterval = setInterval(function() {
                    if (intCountDeleted === self.productAttributeTrashBin.length) {
                        clearInterval(objInterval);
                        defer.resolve(true);
                        self.productAttributeTrashBin = [];
                    }
                }, 500);
            } else {
                defer.resolve(true);
            }

            return defer.promise;
        };

        self.item = {
            model: ProductService.GetData(),
            attributes: []
        };
        self.productAttributeTrashBin = [];

        self.shouldDisplayAttribute = function(strTab) {
            return function (attribute) {
                switch (strTab) {
                    case 'attributeTab':
                        return !_.includes(['NAME', 'DESCRIPTION', 'IMAGE_THUMBNAIL', 'IMAGE_ORIGINAL', 'IMAGE_GALLERY'], attribute.handle);

                    case 'galleryTab':
                        return _.includes(['IMAGE_ORIGINAL', 'IMAGE_GALLERY'], attribute.handle);

                    default:
                        return true;
                }
            };
        };

        self.toggleAttribute = function(strType, intIndex) {
            if (strType === 'add') {
                addAttribute({
                    handle: '',
                    description: 'other',
                    value: null,
                    custom: true
                });
            } else {
                intIndex = _.findKey(self.item.attributes, { id: intIndex });

                self.item.attributes.splice(intIndex, 1);
            }
        };

        self.trashProductAttribute = function(strHandle, intIndex) {
            intIndex = _.findKey(self.item.attributes, { id: intIndex });

            self.item.attributes.splice(intIndex, 1);

            if (!_.includes(self.productAttributeTrashBin, strHandle)) {
                self.productAttributeTrashBin.push(strHandle);
            }
        };

        self.addPhotoGallery = function() {
            var objProductAttributeGallery = ProductAttributeService.GetHandleFromDictionary('IMAGE_GALLERY');

            if (objProductAttributeGallery) {
                var cloneProductAttributeGallery = angular.copy(objProductAttributeGallery);

                cloneProductAttributeGallery.value = null;

                addAttribute(cloneProductAttributeGallery);
            }
        };

        self.resetAttribute = function () {            
            if (bolSellProductEditPage && confirm('Warning: Are you sure want to reset all attributes?')) {
                if (bolSubmitting) {
                    return false;
                }

                bolSubmitting = true;

                ProductAttributeService.DeleteAllAttribute()
                    .then(function(bolResult) {
                        if (bolResult) {
                            angular.forEach(self.item.attributes, function (objDefaultAttribute, intIndex) {
                                self.item.attributes[intIndex]['value'] = null;
                            });   
                        }
                    })
                    .finally(function() {
                        bolSubmitting = false;
                    });
            }
        };

        self.save = function() {
            if (bolSubmitting) {
                return false;
            }

            bolSubmitting = true;

            shouldDeleteAttributes().then(function() {
                ProductService.Save(self.item.model)
                    .then(function(response) {
                        if (response.handle) {
                            var objProductAttributeName = ProductAttributeService.GetHandleFromDictionary('NAME');

                            if (objProductAttributeName) {
                                var intProductAttributeNameIndex = _.findKey(self.item.attributes, objProductAttributeName);
                                self.item.attributes[intProductAttributeNameIndex]['value'] = self.item.model.name;
                            }

                            var objProductAttributeDescription = ProductAttributeService.GetHandleFromDictionary('DESCRIPTION');

                            if (objProductAttributeDescription) {
                                var intProductAttributeDescriptionIndex = _.findKey(self.item.attributes, objProductAttributeDescription);
                                self.item.attributes[intProductAttributeDescriptionIndex]['value'] = self.item.model.description;
                            }

                            var objProductAttributeImageThumbnail = ProductAttributeService.GetHandleFromDictionary('IMAGE_THUMBNAIL');
                            var objProductAttributeImageOriginal = ProductAttributeService.GetHandleFromDictionary('IMAGE_ORIGINAL');

                            if (objProductAttributeImageThumbnail) {
                                var intProductAttributeImageThumbnailIndex = _.findKey(self.item.attributes, objProductAttributeImageThumbnail);
                                self.item.attributes[intProductAttributeImageThumbnailIndex]['value'] = objProductAttributeImageOriginal.value;
                            }

                            ProductAttributeService.Save(self.item.attributes);

                            if (!bolSellProductEditPage) {
                                $location.path('/product/sell/' + response.handle);
                            }

                            WatchService.Notify('OK_REQUEST', 'SHOW_TOAST_OK', ['Product successfully saved.']);
                        }
                    })
                    .finally(function() {
                        bolSubmitting = false;
                    });
            });
        };

        (function() {
            AuthService.RefreshAuth()
                .then(function(response) {
                    if (!response.user_id) {
                        window.location = '/login';
                    } else {
                        ProductAttributeService.GetAllByUserId(response.user_id)
                            .then(function(response) {
                                self.item.attributes = _.sortBy(response, ['handle']);

                                refreshDomSelectAttribute();
                            })
                            .finally(function() {
                                if (bolSellProductEditPage) {
                                    ProductAttributeService.GetByID()
                                        .then(function (response) {
                                            _.mapValues(response, function(objAttribute) {
                                                var bolFound = false;

                                                angular.forEach(self.item.attributes, function (objDefaultAttribute, intIndex) {
                                                    if (objAttribute) {
                                                        if ((objAttribute.handle == objDefaultAttribute.handle) && !bolFound) {
                                                            bolFound = true;

                                                            if (self.item.attributes[intIndex]['value']) {
                                                                addAttribute(objAttribute);
                                                            } else {
                                                                self.item.attributes[intIndex]['value'] = objAttribute.value;
                                                            }
                                                        }
                                                    }
                                                });
                                            });

                                            self.item.attributes = _.sortBy(self.item.attributes, ['handle']);
                                        });
                                }
                            });
                    }
                });
            
            if (bolSellProductEditPage) {
                self.item.model.handle = $routeParams.handle;

                ProductService.GetByIDToSell()
                    .then(function(objProduct) {
                        self.item.model = objProduct;
                    });
            } else {
                self.item.model = _.mapValues(self.item.model, function() {
                    return null;
                });
            }
        })();
    }
})(window.angular, window._);