(function(angular) {
    "use strict";

    angular.module('Product')
        .controller('ProductController', [
            '$routeParams',
            '$location',
            'ProductCampaignService',
            'PaginatorService',
            ProductController
        ]);

    function ProductController($routeParams, $location, ProductCampaignService, PaginatorService) {
        var self = this;

        PaginatorService.setLimit(self.limit || 12);

        self.products = [];

        self.getTemplate = function() {
            return self.template || '/angular/modules/product/views/components/list.html';
        };

        var init = function() {
	        var objQueryParams = $location.search();
	        
	        self.subStr = (/MAIN/).test(objQueryParams.category || null);

            PaginatorService.setParam({
               category: objQueryParams.category || null
            });
            
            if ($routeParams.userId) {
                ProductCampaignService.CountAllByUser($routeParams.userId)
                    .then(function (response) {
                        PaginatorService.setRecordCount(response);
                    });
            } else {
                ProductCampaignService.CountAll()
                    .then(function (response) {
                        PaginatorService.setRecordCount(response);
                    });
            }
            
            
	        refresh();
        };

        var refresh = function() {
	        self.showOverlay = true;
            
            self.products = [];

            if ($routeParams.userId) {
                ProductCampaignService.GetAllByUser($routeParams.userId)
                    .then(function(response) {
                        self.products = response;
                    })
                    .finally(function() {
                        self.showOverlay = false;
                    });
            } else {
                ProductCampaignService.GetAll(PaginatorService.getParam())
                    .then(function(response) {
                        self.products = response;
                    })
                    .finally(function() {
                        self.showOverlay = false;
                    });
            }

        };

        self.prevPage = function() {
            PaginatorService.prevPage(refresh);
        };
        self.nextPage = function() {
            PaginatorService.nextPage(refresh);
        };
        self.hidePrevBtn = PaginatorService.hidePrevIndicator;
        self.hideNextBtn = PaginatorService.hideNextIndicator;
        self.currentPage = PaginatorService.getPage;
        self.totalPage = PaginatorService.getTotalPage;
        
        init();
    }
})(window.angular);