(function(angular) {

    angular.module('Product')
        .factory('ProductBuilder', [
            '$filter',
            ProductBuilder
        ]);

    function ProductBuilder($filter) {

        function Product(objProduct) {

            var strHandle = objProduct.handle || null;
            var strOwner = objProduct.owner || null;
            var intOwnerId = objProduct.ownerId || null;
            var intQuantity = objProduct.quantity || null;
            var decPrice = objProduct.price || null;
            var dtDatePosted = new Date(objProduct.datePosted) || null;
            var strCategory = objProduct.category || null;
            var strVisibility = objProduct.visibility || 'HIDDEN';
            var strName = objProduct.name || null;
            var strDescription = objProduct.description || null;
            var strPhotoOriginal = objProduct.imageOriginal || null;
            var strPhotoThumbnail = objProduct.imageThumbnail || null;
            var strCampaignState = objProduct.campaignState || null;
            var intCampaignId = objProduct.campaignId || null;
            var intTimeStart = objProduct.timeStart || null;
            var intTimeEnd = objProduct.timeEnd || null;
            var intTimeDuration = objProduct.timeDuration;

            this.getHandle = function () {
                return strHandle;
            };

            this.getOwner = function() {
                return strOwner;
            };
            
            this.getUserId = function() {
                return intOwnerId;
            };

            this.getQuantity = function() {
                return intQuantity;
            };

            this.getPrice = function() {
                return $filter('currency')(decPrice, 'Php ', 2) || 'Contact Seller';
            };

            this.getDatePosted = function () {
                return $filter('date')(dtDatePosted, 'EEE, MMM dd, yyyy');
            };

            this.getCategory = function() {
                return strCategory;
            };

            this.getVisibility = function () {
                return strVisibility;
            };

            this.getName = function() {
                return strName;
            };

            this.getDescription = function () {
                return strDescription;
            };

            this.getPhotoOriginal = function () {
                return strPhotoOriginal;
            };
            
            this.getPhotoThumbnail = function () {
                return strPhotoThumbnail;
            };
            
            this.getCampaignState = function () {
                return strCampaignState;    
            };

            this.getCampaignId = function () {
                return intCampaignId;
            };
            
            this.getTimeStart = function() {
                return intTimeStart;  
            };
            
            this.getTimeEnd = function() {
                return intTimeEnd;
            };
            
            this.getTimeDuration = function() {
                return intTimeDuration;
            };

            this.setCampaignId = function (intId) {
                intCampaignId = intId;
            };

            this.setCampaignState = function (strState) {
                strCampaignState = strState;
            };
        }

        return {
            build: function(objProduct) {
                return new Product(objProduct);   
            }
        };
    }

})(window.angular);