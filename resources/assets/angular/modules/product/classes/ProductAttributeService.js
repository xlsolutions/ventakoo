(function(_, angular) {
    "use strict";

    angular.module('Product')
        .service('ProductAttributeService', [
            '$q',
            'ProductEntity',
            'ProductAttributeEntity',
            'ProductAttributeEntitySet',
            'ProductAttributeDictionary',
            'HttpService',
            ProductAttributeService
        ]);

    function ProductAttributeService($q, 
                                     ProductEntity, 
                                     ProductAttributeEntity, 
                                     ProductAttributeEntitySet, 
                                     ProductAttributeDictionary, 
                                     HttpService) {

        var self = this;
        var objCloneEntity = angular.copy(ProductAttributeEntity);
        var strEntity = JSON.stringify([]);
        var intAttributeID = 0;

        var Reset = function() {
            strEntity = JSON.stringify([]);
        };
        
        self.GetClientID = function() {
            return intAttributeID++;    
        };

        self.GetData = function() {
            return ProductAttributeEntitySet;
        };

        self.SetData = function(unkData) {
            ProductAttributeEntitySet = unkData;

            strEntity = JSON.stringify(ProductAttributeEntitySet);
        };

        self.NormalizeDataSet = function(unkData) {
            return _.assign(angular.copy(objCloneEntity), unkData);
        };

        /**
         * To be able to use this properly Product Handle should be set somewhere
         *
         * Example:
         *
         * 1. In a component bindings <my-component product-id="PUT_BINDING_HANDLE_HERE"></my-component>,
         *   then set the ProductEntity.handle in that component's controller
         *
         * 2. By manually setting the ProductEntity.handle = ?
         *
         * @return: promise
         *
         * */
        self.GetByID = function() {
            var strHandle = ProductEntity.handle;

            if (strHandle && ((JSON.stringify(ProductAttributeEntitySet) === strEntity) && (ProductAttributeEntitySet.length > 0))) {
                return $q.when(self.GetData());
            } else if (strHandle) {
                return HttpService.GetList('/rest/1/product/' + strHandle + '/attribute')
                    .then(function(response) {
                        self.SetData(response);

                        return self.GetData();
                    });
            } else {
                return $q.when(false);
            }
        };

        /**
         *
         * @return: promise
         *
         * */
        self.GetAllByUserId = function(intUserId) {
            Reset();

            return HttpService.GetList('/rest/1/user/' + intUserId + '/product/attribute')
                .then(function(response) {
                    if (!_.isUndefined(response)) {
                        ProductAttributeEntitySet = [];

                        for(var intIndex in response) {
                            
                            if (response[intIndex] && response[intIndex]['handle']) {
                                var objAttributeEntity = self.NormalizeDataSet(response[intIndex]);

                                objAttributeEntity['id'] = self.GetClientID();

                                ProductAttributeEntitySet.push(objAttributeEntity);

                                ProductAttributeDictionary[ProductAttributeEntitySet[intIndex]['handle']] = ProductAttributeEntitySet[intIndex];   
                            }
                        }
                    }

                    return ProductAttributeEntitySet;
                });
        };

        /**
         * @return: promise
         *
         * */
        self.Save = function(arrProductAttributeEntity) {
            var strProductAttributeEntity = JSON.stringify(arrProductAttributeEntity);
            var strProductHandle = ProductEntity.handle;

            if (strEntity === strProductAttributeEntity) {
                return $q.when(self.GetData());
            } else if (strProductHandle) {
                return HttpService.Post('/rest/1/product/' + strProductHandle + '/attribute', arrProductAttributeEntity)
                    .then(function() {
                        self.SetData(arrProductAttributeEntity);
                        
                        return strProductHandle;
                    });
            }
        };

        /**
         *
         * @return: promise
         *
         * */
        self.DeleteAllAttribute = function() {
            var strProductHandle = ProductEntity.handle;
            
            if (strProductHandle) {
                return HttpService.Delete('/rest/1/product/' + strProductHandle, 'attribute')
                    .then(function() {
                        return true;
                    });
            } else {
                return $q.when(false);
            }
        };

        /**
         *
         * @return: promise
         *
         * */
        self.DeleteAttribute = function(strHandle) {
            var strProductHandle = ProductEntity.handle;

            if (strProductHandle) {
                return HttpService.Delete('/rest/1/product/' + strProductHandle + '/attribute', strHandle)
                    .then(function() {
                        return true;
                    });
            } else {
                return $q.when(false);
            }
        };

        /**
         * Used to find the record in a dictionary
         *
         * @return object | null
         *
         * */
        self.GetHandleFromDictionary = function(strHandle) {
            return ProductAttributeDictionary[strHandle] || null;
        };
        
        return self;
    }
})(window._, window.angular);