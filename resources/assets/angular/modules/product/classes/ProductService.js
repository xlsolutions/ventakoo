(function(_, angular) {
    "use strict";
    
    angular.module('Product')
        .service('ProductService', [
            '$q',
            'ProductBuilder',
            'ProductEntity',
            'ProductEntitySet',
            'HttpService',
            ProductService
        ]);

    function ProductService($q,
                            ProductBuilder,
                            ProductEntity, 
                            ProductEntitySet, 
                            HttpService) {

        var self = this;
        var objCloneEntity = angular.copy(ProductEntity);
        var strEntity = JSON.stringify(ProductEntity);
        
        var Reset = function() {
            strEntity = JSON.stringify(objCloneEntity);
        };

        var SetData = function(unkData) {
            ProductEntity = _.assign(self.GetData(), unkData);

            strEntity = JSON.stringify(ProductEntity);
        };
        
        self.GetData = function() {
            return ProductEntity;
        };
        
        self.NormalizeDataSet = ProductBuilder.build;

        /**
         * To be able to use this properly Product Handle should be set somewhere
         * 
         * Example: 
         * 
         * 1. In a component bindings <my-component product-id="PUT_BINDING_HANDLE_HERE"></my-component>, 
         *   then set the ProductEntity.handle in that component's controller
         *   
         * 2. By manually setting the ProductEntity.handle = ?
         *
         * @return: promise
         *
         * */
        self.GetByID = function() {
            var strHandle = ProductEntity.handle;
            
            if (strHandle && (JSON.stringify(ProductEntity) === strEntity)) {
                return $q.when(self.GetData());
            } else if (strHandle) {
                return HttpService.Get('/rest/1/product/' + strHandle)
                    .then(function(response) {
                        SetData(response);

                        return self.GetData();
                    });
            } else {
                return $q.when(false);
            }
        };

        /**
         * @readme: get/access product by the owner only
         * To be able to use this properly Product Handle should be set somewhere
         *
         * Example:
         *
         * 1. In a component bindings <my-component product-id="PUT_BINDING_HANDLE_HERE"></my-component>,
         *   then set the ProductEntity.handle in that component's controller
         *
         * 2. By manually setting the ProductEntity.handle = ?
         *
         * @return: promise
         *
         * */
        self.GetByIDToSell = function() {
            var strHandle = ProductEntity.handle;

            return HttpService.Get('/rest/1/sell/product/' + strHandle)
                .then(function(response) {
                    SetData(response);

                    return self.GetData();
                });
        };

        /**
         *
         * @return: promise
         *
         * */
        self.GetAll = function(objParam) {
            Reset();
            
            return HttpService.GetList('/rest/1/product', objParam)
                .then(function(response) {
                    ProductEntitySet = [];
                    
                    _.forEach(response, function(objProduct) {
                        ProductEntitySet.push(self.NormalizeDataSet(objProduct));
                    });

                    return ProductEntitySet;
                });
        };
        
        /**
         *
         * @return: promise
         *
         * */
        self.GetAllByUser = function(intUserId, objParam, bolCache) {
            Reset();

            return HttpService.GetList('/rest/1/user/' + intUserId + '/product', objParam, bolCache)
                .then(function(response) {
                    ProductEntitySet = [];

                    _.forEach(response, function(objProduct) {
                        ProductEntitySet.push(self.NormalizeDataSet(objProduct));
                    });

                    return ProductEntitySet;
                });
        };

        /**
         *
         * @return: promise
         *
         * */
        self.CountAll = function() {
            Reset();

            ProductEntitySet = [];

            return HttpService.Get('/rest/1/product/count')
                .then(function(response) {
                    if (!_.isUndefined(response)) {
                        return response;
                    }
                });
        };

        /**
         *
         * @return: promise
         *
         * */
        self.CountAllByUser = function(intUserId) {
            Reset();

            ProductEntitySet = [];

            return HttpService.Get('/rest/1/user/' + intUserId + '/product/count')
                .then(function(response) {
                    if (!_.isUndefined(response)) {
                        return response;
                    }
                });
        };

        /**
         *
         * @return: promise
         *
         * */
        self.Save = function(objProductEntity) {
            var strProductEntity = JSON.stringify(objProductEntity);
            
            if (strEntity === strProductEntity) {
                return $q.when(self.GetData());
            } else {
                return HttpService.Post('/rest/1/product', objProductEntity)
                    .then(function(response) {
                        SetData(objProductEntity);

                        ProductEntity.handle = response;
                        
                        return self.GetData();
                    });
            }
        };

        /**
         *
         * @return: promise
         *
         * */
        self.Delete = function() {
            if (ProductEntity.handle) {
                return HttpService.Delete('/rest/1/product/', ProductEntity.handle)
                    .then(function() {
                        SetData(objCloneEntity);

                        return true;
                    });
            } else {
                return $q.when(false);
            }
        };

        return self;
    }
})(window._, window.angular);