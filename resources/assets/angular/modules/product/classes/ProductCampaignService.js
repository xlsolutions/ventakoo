(function(_, angular) {
    "use strict";

    angular.module('Product')
        .service('ProductCampaignService', [
            '$q',
            'ProductBuilder',
            'ProductEntity',
            'ProductEntitySet',
            'HttpService',
            ProductCampaignService
        ]);

    function ProductCampaignService($q,
                                    ProductBuilder,
                                    ProductEntity,
                                    ProductEntitySet,
                                    HttpService) {

        var self = this;
        var objCloneEntity = angular.copy(ProductEntity);
        var strEntity = JSON.stringify(ProductEntity);

        var Reset = function() {
            strEntity = JSON.stringify(objCloneEntity);
        };

        var SetData = function(unkData) {
            ProductEntity = _.assign(self.GetData(), unkData);

            strEntity = JSON.stringify(ProductEntity);
        };

        self.GetData = function() {
            return ProductEntity;
        };

        self.NormalizeDataSet = ProductBuilder.build;

        /**
         * To be able to use this properly Product Handle should be set somewhere
         *
         * Example:
         *
         * 1. In a component bindings <my-component product-id="PUT_BINDING_HANDLE_HERE"></my-component>,
         *   then set the ProductEntity.handle in that component's controller
         *
         * 2. By manually setting the ProductEntity.handle = ?
         *
         * @return: promise
         *
         * */
        self.GetByID = function() {
            var strHandle = ProductEntity.handle;

            if (strHandle && (JSON.stringify(ProductEntity) === strEntity)) {
                return $q.when(self.GetData());
            } else if (strHandle) {
                return HttpService.Get('/rest/1/product/campaign/' + strHandle)
                    .then(function(response) {
                        SetData(response);

                        return self.GetData();
                    });
            } else {
                return $q.when(false);
            }
        };

        /**
         *
         * @return: promise
         *
         * */
        self.GetAll = function(objParam) {
            Reset();

            return HttpService.Get('/rest/1/product/campaign', objParam)
                .then(function(response) {
                    ProductEntitySet = [];

                    _.forEach(response, function(objProduct) {
                        ProductEntitySet.push(self.NormalizeDataSet(objProduct));
                    });

                    return ProductEntitySet;
                });
        };

        /**
         *
         * @return: promise
         *
         * */
        self.GetAllByUser = function(intUserId, objParam) {
            Reset();

            return HttpService.Get('/rest/1/user/' + intUserId + '/product/campaign', objParam)
                .then(function(response) {
                    ProductEntitySet = [];

                    _.forEach(response, function(objProduct) {
                        ProductEntitySet.push(self.NormalizeDataSet(objProduct));
                    });

                    return ProductEntitySet;
                });
        };

        /**
         *
         * @return: promise
         *
         * */
        self.CountAll = function() {
            Reset();

            ProductEntitySet = [];

            return HttpService.Get('/rest/1/product/campaign/count')
                .then(function(response) {
                    if (!_.isUndefined(response)) {
                        return response;
                    }
                });
        };

        /**
         *
         * @return: promise
         *
         * */
        self.CountAllByUser = function(intUserId) {
            Reset();

            ProductEntitySet = [];

            return HttpService.Get('/rest/1/user/' + intUserId + '/product/campaign/count')
                .then(function(response) {
                    if (!_.isUndefined(response)) {
                        return response;
                    }
                });
        };
        
        return self;
    }
})(window._, window.angular);