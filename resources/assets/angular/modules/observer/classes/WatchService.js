(function(angular) {
    "use strict";
    
    angular.module('Observer')
        .service('WatchService', [
            WatchService
        ]);

    function WatchService() {
        var arrWatcher = {};

        this.Notify = function(strName, strChannel, unkData) {
            if (typeof arrWatcher[strName] != 'undefined' && typeof arrWatcher[strName][strChannel] != 'undefined') {
                arrWatcher[strName][strChannel](unkData);
            }
        };

        this.Observe = function(strName, strChannel, fnCallback) {
            if (typeof fnCallback != 'function') {
                fnCallback = function() {
                    console.log(strName, strChannel, 'fnCallback is not a function');
                }
            }

            if (!arrWatcher[strName]) {
                arrWatcher[strName] = {};
            }

            arrWatcher[strName][strChannel] = fnCallback;
        };

        return this;
    }
})(window.angular);