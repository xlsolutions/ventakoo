(function(angular) {
    "use strict";

    angular.module('PayPal', ['Http']);
})(window.angular);