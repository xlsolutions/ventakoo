(function(_, angular) {
    "use strict";

    angular.module('PayPal')
        .service('PayPalOrderService', [
            'PayPalOrderBuilder',
            'PayPalOrderEntity',
            'HttpService',
            PayPalOrderService
        ]);

    function PayPalOrderService(PayPalOrderBuilder,
                                PayPalOrderEntity,
                                HttpService) {
        var self = this;
        
        var strEntity = JSON.stringify(PayPalOrderEntity);

        var SetData = function(unkData) {
            PayPalOrderEntity = _.assign(self.GetData(), unkData);

            strEntity = JSON.stringify(PayPalOrderEntity);
        };

        self.GetData = function() {
            return PayPalOrderEntity;
        };

        self.NormalizeDataSet = PayPalOrderBuilder.build;

        /**
         * To be able to use this properly PayPalOrderEntity orderId should be set somewhere
         *
         * Example:
         *
         * 1. In a component bindings <my-component order-id="PUT_BINDING_HANDLE_HERE"></my-component>,
         *   then set the PayPalOrderEntity.orderId in that component's controller
         *
         * 2. By manually setting the PayPalOrderEntity.orderId = ?
         *
         * @return: promise
         *
         * */
        self.GetByUserIdAndOrderId = function(intUserId, intOrderId) {
            return HttpService.Get('/rest/1/user/' + intUserId + '/order/' + intOrderId + '/paypal')
                .then(function(response) {
                    SetData(self.NormalizeDataSet(response));

                    return self.GetData();
                });
        };

        return self;
    }
})(window._, window.angular);