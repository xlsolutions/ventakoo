(function(angular, _) {

    angular.module('PayPal')
        .factory('PayPalOrderBuilder', [
            '$filter',
            PayPalOrderBuilder
        ]);

    function PayPalOrderBuilder($filter) {

        function PayPalOrder(objPayPalOrder) {
            var transactionId = objPayPalOrder.transactionId || null;
            var ipnTrackId = objPayPalOrder.ipnTrackId || null;
            var eligibility = objPayPalOrder.eligibility || null;
            var paymentDate = objPayPalOrder.paymentDate || null;
            var paymentStatus = objPayPalOrder.paymentStatus || null;
            var pendingReason = objPayPalOrder.pendingReason || '-';
            var verifySign = objPayPalOrder.verifySign || null;
            var reasonCode = objPayPalOrder.reasonCode || null;
            var paymentType = objPayPalOrder.paymentType || null;
            var transactionType = objPayPalOrder.transactionType || null;
            var quantity = objPayPalOrder.quantity || 0;
            var currency = objPayPalOrder.currency || null;
            var gross = objPayPalOrder.gross || 0;
            var xchangeRate = objPayPalOrder.xchangeRate || 0;
            var transactionFee = objPayPalOrder.transactionFee || 0;
            var itemName = objPayPalOrder.itemName || null;
            var itemNumber = objPayPalOrder.itemNumber || null;
            var payerAddressStatus = objPayPalOrder.payerAddressStatus || null;
            var payerAddressStreet = objPayPalOrder.payerAddressStreet || null;
            var payerAddressZip = objPayPalOrder.payerAddressZip || null;
            var payerAddressCountryCode = objPayPalOrder.payerAddressCountryCode || null;
            var payerAddressName = objPayPalOrder.payerAddressName || null;
            var payerAddressCountry = objPayPalOrder.payerAddressCountry || null;
            var payerAddressCity = objPayPalOrder.payerAddressCity || null;
            var payerAddressState = objPayPalOrder.payerAddressState || null;
            var payerEmail = objPayPalOrder.payerEmail || null;
            var payerId = objPayPalOrder.payerId || null;
            var payerFirstName = objPayPalOrder.payerFirstName || null;
            var payerLastName = objPayPalOrder.payerLastName || null;
            var payerStatus = objPayPalOrder.payerStatus || null;
            var payerResidenceCountry = objPayPalOrder.payerResidenceCountry || null;
            
            this.getIpnTrackId = function() {
                return ipnTrackId;    
            };

            this.getEligibility = function() {
                return eligibility;
            };
            
            this.getVerifySign = function() {
                return verifySign;
            };
            
            this.getExchangeRate = function() {
                return xchangeRate;
            };
            
            this.getPayerEmail = function() {
                return payerEmail;
            };

            this.getPayerId = function() {
                return payerId;
            };
            
            this.getTransactionId = function() {
                return transactionId;
            };
            
            this.getPaymentDate = function() {
                return paymentDate;
            };
            
            this.getPaymentStatus = function() {
                return paymentStatus;
            };
            
            this.getPendingReason = function() {
                return pendingReason;
            };
            
            this.getReasonCode = function() {
                return reasonCode;
            };
            
            this.getPaymentType = function() {
                return paymentType;
            };
            
            this.getTransactionType = function() {
                return transactionType;
            };
            
            this.getQuantity = function() {
                return quantity;
            };
            
            this.getCurrency = function() {
                return currency;
            };
            
            this.getGross = function() {
                return $filter('currency')(gross, '', 2);
            };

            this.getGrossWithCurrency = function() {
                return $filter('currency')(gross, currency, 2);
            };
            
            this.getTransactionFee = function() {
                return transactionFee;
            };
            
            this.getItemName = function() {
                return itemName;
            };
            
            this.getItemNumber = function() {
                return itemNumber;
            };
            
            this.getPayerAddressStatus = function() {
                return payerAddressStatus;
            };
            
            this.getPayerAddressStreet = function() {
                return payerAddressStreet;
            };
            
            this.getPayerAddressZip = function() {
                return payerAddressZip;
            };
            
            this.getPayerAddressCountryCode = function() {
                return payerAddressCountryCode;
            };
            
            this.getPayerAddressName = function() {
                return payerAddressName;
            };
            
            this.getPayerAddressCountry = function() {
                return payerAddressCountry;
            };
            
            this.getPayerAddressCity = function() {
                return payerAddressCity;
            };
            
            this.getPayerAddressState = function() {
                return payerAddressState;
            };
            
            this.getPayerFirstName = function() {
                return payerFirstName;
            };
            
            this.getPayerLastName = function() {
                return payerLastName;
            };
            
            this.getPayerStatus = function() {
                return payerStatus;
            };
            
            this.getPayerResidenceCountry = function() {
                return payerResidenceCountry;
            };
        }

        return {
            build: function(objPayPalOrder) {
                return new PayPalOrder(objPayPalOrder);   
            }
        };
    }

})(window.angular, window._);