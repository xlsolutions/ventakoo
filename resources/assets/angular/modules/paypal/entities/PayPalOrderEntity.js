(function(angular) {
    "use strict";

    angular.module('PayPal')
        .value('PayPalOrderEntity', {
            transactionId: null,
            paymentDate: null,
            paymentStatus: null,
            pendingReason: null,
            reasonCode: null,
            paymentType: null,
            transactionType: null,
            quantity: 0,
            currency: null,
            gross: 0,
            transactionFee: 0,
            itemName: null,
            itemNumber: null,
            payerAddressStatus: null,
            payerAddressStreet: null,
            payerAddressZip: null,
            payerAddressCountryCode: null,
            payerAddressName: null,
            payerAddressCountry: null,
            payerAddressCity: null,
            payerAddressState: null,
            payerFirstName: null,
            payerLastName: null,
            payerStatus: null,
            payerResidenceCountry: null
        });
})(window.angular);