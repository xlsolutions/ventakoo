(function(angular) {
    "use strict";

    angular.module('Auth')
        .value('AuthEntity', {
            user_id: null,
            role: null,
            accountBalance: null
        });
})(window.angular);