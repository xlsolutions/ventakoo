(function(_, angular) {
    "use strict";

    angular.module('Auth')
        .service('AuthService', [
            '$q',
            'AuthEntity',
            'HttpService',
            AuthService
        ]);

    function AuthService($q,
                         AuthEntity,
                         HttpService
    ) {

        var self = this;
        var strEntity = JSON.stringify(AuthEntity);

        self.SetData = function(unkData) {
            AuthEntity = _.assign(self.GetData(), unkData);

            strEntity = JSON.stringify(AuthEntity);
        };

        self.GetData = function(key) {
            if (key) {
                return AuthEntity[key] || null;
            }

            return AuthEntity;
        };

        self.RefreshAuth = function(bolForce) {
            if (bolForce) {
                return HttpService.Get('/rest/1/auth/user')
                    .then(function(response) {
                        self.SetData(response);

                        return self.GetData();
                    });
            }
            
            if (self.GetData('user_id') && JSON.stringify(self.GetData()) === strEntity) {
                return $q.when(self.GetData());
            } else if (!self.GetData('user_id')) {
                return HttpService.Get('/rest/1/auth/user')
                    .then(function(response) {
                        self.SetData(response);

                        return self.GetData();
                    });
            } else {
                return $q.when(null);
            }
        };

        return self;
    }
})(window._, window.angular);