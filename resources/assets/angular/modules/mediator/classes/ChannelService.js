(function(angular) {
    "use strict";
    
    angular.module('Mediator')
        .service('ChannelService', [
            ChannelService
        ]);

    function ChannelService() {
        var arrChannel = [];

        this.Publish = function(strName, unkData) {
            if (!arrChannel[strName]) {
                return;
            }

            for(var strChannel in arrChannel[strName]) {
                for (var intSubscriber in arrChannel[strName][strChannel]) {
                    if (typeof arrChannel[strName][strChannel][intSubscriber] != 'function') {
                        continue;
                    }

                    arrChannel[strName][strChannel][intSubscriber](unkData);
                }
            }
        };

        this.Subscribe = function(strName, strChannel, fnCallback) {
            if (typeof fnCallback != 'function') {
                return function(){};
            }

            arrChannel[strName] = arrChannel[strName] || {};
            arrChannel[strName][strChannel] = arrChannel[strName][strChannel] || [];

            arrChannel[strName][strChannel].push(fnCallback);

            return function() {
                arrChannel[strName][strChannel].pop();
            }
        };

        return this;
    }
})(window.angular);