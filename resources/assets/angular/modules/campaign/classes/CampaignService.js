(function(_, angular) {
    "use strict";

    angular.module('Campaign')
        .service('CampaignService', [
            '$q',
            'CampaignEntity',
            'HttpService',
            CampaignService
        ]);

    function CampaignService($q,
                             CampaignEntity,
                             HttpService) {

        var self = this;
        var objCloneEntity = angular.copy(CampaignEntity);
        var strEntity = JSON.stringify(CampaignEntity);

        self.Reset = function() {
            CampaignEntity = objCloneEntity;
            strEntity = JSON.stringify(objCloneEntity);
        };

        var SetData = function(unkData) {
            CampaignEntity = _.assign(self.GetData(), unkData);

            strEntity = JSON.stringify(CampaignEntity);
        };

        self.GetData = function() {
            return CampaignEntity;
        };
        
        /**
         *
         * @return: promise
         *
         * */
        self.Save = function(objCampaignEntity) {
            var strCampaignEntity = JSON.stringify(objCampaignEntity);

            if (strEntity === strCampaignEntity) {
                return $q.when(self.GetData());
            } else {
                return HttpService.Post('/rest/1/product/campaign', objCampaignEntity)
                    .then(function(response) {
                        if (response) {
                            objCampaignEntity.campaignId = response;
                        }
                        
                        SetData(objCampaignEntity);
                        
                        return self.GetData();
                    });
            }
        };

        return self;
    }
})(window._, window.angular);