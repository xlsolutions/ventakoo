(function(angular) {
    "use strict";

    angular.module('Campaign')
        .value('CampaignEntity', {
            campaignId: 0,
            auto_publish: 0,
            productId: null,
            post_status: null
        });
})(window.angular);