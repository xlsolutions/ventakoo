(function(_, angular) {
    "use strict";

    angular.module('Paginator')
        .service('PaginatorService', [
            PaginatorService
        ]);

    function PaginatorService() {

        function Paginator() {
            var limit = 10;
            var page = 1;
            var recordCount = 0;
            var totalPage = 1;
            var objParam = {};

            this.setLimit = function(intLimit) {
                limit = intLimit;
            };
            
            this.getPage = function() {
                return page;    
            };
            
            this.getTotalPage = function() {
                return totalPage;   
            };
            
            this.setRecordCount = function(intRecord) {
                recordCount = intRecord;

                totalPage = Math.ceil(intRecord / limit);
            };
            
            this.getRecordCount = function() {
                return recordCount;
            };

            this.prevPage = function(unkCallback) {
                if (page === 1) {
                    return false;
                }

                page--;

                unkCallback();
            };

            this.nextPage = function(unkCallback) {
                if (page >= totalPage) {
                    return false;
                }

                page++;

                unkCallback();
            };

            this.getParam = function() {
                return _.assign({
                    limit: limit,
                    page: page
                }, objParam);
            };

            this.setParam = function(unkObject) {
                objParam = unkObject;
            };

            this.hidePrevIndicator = function() {
                return page === 1;
            };

            this.hideNextIndicator = function() {
                return page === totalPage;
            };
        }

        return new Paginator();
    }
})(window._, window.angular);