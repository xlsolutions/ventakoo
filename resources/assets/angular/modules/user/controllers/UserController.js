(function(angular) {
    "use strict";

    angular.module('User')
        .controller('UserController', [
            '$routeParams',
            'AuthService',
            'UserService',
            UserController
        ]);

    function UserController($routeParams, AuthService, UserService) {
        var self = this;

        self.bolLogin = false;
        
        var init = function() {            
            UserService.GetByHandle($routeParams.id)
                .then(function(response) {
                    self.profile = response;
                })
                .finally(function () {
                    AuthService.RefreshAuth()
                        .then(function(response) {
                            if (response.user_id) {
                                self.bolLogin = true;
                            }
                        });
                });
        };

        init();
    }
})(window.angular);