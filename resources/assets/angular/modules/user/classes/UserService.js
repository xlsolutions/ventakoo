(function(_, angular) {
    "use strict";

    angular.module('User')
        .service('UserService', [
            '$q',
            'UserBuilder',
            'UserEntity',
            'HttpCacheService',
            UserService
        ]);

    function UserService($q,
                         UserBuilder,
                         UserEntity,
                         HttpCacheService) {

        var self = this;
        var strEntity = JSON.stringify(UserEntity);

        var SetData = function(unkData) {
            UserEntity = _.assign(self.GetData(), unkData);

            strEntity = JSON.stringify(UserEntity);
        };

        self.GetData = function() {
            return UserEntity;
        };

        /**
         * To be able to use this properly User 'handle' should be set somewhere
         *
         * Example:
         *
         * 1. In a component bindings <my-component user-id="PUT_BINDING_HANDLE_HERE"></my-component>,
         *   then set the UserEntity.handle in that component's controller
         *
         * 2. By manually setting the UserEntity.handle = ?
         *
         * @return: promise
         *
         * */
        self.GetByID = function() {
            var strHandle = UserEntity.id;

            if (strHandle && (JSON.stringify(UserEntity) === strEntity)) {
                return $q.when(UserBuilder.build(self.GetData()));
            } else if (strHandle) {
                return HttpCacheService.Cache().Get('/rest/1/user/' + strHandle)
                    .then(function(response) {
                        response.id = strHandle;
                        
                        SetData(response);

                        return UserBuilder.build(self.GetData());
                    });
            } else {
                return $q.when(false);
            }
        };
        
        
        self.GetByHandle = function(strHandle) {
            UserEntity.id = strHandle;
            
            return self.GetByID();
        };

        return self;
    }
})(window._, window.angular);