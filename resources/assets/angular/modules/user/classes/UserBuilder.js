(function(angular) {

    angular.module('User')
        .factory('UserBuilder', [
            '$filter',
            UserBuilder
        ]);

    function UserBuilder($filter) {

        function User(objUser) {

            var intUserId = objUser.id || null;
            var strFirstName = objUser.firstName || null;
            var strLastName = objUser.lastName || null;
            var dtLastLogin = new Date(objUser.lastLogin) || null;
            var dtDateJoined = new Date(objUser.dateJoined) || null;
            var strCountry = objUser.country || null;
            var strState = objUser.state || null;
            var strCity = objUser.city || null;
            var strMobile = objUser.mobile || null;
            var strEmail = objUser.email || null;
            var strPhoto = objUser.photo || null;

            this.getId = function() {
                return intUserId;    
            };
            
            this.getFirstName = function () {
                return strFirstName;
            };

            this.getLastName = function() {
                return strLastName;
            };
            
            this.getFullName = function() {
                return strFirstName + ' ' + strLastName;    
            };

            this.getLastLogin = function() {
                return $filter('date')(dtLastLogin, 'EEE, MMM dd, yyyy h:mm a');
            };

            this.getDateJoined = function() {
                return $filter('date')(dtDateJoined, 'EEE, MMM dd, yyyy');
            };

            this.getCountry = function () {
                return strCountry;
            };

            this.getState = function() {
                return strState;
            };

            this.getCity = function () {
                return strCity;
            };

            this.getEmail = function () {
                return strEmail;
            };

            this.getMobile = function () {
                return strMobile;
            };

            this.getPhoto = function () {
                return strPhoto;
            };
        }

        return {
            build: function(objUser) {
                return new User(objUser);
            }
        };
    }

})(window.angular);