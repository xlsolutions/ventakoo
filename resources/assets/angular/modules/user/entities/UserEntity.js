(function(angular) {
    "use strict";
    
    angular.module('User')
        .value('UserEntity', {
            id: null,
            firstName: null,
            lastName: null,
            lastLogin: null,
            dateJoined: null,
            country: null,
            state: null,
            city: null,
            mobile: null,
            email: null,
            photo: null
        });
})(window.angular);