(function(angular) {
    "use strict";

    angular.module('User',
        [
            'Http',
            'ngRoute'
        ]);

})(window.angular);