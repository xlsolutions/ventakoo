(function(angular) {
    "use strict";

    angular.module('User')
        .config([
            '$routeProvider',
            '$locationProvider',
            RouteConfig
        ]);

    function RouteConfig($routeProvider, $locationProvider) {
        $locationProvider.html5Mode({
            enabled: false,
            requireBase: false
        });

        $routeProvider
            .when('/profile/:id', {
                templateUrl: '/angular/modules/user/views/pages/profile.html',
                controller: 'UserController',
                controllerAs: 'user'
            });
    }

})(window.angular);