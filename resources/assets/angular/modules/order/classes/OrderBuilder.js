(function(angular, _) {

    angular.module('Order')
        .factory('OrderBuilder', [
            '$filter',
            OrderBuilder
        ]);

    function OrderBuilder($filter) {

        function Order(objOrder) {

            var intOrderId = objOrder.orderId || null;
            var decPrice = objOrder.price || 0;
            var strStatus = objOrder.orderStatus || null;
            var bolHasPayPalTransaction = objOrder.hasPayPalTransaction || null;
            var dtDate = objOrder.dateOrdered || null;

            this.getOrderId = function () {
                return _.padStart(intOrderId, 6, '0');
            };

            this.getPrice = function() {
                return $filter('currency')(decPrice, 'Php ', 2);
            };
            
            this.getStatus = function() {
                return strStatus.replace('Payment', '');
            };

            this.getDate = function() {
                return $filter('date')(dtDate, 'EEE, MMM dd, yyyy h:mm a');
            };
            
            this.getHasPayPalTransaction = function() {
                return bolHasPayPalTransaction > 0;
            };
        }

        return {
            build: function(objOrder) {
                return new Order(objOrder);   
            }
        };
    }

})(window.angular, window._);