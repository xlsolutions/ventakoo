(function(angular, _) {

    angular.module('Order')
        .factory('OrderItemBuilder', [
            '$filter',
            OrderItemBuilder
        ]);

    function OrderItemBuilder($filter) {

        function OrderItem(objOrderItem) {

            var decPrice = objOrderItem.price || 0;
            var strItemType = objOrderItem.itemType || null;

            this.getPrice = function() {
                return $filter('currency')(decPrice, 'Php ', 2);
            };
            
            this.getItemType = function() {
                return strItemType;
            };
        }

        return {
            build: function(objOrderItem) {
                return new OrderItem(objOrderItem);
            }
        };
    }

})(window.angular, window._);