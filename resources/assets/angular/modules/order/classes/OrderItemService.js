(function(_, angular) {
    "use strict";

    angular.module('Order')
        .service('OrderItemService', [
            'OrderItemBuilder',
            'OrderItemEntitySet',
            'HttpService',
            OrderItemService
        ]);

    function OrderItemService(OrderItemBuilder,
                              OrderItemEntitySet,
                              HttpService) {

        var self = this;
        
        self.NormalizeDataSet = OrderItemBuilder.build;

        /**
         * @return: promise
         * */
        self.GetAllByUser = function(intUserId, intOrderId, objParam) {
            return HttpService.GetList('/rest/1/user/' + intUserId + '/order/' + intOrderId + '/item', objParam)
                .then(function(response) {
                    OrderItemEntitySet = [];

                    _.forEach(response, function(objOrder) {
                        OrderItemEntitySet.push(self.NormalizeDataSet(objOrder));
                    });

                    return OrderItemEntitySet;
                });
        };

        return self;
    }
})(window._, window.angular);