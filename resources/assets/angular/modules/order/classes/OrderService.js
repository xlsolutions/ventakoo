(function(_, angular) {
    "use strict";

    angular.module('Order')
        .service('OrderService', [
            '$q',
            'OrderBuilder',
            'OrderEntity',
            'OrderEntitySet',
            'HttpService',
            OrderService
        ]);

    function OrderService($q,
                          OrderBuilder,
                          OrderEntity,
                          OrderEntitySet,
                          HttpService) {

        var self = this;
        var objCloneEntity = angular.copy(OrderEntity);
        var strEntity = JSON.stringify(OrderEntity);

        var Reset = function() {
            strEntity = JSON.stringify(objCloneEntity);
        };

        var SetData = function(unkData) {
            OrderEntity = _.assign(self.GetData(), unkData);

            strEntity = JSON.stringify(OrderEntity);
        };

        self.GetData = function() {
            return OrderEntity;
        };

        self.NormalizeDataSet = OrderBuilder.build;

        /**
         * To be able to use this properly Order orderId should be set somewhere
         *
         * Example:
         *
         * 1. In a component bindings <my-component order-id="PUT_BINDING_HANDLE_HERE"></my-component>,
         *   then set the OrderEntity.orderId in that component's controller
         *
         * 2. By manually setting the OrderEntity.orderId = ?
         *
         * @return: promise
         *
         * */
        self.GetByID = function() {
            var strHandle = OrderEntity.orderId;

            if (strHandle && (JSON.stringify(OrderEntity) === strEntity)) {
                return $q.when(self.GetData());
            } else if (strHandle) {
                return HttpService.Get('/rest/1/order/' + strHandle)
                    .then(function(response) {
                        SetData(response);

                        return self.GetData();
                    });
            } else {
                return $q.when(false);
            }
        };

        /**
         *
         * @return: promise
         *
         * */
        self.GetAllByUser = function(intUserId, objParam) {
            Reset();

            return HttpService.GetList('/rest/1/user/' + intUserId + '/order', objParam)
                .then(function(response) {
                    OrderEntitySet = [];

                    _.forEach(response, function(objOrder) {
                        OrderEntitySet.push(self.NormalizeDataSet(objOrder));
                    });

                    return OrderEntitySet;
                });
        };

        /**
         *
         * @return: promise
         *
         * */
        self.CountAllByUser = function(intUserId) {
            Reset();

            OrderEntitySet = [];

            return HttpService.Get('/rest/1/user/' + intUserId + '/order/count')
                .then(function(response) {
                    if (!_.isUndefined(response)) {
                        return response;
                    }
                });
        };

        return self;
    }
})(window._, window.angular);