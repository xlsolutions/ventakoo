(function(angular) {
    "use strict";

    angular.module('Order')
        .value('OrderItemEntity', {
            price: 0,
            itemType: null
        })
        .value('OrderItemEntitySet', []);
})(window.angular);