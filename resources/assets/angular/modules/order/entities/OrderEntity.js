(function(angular) {
    "use strict";

    angular.module('Order')
        .value('OrderEntity', {
            orderId: 0,
            hasPayPalTransaction: null,
            orderStatus: null,
            price: 0,
            dateOrdered: null
        })
        .value('OrderEntitySet', []);
})(window.angular);