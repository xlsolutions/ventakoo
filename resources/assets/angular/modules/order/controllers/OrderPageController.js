(function(angular) {
    "use strict";

    angular.module('Order')
        .controller('OrderPageController', [
            'OrderService',
            'AuthService',
            'PaginatorService',
            OrderPageController
        ]);

    function OrderPageController(OrderService, AuthService, PaginatorService) {
        var self = this;

        self.userId = 0;
        self.orders = [];

        var refresh = function() {
            self.orders = [];
            
            OrderService.GetAllByUser(self.userId, PaginatorService.getParam())
                .then(function(response) {
                    self.orders = response;
                });
        };
        
        var init = function() {
            PaginatorService.setLimit(10);

            AuthService.RefreshAuth()
                .then(function(response) {
                    self.userId = response.user_id;
                    
                    OrderService.CountAllByUser(self.userId)
                        .then(function (response) {
                            PaginatorService.setRecordCount(response);
                        });

                    refresh();
                }, function() {
                    window.location = '/login';
                });
        };

        self.prevPage = function() {
            PaginatorService.prevPage(refresh);
        };
        self.nextPage = function() {
            PaginatorService.nextPage(refresh);
        };
        self.hidePrevBtn = PaginatorService.hidePrevIndicator;
        self.hideNextBtn = PaginatorService.hideNextIndicator;
        self.currentPage = PaginatorService.getPage;
        self.totalPage = PaginatorService.getTotalPage;
        self.recordCount = PaginatorService.getRecordCount;
        
        init();
    }
})(window.angular);