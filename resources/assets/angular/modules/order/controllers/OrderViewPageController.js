(function(angular) {
    "use strict";

    angular.module('Order')
        .controller('OrderViewPageController', [
            '$routeParams',
            'OrderService',
            'OrderItemService',
            'PayPalOrderService',
            'AuthService',
            OrderViewPageController
        ]);

    function OrderViewPageController($routeParams, 
                                     OrderService,
                                     OrderItemService,
                                     PayPalOrderService,
                                     AuthService
    ) {
        var self = this;

        self.userId = 0;
        
        var init = function() {
            AuthService.RefreshAuth()
                .then(function(response) {
                    self.userId = response.user_id;

                    self.order = OrderService.GetData();
                    self.order.orderId = $routeParams.orderId || 0;
                    
                    OrderService.GetByID()
                        .then(function (response) {
                            self.order = OrderService.NormalizeDataSet(response);
                            
                            if (self.order.getHasPayPalTransaction()) {
                                PayPalOrderService.GetByUserIdAndOrderId(self.userId, self.order.getOrderId())
                                    .then(function(response) {
                                        self.paypal = response;
                                    });     
                            }
                        });

                    OrderItemService.GetAllByUser(self.userId, self.order.orderId)
                        .then(function(response) {
                            self.orderItem = response;
                        });
                }, function() {
                    window.location = '/login';
                });
        };

        init();
    }
})(window.angular);