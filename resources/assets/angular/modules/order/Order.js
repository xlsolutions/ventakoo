(function(angular) {
    "use strict";

    angular.module('Order', [
        'ngRoute',
        'Http',
        'Auth',
        'PayPal'
    ]);
})(window.angular);