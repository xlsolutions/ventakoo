(function(angular) {
    "use strict";

    angular.module('Order')
        .config([
            '$routeProvider',
            '$locationProvider',
            RouteConfig
        ]);

    function RouteConfig($routeProvider, $locationProvider) {
        $locationProvider.html5Mode({
            enabled: false,
            requireBase: false
        });

        $routeProvider
            //@todo: convert to component
            .when('/order', {
                templateUrl: '/angular/modules/order/views/pages/index.html',
                controller: 'OrderPageController',
                controllerAs: 'order'
            })
            //@todo: convert to component
            .when('/order/:orderId', {
                templateUrl: '/angular/modules/order/views/pages/view.html',
                controller: 'OrderViewPageController',
                controllerAs: 'orderView'
            });
    }

})(window.angular);