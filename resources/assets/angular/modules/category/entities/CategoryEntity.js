(function(angular) {
    "use strict";

    angular.module('Category')
        .value('CategoryEntity', {
            handle: null,
            category: null,
            parentHandle: null
        })
        .value('CategoryEntitySet', [])
        .value('CategoryDictionary', {});
})(window.angular);