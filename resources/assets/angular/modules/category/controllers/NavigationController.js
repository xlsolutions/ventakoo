(function(angular) {
    "use strict";
	
	angular.module('Category')
		.controller('NavigationController', [
			'$location',
			'AuthService',
			'CategoryService',
			NavigationController
		]);
	
	function NavigationController($location, AuthService, CategoryService) {
        var self = this;
		
        self.category = null;
        self.navCategories = [];
        self.navSubCategories = [];
		
		self.reloadProducts = function(category) {
			if (!category)
			{
				self.category = 'MAIN_PRODUCTS';
			}
			else
			{
				self.category = category;
			}
			
			self.navCategories = CategoryService.GetByParentHandle(self.category);
		};
		
		self.reloadSubCategory = function(category)
		{
			self.category = category;
			
			self.navSubCategories = CategoryService.GetByParentHandle(self.category);
		};
		
		var init = function() {
			var objQueryParams = $location.search();

			CategoryService.GetAll()
				.finally(function() {
					self.navCategories = CategoryService.GetByParentHandle(objQueryParams.category);

                    self.reloadProducts();
                });

			AuthService.RefreshAuth()
				.then(function(response) {
					self.bolLogin = response.user_id > 0;
					self.accountBalance = response.accountBalance;
				});
		};
		
		init();
    }
})(window.angular);