(function(_, angular) {
    "use strict";

    angular.module('Category')
        .controller('FormSelectCategoryController', [
            '$scope',
            'CategoryService',
            FormSelectCategoryController
        ]);

    function FormSelectCategoryController($scope, CategoryService) {

        var self = this;

        self.type = null;
        self.tmpModelValue = [];
        self.modelValue = null;
        self.dictionary = [];
        self.collection = [];
        self.selectedValues = [];
        self.populateParentCategory = [];

        var populateSelect = function(strHandle) {
            var tmpCollection = [];

            angular.forEach(self.dictionary, function(objCategory) {
                if (objCategory.parentHandle === strHandle) {
                    tmpCollection.push(objCategory);
                }
            });

            if (tmpCollection.length > 0) {
                self.collection.push(tmpCollection);
            }
        };

        var getParentCategory = function (strHandle) {
            angular.forEach(self.dictionary, function(objCategory) {
                if (objCategory.handle === strHandle) {
                    self.populateParentCategory.push(objCategory.handle);

                    getParentCategory(objCategory.parentHandle);
                }
            });
        };
        
        var populateCategoryOnceByProductCategory = function() {
            var objDestroyWatcher = $scope.$watch(
                function() {
                    return self.modelValue;
                }, 
                function(newVal) {
                    if (newVal) {
                        self.populateParentCategory = [];
    
                        getParentCategory(newVal);

                        self.populateParentCategory.pop();

                        angular.forEach(self.populateParentCategory.reverse(), function(strHandle, intIndex) {
                            self.tmpModelValue[intIndex] = strHandle;

                            self.select(strHandle, intIndex);

                            //@fixme: this should be auto select upon setting self.tmpModelValue[intIndex] but it isn't
                            setTimeout(function () {
                                angular.element(document.querySelector('select option[value="' + strHandle + '"]')).attr('selected', 'selected');
                            });
                        });

                        objDestroyWatcher();
                    }
                }
            );
        };

        function init() {
            if (self.dictionary.length < 1) {
                CategoryService.GetAll()
                    .then(function(response) {
                        if (response.length > 0) {
                            self.dictionary = response;

                            populateSelect(self.type);

                            populateCategoryOnceByProductCategory();
                        }
                    });
            }
        }

        self.select = function(strHandle, intIndex) {
            if (!_.isUndefined(intIndex)) {
                self.modelValue = strHandle;
                
                intIndex = parseInt(intIndex);

                var objCategory = CategoryService.GetHandleFromDictionary(strHandle);

                if (objCategory) {
                    self.selectedValues.splice(intIndex, self.selectedValues.length);
                    self.selectedValues.push(objCategory.category);
                }

                intIndex+=1;
                
                self.collection.splice(intIndex, self.collection.length);
            } else if (!_.isUndefined(intIndex) && !strHandle) {
                return false;
            }

            populateSelect(strHandle);
        };
        
        init();
    }
})(window._, window.angular);