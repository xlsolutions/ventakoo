(function(_, angular) {
    "use strict";

    angular.module('Category')
        .service('CategoryService', [
            '$q',
            'CategoryEntity',
            'CategoryEntitySet',
            'CategoryDictionary',
            'HttpService',
            CategoryService
        ]);

    function CategoryService($q,
                             CategoryEntity,
                             CategoryEntitySet,
                             CategoryDictionary,
                             HttpService) {

        var self = this;
        var objCloneEntity = angular.copy(CategoryEntity);
        var strEntity = JSON.stringify(CategoryEntity);

        var Reset = function() {
            strEntity = JSON.stringify(objCloneEntity);
        };

        var GetData = function(key) {
            if (key) {
                return CategoryEntity[key] || null;
            }
            
            return CategoryEntity;
        };

        self.SetData = function(unkData) {
            CategoryEntity = _.assign(GetData(), unkData);

            strEntity = JSON.stringify(CategoryEntity);
        };

        self.NormalizeDataSet = function(unkData) {
            return _.assign(angular.copy(objCloneEntity), unkData);
        };

        /**
         * To be able to use this properly HANDLE should be set somewhere
         *
         * Example:
         *
         * 1. In a component bindings <my-component category-id="PUT_BINDING_ID_HERE"></my-component>,
         *   then set the CategoryEntity.id in that component's controller
         *
         * 2. By manually setting the CategoryEntity.handle = ?
         *
         * @return: promise
         *
         * */
        self.GetByID = function() {
            var strHandle = GetData('handle');

            if (strHandle && (JSON.stringify(GetData()) === strEntity)) {
                return $q.when(GetData());
            } else if (strHandle) {
                return HttpService.Get('/rest/1/category/' + strHandle)
                    .then(function(response) {
                        SetData(response);

                        return GetData();
                    });
            } else {
                return $q.when(false);
            }
        };

        /**
         *
         * @return: promise
         *
         * */
        self.GetAll = function() {            
            Reset();
            
            return HttpService.GetList('/rest/1/category')
                .then(function(response) {
                    CategoryEntitySet = [];
                    
                    if (!_.isUndefined(response)) {
                        for(var intIndex in response) {
                            
                            if (response[intIndex] && response[intIndex]['handle']) {

                                CategoryEntitySet.push(self.NormalizeDataSet(response[intIndex]));

                                CategoryDictionary[CategoryEntitySet[intIndex]['handle']] = CategoryEntitySet[intIndex];   
                            }
                        }
                    }

                    return CategoryEntitySet;
                });
        };

        /**
         *
         * @return: promise
         *
         * */
        self.Save = function() {
            var strCategoryEntity = JSON.stringify(GetData());

            if (strEntity === strCategoryEntity) {
                return $q.when(self.GetData());
            } else {
                return HttpService.Post('/rest/1/category', GetData())
                    .then(function() {
                        SetData(GetData());

                        return GetData();
                    });
            }
        };

        /**
         *
         * @return: promise
         *
         * */
        self.Delete = function() {
            var strHandle = GetData('handle');
            
            if (strHandle) {
                return HttpService.Delete('/rest/1/category/', strHandle)
                    .then(function() {
                        SetData(objCloneEntity);

                        return true;
                    });
            } else {
                return $q.when(false);
            }
        };

        /**
         * Used to find the record in a dictionary
         * 
         * @return object | null
         * 
         * */
        self.GetHandleFromDictionary = function(strHandle) {            
            return CategoryDictionary[strHandle] || null;
        };

        /**
         * Used to collect the parent categories in asc order
         *
         * @return array
         *
         * */
        self.GetParentHandle = function(strHandle, arrCategory) {
            if (CategoryEntitySet) {
                if (!arrCategory) {
                    arrCategory = [];
                }
                
                angular.forEach(CategoryEntitySet, function(objCategory) {
                    if (objCategory.handle === strHandle) {
                        arrCategory.push(objCategory.handle);

                        self.GetParentHandle(objCategory.parentHandle, arrCategory);
                    }
                });
                
                return arrCategory.reverse();
            }
            else {
                console.log('CategoryEntitySet is empty; self.GetAll() should be called first.');
                
                return [];
            }
        };
        
        /**
         * Used to collect the child categories by parent handle
         *
         * @return array
         *
         * */
        self.GetByParentHandle = function(strHandle, arrCategory) {
            if (CategoryEntitySet) {
                if (!arrCategory) {
                    arrCategory = [];
                }
                
                angular.forEach(CategoryEntitySet, function(objCategory) {
                    if (objCategory.parentHandle  === strHandle && objCategory.parentHandle != 0) {
                        arrCategory.push(objCategory);
                    }
                });
                
                return arrCategory;
            }
            else {
                console.log('CategoryEntitySet is empty; self.GetAll() should be called first.');
                
                return [];
            }
        };
        
        return self;
    }
})(window._, window.angular);