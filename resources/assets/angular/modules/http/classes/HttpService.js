(function(_, angular) {
    "use strict";
    
    angular.module('Http')
        .service('HttpService', [
            'Restangular',
            'ChannelService',
            HttpService
        ]);

    function HttpService(Restangular, ChannelService) {

        (function() {
            Restangular.addRequestInterceptor(function(elem, operation) {
                if (operation === "remove") {
                    return undefined;
                }

                return elem;
            });

            Restangular.setErrorInterceptor(function(objResponse) {
                ChannelService.Publish('HTTP_STATUS', objResponse);

                return true;
            });   
        })();

        var objKeyRequestQueue = {};

        this.Get = function(strUrl, unkParam) {
            var strKey = JSON.stringify(unkParam) + strUrl;
            
            if (objKeyRequestQueue[strKey]) {
                return objKeyRequestQueue[strKey];
            }
            
            objKeyRequestQueue[strKey] = Restangular.oneUrl(_.replace(strUrl, '/', '_'), strUrl).get(unkParam);
            
            return objKeyRequestQueue[strKey];
        };

        this.GetList = function(strUrl, unkParam, bolCache) {
            var strKey = JSON.stringify(unkParam) + strUrl;

            if (bolCache === false) {
                //do nothing
            } else if (objKeyRequestQueue[strKey]) {
                return objKeyRequestQueue[strKey];
            }

            objKeyRequestQueue[strKey] = Restangular.all(strUrl).getList(unkParam);

            return objKeyRequestQueue[strKey];
        };

        this.Post = function(strUrl, unkParam) {
            return Restangular.all(strUrl).post(unkParam);
        };

        this.Delete = function(strUrl, strHandle) {
            return Restangular.all(strUrl + "/" + strHandle).remove();
        };
        
        return this;
    }
})(window._, window.angular);