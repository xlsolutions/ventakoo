(function(_, angular) {
    "use strict";

    angular.module('Http')
        .service('HttpCacheService', [
            '$q',
            'Restangular',
            'HttpService',
            HttpCacheService
        ]);

    function HttpCacheService($q, Restangular, HttpService) {

        var self = this;
        var intMinCacheSeconds = 300; //minimum cache time
        var objCacheKey = {};
        var arrCacheIndex = [];

        var SetLocalStorage = function(cacheKey) {
            if (window.localStorage) {
                window.localStorage.setItem(cacheKey, JSON.stringify(objCacheKey[cacheKey]));
            }
        };

        var GetLocalStorage = function(cacheKey) {
            if (window.localStorage) {
                return JSON.parse(window.localStorage.getItem(cacheKey));
            }

            return objCacheKey[cacheKey];
        };

        self.prototype = Object.create(HttpService);
        
        /**
         * Set cache time
         *
         * @param intSeconds integer
         *
         * @return: HttpCacheService
         *
         * */
        self.prototype.Cache = function(intSeconds) {
            if (!intSeconds) {
                intSeconds = intMinCacheSeconds;
            } else if (parseInt(intSeconds, 10) > intMinCacheSeconds) {
                intSeconds = intMinCacheSeconds;

                console.log("Couldn't set cache time lower than " + intMinCacheSeconds + " seconds, defaulting it to " + intMinCacheSeconds + " seconds");
            }

            var objDate = new Date();
            objDate.setSeconds(intSeconds);

            arrCacheIndex.push({
                cacheKey: null,
                expiry: objDate.getTime()
            });

            return self.prototype;
        };

        /**
         * Override method
         *
         * @param strUrl string
         * @param unkParam object
         *
         * @return: promise
         *
         * */
        self.prototype.Get = function(strUrl, unkParam) {
            var objDate = new Date();
            var cacheKey = unkParam ? strUrl + "_" + JSON.stringify(unkParam) : strUrl;
            var objCache = GetLocalStorage(cacheKey);
            
            // Return cache result if cache key exists & not expired & not empty
            if (objCacheKey[cacheKey] && objCache && ((objCache['expiry'] >= objDate.getTime()) && objCache['result'])) {
               return $q.when(objCacheKey[cacheKey]['result']);
            } else {
                console.log('Invalid cache found: ', objCache);
                
                objCache = arrCacheIndex.pop();

                objCache.cacheKey = cacheKey;

                objCacheKey[cacheKey] = {
                    result: null,
                    expiry: objCache.expiry
                };

                SetLocalStorage(cacheKey);

                return Restangular.oneUrl(_.replace(strUrl, '/', '_'), strUrl).get(unkParam)
                    .then(function(response) {
                        objCacheKey[cacheKey]['result'] = response;

                        SetLocalStorage(cacheKey);

                        return response;
                    });
            }
        };

        /**
         * Override method
         *
         * @param strUrl string
         * @param unkParam object
         *
         * @return: promise
         *
         * */
        self.prototype.GetList = function(strUrl, unkParam) {
            var objDate = new Date();
            var cacheKey = unkParam ? strUrl + "_" + JSON.stringify(unkParam) : strUrl;
            var objCache = GetLocalStorage(cacheKey);

            // Return cache result if cache key exists & not expired & not empty
            if (objCacheKey[cacheKey] && objCache && ((objCache['expiry'] >= objDate.getTime()) && objCache['result'])) {
                return $q.when(objCacheKey[cacheKey]['result']);
            } else {
                console.log('Invalid cache found: ', objCache);

                objCache = arrCacheIndex.pop();

                objCache.cacheKey = cacheKey;

                objCacheKey[cacheKey] = {
                    result: null,
                    expiry: objCache.expiry
                };

                SetLocalStorage(cacheKey);

                return Restangular.all(strUrl).getList(unkParam)
                    .then(function(response) {
                        objCacheKey[cacheKey]['result'] = response;

                        SetLocalStorage(cacheKey);

                        return response;
                    });
            }
        };
        
        return self.prototype;
    }
})(window._, window.angular);