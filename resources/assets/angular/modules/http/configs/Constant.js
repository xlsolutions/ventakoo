(function(angular) {
    "use strict";

    angular.module('Http')
        .constant('HTTP_STATUS_CODES', {
            BAD_REQUEST: 400,
            REQUEST_TOO_LARGE: 413,
            UNPROCESSABLE_ENTITY: 422,
            NOT_AUTHORIZE: 401,
            FORBIDDEN: 403,
            NOT_FOUND: 404,
            METHOD_NOT_ALLOWED: 405,
            SERVER_ERROR: 500
        });
    
})(window.angular);