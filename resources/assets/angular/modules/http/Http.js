(function(angular) {
    "use strict";
    
    angular.module('Http', ['restangular', 'Mediator']);
})(window.angular);