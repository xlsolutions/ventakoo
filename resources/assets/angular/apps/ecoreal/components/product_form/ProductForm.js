(function(angular) {
    "use strict";

    angular.module('Ecoreal')
        .component('productForm', {
            templateUrl: '/angular/modules/product/views/forms/sell.html',
            controller: 'ProductSellController',
            controllerAs: 'productSell'
        });
})(window.angular);