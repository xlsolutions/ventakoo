(function(angular) {
    "use strict";

    angular.module('Ecoreal')
        .component('formSelectCategory', {
            templateUrl: '/angular/modules/category/views/components/form-select-category.html',
            controller: 'FormSelectCategoryController',
            controllerAs: 'formSelectCategory',
            bindings: {
                modelValue: '=',
                type: '@'
            }
        });
    
})(window.angular);