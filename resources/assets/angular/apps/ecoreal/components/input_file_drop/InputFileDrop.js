(function(angular) {
    "use strict";

    angular.module('Ecoreal')
        .directive('inputFileDrop', [
            'ChannelService',
            function(ChannelService) {
                return {
                    link: function(scope, element) {

                        var zone = new FileDrop(element[0], {
                            iframe: {
                                url: scope.uploadUrl,
                                force: true
                            }
                        });

                        zone.event('send', function (files) {
                            files.each(function (file) {
                                // React on successful AJAX upload:
                                file.event('done', function (xhr) {
                                    xhr = JSON.parse(xhr.responseText);

                                    if (xhr) {
                                        // Here, 'this' points to fd.File instance.
                                        scope.$apply(function() {
                                            scope.modelValue = "data:" + xhr.data.mimeType + ";base64," + xhr.data.base64;
                                        });
                                    }
                                });

                                file.event('error', function (e, xhr) {
                                    var objData = JSON.parse(xhr.responseText);
                                    
                                    xhr.data = objData.data || null;

                                    ChannelService.Publish('HTTP_STATUS', xhr);
                                });

                                file.sendTo(scope.uploadUrl);
                            });
                        });

                        // Successful iframe upload uses separate mechanism
                        // from proper AJAX upload hence another listener:
                        zone.event('iframeDone', function (xhr) {                            
                            if (xhr.status == 200) {
                                scope.$apply(function() {
                                    scope.modelValue = "data:" + xhr.data.mimeType + ";base64," + xhr.data.base64;
                                });
                            } else {
                                ChannelService.Publish('HTTP_STATUS', xhr);
                            }
                        });
                    },
                    restrict: 'A',
                    scope: {
                        modelValue: '=',
                        targetElement: '@',
                        uploadUrl: '@inputFileDrop'
                    },
                    template: '<fieldset id="{{ targetElement }}"><legend><small>Drop a file inside...</small></legend><p><small>or click here to <em>Browse</em>...</small></p></fieldset>'
                };
            }
        ]);
})(window.angular);