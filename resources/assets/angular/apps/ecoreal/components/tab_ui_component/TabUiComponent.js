(function(angular) {
    "use strict";

    angular.module('Ecoreal')
        .component('tabUiComponent', {
            bindings: {
                targetElementId: '@'
            },
            controllerAs: 'tabUi',
            controller: [
                '$scope',
                function($scope) {
                    var self = $scope.tabUi;
                    var strTargetElement = self.targetElementId;

                    self.domElementID = strTargetElement.replace('#', '');

                    var reset = function() {
                        angular.element(document.querySelectorAll(strTargetElement + ' .nav.nav-tabs li')).removeClass('active');
                        angular.element(document.querySelectorAll(strTargetElement + ' .tab-pane')).removeClass('active in');
                    };

                    var show = function(domElement) {
                        domElement = angular.element(domElement);

                        domElement.addClass('active');
                        angular.element(document.querySelector(strTargetElement + ' ' + domElement.attr('target-element-id'))).addClass('active in');
                    };

                    angular.element(document).on('click', strTargetElement + ' .nav.nav-tabs li', function() {
                        reset();
                        show(this);
                    });
                }
            ],
            transclude: true,
            template: '<div id="{{ tabUi.domElementID }}" ng-transclude=""></div>'
        });
})(window.angular);