(function(angular) {
    "use strict";

    angular.module('Ecoreal')
        .component('appNavigation', {
	        bindings: {
		        navType: '@'
	        },
	        templateUrl: '/angular/modules/category/views/components/navigation.html',
            controller: 'NavigationController',
            controllerAs: 'appNavigation'
        });
})(window.angular);