(function(angular) {
    "use strict";

    angular.module('Ecoreal')
        .component('contactUsIssue', {
            templateUrl: '/angular/apps/ecoreal/views/common/contact-us-issue.html'
        });

})(window.angular);