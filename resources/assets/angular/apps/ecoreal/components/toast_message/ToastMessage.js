(function(angular, _) {
    "use strict";

    angular.module('Ecoreal')
        .directive('toastMessage', function() {
            return {
                scope: {
                    position: '@?',
                    name: '@',
                    channel: '@',
                    toastMessage: '@'
                },
                controller: [
                    '$scope',
                    'WatchService',
                    function ($scope, WatchService) {

                        var self = this;
                        var arrToastPositions = ['bottom-center', 'bottom-right', 'bottom-left', 'top-center', 'top-right', 'top-left'];
                        
                        if (!_.includes(arrToastPositions, $scope.position)) {
                            $scope.position = 'top-center';
                        }

                        self.messages = self.messages || {};
                        self.messages[$scope.name] = self.messages[$scope.name] || {};
                        self.messages[$scope.name][$scope.channel] = [];

                        WatchService.Observe($scope.name, $scope.channel, function(arrData) {
                            var arrMessage = self.getToast();

                            angular.forEach(arrData, function (data) {
                                arrMessage.push(data);
                            });
                            
                            $scope.$applyAsync();
                        });

                        self.getToast = function () {
                            return self.messages[$scope.name][$scope.channel];
                        };

                        self.removeToast = function(intIndex) {
                            self.messages[$scope.name][$scope.channel].splice(intIndex, 1);
                        };
                    }
                ],
                controllerAs: 'toast',
                restrict: 'A',
                template: '<div class="toast-message {{ position }}">' +
                '<div class="alert alert-{{ toastMessage }}" ng-repeat="message in toast.getToast() track by $index">' +
                '<p> <button type="button" class="close" ng-click="toast.removeToast($index)">x</button> <span ng-bind="message"></span> </p>' +
                '</div>' +
                '</div>'
            };
        });

})(window.angular, window._);