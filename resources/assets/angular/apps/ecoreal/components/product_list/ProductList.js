(function(angular) {
    "use strict";

    angular.module('Ecoreal')
        .component('productList', {
            bindings: {
                limit: '@',
                template: '@'
            },
            template: '<div ng-include="productList.getTemplate()"></div>',
            controller: 'ProductController',
            controllerAs: 'productList'
        });
})(window.angular);