(function(angular) {
    "use strict";

    angular.module('Ecoreal')
        .directive('userRoleDirective', [
            'AuthService',
            function(AuthService) {
                return {
                    link: function(scope, element) {

                        AuthService.RefreshAuth()
                            .then(function(response) {
                                if (scope.userRoleDirective != response.role) {
                                    element.addClass('hidden');
                                }
                            });
                    },
                    restrict: 'A',
                    scope: {
                        userRoleDirective: '@'
                    }
                };
            }
        ]);
})(window.angular);