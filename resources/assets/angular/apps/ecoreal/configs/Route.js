(function(angular) {
    "use strict";

    angular.module('Ecoreal')
        .config([
            '$routeProvider',
            '$locationProvider',
            RouteConfig
        ]);

    function RouteConfig($routeProvider, $locationProvider) {
        $locationProvider.html5Mode({
            enabled: false,
            requireBase: false
        });

        $routeProvider
            .when('/about', {
                templateUrl: '/angular/apps/ecoreal/views/common/about-us.html'
            })
            .when('/404', {
                templateUrl: '/angular/apps/ecoreal/views/landing/404.html'
            })
            .when('/403', {
                templateUrl: '/angular/apps/ecoreal/views/landing/403.html'
            })
            .when('/500', {
                templateUrl: '/angular/apps/ecoreal/views/landing/500.html'
            })
            .otherwise({
                redirectTo: '/product'
            });
    }
})(window.angular);