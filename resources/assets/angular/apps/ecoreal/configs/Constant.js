(function(angular) {
    "use strict";

    angular.module('Ecoreal')
        .constant('COMMON_TEMPLATES', {
            introCarousel: 'angular/apps/ecoreal/views/common/intro-carousel.html'
        });

})(window.angular);