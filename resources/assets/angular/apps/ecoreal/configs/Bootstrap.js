(function(angular) {
    "use strict";

    angular.module('Ecoreal')
        .run([
            '$location',
            'HTTP_STATUS_CODES',
            'WatchService',
            'ChannelService',
            Bootstrap
        ]);
    
    function Bootstrap($location,
                       HTTP_STATUS_CODES,
                       WatchService,
                       ChannelService
    ) {

        ChannelService.Subscribe('HTTP_STATUS', 'Ecoreal.Bootstrap', function (objResponse) {
            switch(objResponse.status) {
                case HTTP_STATUS_CODES.UNPROCESSABLE_ENTITY:
                case HTTP_STATUS_CODES.BAD_REQUEST:
                case HTTP_STATUS_CODES.REQUEST_TOO_LARGE:
                    WatchService.Notify('BAD_REQUEST', 'SHOW_TOAST_ERROR', objResponse.data || [objResponse.statusText]);
                    break;
                
                case HTTP_STATUS_CODES.SERVER_ERROR:
                    $location.path('/500');
                    break;
                
                case HTTP_STATUS_CODES.NOT_AUTHORIZE:
                    WatchService.Notify('UNAUTHORIZED', 'MODAL_LOGIN_POPUP', true);
                    break;
                
                case HTTP_STATUS_CODES.FORBIDDEN:
                case HTTP_STATUS_CODES.METHOD_NOT_ALLOWED:
                    $location.path('/403');
                    break;
                
                case HTTP_STATUS_CODES.NOT_FOUND:
                    $location.path('/404');
                    break;
            }
        });
    }

})(window.angular);