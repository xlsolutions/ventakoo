(function(angular) {
    "use strict";
    
    angular.module('Ecoreal', 
        [
            'ngRoute',
            'ngAnimate',
            'Http',
            'Mediator',
            'Observer',
            'Product',
            'Category',
            'Auth',
            'Order'
        ]);
    
})(window.angular);