(function(angular) {
    "use strict";
    
    angular.module('Ecoreal')
        .controller('AppRootController', [
            'COMMON_TEMPLATES',
            AppRootController
        ]);

    function AppRootController(COMMON_TEMPLATES) {

        this.view = {
            name: 'Project Roger',
            template: COMMON_TEMPLATES
        };

        return this;
    }

})(window.angular);