### Update node modules if uglify is undefined

```
sudo npm install
```

### Updating Scripts

```
grunt uglify
```

Then

```
grunt copy
```

### Angular Folder Structure

> angular/app.min.js - compiled / join files.

> angular

> > apps

> > > ecoreal

> > > > components - website components

> > > > > app_root - base or body component

> > > > > > controllers - controllers for this component

> > > > > > views - templates for this component

> > > > > > services - services for this component

> > > > > > AppRoot.js - declare module

> > > > Ecoreal.js - declare module

> > modules

> > > http

> > > > classes

> > > > > Http.js - declare module

> > > > > HttpService.js - reusable http service

> > > mediator

> > > > > Mediator.js - declare module

> > > > > ChannelService.js - publish / subscribe service; MULTIPLE WATCHER

> > > observer

> > > > > Observer.js - declare module

> > > > > WatchService.js - publish / subscribe service; SINGLE WATCHER

> > > product

> > > > > Product.js - declare module

> > > > > ProductService.js - product service where you get the details, update and etc

> > > product_attribute