<!doctype html>
<html ng-app="Ecoreal" ng-controller="AppRootController as appRoot">
<head>
    @include('ecoreal.web.includes.head')
</head>
<body>
    <nav class="navbar navbar-default navbar-fixed-top" id="my-navbar">
        @include('ecoreal.web.includes.nav')
    </nav>

    @yield('body-wrapper')

    @include('ecoreal.web.includes.footer')
</body>
</html>