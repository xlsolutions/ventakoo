<div class="row text-center">
    <div class="col-md-3">
        <ul class="list-group">
            <li class="list-group-item text-muted">
                <h3><span class="fa fa-paper-plane-o"></span></h3>
                <h2 class="add-margin-lr-15 pricing-plan">{{ \App\Providers\ViewDataServiceProvider::get('FREE_AD_POST_CAMPAIGN')['name']  }}</h2>
                <br/>
            </li>
            <li class="list-group-item">
                Free 5 Campaigns
            </li>
            <li class="list-group-item">
                31 Days Expiration / Campaign
            </li>
            <li class="list-group-item text-muted">
                <h3>&#8369;{{ \App\Providers\ViewDataServiceProvider::get('FREE_AD_POST_CAMPAIGN')['price']  }}</h3>
                <span class="invisible">-</span>
                <p>&#8369;0.00 / campaign</p>
                <a href="/account" class="btn btn-default">Upgrade Account</a>
            </li>
        </ul>
    </div>

    <div class="col-md-3">
        <ul class="list-group">
            <li class="list-group-item text-warning">
                <h3><span class="fa fa-paper-plane-o"></span></h3>
                <h2 class="add-margin-lr-15 pricing-plan">{{ \App\Providers\ViewDataServiceProvider::get('STARTER_AD_POST_CAMPAIGN')['name']  }}</h2>
                <br/>
            </li>
            <li class="list-group-item">
                20 Campaigns
            </li>
            <li class="list-group-item">
                No Expiration
            </li>
            <li class="list-group-item text-warning">
                <h3>&#8369;{{ \App\Providers\ViewDataServiceProvider::get('STARTER_AD_POST_CAMPAIGN')['price']  }}</h3>
                <span class="invisible">-</span>
                <p>&#8369;50.00 / campaign</p>
                <a href="/account?campaignPlan=1" class="btn btn-warning">Upgrade Account</a>
            </li>
        </ul>
    </div>
    <div class="col-md-3">
        <ul class="list-group">
            <li class="list-group-item text-success">
                <h3><span class="fa fa-paper-plane-o"></span></h3>
                <h2 class="add-margin-lr-15 pricing-plan">{{ \App\Providers\ViewDataServiceProvider::get('PRO_AD_POST_CAMPAIGN')['name']  }}</h2>
                <br/>
            </li>
            <li class="list-group-item">
                44 Campaigns
            </li>
            <li class="list-group-item">
                No Expiration
            </li>
            <li class="list-group-item text-success">
                <h3>&#8369;{{ \App\Providers\ViewDataServiceProvider::get('PRO_AD_POST_CAMPAIGN')['price']  }}</h3>
                <span class="text-muted"><s>&#8369;50.00 / campaign</s></span>
                <p>&#8369;45.00 / campaign</p>
                <a href="/account?campaignPlan=1" class="btn btn-success">Upgrade Account</a>
            </li>
        </ul>
    </div>
    <div class="col-md-3">
        <ul class="list-group">
            <li class="list-group-item text-danger">
                <h3><span class="fa fa-paper-plane-o"></span></h3>
                <h2 class="add-margin-lr-15 pricing-plan">{{ \App\Providers\ViewDataServiceProvider::get('BUSINESS_AD_POST_CAMPAIGN')['name']  }}</h2>
                <br/>
            </li>
            <li class="list-group-item">
                75 Campaigns
            </li>
            <li class="list-group-item">
                No Expiration
            </li>
            <li class="list-group-item text-danger">
                <h3>&#8369;{{ \App\Providers\ViewDataServiceProvider::get('BUSINESS_AD_POST_CAMPAIGN')['price']  }}</h3>
                <span class="text-muted"><s>&#8369;50.00 / campaign</s></span>
                <p>&#8369;40.00 / campaign</p>
                <a href="/account?campaignPlan=1" class="btn btn-danger">Upgrade Account</a>
            </li>
        </ul>
    </div>
</div>