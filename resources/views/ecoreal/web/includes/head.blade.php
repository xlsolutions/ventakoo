<meta charset="utf-8">
<meta http-equiv="X-UA-Compatible"  content="IE=edge">
<meta name="viewport"               content="width=device-width, initial-scale=1">
<title>{{ \App\Providers\ViewDataServiceProvider::get('page_title') }}</title>

<meta name="description"    content="{{ \App\Providers\ViewDataServiceProvider::get('meta_description') }}">
<meta name="keywords"       content="{{ \App\Providers\ViewDataServiceProvider::get('meta_keyword') }}">
<meta name="author"         content="{{ \App\Providers\ViewDataServiceProvider::get('meta_author') }}">

<link href="https://fonts.googleapis.com/css?family=Montserrat:400,700"     rel="stylesheet" type="text/css">
<link href="https://fonts.googleapis.com/css?family=Open+Sans:400,600,300"  rel="stylesheet" type="text/css">

<!-- =========================
        FAV AND TOUCH ICONS
============================== -->
<link rel="icon" href="/themes/ecoreal/1.4/images/favicon.ico">
<link rel="apple-touch-icon" href="/themes/ecoreal/1.4/images/apple-touch-icon.png">
<link rel="apple-touch-icon" sizes="72x72" href="/themes/ecoreal/1.4/images/apple-touch-icon-72x72.png">
<link rel="apple-touch-icon" sizes="114x114" href="/themes/ecoreal/1.4/images/apple-touch-icon-114x114.png">

<!-- =========================
        STYLESHEETS
============================== -->
{{ \App\Providers\ThemeLayoutServiceProvider::loadCSS() }}

<!-- HTML5 shim and Respond.js for IE8 support of HTML5 elements and media queries -->
<!-- WARNING: Respond.js doesn't work if you view the page via file:// -->
<!--[if lt IE 9]>
<script src="https://oss.maxcdn.com/html5shiv/3.7.3/html5shiv.min.js"></script>
<script src="https://oss.maxcdn.com/respond/1.4.2/respond.min.js"></script>
<![endif]-->