<footer class="footer">
    <div class="footer-overlay">
        <div class="container">
            <div class="row subscribe hidden">
                <form id="mc-form">
                    <div class="col-md-5 newsletter">
                        <span>Subscribe to our newsletter</span>
                    </div><!-- end .col-md-5 -->
                    <div class="col-md-5 subscribe-email">
                        <input type="email" placeholder="Email Address" id="mc-email">
                    </div><!-- end .col-md-5 -->
                    <div class="col-md-2">
                        <input type="submit" value="Subscribe">
                    </div><!-- end .col-md-2 -->
                </form>
            </div><!-- end .row -->
            <div class="footer-middle">
                <div class="row">
                    <div class="col-sm-6">
                        <div class="footer-widget latest-news">
                            <h3>Latest Campaign</h3>
                            <div class="row">
                                <div class="col-sm-6 col-md-6">
                                    <product-list limit="3" template="product-list-portrait.html"></product-list>
                                </div>
                                <div class="col-sm-6 col-md-6"></div>
                            </div>
                        </div><!-- end .about-us -->
                    </div><!-- end .col-md-3 -->
                    <div class="col-sm-6">
                        <div class="footer-widget contact-info">
                            <h3>Contact Info</h3>
                            <div class="contact-info">
                                <p class="clearfix">
                                    <span class="contact-icon pull-left"><i class="fa fa-map-marker fa-2x"></i></span>
                                    <span class="contact-details">25th & 26th Street, 6432 Bonifacio Global City,</span><br/>
                                    <span class="contact-details">1634 Taguig City, Philippines</span><br/>
                                </p>
                                <p class="clearfix">
                                    <span class="contact-icon pull-left"><i class="fa fa-envelope fa-2x"></i></span>
                                    <span class="contact-details">info@ventakoo.com</span>
                                </p>
                                <p class="clearfix">
                                    <span class="contact-icon pull-left"><i class="fa fa-globe fa-2x"></i></span>
                                    <span class="contact-details">www.ventakoo.com</span>
                                </p>
                                <p class="clearfix">
                                    <span class="contact-icon pull-left"><i class="fa fa-facebook-official fa-2x"></i></span>
                                    <span class="contact-details">www.facebook.com/ventakoo</span>
                                </p>
                            </div>
                        </div>
                    </div>
                </div>
            </div><!-- end .footer-middle -->
        </div>
    </div>
</footer>

<footer>
    <div class="footer-overlay">
        <section class="footer-below">
            <div class="container">
                <div class="row">
                    <div class="col-md-6 col-sm-6 copy-right">
                        <p>Copyright &copy; {{date('Y')}} <a href="#">{{ \App\Providers\ViewDataServiceProvider::get('company') }}</a> All rights reserved.</p>
                    </div><!-- end .col-md-6 -->
                    <div class="col-md-6 col-sm-6 payment">
                        <ul class="list-unstyled list-inline">
                            <li><span>We Accept:</span></li>
                            <li><i class="fa fa-cc-paypal" aria-hidden="true"></i></li>{{--
                            <li><i class="fa fa-cc-stripe" aria-hidden="true"></i></li>
                            <li><i class="fa fa-cc-visa" aria-hidden="true"></i></li>
                            <li><i class="fa fa-cc-mastercard" aria-hidden="true"></i></li>--}}
                        </ul>
                    </div><!-- end .col-md-6 -->
                </div><!-- end .row -->
            </div><!-- end .containter -->
        </section>
    </div>
</footer>

<div data-toast-message="danger" data-name="BAD_REQUEST" data-channel="SHOW_TOAST_ERROR"></div>
<div data-toast-message="success" data-name="OK_REQUEST" data-channel="SHOW_TOAST_OK"></div>

<script type="text/ng-template" id="product-list-portrait.html">
    <div class="container-fluid">
        <div class="list-group"
             ng-repeat="product in productList.products track by $index">
            <div class="list-group-item" style="background: #efefef">
                <a ng-href="/#/product/@{{ product.getHandle() }}">
                    <img ng-src="@{{ product.getPhotoThumbnail() }}" style="max-height: 100px" class="img-responsive center-block item-photo" alt="" />
                </a>
                <br/>
                <span class="text-muted">
                    <span class="text-capitalize" ng-bind="product.getName()"></span><br/>
                    <span class="fa fa-user"></span>
                    <a ng-href="/#/user/@{{ product.getUserId() }}/product" ng-bind="product.getOwner()" class="text-capitalize"></a>
                </span>
            </div>
        </div>
    </div>
</script>

<!-- =========================
                SCRIPTS
         ========================= -->
{{ \App\Providers\ThemeLayoutServiceProvider::loadJS() }}

@yield('footer-scripts')