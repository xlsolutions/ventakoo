{{-- Main Header --}}
<div class="navbar-header">
    <button type="button" class="navbar-toggle collapsed" data-toggle="collapse" data-target="#navbar-collapse" aria-expanded="false">
        <span class="sr-only">Toggle navigation</span>
        <span class="icon-bar"></span>
        <span class="icon-bar"></span>
        <span class="icon-bar"></span>
    </button>
    <a class="navbar-brand" href="/">
        <img src="/themes/ecoreal/1.4/images/favicon.ico" alt=""/>
    </a>
</div>

{{-- Categories --}}
<app-navigation></app-navigation>