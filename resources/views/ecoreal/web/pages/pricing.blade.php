@extends('ecoreal.web.layouts.default')

@section('body-wrapper')
    <div id="main-wrapper">
        <div class="container" id="pricing-page">
            <div class="row-fluid">
                @if(!\App\Providers\AuthServiceProvider::bolLogin())
                    @include('ecoreal.web.includes.pricing_no_auth')
                @else
                    @if(!\App\Providers\ViewDataServiceProvider::get('isBuyer'))
                        @include('ecoreal.web.includes.pricing_auth_seller')
                    @else
                        @include('ecoreal.web.includes.pricing_auth_buyer')
                    @endif
                @endif
            </div>
        </div>
    </div>
@endsection