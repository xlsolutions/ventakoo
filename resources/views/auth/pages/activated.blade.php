@extends('ecoreal.web.layouts.default')

@section('body-wrapper')
    <div class="container">
        <div class="container-fluid">
            <div class="col-md-3"></div>

            <div class="col-md-6">
                <div class="row">
                    <h3 class="page-header text-capitalize text-center text-muted">Your {{ \App\Providers\ViewDataServiceProvider::get('company') }} <span class="text-lowercase">account is now activated</span>!</h3>

                    <div class="alert alert-success">
                        <p class="form-control-static lead">
                            You may now <a href="/login"><em>login</em></a> your account.
                        </p>
                    </div>

                    <div class="row">
                        <blockquote class="pull-right">
                            <small>Congrats!</small>
                        </blockquote>
                    </div>
                </div>
            </div>

            <div class="col-md-3"></div>
        </div>
    </div>
@endsection