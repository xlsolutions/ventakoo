@extends('ecoreal.web.layouts.default')

@section('body-wrapper')
    <div class="container">
        <div class="container-fluid">
            <div class="col-md-4"></div>

            <div class="col-md-4">
                <div class="row">
                    @include('auth.partials.login')

                    <div class="row">
                        <h4 class="page-header text-capitalize text-center text-muted">New <span class="text-lowercase">to</span> {{ \App\Providers\ViewDataServiceProvider::get('company') }} ?</h4>

                        <p class="form-control-static text-center">
                            <a href="/register" class="btn btn-link btn-lg">
                            <span class="text-success">
                                Start here <span class="fa fa-angle-double-right"></span>
                            </span>
                            </a>
                        </p>
                    </div>
                </div>
            </div>

            <div class="col-md-4"></div>
        </div>
    </div>
@endsection