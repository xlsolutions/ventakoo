@extends('ecoreal.web.layouts.default')

@section('body-wrapper')
    <div class="container">
        <div class="container-fluid">
            <div class="col-md-4"></div>

            <div class="col-md-4">
                <div class="row">

                    @if($change_password === true)
                        @include('auth.partials.forgot_password_change')
                    @else
                        @include('auth.partials.forgot_password_verify')
                    @endif

                    @if($change_password === false)
                        <div class="row">
                            <h4 class="page-header text-lowercase text-center text-muted"><span class="text-capitalize">I</span> remember my password</h4>

                            <p class="form-control-static text-center">
                                <a href="/login" class="btn btn-link btn-lg">
                        <span class="text-success">
                            Sign in <span class="fa fa-angle-double-right"></span>
                        </span>
                                </a>
                            </p>
                        </div>
                    @endif
                </div>
            </div>

            <div class="col-md-4"></div>
        </div>
    </div>
@endsection