@extends('ecoreal.web.layouts.default')

@section('body-wrapper')
    <div class="container">
        <div class="container-fluid">
            <div class="col-md-3"></div>

            <div class="col-md-6">
                <div class="row">
                    <h2 class="page-header text-capitalize text-center text-muted">Welcome to {{ \App\Providers\ViewDataServiceProvider::get('company') }}!</h2>

                    <div class="alert alert-success">
                        <p class="form-control-static lead">
                            Before we start, all you have to do is to confirm your account. Please check your email and follow the instructions.
                        </p>
                        <small>Note: Email can be delayed between 1-3 minutes.</small>
                    </div>

                    <div class="row">
                        <blockquote class="pull-right">
                            <small>Thank you</small>
                        </blockquote>
                    </div>

                    <div id="login">
                        <h4 class="page-header text-lowercase text-center text-muted"><span class="text-capitalize">A</span>lready confirmed your account ?</h4>

                        <p class="form-control-static text-center">
                            <a href="/login" class="btn btn-link btn-lg">
                                <span class="text-success">Sign in <span class="fa fa-angle-double-right"></span></span>
                            </a>
                        </p>
                    </div>

                    <form id="verify_email" action="/verify_email" method="post" class="form-horizontal hidden">

                        {{ csrf_field() }}

                        <h4 class="page-header text-lowercase text-center text-danger"><span class="text-capitalize">D</span>idn't receive your confirmation email ?</h4>

                        <input type="hidden" name="email" value="{{ $email }}" maxlength="100" />

                        <p class="form-control-static text-center">
                            <button class="btn btn-default btn-lg">Resend</button>
                        </p>

                    </form>
                </div>
            </div>

            <div class="col-md-3"></div>
        </div>
    </div>
@endsection

@section('footer-scripts')
    <script>
        (function() {
            setTimeout(function() {
                document.getElementById('login').className = 'hidden';
                document.getElementById('verify_email').className = 'form-horizontal';
            }, 60000);
        })();
    </script>
@endsection