@extends('ecoreal.web.layouts.default')

@section('body-wrapper')
    <div class="container">
        <div class="container-fluid">
            <div class="col-md-4"></div>

            <div class="col-md-4">
                <div class="row">

                    @if($registration_type === 'SELLER')
                        @include('auth.partials.register_seller')
                    @else
                        @include('auth.partials.register')
                    @endif

                    <div class="row">
                        <h4 class="page-header text-lowercase text-center text-muted"><span class="text-capitalize">A</span>lready a member ?</h4>

                        <p class="form-control-static text-center">
                            <a href="/login" class="btn btn-link btn-lg">
                            <span class="text-success">
                                Sign in <span class="fa fa-angle-double-right"></span>
                            </span>
                            </a>
                        </p>
                    </div>
                </div>
            </div>

            <div class="col-md-4"></div>
        </div>
    </div>
@endsection

@section('footer-scripts')
    <script src="{!! asset('/js/register.js') !!}"></script>
@endsection