@extends('ecoreal.web.layouts.default')

@section('body-wrapper')
    <div class="container">
    <div class="container-fluid">
        <div class="col-md-4"></div>

        <div class="col-md-4">
            <div class="row">
                <div id="registration-container" class="row-fluid">

                    <form action="/account/update" method="POST" name="UserUpdateInformationPageForm" id="UserUpdateInformationPageForm" class="form-horizontal" enctype="multipart/form-data">

                        {{ csrf_field() }}

                        <div class="form-group">
                            <h2 class="page-header text-capitalize">Update Account</h2>
                        </div>

                        <div class="form-group">
                            <div class="post-thumbnails">
                                <img src="{{ $strImageSource }}" alt="{{ $firstname }} {{ $lastname }}" class="img-thumbnail center-block" width="170" height="170" />
                            </div>
                        </div>
                        <div class="form-group">
                            @if(isset($strImageSource) AND $strImageSource !== '')
                                <label for="user_image" class="control-label">Update Image</label>
                            @else
                                <label for="user_image" class="control-label">Upload Image</label>
                            @endif

                            <input type="file"
                                   name="user_image"
                                   id="user_image"
                                   class="form-control input-lg" />
                        </div>

                        {!! \App\Providers\FlashMessageServiceProvider::show('success') !!}

                        {!! \App\Providers\FlashMessageServiceProvider::show('danger') !!}

                        @if($user_type === 'BUYER')
                            <div class="form-group checkbox">
                                <label for="convert_type">
                                    <input type="checkbox"
                                           name="convert_type"
                                           id="convert_type">
                                    Convert account to SELLER</label>
                            </div>

                            <br/>
                        @endif

                        <div class="form-group">
                            <label for="firstname" class="control-label">First Name</label>

                            <input type="text"
                                   name="firstname"
                                   id="firstname"
                                   class="form-control input-lg"
                                   value="{{ $firstname }}"
                                   maxlength="64" />
                        </div>

                        <div class="form-group">
                            <label for="lastname" class="control-label">Last Name</label>

                            <input type="text"
                                   name="lastname"
                                   id="lastname"
                                   class="form-control input-lg"
                                   value="{{ $lastname }}"
                                   maxlength="64" />
                        </div>

                        <div class="form-group">
                            <label for="email_address" class="control-label">Email</label>

                            <input type="email"
                                   name="email_address"
                                   id="email_address"
                                   class="form-control input-lg"
                                   maxlength="100"
                                   value="{{ $email_address }}"
                                   disabled
                            />
                        </div>

                        <div class="form-group">
                            <label for="mobile_number" class="control-label">Mobile</label>

                            <input type="text"
                                   name="mobile_number"
                                   id="mobile_number"
                                   class="form-control input-lg"
                                   value="{{ $mobile_number }}"
                                   maxlength="20" />
                        </div>

                        <div class="form-group">
                            <label for="phone_number" class="control-label">Phone</label>

                            <input type="text"
                                   name="phone_number"
                                   id="phone_number"
                                   class="form-control input-lg"
                                   value="{{ $phone_number }}"
                                   maxlength="20" />
                        </div>

                        <div class="form-group">
                            <label for="country" class="control-label">Country</label>

                            <select class="form-control input-lg" name="country" id="country" >
                                <option value=""> select </option>
                                @foreach ($countries as $objCountry)
                                    <option value="{{ $objCountry['country_code'] }}" @if($objCountry['country_code'] === $country) selected="selected" @endif>{{ $objCountry['country_name'] }}</option>
                                @endforeach
                            </select>
                        </div>

                        <div class="form-group">
                            <label for="state" class="control-label">State</label>

                            <select class="form-control input-lg" name="state" id="state" old-value="{{ $state }}">
                                <option value=""> select </option>
                            </select>
                        </div>

                        <div class="form-group">
                            <label for="city" class="control-label">City</label>

                            <select class="form-control input-lg" name="city" id="city" old-value="{{ $city }}">
                                <option value=""> select </option>
                            </select>
                        </div>

                        <div class="form-group">
                            <label for="street" class="control-label">Street</label>

                            <textarea name="street" id="street" cols="30" rows="5" class="form-control input-lg">{{ $street }}</textarea>
                        </div>

                        <div class="page-header">
                            <h4 class="text-center">Change Your Login Credentials?</h4>
                        </div>

                        <div class="form-group">
                            <label for="old_password" class="control-label">Old Password</label>

                            <input type="password"
                                   name="old_password"
                                   id="old_password"
                                   class="form-control input-lg"
                                   placeholder="please input your old password"
                                   maxlength="255"/>
                        </div>

                        <div class="form-group">
                            <label for="new_password" class="control-label">New Password</label>

                            <input type="password"
                                   name="new_password"
                                   id="new_password"
                                   class="form-control input-lg"
                                   placeholder="new password atleast 8 characters"
                                   maxlength="255"/>
                        </div>

                        <div class="form-group">
                            <label for="confirm_password" class="control-label">Retype your password</label>

                            <input type="password"
                                   name="confirm_password"
                                   id="confirm_password"
                                   class="form-control input-lg"
                                   placeholder="confirm new password"
                                   maxlength="255"/>
                        </div>

                        <div class="form-group">
                            <button class="btn btn-success btn-lg btn-block">Update {{ \App\Providers\ViewDataServiceProvider::get('company') }} account</button>
                        </div>

                    </form>

                </div>

            </div>
        </div>

        <div class="col-md-4"></div>
    </div>
</div>
@endsection

@section('footer-scripts')
    <script src="{!! asset('/js/register.js') !!}"></script>
@endsection