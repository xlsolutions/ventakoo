@extends('ecoreal.web.layouts.default')

@section('body-wrapper')
    <div class="container">
        <div class="container-fluid">
            <div class="col-md-3"></div>

            <div class="col-md-6">
                <div class="row">
                    <h3 class="page-header text-capitalize text-center text-muted">Forgot <span class="text-lowercase">password completed</span>!</h3>

                    <div class="alert alert-success">
                        <p class="form-control-static lead">
                            You have successfully changed your password. You may now login your account <a href="/login">here</a>.
                        </p>
                    </div>

                    <div class="row">
                        <blockquote class="pull-right">
                            <small>Regards,</small>
                            <br/> Team {{ \App\Providers\ViewDataServiceProvider::get('company') }}
                        </blockquote>
                    </div>
                </div>
            </div>

            <div class="col-md-3"></div>
        </div>
    </div>
@endsection