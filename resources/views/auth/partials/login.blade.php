<div class="row-fluid">

    <form action="/login" method="post" name="LoginPageForm" id="LoginPageForm" class="form-horizontal">

        {{ csrf_field() }}

        <div class="form-group">
            <h2 class="page-header text-capitalize">Sign in</h2>
        </div>

        {!! \App\Providers\FlashMessageServiceProvider::show('danger') !!}

        <div class="form-group">
            <label for="email_address" class="control-label">Email</label>

            <input type="email"
                   name="email_address"
                   id="email_address"
                   class="form-control input-lg"
                   maxlength="100"
                   value="{{ old('email_address') }}"
                   required />
        </div>

        <div class="form-group">
            <label for="password" class="control-label">Password</label>

            <input type="password"
                   name="password"
                   id="password"
                   class="form-control input-lg"
                   maxlength="255"
                   required />
        </div>

        <div class="form-group">
            <p class="text-right">
                <small>
                    <a href="/forgot_password" class="text-muted">
                        I forgot my password
                        <span class="fa fa-question-circle-o fa-lg"></span>
                    </a>
                </small>
            </p>
        </div>

        <div class="form-group">
            <button class="btn btn-success btn-lg">Sign in</button>
        </div>

    </form>

</div>