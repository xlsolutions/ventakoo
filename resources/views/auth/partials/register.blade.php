<div id="registration-container" class="row-fluid">

    <form action="/register" method="POST" name="RegistrationPageForm" id="RegistrationPageForm" class="form-horizontal">

        {{ csrf_field() }}

        <div class="form-group">
            <h2 class="page-header text-capitalize">Create account</h2>
        </div>

        {!! \App\Providers\FlashMessageServiceProvider::show('danger') !!}

        <div class="form-group">
            <label for="email_address" class="control-label">Email</label>

            <input type="email"
                   name="email_address"
                   id="email_address"
                   class="form-control input-lg"
                   maxlength="100"
                   value="{{ $email_address }}"
                   required />
        </div>

        <div class="form-group">
            <label for="password" class="control-label">Password</label>

            <input type="password"
                   name="password"
                   id="password"
                   class="form-control input-lg"
                   placeholder="atleast 8 characters"
                   maxlength="255"
                   required />
        </div>

        <div class="form-group">
            <label for="confirm_password" class="control-label">Retype your password</label>

            <input type="password"
                   name="confirm_password"
                   id="confirm_password"
                   class="form-control input-lg"
                   maxlength="255"
                   required />
        </div>

        <div class="form-group">
            <button class="btn btn-success btn-lg btn-block">Create your {{ \App\Providers\ViewDataServiceProvider::get('company') }} account</button>
        </div>

        <div class="form-group">
            <p class="form-control-static">
                <small>By creating an account, you agree to {{ \App\Providers\ViewDataServiceProvider::get('company') }}'s <a href="#">Conditions of Use</a> and <a href="#">Privacy Notice</a>.</small>
            </p>
        </div>

    </form>

</div>