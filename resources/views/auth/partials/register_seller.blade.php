<div id="registration-container" class="row-fluid">

    <form action="/register" method="POST" name="RegistrationPageForm" id="RegistrationPageForm" class="form-horizontal">

        {{ csrf_field() }}

        <input type="hidden" name="type" value="SELLER" />

        <div class="form-group">
            <h2 class="page-header text-capitalize">Create account</h2>
        </div>

        {!! \App\Providers\FlashMessageServiceProvider::show('danger') !!}

        <div class="form-group">
            <label for="firstname" class="control-label">First Name</label>

            <input type="text"
                   name="firstname"
                   id="firstname"
                   class="form-control input-lg"
                   value="{{ $firstname }}"
                   maxlength="64" />
        </div>

        <div class="form-group">
            <label for="lastname" class="control-label">Last Name</label>

            <input type="text"
                   name="lastname"
                   id="lastname"
                   class="form-control input-lg"
                   value="{{ $lastname }}"
                   maxlength="64" />
        </div>

        <div class="form-group">
            <label for="email_address" class="control-label">Email</label>

            <input type="email"
                   name="email_address"
                   id="email_address"
                   class="form-control input-lg"
                   maxlength="100"
                   value="{{ $email_address }}"
                   required />
        </div>

        <div class="form-group">
            <label for="mobile_number" class="control-label">Mobile</label>

            <input type="text"
                   name="mobile_number"
                   id="mobile_number"
                   class="form-control input-lg"
                   value="{{ $mobile_number }}"
                   maxlength="20" />
        </div>

        <div class="form-group">
            <label for="phone_number" class="control-label">Phone</label>

            <input type="text"
                   name="phone_number"
                   id="phone_number"
                   class="form-control input-lg"
                   value="{{ $phone_number }}"
                   maxlength="20" />
        </div>

        <div class="form-group">
            <label for="country" class="control-label">Country</label>

            <select class="form-control input-lg" name="country" id="country">
                <option value=""> select </option>
                @foreach ($countries as $objCountry)
                    <option value="{{ $objCountry['country_code'] }}"
                            @if($country == $objCountry['country_code'])
                            selected="selected"
                            @endif
                    >{{ $objCountry['country_name'] }}</option>
                @endforeach
            </select>
        </div>

        <div class="form-group">
            <label for="state" class="control-label">State</label>

            <select class="form-control input-lg" name="state" id="state" old-value="{{ $state }}">
                <option value=""> select </option>
            </select>
        </div>

        <div class="form-group">
            <label for="city" class="control-label">City</label>

            <select class="form-control input-lg" name="city" id="city" old-value="{{ $city }}">
                <option value=""> select </option>
            </select>
        </div>

        <div class="form-group">
            <label for="street" class="control-label">Address</label>

            <textarea name="street" id="street" cols="30" rows="5"
                      class="form-control input-lg"
                      placeholder="Unit or House # / Street / Barangay">{{ $street }}</textarea>
        </div>

        <div class="page-header">
            <h4 class="text-center">Your Login Credentials</h4>
        </div>

        <div class="form-group">
            <label for="password" class="control-label">Password</label>

            <input type="password"
                   name="password"
                   id="password"
                   class="form-control input-lg"
                   placeholder="atleast 8 characters"
                   maxlength="255"
                   required />
        </div>

        <div class="form-group">
            <label for="confirm_password" class="control-label">Retype your password</label>

            <input type="password"
                   name="confirm_password"
                   id="confirm_password"
                   class="form-control input-lg"
                   maxlength="255"
                   required />
        </div>

        <div class="form-group">
            <button class="btn btn-success btn-lg btn-block">Create your {{ \App\Providers\ViewDataServiceProvider::get('company') }} account</button>
        </div>

        <div class="form-group">
            <p class="form-control-static">
                <small>By creating an account, you agree to {{ \App\Providers\ViewDataServiceProvider::get('company') }}'s <a href="#">Conditions of Use</a> and <a href="#">Privacy Notice</a>.</small>
            </p>
        </div>

    </form>

</div>