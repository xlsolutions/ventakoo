<div class="row-fluid">

    <form action="/forgot_password_verify" method="post" name="ForgotPasswordChangePageForm" id="ForgotPasswordChangePageForm" class="form-horizontal">

        <input type="hidden" value="{{$code}}" name="code" />

        {{ csrf_field() }}

        <div class="form-group">
            <h2 class="page-header text-capitalize">Change password</h2>
        </div>

        {!! \App\Providers\FlashMessageServiceProvider::show('danger') !!}

        <div class="form-group">
            <label for="password" class="control-label">Password</label>

            <input type="password"
                   name="password"
                   id="password"
                   class="form-control input-lg"
                   placeholder="atleast 8 characters"
                   maxlength="255"
                   required />
        </div>

        <div class="form-group">
            <label for="confirm_password" class="control-label">Retype your password</label>

            <input type="password"
                   name="confirm_password"
                   id="confirm_password"
                   class="form-control input-lg"
                   maxlength="255"
                   required />
        </div>

        <div class="form-group">
            <button class="btn btn-success btn-lg">Change</button>
        </div>

    </form>

</div>