<div class="row-fluid">

    <form action="/forgot_password" method="post" name="ForgotPasswordPageForm" id="ForgotPasswordPageForm" class="form-horizontal">

        {{ csrf_field() }}

        <div class="form-group">
            <h2 class="page-header text-capitalize">Forgot password confirmation</h2>
        </div>

        {!! \App\Providers\FlashMessageServiceProvider::show('danger') !!}

        <div class="form-group">
            <label for="email_address" class="control-label">Email</label>

            <input type="email"
                   name="email_address"
                   id="email_address"
                   class="form-control input-lg"
                   maxlength="100"
                   value="{{ old('email_address') }}"
                   required />
        </div>

        <div class="form-group">
            <button class="btn btn-success btn-lg">Send</button>
        </div>

    </form>

</div>