If you did not request this change, ignore this email {{ "\r\n" }}

{{ $email . "\r\n" }}

To change your password, paste this link into your web browser: {{ "\r\n" }}

{{ \App\Providers\ViewDataServiceProvider::get('app_url') }}/forgot_password_verify?code={{ $code . "\r\n" }}

Best Regards, {{ "\r\n" }}
Team {{ \App\Providers\ViewDataServiceProvider::get('company') }}