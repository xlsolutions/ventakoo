One last step is required {{ "\r\n" }}

{{ $email . "\r\n" }}

Paste this link into your web browser: {{ "\r\n" }}

{{ \App\Providers\ViewDataServiceProvider::get('app_url') }}/verify_email?code={{ $code . "\r\n" }}&campaignPlan={{ $campaignPlan }}

Best Regards, {{ "\r\n" }}
Team {{ \App\Providers\ViewDataServiceProvider::get('company') }}