@extends('ecoreal.web.layouts.default')

@section('body-wrapper')
    <div class="container">
        <div class="container-fluid">
            <h1 class="page-header text-capitalize">
                <span class="fa fa-cog fa-spin"></span> We're updating {{ \App\Providers\ViewDataServiceProvider::get('company') }} for you
            </h1>
            <p class="lead">Unfortunately {{ \App\Providers\ViewDataServiceProvider::get('company') }} is down for a bit of maintenance right now. We will be back soon.</p>
        </div>
    </div>
@endsection