# ManilaDevOps Project

### Generate SSH Key

[Click here](https://help.github.com/articles/generating-a-new-ssh-key-and-adding-it-to-the-ssh-agent/) to generate your SSH Key.

### Clone Project from git

```
cd ~

mkdir git

cd git

git clone git@bitbucket.org:projectrog/project-roger.git ProjectRoger
```

### Then install / update vagrant

```
wget https://releases.hashicorp.com/vagrant/1.8.7/vagrant_1.8.7_x86_64.deb

sudo dpkg -i vagrant_1.8.7_x86_64.deb
```


### Installing Homestead to vagrant

```
cd ~

git clone git@bitbucket.org:projectrog/homestead.git Homestead

cd ~/Homestead
```

### Run this on your Homestead directory

```
bash init.sh
```

### Homestead YAML File (IMPORTANT)

```
sudo gedit .homestead/Homestead.yaml
```

###### Copy all of this to your Homestead.yaml:

Specify your <user_name> and <directory>

```
---
ip: "192.168.10.10"
memory: 2048
cpus: 1
provider: virtualbox
php: 5.6

authorize: ~/.ssh/id_rsa.pub

keys:
    - ~/.ssh/id_rsa

folders:
    - map: /home/<user_name>/git
      to: /home/vagrant/<your_directory>

sites:
    - map: dev.project-roger.app
      to: /home/vagrant/<directory>/ProjectRoger/public

databases:
    - homestead
```

### Add this to your hosts file

```
sudo gedit /etc/hosts

192.168.10.10 dev.project-roger.app
```

### Create an .env file inside the project

##### Put this inside:

```
APP_ENV=local
APP_DEBUG=true
APP_KEY=base64:ayMXcFisWpq0XqWxAqrJSqcskPTY11DS9v4OFn8C0Es=
APP_URL=http://localhost

DB_CONNECTION=mysql
DB_HOST=192.168.10.10
DB_PORT=3306
DB_DATABASE=homestead
DB_USERNAME=homestead
DB_PASSWORD=secret

CACHE_DRIVER=file
SESSION_DRIVER=file
QUEUE_DRIVER=sync

REDIS_HOST=192.168.10.10
REDIS_PASSWORD=null
REDIS_PORT=6379

MAIL_DRIVER=smtp
MAIL_HOST=mailtrap.io
MAIL_PORT=2525
MAIL_USERNAME=null
MAIL_PASSWORD=null
MAIL_ENCRYPTION=null
```

### Setting the Database (MySQL Workbench to Homestead)

> * Connection Name : PRSite_DB
> * Connection Method : Standard TCP/IP over SSH
> * SSH Hostname: 192.168.10.10
> * SSH Username: vagrant
> * SSH Password: vagrant
> * MySQL Hostname: 192.168.10.10
> * MySQL Server Port: 3306
> * MySQL Username: homestead
> * MySQL Password: secret

### Start Vagrant
```
cd ~/Homestead
vagrant box update
vagrant up --provision
vagrant ssh
```

### Having Errors?
##### Go to your project directory on vagrant (should be on ssh)

```
cd <your_directory>/ProjectRoger
```

##### Run the following on your vagrant

```
composer clearcache
composer install

if an error occurs 
composer update --no-scripts

```

### If you want to rollback the database all you need to run is:

##### with Seeder
```
php artisan migrate:refresh --seed
```

### without Seeder
```
php artisan migrate:refresh
```

##### Run the following outside your vagrant
```
sudo npm install --save-dev
chmod -R 0777 storage
```