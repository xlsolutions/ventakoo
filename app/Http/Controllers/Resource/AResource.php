<?php

namespace App\Http\Controllers\Resource;

use App\Http\Controllers\Controller;
use Illuminate\Database\Eloquent\Builder;
use App\Http\Requests\Request;
use Illuminate\Http\Response;

abstract class AResource extends Controller
{
    private $objResource;
    
    abstract public function getMethod(Request $request, $strHandle);
    abstract public function getAllMethod(Request $request);
    abstract public function postMethod(Request $request);
    abstract public function postUpdateMethod(Request $request, $strHandle);
    abstract public function deleteMethod(Request $request, $strHandle);

    abstract protected function buildResource(Request $request);
    
    protected function setResource(Builder $objResource)
    {
        $this->objResource = $objResource;
    }
    
    protected function getResource()
    {
        return $this->objResource;
    }

    protected function sendJson($unkData, $intHttpStatusCode = Response::HTTP_OK)
    {
        return response()->json($unkData, $intHttpStatusCode);
    }
    
    protected function sendBadRequest(array $arrData)
    {
        return $this->sendJson($arrData, Response::HTTP_BAD_REQUEST);
    }

    protected function sendNotFound(array $arrData)
    {
        return $this->sendJson($arrData, Response::HTTP_NOT_FOUND);
    }

    protected function sendUnauthorized(array $arrData)
    {
        return $this->sendJson($arrData, Response::HTTP_UNAUTHORIZED);
    }

    protected function sendMethodNotAllowed(array $arrData)
    {
        return $this->sendJson($arrData, Response::HTTP_METHOD_NOT_ALLOWED);
    }

    protected function sendInternalError(array $arrData)
    {
        return $this->sendJson($arrData, Response::HTTP_INTERNAL_SERVER_ERROR);
    }

    protected function sendResponseOk(array $arrData)
    {
        return $this->sendJson($arrData, Response::HTTP_OK);
    }
}
