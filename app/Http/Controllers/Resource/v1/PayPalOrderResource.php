<?php

namespace App\Http\Controllers\Resource\v1;

use App\Http\Controllers\Resource\AResource;
use App\Http\Requests\Request;
use App\Models\PayPal\TPayPalTrans;

class PayPalOrderResource extends AResource
{
    protected $objTPayPalTrans;

    public $intUserId;

    public function __construct(TPayPalTrans $objTPayPalTrans)
    {
        $this->objTPayPalTrans = $objTPayPalTrans;

        $this->setResource(TPayPalTrans::query());
    }

    /**
     * Used to get paypal transaction info by order handle
     *
     * @param $objRequest Request
     * @param $strHandle string
     *
     * @return string json format
     *
     * */
    public function getMethod(Request $objRequest, $strHandle)
    {
        try
        {
            $this->setResource($this->objTPayPalTrans->selectOne());

            $objTPayPalTrans = $this->getResource()
                ->where('t_paypal_trans.order_id', $strHandle)
                ->where('t_order.user_id', $this->intUserId)
                ->get()
                ->first();

            if ($objTPayPalTrans)
            {
                return $this->sendJson($objTPayPalTrans);
            }
            else
            {
                return $this->sendNotFound([]);
            }
        }
        catch (\Exception $objError)
        {
            syslog(LOG_CRIT, __FILE__ . ":" . __METHOD__ . ":" . __LINE__ . " => {$objError->getMessage()}");

            return $this->sendInternalError([]);
        }
    }

    /**
     * Used to get all order item
     *
     * @param $objRequest Request
     *
     * @return string json format
     *
     * */
    public function getAllMethod(Request $objRequest)
    {
        return $this->sendMethodNotAllowed([]);
    }

    /**
     * Method not allowed
     *
     * @param $objRequest Request
     *
     * @return string json format
     *
     * */
    public function postMethod(Request $objRequest)
    {
        return $this->sendMethodNotAllowed([]);
    }

    /**
     * Method not allowed
     *
     * @param $objRequest Request
     * @param $strHandle string
     *
     * @return string json format
     *
     * */
    public function postUpdateMethod(Request $objRequest, $strHandle)
    {
        return $this->sendMethodNotAllowed([]);
    }

    /**
     * Method not allowed
     *
     * @param $objRequest Request
     * @param $strHandle string
     *
     * @return string json format
     *
     * */
    public function deleteMethod(Request $objRequest, $strHandle)
    {
        return $this->sendMethodNotAllowed([]);
    }

    /**
     * Used to filter a order item (filters are optional)
     *
     * @param $objRequest Request
     *
     * @return void
     *
     * */
    protected function buildResource(Request $objRequest)
    {

    }
}
