<?php

namespace App\Http\Controllers\Resource\v1;

use App\Http\Controllers\Resource\AResource;
use App\Models\Products\TProduct;
use App\Models\Products\TProductAttribute;
use App\Models\Constants\CProductInformation;
use App\Http\Requests\Request;
use App\Models\Users\TUserProductAndServiceLinker;
use App\Providers\AuthServiceProvider;
use Intervention\Image\Facades\Image;
use DB;

class ProductAttributeResource extends AResource
{
    private $objTProductAttributeAttribute;
    private $objTProduct;
    private $objCProductInformation;
    private $objTUserProductAndServiceLinker;

    private $arrFileExtension = [
        'image/png' => 'png',
        'image/jpeg' => 'jpg',
        'image/jpg' => 'jpg',
        'image/gif' => 'gif'
    ];

    public $intUserId;
    public $strProductHandle;
    public $strProductAttributeHandle;

    public function __construct(TProductAttribute $objTProductAttributeAttribute, TProduct $objTProduct, CProductInformation $objCProductInformation, TUserProductAndServiceLinker $objTUserProductAndServiceLinker)
    {
        $this->objTProductAttributeAttribute = $objTProductAttributeAttribute;
        $this->objTProduct = $objTProduct;
        $this->objCProductInformation = $objCProductInformation;
        $this->objTUserProductAndServiceLinker = $objTUserProductAndServiceLinker;

        $this->setResource(TProductAttribute::query());
    }

    /**
     * Used to get the product attribute by product attribute handle
     *
     * @param $objRequest Request
     * @param $strProductAttributeHandle string
     *
     * @return string json format
     *
     * */
    public function getMethod(Request $objRequest, $strProductAttributeHandle)
    {
        try
        {
            $this->setResource($this->objTProductAttributeAttribute->select());

            $arrTProductAttribute = $this->getResource()
                ->where('t_product.handle', $this->strProductHandle)
                ->where('c_product_information.handle', $strProductAttributeHandle)
                ->get()
                ->toArray();

            if (count($arrTProductAttribute) === 1)
            {
                foreach($arrTProductAttribute as &$arrData)
                {
                    if (in_array($arrData['handle'], ['IMAGE_THUMBNAIL', 'IMAGE_GALLERY', 'IMAGE_ORIGINAL']))
                    {
                        $arrValue = explode('.', $arrData['value']);
                        $strFileExt = end($arrValue);

                        $arrMimeType = array_flip($this->arrFileExtension);

                        if (@file_exists($arrData['value']))
                        {
                            $arrData['value'] = "data:{$arrMimeType[$strFileExt]};base64," . base64_encode(file_get_contents($arrData['value']));
                        }
                    }
                }

                return $this->sendJson($arrTProductAttribute[0]);
            }
            else
            {
                return $this->sendNotFound([]);
            }
        }
        catch (\Exception $objError)
        {
            syslog(LOG_CRIT, __FILE__ . ":" . __METHOD__ . ":" . __LINE__ . " => {$objError->getMessage()}");

            return $this->sendInternalError([]);
        }
    }

    /**
     * Used to get all product attributes by product handle
     *
     * @param $objRequest Request
     *
     * @return string json format
     *
     * */
    public function getAllMethod(Request $objRequest)
    {
        try
        {
            $this->setResource($this->objTProductAttributeAttribute->select());

            $arrTProductAttribute = $this->getResource()
                ->where('t_product.handle', $this->strProductHandle)
                ->get()
                ->toArray();

            if ($arrTProductAttribute)
            {
                foreach($arrTProductAttribute as &$arrData)
                {
                    if (in_array($arrData['handle'], ['IMAGE_THUMBNAIL', 'IMAGE_GALLERY', 'IMAGE_ORIGINAL']))
                    {
                        $arrValue = explode('.', $arrData['value']);
                        $strFileExt = end($arrValue);

                        $arrMimeType = array_flip($this->arrFileExtension);

                        if (@file_exists($arrData['value']))
                        {
                            $arrData['value'] = "data:{$arrMimeType[$strFileExt]};base64," . base64_encode(file_get_contents($arrData['value']));
                        }
                    }
                }
            }

            return $this->sendJson($arrTProductAttribute);
        }
        catch (\Exception $objError)
        {
            syslog(LOG_CRIT, __FILE__ . ":" . __METHOD__ . ":" . __LINE__ . " => {$objError->getMessage()}");

            return $this->sendInternalError([]);
        }
    }

    /**
     * Used to get all product attributes by user id
     *
     * @param $objRequest Request
     *
     * @return string json format
     *
     * */
    public function getAllByUserMethod(Request $objRequest)
    {
        try
        {
            $arrTProductAttribute = CProductInformation::select(
                'c_product_information.product_attribute_type as description',
                'c_product_information.handle as handle'
            )
                ->join('t_user_product_and_service_linker', 't_user_product_and_service_linker.product_information_id', '=', 'c_product_information.product_information_id')
                ->where('t_user_product_and_service_linker.user_id', '=', $this->intUserId)
                ->get()
                ->toArray();

            return $this->sendJson($arrTProductAttribute);
        }
        catch (\Exception $objError)
        {
            syslog(LOG_CRIT, __FILE__ . ":" . __METHOD__ . ":" . __LINE__ . " => {$objError->getMessage()}");

            return $this->sendInternalError([]);
        }
    }

    /**
     * Used to create/update a product attribute by product and multiple product attribute handle
     *
     * @param $objRequest Request
     *
     * @return string json format
     *
     * */
    public function postMethod(Request $objRequest)
    {
        try
        {
            DB::beginTransaction();
            
            $objTProduct = TProduct::where('handle', $this->strProductHandle)
                ->where('user_id', AuthServiceProvider::getUserAuth()->getUserId())
                ->first();

            if (!$objTProduct)
            {
                return $this->sendNotFound(['PRODUCT_NOT_FOUND']);
            }

            $intProductID = $objTProduct->product_id;
            $arrRequest = $objRequest->all();
            $arrHandle = [];

            if (empty($arrRequest))
            {
                return $this->sendBadRequest(['RECEIVED_EMPTY_REQUEST']);
            }

            foreach($arrRequest as $intIndex => &$arrData)
            {
                $strValue = trim($arrData['value']);

                // skip empty values
                if (!$strValue)
                {
                    unset($arrRequest[$intIndex]);

                    continue;
                }

                if ($arrData['handle'])
                {
                    $arrHandle[$arrData['handle']][] = $intIndex;
                }

                $arrData['value'] = $strValue;
                $arrData['product_information_id'] = 0;
            }

            $arrCProductInformation = CProductInformation::whereIn('handle', array_keys($arrHandle))->pluck('handle', 'product_information_id');

            if ($arrCProductInformation)
            {
                foreach($arrCProductInformation as $intID => $strHandle)
                {
                    foreach($arrHandle[$strHandle] as $intRequestIndex)
                    {
                        $arrRequest[$intRequestIndex]['product_information_id'] = $intID; // set product attribute id
                    }
                }
            }

            $this->setResource($this->objTProductAttributeAttribute->selectWithID());

            $arrCompareProductAttributes = $this->getResource()
                ->where('t_product.product_id', $intProductID)
                ->where('t_product.user_id', AuthServiceProvider::getUserAuth()->getUserId())
                ->get()
                ->toArray();

            if ($arrCompareProductAttributes)
            {
                foreach($arrCompareProductAttributes as $intIndex => $arrProductAttributes)
                {
                    $strProductAttributeHandle = $arrProductAttributes['handle'];

                    $arrCompareProductAttributes[$strProductAttributeHandle][$intIndex] = $arrProductAttributes;

                    unset($arrCompareProductAttributes[$intIndex]);
                }
            }

            $this->updateAttributes($intProductID, $arrRequest, $arrCompareProductAttributes);

            DB::commit();
            
            return $this->sendResponseOk([]);
        }
        catch (\Exception $objError)
        {
            DB::rollBack();
            
            syslog(LOG_CRIT, __FILE__ . ":" . __METHOD__ . ":" . __LINE__ . " => {$objError->getMessage()}");

            return $this->sendInternalError([]);
        }
    }

    /**
     * Used to update a product attribute by product attribute handle
     *
     * @param $objRequest Request
     * @param $strProductAttributeHandle
     *
     * @return string json format
     *
     * */
    public function postUpdateMethod(Request $objRequest, $strProductAttributeHandle)
    {
        return $this->sendMethodNotAllowed([]);
    }

    /**
     * Used to delete a product attribute by product and product attribute
     *
     * @param $objRequest Request
     * @param $strProductAttributeHandle string
     *
     * @return string json format
     *
     * */
    public function deleteMethod(Request $objRequest, $strProductAttributeHandle)
    {
        try
        {
            $bolResult = $this->getResource()
                ->join('t_product', 't_product.product_id', '=', 't_product_attribute.product_id')
                ->join('c_product_information', 'c_product_information.product_information_id', '=', 't_product_attribute.product_information_id')
                ->where('t_product.handle', $this->strProductHandle)
                ->where('t_product.user_id', AuthServiceProvider::getUserAuth()->getUserId())
                ->where('c_product_information.handle', $strProductAttributeHandle)
                ->forceDelete();

            if ($bolResult)
            {
                return $this->sendResponseOk([]);
            }
            else
            {
                return $this->sendNotFound([]);
            }
        }
        catch (\Exception $objError)
        {
            syslog(LOG_CRIT, __FILE__ . ":" . __METHOD__ . ":" . __LINE__ . " => {$objError->getMessage()}");

            return $this->sendInternalError([]);
        }
    }

    /**
     * Used to delete all product attributes by product
     *
     * @param $objRequest Request
     * @param $strProductHandle string - the product handle
     *
     * @return string json format
     *
     * */
    public function deleteAllMethod(Request $objRequest, $strProductHandle)
    {
        try
        {
            DB::beginTransaction();
            
            $this->getResource()
                ->join('t_product', 't_product.product_id', '=', 't_product_attribute.product_id')
                ->join('c_product_information', 'c_product_information.product_information_id', '=', 't_product_attribute.product_information_id')
                ->where('t_product.handle', $strProductHandle)
                ->whereNotIn('c_product_information.handle', [
                    'NAME',
                    'DESCRIPTION'
                ])
                ->where('t_product.user_id', AuthServiceProvider::getUserAuth()->getUserId())
                ->forceDelete();

            DB::commit();

            $this->sendResponseOk([]);
        }
        catch (\Exception $objError)
        {
            DB::rollBack();
            
            syslog(LOG_CRIT, __FILE__ . ":" . __METHOD__ . ":" . __LINE__ . " => {$objError->getMessage()}");

            return $this->sendInternalError([]);
        }
    }

    /**
     * Not a applicable for product attribute
     *
     * @param $objRequest Request
     *
     * @return void
     *
     * */
    protected function buildResource(Request $objRequest)
    {
        // do nothing
    }

    private function updateAttributes($intProductID, $arrRequest, $arrCompareProductAttributes)
    {
        foreach($arrRequest as $arrData)
        {
            $this->setResource(TProductAttribute::query());

            $this->handleImage($intProductID, $arrData);

            if ($arrData['product_information_id'] > 0)
            {
                // update existing product attribute
                if (isset($arrCompareProductAttributes[$arrData['handle']]) && count($arrCompareProductAttributes[$arrData['handle']]) > 0)
                {
                    $arrProductAttribute = array_pop($arrCompareProductAttributes[$arrData['handle']]);

                    // skip no change in value
                    if ($arrData['value'] == $arrProductAttribute['value'])
                    {
                        continue;
                    }

                    $this->getResource()
                        ->where('product_attribute_id', $arrProductAttribute['id'])
                        ->update([
                            'value' => $arrData['value']
                        ]);
                }
                // insert new product attribute
                else
                {
                    $this->getResource()->insert([
                        'product_id' => $intProductID,
                        'product_information_id' => $arrData['product_information_id'],
                        'value' => $arrData['value']
                    ]);
                }
            }
            // insert custom attribute
            else
            {
                $objCProductInformation = CProductInformation::firstOrCreate([
                    'product_attribute_type' => $arrData['description'],
                    'handle' => str_replace(" ", "_", strtoupper($arrData['description'])),
                    'status' => 1
                ]);

                TProductAttribute::create([
                    'product_id' => $intProductID,
                    'product_information_id' => $objCProductInformation->product_information_id,
                    'value' => $arrData['value']
                ]);

                TUserProductAndServiceLinker::firstOrCreate([
                    'product_information_id' => $objCProductInformation->product_information_id,
                    'user_id' => AuthServiceProvider::getUserAuth()->getUserId()
                ]);
            }
        }
    }

    private function handleImage($intProductID, &$arrData)
    {
        if (in_array($arrData['handle'], ['IMAGE_ORIGINAL', 'IMAGE_THUMBNAIL', 'IMAGE_GALLERY']))
        {
            $arrData['value'] = $this->saveImage($intProductID, $arrData);
        }
    }

    private function saveImage($intProductID, $arrData)
    {
        static $intImageGalleryCounter = 0;

        list($strBase64, $strImage) = explode(',', $arrData['value']);
        list($strMimeType, $strBase64) = explode(';', $strBase64);

        $strMimeType = str_replace('data:', '', $strMimeType);

        // save image file
        switch ($arrData['handle'])
        {
            case 'IMAGE_THUMBNAIL':

                $intWidth = 200;

                $objImage = Image::make($strImage);
                $objImage->resize($intWidth, null, function ($objConstraint) {
                    $objConstraint->aspectRatio(); //@readme: resize the image to a width and constrain aspect ratio (auto height)
                });

                $strPath = "../resources/sysclient/images/product/thumbnail/product_{$intProductID}_" . date('YmdHis') . ".{$this->arrFileExtension[$strMimeType]}";
                $objImage->save($strPath);

                break;

            case 'IMAGE_ORIGINAL':

                $intWidth = 1000;

                $objImage = Image::make($strImage);
                $objImage->resize($intWidth, null, function ($objConstraint) {
                    $objConstraint->aspectRatio(); //@readme: resize the image to a width and constrain aspect ratio (auto height)
                });

                $strPath = "../resources/sysclient/images/product/default/product_{$intProductID}_" . date('YmdHis') . ".{$this->arrFileExtension[$strMimeType]}";
                $objImage->save($strPath);

                break;

            case 'IMAGE_GALLERY':

                $intImageGalleryCounter++;

                $intWidth = 800;

                $objImage = Image::make($strImage);
                $objImage->resize($intWidth, null, function ($objConstraint) {
                    $objConstraint->aspectRatio(); //@readme: resize the image to a width and constrain aspect ratio (auto height)
                });

                $strPath = "../resources/sysclient/images/product/gallery/product_{$intProductID}_" . $intImageGalleryCounter . ".{$this->arrFileExtension[$strMimeType]}";
                $objImage->save($strPath);

                break;

            default:

                $strPath = "";
        }

        return $strPath;
    }
}
