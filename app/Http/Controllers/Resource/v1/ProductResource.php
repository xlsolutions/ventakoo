<?php

namespace App\Http\Controllers\Resource\v1;

use App\Events\ExpireCampaignProductEvent;
use App\Http\Controllers\Resource\AResource;
use App\Models\Products\TProduct;
use App\Models\Category\TCategory;
use App\Models\Constants\CStatus;
use App\Models\Products\TProductAttribute;
use App\Http\Requests\Request;
use App\Providers\AuthServiceProvider;
use DB;

class ProductResource extends AResource
{
    protected $objTProduct;
    protected $objTCategory;
    protected $objCStatus;
    protected $objTProductAttribute;

    public $intUserId;

    public function __construct(TProduct $objTProduct, TCategory $objTCategory, CStatus $objCStatus, TProductAttribute $objTProductAttribute)
    {
        $this->objTProduct = $objTProduct;
        $this->objTCategory = $objTCategory;
        $this->objCStatus = $objCStatus;
        $this->objTProductAttribute = $objTProductAttribute;

        $this->setResource(TProduct::query());
    }

    /**
     * Used to get a product by product handle
     *
     * @param $objRequest Request
     * @param $strHandle string
     *
     * @return string json format
     *
     * */
    public function getMethod(Request $objRequest, $strHandle)
    {
        try
        {
            $this->setResource($this->objTProduct->select());

            $objTProduct = $this->getResource()
                ->whereNull('t_product.deleted_at')
                ->where('t_product.handle', $strHandle)
                ->get()->toArray();

            if (count($objTProduct) === 1)
            {
                $this->setResource($this->objTProduct->selectAttribute());

                $objTProductAttributeSet = $this->getResource()->where('t_product.handle', $strHandle)
                    ->get()->toArray();

                foreach($objTProductAttributeSet as $objTProductAttribute)
                {
                    $strType = lcfirst(str_replace(' ', '', ucwords(str_replace('_', ' ', strtolower($objTProductAttribute['handle'])))));

                    $strValue = $objTProductAttribute['value'];

                    if (in_array($objTProductAttribute['handle'], ['IMAGE_THUMBNAIL', 'IMAGE_ORIGINAL']))
                    {
                        $strValue = $this->extractProductImage($strValue);
                    }

                    $objTProduct[0][$strType] = $strValue;
                }

                unset($objTProductAttributeSet);

                //@readme: delay update of expiring campaign due to mysql event not working for shared hosting service
                if ($objTProduct[0]['campaignId'] && !is_null($objTProduct[0]['timeStart']))
                {
                    $objExpireCampaignProductEvent = new ExpireCampaignProductEvent();
                    $objExpireCampaignProductEvent->setCampaignProductId($objTProduct[0]['campaignId']);
                    event($objExpireCampaignProductEvent);
                }

                return $this->sendJson($objTProduct[0]);
            }
            else
            {
                return $this->sendNotFound([]);
            }
        }
        catch (\Exception $objError)
        {
            syslog(LOG_CRIT, __FILE__ . ":" . __METHOD__ . ":" . __LINE__ . " => {$objError->getMessage()}");

            return $this->sendInternalError([]);
        }
    }

    /**
     * Used to get a product by product handle to sell by owner
     *
     * @param $objRequest Request
     * @param $strHandle string
     *
     * @return string json format
     *
     * */
    public function getSellMethod(Request $objRequest, $strHandle)
    {
        try
        {
            $this->setResource($this->objTProduct->select());

            $objTProduct = $this->getResource()
                ->where('t_product.user_id', AuthServiceProvider::getUserAuth()->getUserId())
                ->whereNull('t_product.deleted_at')
                ->where('t_product.handle', $strHandle)
                ->get()->toArray();

            if (count($objTProduct) === 1)
            {
                $this->setResource($this->objTProduct->selectAttribute());

                $objTProductAttributeSet = $this->getResource()->where('t_product.handle', $strHandle)
                    ->get()->toArray();

                foreach($objTProductAttributeSet as $objTProductAttribute)
                {
                    $strType = lcfirst(str_replace(' ', '', ucwords(str_replace('_', ' ', strtolower($objTProductAttribute['handle'])))));

                    $strValue = $objTProductAttribute['value'];

                    if (in_array($objTProductAttribute['handle'], ['IMAGE_THUMBNAIL', 'IMAGE_ORIGINAL']))
                    {
                        $strValue = $this->extractProductImage($strValue);
                    }

                    $objTProduct[0][$strType] = $strValue;
                }

                unset($objTProductAttributeSet);

                return $this->sendJson($objTProduct[0]);
            }
            else
            {
                return $this->sendNotFound([]);
            }
        }
        catch (\Exception $objError)
        {
            syslog(LOG_CRIT, __FILE__ . ":" . __METHOD__ . ":" . __LINE__ . " => {$objError->getMessage()}");

            return $this->sendInternalError([]);
        }
    }

    /**
     * Used to count all products
     *
     * @param $objRequest Request
     *
     * @return string json format
     *
     * */
    public function getCountMethod(Request $objRequest)
    {
        try
        {
            $this->buildResource($objRequest);

            $intCount = $this->getResource()->count();

            return $this->sendJson($intCount);
        }
        catch (\Exception $objError)
        {
            syslog(LOG_CRIT, __FILE__ . ":" . __METHOD__ . ":" . __LINE__ . " => {$objError->getMessage()}");

            return $this->sendInternalError([]);
        }
    }

    /**
     * Used to get all products
     *
     * @param $objRequest Request
     *
     * @return string json format
     *
     * */
    public function getAllMethod(Request $objRequest)
    {
        try
        {
            $this->setResource($this->objTProduct->select());

            $this->buildResource($objRequest);

            $objTProductSet = $this->getResource()->get()->toArray();

            if ($objTProductSet)
            {
                $arrTProductHandle = [];

                foreach($objTProductSet as $intIndex => &$objTProduct)
                {
                    $objTProduct['campaignState'] = 'DRAFT';

                    $arrTProductHandle[$objTProduct['handle']] = $intIndex;
                }

                $strWhereProductHandle ="'" . implode("','",array_keys($arrTProductHandle)) . "'";

                $strQuery = " SELECT c_ad_post_status.handle AS campaignState, recent_note.handle FROM t_ad_post_note";
                $strQuery .= " INNER JOIN (";
                $strQuery .= " SELECT MAX(t1.ad_post_note_id) as recentNoteId, t3.handle FROM t_ad_post_note AS t1";
                $strQuery .= " INNER JOIN t_ad_post AS t2 ON t2.ad_post_id = t1.ad_post_id";
                $strQuery .= " INNER JOIN t_product AS t3 ON t3.product_id = t2.product_id";
                $strQuery .= " WHERE t3.handle IN ({$strWhereProductHandle}) AND t3.deleted_at IS NULL";
                $strQuery .= " GROUP BY t2.ad_post_id, t3.product_id";
                $strQuery .= " ) AS recent_note ON recent_note.recentNoteId = t_ad_post_note.ad_post_note_id";
                $strQuery .= " INNER JOIN c_ad_post_status ON c_ad_post_status.ad_post_status_id = t_ad_post_note.ad_post_status_id";

                $objTAdPostNoteSet = DB::select($strQuery);

                if ($objTAdPostNoteSet)
                {
                    foreach($objTAdPostNoteSet as $objTAdPostNote)
                    {
                        $intTProductSetIndex = $arrTProductHandle[$objTAdPostNote->handle];

                        $objTProductSet[$intTProductSetIndex]['campaignState'] = $objTAdPostNote->campaignState;
                    }

                    unset($objTAdPostNoteSet);
                }

                $this->setResource($this->objTProduct->selectAttribute());

                $objTProductAttributeSet = $this->getResource()->whereIn('t_product.handle', array_keys($arrTProductHandle))->get()->toArray();

                if ($objTProductAttributeSet)
                {
                    foreach($objTProductAttributeSet as $objTProductAttribute)
                    {
                        $strType = strtolower($objTProductAttribute['handle']);
                        $strValue = $objTProductAttribute['value'];

                        if (in_array($objTProductAttribute['handle'], ['IMAGE_THUMBNAIL', 'IMAGE_ORIGINAL']))
                        {
                            $strType = lcfirst(str_replace(' ', '', ucwords(str_replace('_', ' ', strtolower($objTProductAttribute['handle'])))));

                            $strValue = $this->extractProductImage($strValue);
                        }

                        $intTProductSetIndex = $arrTProductHandle[$objTProductAttribute['product_handle']];

                        $objTProductSet[$intTProductSetIndex][$strType] = $strValue;

                        if ($objTProductAttribute['handle'] === 'NAME')
                        {
                            if (!isset($objTProductSet[$intTProductSetIndex]['imageOriginal']))
                            {
                                $objTProductSet[$intTProductSetIndex]['imageOriginal'] = self::getLetterPhotoByLetter($strValue[0]);
                                $objTProductSet[$intTProductSetIndex]['imageThumbnail'] = self::getLetterPhotoByLetter($strValue[0]);
                            }
                            else if (empty($objTProductSet[$intTProductSetIndex]['imageOriginal']))
                            {
                                $objTProductSet[$intTProductSetIndex]['imageOriginal'] = self::getLetterPhotoByLetter($strValue[0]);
                                $objTProductSet[$intTProductSetIndex]['imageThumbnail'] = self::getLetterPhotoByLetter($strValue[0]);
                            }
                        }
                    }

                    unset($objTProductAttributeSet);
                }
            }

            return $this->sendJson($objTProductSet);
        }
        catch (\Exception $objError)
        {
            syslog(LOG_CRIT, __FILE__ . ":" . __METHOD__ . ":" . __LINE__ . " => {$objError->getMessage()}");

            return $this->sendInternalError([]);
        }
    }

    /**
     * Used to create a product
     *
     * @param $objRequest Request
     *
     * @return string json format
     *
     * */
    public function postMethod(Request $objRequest)
    {
        try
        {
            DB::beginTransaction();

            $intUserID = AuthServiceProvider::getUserAuth()->getUserId();

            $objTCategory = TCategory::where('handle', $objRequest->json('category'))->first();
            $objCStatus = CStatus::where('handle', $objRequest->json('visibility'))->first();

            $strDateToday = date('Y-m-d H:i:s');
            $strHandle = sha1($intUserID . $strDateToday . uniqid());

            TProduct::create([
                'handle' => $strHandle,
                'user_id' => $intUserID,
                'category_id' => $objTCategory->category_id,
                'quantity' => $objRequest->json('quantity'),
                'price' => $objRequest->json('price'),
                'status_id' => $objCStatus->status_id
            ]);

            DB::commit();

            return $this->sendJson($strHandle);
        }
        catch (\Exception $objError)
        {
            DB::rollBack();

            syslog(LOG_CRIT, __FILE__ . ":" . __METHOD__ . ":" . __LINE__ . " => {$objError->getMessage()}");

            return $this->sendBadRequest(["Couldn't update product {$strHandle}"]);
        }
    }

    /**
     * Used to update a product by product handle
     *
     * @param $objRequest Request
     * @param $strHandle string
     *
     * @return string json format
     *
     * */
    public function postUpdateMethod(Request $objRequest, $strHandle)
    {
        try
        {
            DB::beginTransaction();

            $objTCategory = TCategory::where('handle', $objRequest->json('category'))->first();
            $objCStatus = CStatus::where('handle', $objRequest->json('visibility'))->first();

            $this->getResource()
                ->where('user_id', AuthServiceProvider::getUserAuth()->getUserId())
                ->where('handle', $strHandle)
                ->update([
                    'category_id' => $objTCategory->category_id,
                    'quantity' => $objRequest->json('quantity'),
                    'price' => $objRequest->json('price'),
                    'status_id' => $objCStatus->status_id
                ]);

            DB::commit();

            return $this->sendJson($strHandle);
        }
        catch (\Exception $objError)
        {
            DB::rollBack();

            syslog(LOG_CRIT, __FILE__ . ":" . __METHOD__ . ":" . __LINE__ . " => {$objError->getMessage()}");

            return $this->sendInternalError([]);
        }
    }

    /**
     * Used to delete a product by product handle
     *
     * @param $objRequest Request
     * @param $strHandle string
     *
     * @return string json format
     *
     * */
    public function deleteMethod(Request $objRequest, $strHandle)
    {
        try
        {
            DB::beginTransaction();

            $bolResult = $this->getResource()
                ->where('user_id', AuthServiceProvider::getUserAuth()->getUserId())
                ->where('handle', $strHandle)
                ->delete();

            DB::commit();

            if ($bolResult)
            {
                return $this->sendResponseOk([]);
            }
            else
            {
                return $this->sendNotFound([]);
            }
        }
        catch (\Exception $objError)
        {
            DB::rollBack();

            syslog(LOG_CRIT, __FILE__ . ":" . __METHOD__ . ":" . __LINE__ . " => {$objError->getMessage()}");

            return $this->sendInternalError([]);
        }
    }

    /**
     * Used to filter a product (filters are optional)
     * example: /rest/1/product?page=1&limit=24&publish=RECENT&category=SUB_CARS&status=PUBLIC&priceRangeStart=10&priceRangeEnd=100
     *
     *
     * @param $objRequest Request
     *
     * @return void
     *
     * */
    protected function buildResource(Request $objRequest)
    {
        if ($objRequest->input('user_id') || $this->intUserId)
        {
            $this->setResource($this->getResource()->where('t_product.user_id', $objRequest->input('user_id', $this->intUserId)));
        }

        if ($objRequest->input('excludeProductHandle'))
        {
            $this->setResource($this->getResource()->where('t_product.handle', '!=', $objRequest->input('excludeProductHandle')));
        }

        if ($objRequest->input('campaignState') && $this->intUserId)
        {
            $strQuery = " SELECT MAX(t1.ad_post_note_id) as recentNoteId, t1.ad_post_id FROM t_ad_post_note AS t1";
            $strQuery .= " INNER JOIN t_ad_post AS t2 ON t2.ad_post_id = t1.ad_post_id";
            $strQuery .= " INNER JOIN t_product AS t3 on t3.product_id = t2.product_id";
            $strQuery .= " WHERE t3.user_id = {$this->intUserId}";
            $strQuery .= " GROUP BY t1.ad_post_id";

            $this->setResource($this->getResource()->leftJoin(DB::raw("({$strQuery}) AS recent_note"), 'recent_note.ad_post_id', '=', 't_ad_post.ad_post_id'));
            $this->setResource($this->getResource()->leftJoin('t_ad_post_note', 't_ad_post_note.ad_post_note_id', '=', 'recent_note.recentNoteId'));
            $this->setResource($this->getResource()->leftJoin('c_ad_post_status', 'c_ad_post_status.ad_post_status_id', '=', 't_ad_post_note.ad_post_status_id'));
            $this->setResource($this->getResource()->where('c_ad_post_status.handle', $objRequest->input('campaignState')));
        }

        if ($objRequest->input('status') === "PUBLIC" || $objRequest->input('status') == "HIDDEN")
        {
            $this->setResource($this->getResource()->where('c_status.handle', '=', $objRequest->input('status')));
        }

        if ($objRequest->input('priceRangeStart') && $objRequest->input('priceRangeEnd'))
        {
            $this->setResource($this->getResource()->where('t_product.price', '>=', (integer)$objRequest->input('priceRangeStart')));
            $this->setResource($this->getResource()->where('t_product.price', '<=', (integer)$objRequest->input('priceRangeEnd')));
        }

        $strPublished = $objRequest->input('publish');

        if ($strPublished)
        {
            switch($strPublished)
            {
                case 'RECENT':
                    $strDateTimeRangeStart = date('Y-m-d H:i:s', strtotime('-2 hours'));
                    $strDateTimeRangeEnd = date('Y-m-d H:i:s');
                    break;

                case 'TODAY':
                    $strDateTimeRangeStart = date('Y-m-d');
                    $strDateTimeRangeEnd = date('Y-m-d');
                    break;

                case 'YESTERDAY':
                    $strDateTimeRangeStart = date('Y-m-d H:i:s', strtotime('yesterday'));
                    $strDateTimeRangeEnd = date('Y-m-d H:i:s');
                    break;

                case 'THIS_WEEK':
                    $strDateTimeRangeStart = date('Y-m-d H:i:s', strtotime('this week'));
                    $strDateTimeRangeEnd = date('Y-m-d H:i:s');
                    break;
            }

            $this->setResource($this->getResource()->where('t_product.created_at', '>=', $strDateTimeRangeStart));
            $this->setResource($this->getResource()->where('t_product.created_at', '<=', $strDateTimeRangeEnd));
        }

        if ($objRequest->input('category'))
        {
            switch ($objRequest->input('category'))
            {
                case 'MAIN_PRODUCTS':
                case 'MAIN_SERVICES':
                    $this->setResource($this->getResource()->join('t_category AS parent_category', 'parent_category.category_id', '=', 't_category.parent_id'));
                    $this->setResource($this->getResource()->where('parent_category.handle', '=', $objRequest->input('category')));

                    break;

                default:
                    $this->setResource($this->getResource()->where('t_category.handle', '=', $objRequest->input('category')));
            }
        }

        $intPage = $objRequest->input('page')?: 1;
        $intLimit = $objRequest->input('limit')?: 12;

        $intOffset = ((int)$intPage * (int)$intLimit) - $intLimit;

        $this->setResource($this->getResource()->offset($intOffset));
        $this->setResource($this->getResource()->limit($intLimit));
    }

    protected function extractProductImage($strValue)
    {
        $arrFileExtension = [
            'image/png' => 'png',
            'image/jpeg' => 'jpg',
            'image/jpg' => 'jpg',
            'image/gif' => 'gif'
        ];

        $arrValue = explode('.', $strValue);
        $strFileExt = end($arrValue);

        $arrMimeType = array_flip($arrFileExtension);

        if (@file_exists($strValue))
        {
            return "data:{$arrMimeType[$strFileExt]};base64," . base64_encode(file_get_contents($strValue));
        }

        return null;
    }
}
