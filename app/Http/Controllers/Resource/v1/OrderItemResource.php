<?php

namespace App\Http\Controllers\Resource\v1;

use App\Http\Controllers\Resource\AResource;
use App\Http\Requests\Request;
use App\Models\Order\TOrderItem;

class OrderItemResource extends AResource
{
    protected $objTOrderItem;

    public $intUserId;
    public $intOrderId;

    public function __construct(TOrderItem $objTOrderItem)
    {
        $this->objTOrderItem = $objTOrderItem;

        $this->setResource(TOrderItem::query());
    }

    /**
     * @param $objRequest Request
     * @param $strHandle string
     *
     * @return string json format
     * */
    public function getMethod(Request $objRequest, $strHandle)
    {
        return $this->sendMethodNotAllowed([]);
    }

    /**
     * Used to get all order item
     *
     * @param $objRequest Request
     *
     * @return string json format
     * */
    public function getAllMethod(Request $objRequest)
    {
        try
        {
            $this->setResource($this->objTOrderItem->selectAll());

            $this->getResource()->where('t_order.order_id', $this->intOrderId);
            $this->getResource()->where('t_order.user_id', $this->intUserId);

            $this->buildResource($objRequest);

            $objTOrderSet = $this->getResource()->get()->toArray();

            return $this->sendJson($objTOrderSet);
        }
        catch (\Exception $objError)
        {
            syslog(LOG_CRIT, __FILE__ . ":" . __METHOD__ . ":" . __LINE__ . " => {$objError->getMessage()}");

            return $this->sendInternalError([]);
        }
    }

    /**
     * Method not allowed
     *
     * @param $objRequest Request
     *
     * @return string json format
     * */
    public function postMethod(Request $objRequest)
    {
        return $this->sendMethodNotAllowed([]);
    }

    /**
     * Method not allowed
     *
     * @param $objRequest Request
     * @param $strHandle string
     *
     * @return string json format
     * */
    public function postUpdateMethod(Request $objRequest, $strHandle)
    {
        return $this->sendMethodNotAllowed([]);
    }

    /**
     * Method not allowed
     *
     * @param $objRequest Request
     * @param $strHandle string
     *
     * @return string json format
     * */
    public function deleteMethod(Request $objRequest, $strHandle)
    {
        return $this->sendMethodNotAllowed([]);
    }

    /**
     * Used to filter a order item (filters are optional)
     *
     * @param $objRequest Request
     *
     * @return void
     * */
    protected function buildResource(Request $objRequest)
    {

    }
}
