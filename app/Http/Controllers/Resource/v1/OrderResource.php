<?php

namespace App\Http\Controllers\Resource\v1;

use App\Http\Controllers\Resource\AResource;
use App\Models\Order\TOrder;
use App\Http\Requests\Request;

class OrderResource extends AResource
{
    protected $objTOrder;

    public $intUserId;

    public function __construct(TOrder $objTOrder)
    {
        $this->objTOrder = $objTOrder;

        $this->setResource(TOrder::query());
    }

    /**
     * Used to get an order by order handle
     *
     * @param $objRequest Request
     * @param $strHandle string
     *
     * @return string json format
     *
     * */
    public function getMethod(Request $objRequest, $strHandle)
    {
        try
        {
            $this->setResource($this->objTOrder->selectOne());

            $objTOrder = $this->getResource()
                ->where('t_order.order_id', $strHandle)
                ->get()
                ->first();

            if ($objTOrder)
            {
                $objTOrder['dateOrdered'] = strtotime($objTOrder['dateOrdered']) * 1000;

                return $this->sendJson($objTOrder);
            }
            else
            {
                return $this->sendNotFound([]);
            }
        }
        catch (\Exception $objError)
        {
            syslog(LOG_CRIT, __FILE__ . ":" . __METHOD__ . ":" . __LINE__ . " => {$objError->getMessage()}");

            return $this->sendInternalError([]);
        }
    }

    /**
     * Used to count all order
     *
     * @param $objRequest Request
     *
     * @return string json format
     *
     * */
    public function getCountMethod(Request $objRequest)
    {
        try
        {
            $this->buildResource($objRequest);

            $intCount = $this->getResource()->count();

            return $this->sendJson($intCount);
        }
        catch (\Exception $objError)
        {
            syslog(LOG_CRIT, __FILE__ . ":" . __METHOD__ . ":" . __LINE__ . " => {$objError->getMessage()}");

            return $this->sendInternalError([]);
        }
    }

    /**
     * Used to get all order
     *
     * @param $objRequest Request
     *
     * @return string json format
     *
     * */
    public function getAllMethod(Request $objRequest)
    {
        try
        {
            $this->setResource($this->objTOrder->selectAll());

            $this->buildResource($objRequest);

            $objTOrderSet = $this->getResource()->get()->toArray();

            if ($objTOrderSet)
            {
                foreach ($objTOrderSet as &$objOrder)
                {
                    $objOrder['dateOrdered'] = strtotime($objOrder['dateOrdered']) * 1000;
                }
            }

            return $this->sendJson($objTOrderSet);
        }
        catch (\Exception $objError)
        {
            syslog(LOG_CRIT, __FILE__ . ":" . __METHOD__ . ":" . __LINE__ . " => {$objError->getMessage()}");

            return $this->sendInternalError([]);
        }
    }

    /**
     * Method not allowed
     *
     * @param $objRequest Request
     *
     * @return string json format
     *
     * */
    public function postMethod(Request $objRequest)
    {
        return $this->sendMethodNotAllowed([]);
    }

    /**
     * Method not allowed
     *
     * @param $objRequest Request
     * @param $strHandle string
     *
     * @return string json format
     *
     * */
    public function postUpdateMethod(Request $objRequest, $strHandle)
    {
        return $this->sendMethodNotAllowed([]);
    }

    /**
     * Method not allowed
     *
     * @param $objRequest Request
     * @param $strHandle string
     *
     * @return string json format
     *
     * */
    public function deleteMethod(Request $objRequest, $strHandle)
    {
        return $this->sendMethodNotAllowed([]);
    }

    /**
     * Used to filter a order (filters are optional)
     *
     * @param $objRequest Request
     *
     * @return void
     *
     * */
    protected function buildResource(Request $objRequest)
    {
        $intPage = $objRequest->input('page')?: 1;
        $intLimit = $objRequest->input('limit')?: 12;

        $intOffset = ((int)$intPage * (int)$intLimit) - $intLimit;

        $this->setResource($this->getResource()->where('t_order.user_id', self::getUserId()));
        $this->setResource($this->getResource()->offset($intOffset));
        $this->setResource($this->getResource()->limit($intLimit));
    }
}
