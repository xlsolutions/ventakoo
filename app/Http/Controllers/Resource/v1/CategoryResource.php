<?php

namespace App\Http\Controllers\Resource\v1;

use App\Http\Controllers\Resource\AResource;
use App\Models\Category\TCategory;
use App\Http\Requests\Request;
use DB;

class CategoryResource extends AResource
{
    private $objTCategory;

    public function __construct(TCategory $objTCategory)
    {
        $this->objTCategory = $objTCategory;

        $this->setResource(TCategory::query());
    }

    /**
     * Used to get the categories
     * - when $strHandle is NOT NULL it should return one category
     * - when $strHandle is NULL it should return all categories
     *
     * @param $objRequest Request
     * @param $strHandle string
     *
     * @return string json format
     *
     * */
    public function getMethod(Request $objRequest, $strHandle)
    {
        try
        {
            $this->setResource($this->objTCategory->select());

            $objTCategory = $this->getResource()
                ->whereNull('t_category.deleted_at')
                ->where('t_category.handle', $strHandle)
                ->get()->toArray();

            if (count($objTCategory) === 1)
            {
                return $this->sendJson($objTCategory[0]);
            }
            else
            {
                return $this->sendNotFound([]);
            }
        }
        catch (\Exception $objError)
        {
            syslog(LOG_CRIT, __FILE__ . ":" . __METHOD__ . ":" . __LINE__ . " => {$objError->getMessage()}");

            return $this->sendInternalError([]);
        }
    }

    /**
     * Used to get all categories
     *
     * @param $objRequest Request
     *
     * @return string json format
     *
     * */
    public function getAllMethod(Request $objRequest)
    {
        try
        {
            $this->setResource($this->objTCategory->select());

            $this->buildResource($objRequest);

            $objTCategorySet = $this->getResource()->get()->toArray();

            return $this->sendJson($objTCategorySet);
        }
        catch (\Exception $objError)
        {
            syslog(LOG_CRIT, __FILE__ . ":" . __METHOD__ . ":" . __LINE__ . " => {$objError->getMessage()}");

            return $this->sendInternalError([]);
        }
    }
    
    /**
     * Used to create a category
     * 
     * @param $objRequest Request
     *
     * @return string json format
     *
     * */
    public function postMethod(Request $objRequest)
    {
        try
        {
            DB::beginTransaction();
            
            if ($objRequest->json('parentHandle'))
            {
                $objParentTCategory = TCategory::where('handle', $objRequest->json('parentHandle'))->first();

                $intParentID = $objParentTCategory->category_id;
            }
            else
            {
                $intParentID = null;
            }

            $strDateToday = date('Y-m-d H:i:s');
            $strHandle = sha1($strDateToday . uniqid());

            TCategory::create([
                'handle' => $strHandle,
                'parent_id' => $intParentID,
                'category_name' => $objRequest->json('name')
            ]);
            
            DB::commit();

            return $this->sendJson($strHandle);
        }
        catch (\Exception $objError)
        {
            DB::rollBack();
            
            syslog(LOG_CRIT, __FILE__ . ":" . __METHOD__ . ":" . __LINE__ . " => {$objError->getMessage()}");

            return $this->sendInternalError([]);
        }
    }

    /**
     * Used to update a category
     *
     * @param $objRequest Request
     * @param $strHandle string
     *
     * @return string json format
     *
     * */
    public function postUpdateMethod(Request $objRequest, $strHandle)
    {
        try 
        {
            DB::beginTransaction();
            
            if ($objRequest->json('parentHandle'))
            {
                $objParentTCategory = TCategory::where('handle', $objRequest->json('parentHandle'))->first();

                $intParentID = $objParentTCategory->category_id;
            }
            else
            {
                $intParentID = null;
            }
            
            $this->getResource()
                ->where('handle', $strHandle)
                ->update([
                    'parent_id' => $intParentID,
                    'name' => $objRequest->json('name')
                ]);

            DB::commit();
            
            return $this->sendResponseOk([]);
        } 
        catch (\Exception $objError) 
        {
            DB::rollBack();
            
            syslog(LOG_CRIT, __FILE__ . ":" . __METHOD__ . ":" . __LINE__ . " => {$objError->getMessage()}");

            return $this->sendInternalError([]);
        }
    }

    /**
     * Used to delete a category
     *
     * @param $objRequest Request
     * @param $strHandle string
     *
     * @return string json format
     *
     * */
    public function deleteMethod(Request $objRequest, $strHandle)
    {
        try
        {
            DB::beginTransaction();
            
            $this->getResource()
                ->where('handle', $strHandle)
                ->delete();

            DB::commit();

            return $this->sendResponseOk([]);
        }
        catch (\Exception $objError)
        {
            DB::rollback();
            
            syslog(LOG_CRIT, __FILE__ . ":" . __METHOD__ . ":" . __LINE__ . " => {$objError->getMessage()}");

            return $this->sendInternalError([]);
        }
    }

    /**
     * Used to filter a category (filters are optional)
     * example: /rest/1/category?parentHandle=MAIN_PRODUCTS
     *
     *
     * @param $objRequest Request
     *
     * @return void
     *
     * */
    protected function buildResource(Request $objRequest)
    {
        $this->setResource($this->getResource()->whereNull('t_category.deleted_at'));

        if ($objRequest->input('parentHandle'))
        {
            $this->setResource($this->getResource()->where('parent_category.handle', '=', $objRequest->input('parentHandle')));
        }
    }
}
