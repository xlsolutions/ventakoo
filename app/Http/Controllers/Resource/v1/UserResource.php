<?php

namespace App\Http\Controllers\Resource\v1;

use App\Http\Controllers\Resource\AResource;
use App\Http\Requests\Request;
use App\Models\Users\TUser;
use App\Providers\AuthServiceProvider;

class UserResource extends AResource
{
    private $objTUser;

    public function __construct(TUser $objTUser)
    {
        $this->objTUser = $objTUser;

        $this->setResource(TUser::query());
    }

    /**
     * Used to get user info by user id
     *
     * @param $objRequest Request
     * @param $intUserID integer
     *
     * @return string json format
     *
     * */
    public function getMethod(Request $objRequest, $intUserID)
    {
        try
        {
            if (AuthServiceProvider::bolLogin())
            {
                $this->setResource($this->objTUser->select());
            }
            else
            {
                $this->setResource($this->objTUser->selectWithoutContact());
            }

            $objTUser = $this->getResource()
                ->where('t_user.user_id', $intUserID)
                ->first();

            if (!$objTUser)
            {
                return $this->sendNotFound([]);
            }

            $strPhoto = AuthServiceProvider::getProfilePhotoByUserId($intUserID);

            return $this->sendJson([
                'firstName' => $objTUser['firstname'],
                'lastName' => $objTUser['lastname'],
                'lastLogin' => $objTUser['last_login'],
                'dateJoined' => $objTUser->created_at,
                'country' => $objTUser['country_name'],
                'state' => $objTUser['state_name'],
                'city' => $objTUser['city_name'],
                'mobile' => $objTUser['mobile'],
                'email' => $objTUser['email_address'],
                'photo' => $strPhoto ? : self::getLetterPhotoByLetter($objTUser['firstname'][0])
            ]);
        }
        catch (\Exception $objError)
        {
            syslog(LOG_CRIT, __FILE__ . ":" . __METHOD__ . ":" . __LINE__ . " => {$objError->getMessage()}");

            return $this->sendInternalError([]);
        }
    }

    /**
     * Used to get all users
     *
     * @param $request Request
     *
     * @return string json format
     *
     * */
    public function getAllMethod(Request $request)
    {
        return $this->sendMethodNotAllowed([]);
    }

    /**
     * Used to create a user
     *
     * @param $objRequest Request
     *
     * @return string json format
     *
     * */
    public function postMethod(Request $objRequest)
    {
        return $this->sendMethodNotAllowed([]);
    }

    /**
     * Used to update a user by user id
     *
     * @param $objRequest Request
     * @param $intUserId integer - user id
     *
     * @return string json format
     *
     * */
    public function postUpdateMethod(Request $objRequest, $intUserId)
    {
        return $this->sendMethodNotAllowed([]);
    }

    /**
     * Used to delete a user by user id
     *
     * @param $objRequest Request
     * @param $intUserId integer
     *
     * @return string json format
     *
     * */
    public function deleteMethod(Request $objRequest, $intUserId)
    {
        return $this->sendMethodNotAllowed([]);
    }

    /**
     * Used to filter a user (filters are optional)
     *
     * @param $objRequest Request
     *
     * @return void
     *
     * */
    protected function buildResource(Request $objRequest)
    {
        //do nothing
    }
}
