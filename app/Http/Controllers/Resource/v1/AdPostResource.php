<?php

namespace App\Http\Controllers\Resource\v1;

use App\Classes\Order\CampaignOrder;
use App\Classes\Order\RefundCampaignOrder;
use App\Events\AdPostPublishEvent;
use App\Events\AdPostReviewEvent;
use App\Events\AdPostReviewStatus;
use App\Http\Controllers\Resource\AResource;
use App\Http\Requests\Request;
use App\Models\AdPost\TAdPost;
use App\Models\AdPost\TAdPostExpiration;
use App\Models\Products\TProduct;
use App\Providers\AuthServiceProvider;
use Illuminate\Support\Facades\DB;

class AdPostResource extends AResource
{
    private $objTAdPost;

    public $intProductId;

    public function __construct(TAdPost $objTAdPost)
    {
        $this->objTAdPost = $objTAdPost;

        $this->setResource(TAdPost::query());
    }

    /**
     * Used to get ad post info by ad_post_id
     *
     * @param $objRequest Request
     * @param $intAdPostId integer
     *
     * @return string json format
     *
     * */
    public function getMethod(Request $objRequest, $intAdPostId)
    {
        return $this->sendMethodNotAllowed([]);
    }

    /**
     * Used to get all ad post
     *
     * @param $request Request
     *
     * @return string json format
     *
     * */
    public function getAllMethod(Request $request)
    {
        return $this->sendMethodNotAllowed([]);
    }

    /**
     * Used to create a user
     *
     * @param $objRequest Request
     *
     * @return string json format
     *
     * */
    public function postMethod(Request $objRequest)
    {
        try
        {
            DB::beginTransaction();

            $objTProduct = TProduct::where('handle', $objRequest->json('productId'))->first();

            $intAdPostId = TAdPost::insertGetId([
                'product_id' => $objTProduct['product_id'],
                'auto_publish' => $objRequest->json('auto_publish'),
                'cancel_ad_post' => 0
            ]);

            $this->adPostPublishEvent($objRequest, $intAdPostId);

            DB::commit();

            return $this->sendJson($intAdPostId);
        }
        catch (\Exception $objError)
        {
            DB::rollback();

            syslog(LOG_CRIT, __FILE__ . ":" . __METHOD__ . ":" . __LINE__ . " => {$objError->getMessage()}");

            return $this->sendInternalError([]);
        }
    }

    /**
     * Used to update an ad post by ad_post_id and product_id
     *
     * @param $objRequest Request
     * @param $intAdPostId integer - user id
     *
     * @return string json format
     *
     * */
    public function postUpdateMethod(Request $objRequest, $intAdPostId)
    {
        try
        {
            DB::beginTransaction();

            $objTProduct = TProduct::where('handle', $objRequest->json('productId'))->first();

            $objTAdPostExpiration = TAdPostExpiration::where('ad_post_id', $intAdPostId)->first();

            $arrUpdate = [
                'cancel_ad_post' => $objRequest->json('post_status') === AdPostReviewStatus::CANCELLED ? 1 : 0
            ];

            if (!$objTAdPostExpiration)
            {
                $arrUpdate['auto_publish'] = $objRequest->json('auto_publish');
            }
            else if ($objRequest->json('post_status') === AdPostReviewStatus::CANCELLED)
            {
                $objTAdPostExpiration->delete();
            }
            
            $this->adPostPublishEvent($objRequest, $intAdPostId);

            $this->getResource()
                ->where('ad_post_id', $intAdPostId)
                ->where('product_id', $objTProduct['product_id'])
                ->update($arrUpdate);

            DB::commit();

            return $this->sendResponseOk([]);
        }
        catch (\Exception $objError)
        {
            DB::rollback();

            syslog(LOG_CRIT, __FILE__ . ":" . __METHOD__ . ":" . __LINE__ . " => {$objError->getMessage()}");

            return $this->sendInternalError([]);
        }
    }

    /**
     * Used to delete an ad post by ad_post_id and product_id
     *
     * @param $objRequest Request
     * @param $intAdPostId integer
     *
     * @return string json format
     *
     * */
    public function deleteMethod(Request $objRequest, $intAdPostId)
    {
        return $this->sendMethodNotAllowed([]);
    }

    /**
     * Used to filter an ad post (filters are optional)
     *
     * @param $objRequest Request
     *
     * @return void
     *
     * */
    protected function buildResource(Request $objRequest)
    {
        //do nothing
    }

    /**
     * Used to update a campaign event
     *
     * @param $objRequest Request
     * @param $intAdPostId integer
     *
     * @return void
     *
     * */
    private function adPostPublishEvent(Request $objRequest, $intAdPostId)
    {
        switch($objRequest->json('post_status'))
        {
            case AdPostReviewStatus::CANCELLED:
                $objAdPostReviewEvent = new AdPostReviewEvent($intAdPostId);
                $objAdPostReviewEvent->setIntUserId(AuthServiceProvider::getUserAuth()->getUserId());
                $objAdPostReviewEvent->setReviewStatus(AdPostReviewStatus::CANCELLED);
                event($objAdPostReviewEvent);

                if ($objRequest->getCampaignPreStateHandle() === AdPostReviewStatus::APPROVED)
                {
                    $objRefundCampaignOrder = new RefundCampaignOrder(AuthServiceProvider::getUserAuth()->getUserId(), "REFUND");
                    $objRefundCampaignOrder->addItem($intAdPostId);
                    $objRefundCampaignOrder->setTransactionNote("Cancel and refund a product campaign with id: {$objRequest->json('productId')}");
                    $objRefundCampaignOrder->create();

                    if ($objRefundCampaignOrder->getRefundAmount() > 0)
                    {
                        $objAccountBalanceEvent = $objRequest->getAccountBalanceEvent();
                        $objAccountBalanceEvent->setBalance($objRefundCampaignOrder->getRefundAmount());

                        event($objAccountBalanceEvent);
                    }
                }

                break;

            case AdPostReviewStatus::FOR_REVIEW:
                $objAdPostReviewEvent = new AdPostReviewEvent($intAdPostId);
                $objAdPostReviewEvent->setIntUserId(AuthServiceProvider::getUserAuth()->getUserId());
                $objAdPostReviewEvent->setReviewStatus(AdPostReviewStatus::FOR_REVIEW);
                event($objAdPostReviewEvent);

                /* @todo: remove once there is an admin panel for reviewing and approving */
                $objAdPostReviewEvent = new AdPostReviewEvent($intAdPostId);
                $objAdPostReviewEvent->setReviewStatus(AdPostReviewStatus::REVIEWING);
                event($objAdPostReviewEvent);

                $objAdPostReviewEvent = new AdPostReviewEvent($intAdPostId);
                $objAdPostReviewEvent->setReviewStatus(AdPostReviewStatus::APPROVED);
                event($objAdPostReviewEvent);

                $objAccountBalanceEvent = $objRequest->getAccountBalanceEvent();

                if ($objAccountBalanceEvent && $objAccountBalanceEvent->getBalance() > 0)
                {
                    event($objAccountBalanceEvent);
                }

                $objCampaignOrder = new CampaignOrder(AuthServiceProvider::getUserAuth()->getUserId(), "COMPLETED", $objRequest->getCampaignPostPerPrice());
                $objCampaignOrder->addItem($intAdPostId);

                $strCampaignUrl = getenv('APP_URL') . "/#/product/{$objRequest->json('productId')}";

                $objCampaignOrder->setTransactionNote("Place a product ($strCampaignUrl) campaign for {$objRequest->getCampaignPostPerPrice()}");
                $objCampaignOrder->create();

                if ($objRequest->json('auto_publish'))
                {
                    $objAdPostReviewEvent = new AdPostReviewEvent($intAdPostId);
                    $objAdPostReviewEvent->setReviewStatus(AdPostReviewStatus::START);
                    event($objAdPostReviewEvent);

                    $objAdPostPublishEvent = new AdPostPublishEvent($intAdPostId);
                    $objAdPostPublishEvent->setStart(true);

                    if ($objRequest->getFreeCampaignPost())
                    {
                        $objAdPostPublishEvent->setExpiration(AdPostPublishEvent::DEFAULT_EXPIRY_BY_SECONDS);
                    }

                    event($objAdPostPublishEvent);
                }
                /* @todo: remove once there is an admin panel for reviewing and approving */

                break;

            case AdPostReviewStatus::START:
            case AdPostReviewStatus::PAUSE:
                $objAdPostReviewEvent = new AdPostReviewEvent($intAdPostId);
                $objAdPostReviewEvent->setIntUserId(AuthServiceProvider::getUserAuth()->getUserId());
                $objAdPostReviewEvent->setReviewStatus($objRequest->json('post_status'));
                event($objAdPostReviewEvent);

                $objAdPostPublishEvent = new AdPostPublishEvent($intAdPostId);

                if ($objRequest->json('post_status') === AdPostReviewStatus::START)
                {
                    $objAdPostPublishEvent->setStart(true);

                    if ($objRequest->getFreeCampaignPost() && $objRequest->getCampaignPreStateHandle() === AdPostReviewStatus::APPROVED)
                    {
                        $objAdPostPublishEvent->setExpiration(AdPostPublishEvent::DEFAULT_EXPIRY_BY_SECONDS);
                    }
                }
                else if ($objRequest->json('post_status') === AdPostReviewStatus::PAUSE)
                {
                    $objAdPostPublishEvent->setPause(true);
                }

                event($objAdPostPublishEvent);

                break;
        }
    }
}
