<?php

namespace App\Http\Controllers\Resource\v1;

use App\Http\Controllers\Resource\AResource;
use App\Http\Requests\Request;

class ProductImageResource extends AResource
{
    public function __construct()
    {

    }

    public function getMethod(Request $objRequest, $strHandle)
    {
        return $this->sendMethodNotAllowed([]);
    }
    
    public function getAllMethod(Request $request)
    {
        return $this->sendMethodNotAllowed([]);
    }

    /**
     * Used to upload a temporary image
     *
     * @param $objRequest Request
     *
     * @return string json format
     *
     * */
    public function postMethod(Request $objRequest)
    {
        try
        {
            // Callback name is passed if upload happens via iframe, not AJAX (FileAPI).
            $unkCallback = &$_REQUEST['fd-callback'];

            // Upload data can be POST'ed as raw form data or uploaded via <iframe> and <form>
            // using regular multipart/form-data enctype (which is handled by PHP $_FILES).
            if (!empty($_FILES['fd-file']) and is_uploaded_file($_FILES['fd-file']['tmp_name'])) 
            {
                $strData = file_get_contents($_FILES['fd-file']['tmp_name']);

                $objFinfo = finfo_open(FILEINFO_MIME_TYPE); // return mime type ala mimetype extension
                $strMimeType = finfo_file($objFinfo, $_FILES['fd-file']['tmp_name']);
                finfo_close($objFinfo);
            }
            else 
            {
                // Raw POST data.
                $strData = file_get_contents("php://input");
                $strMimeType = $_SERVER['HTTP_X_FILE_TYPE'];
            }

            $arrValidMimeTypes = [
                'image/png',
                'image/gif',
                'image/jpeg'
            ];

            if ($unkCallback)
            {
                // Callback function given - the caller loads response into a hidden <iframe> so
                // it expects it to be a valid HTML calling this callback function.
                header('Content-Type: text/html; charset=utf-8');
                
                if (!in_array($strMimeType, $arrValidMimeTypes))
                {
                    $strOutput = [
                        'status' => 400,
                        'data' => ['Accepted types are only png, gif, jpeg'],
                        'statusText' => 'Bad Request'
                    ];
                }
                else
                {
                    $strOutput = [
                        'status' => 200,
                        'data' => [
                            'base64' => base64_encode($strData),
                            'mimeType' => $strMimeType
                        ]
                    ];
                }

                // Escape output so it remains valid when inserted into a JS 'string'.
                $strOutput = json_encode($strOutput);

                // Finally output the HTML with an embedded JavaScript to call the function giving
                // it our message(in your app it doesn't have to be a string) as the first parameter.
                echo '<!DOCTYPE html><html><head></head><body><script type="text/javascript">',
                "try{window.top.$unkCallback({$strOutput})}catch(e){}</script></body></html>";

                exit;
            }
            else
            {
                if (!in_array($strMimeType, $arrValidMimeTypes))
                {
                    return $this->sendBadRequest(['Accepted types are only png, gif, jpeg']);
                }
                
                return $this->sendJson([
                    'base64' => base64_encode($strData),
                    'mimeType' => $strMimeType
                ]);
            }
        }
        catch (\Exception $objError)
        {
            syslog(LOG_CRIT, __FILE__ . ":" . __METHOD__ . ":" . __LINE__ . " => {$objError->getMessage()}");

            return $this->sendInternalError([]);
        }
    }
    
    public function postUpdateMethod(Request $request, $strHandle)
    {
        return $this->sendMethodNotAllowed([]);
    }

    public function deleteMethod(Request $objRequest, $strHandle)
    {
        return $this->sendMethodNotAllowed([]);
    }

    protected function buildResource(Request $objRequest)
    {
        return $this->sendMethodNotAllowed([]);
    }
}
