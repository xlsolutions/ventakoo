<?php

namespace App\Http\Controllers;

use App\Providers\AuthServiceProvider;
use Illuminate\Routing\Controller as BaseController;
use Illuminate\Http\Response;

class Controller extends BaseController
{
    protected function sendResponse($unkData, $intHttpStatusCode = Response::HTTP_OK)
    {
        return response(['data' => $unkData], $intHttpStatusCode);
    }
    protected function sendBadRequest(array $arrData)
    {
        return $this->sendResponse($arrData, Response::HTTP_BAD_REQUEST);
    }

    protected function sendNotFound(array $arrData)
    {
        return $this->sendResponse($arrData, Response::HTTP_NOT_FOUND);
    }

    protected function sendUnauthorized(array $arrData)
    {
        return $this->sendResponse($arrData, Response::HTTP_UNAUTHORIZED);
    }

    protected function sendMethodNotAllowed(array $arrData)
    {
        return $this->sendResponse($arrData, Response::HTTP_METHOD_NOT_ALLOWED);
    }

    protected function sendInternalError(array $arrData)
    {
        return $this->sendResponse($arrData, Response::HTTP_INTERNAL_SERVER_ERROR);
    }

    /**
     * Used to get the letter photo by letter
     *
     * @param $strLetter string
     *
     * @throws \Exception
     * @return string
     *
     * */
    static protected function getLetterPhotoByLetter($strLetter)
    {
        $strLetter = strtoupper($strLetter);

        if(@file_exists("../resources/assets/images/material-letter-icon/{$strLetter}.png"))
        {
            $strPhoto = "data:image/jpeg;base64," . base64_encode(file_get_contents("../resources/assets/images/material-letter-icon/{$strLetter}.png"));
        }
        else
        {
            $strPhoto = null;
        }

        return $strPhoto;
    }
    
    static protected function getUserId()
    {
        return AuthServiceProvider::getUserAuth()->getUserId();
    }
}
