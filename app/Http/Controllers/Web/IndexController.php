<?php

namespace App\Http\Controllers\Web;

use Illuminate\Support\Facades\View;
use App\Http\Requests;
use App\Http\Controllers\Controller;

class IndexController extends Controller
{

    public function __construct()
    {
        
    }

    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function index()
    {        
        return View::make('ecoreal.web.pages.index');
    }
}
