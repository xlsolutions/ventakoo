<?php

namespace App\Http\Controllers\Web;

use App\Classes\Order\CampaignPlanOrder;
use App\Events\CancelRecentOrderEvent;
use App\Http\Controllers\Controller;
use App\Models\Constants\CProductPlan;
use App\Models\Users\UserRole;
use App\Providers\AuthServiceProvider;
use App\Providers\ViewDataServiceProvider;
use Illuminate\Support\Facades\View;
use Illuminate\Http\Request;
use DB;

class CampaignPricingPageController extends Controller
{
    /**
     * @param $objRequest Request
     * 
     * @return view
     * */
    public function indexAction(Request $objRequest)
    {
        try
        {
            $arrCProductPlan = CProductPlan::select(
                'handle',
                'name',
                'price'
            )
                ->where('is_available', 1)
                ->get();
            
            if ($arrCProductPlan)
            {
                foreach($arrCProductPlan as $objCProductPlan)
                {
                    ViewDataServiceProvider::add($objCProductPlan->handle, [
                        'name' => $objCProductPlan->name,
                        'price' => $objCProductPlan->price
                    ]);
                }
            }
            else
            {
                throw new \Exception("There are no active pricing plan.");
            }
            
            if (AuthServiceProvider::bolLogin())
            {
                ViewDataServiceProvider::add('isBuyer', UserRole::getRoleByUserId(self::getUserId()) === 'BUYER');
            }
            
            if ($objRequest->input('cancel_order'))
            {
                $objCancelRecentOrderEvent = new CancelRecentOrderEvent();
                $objCancelRecentOrderEvent->setIntUserId(self::getUserId());
                event($objCancelRecentOrderEvent);
            }

            return View::make('ecoreal.web.pages.pricing');
        }
        catch (\Exception $objError)
        {
            syslog(LOG_CRIT, __FILE__ . ":" . __METHOD__ . ":" . __LINE__ . " => {$objError->getMessage()}");

            return redirect('/#/500');
        }
    }

    /**
     * Buy a campaign plan
     *
     * @param $objRequest Request
     *
     * @return view
     * */
    public function postMethod(Request $objRequest)
    {
        try
        {
            DB::beginTransaction();

            $objCProductPlan = CProductPlan::where('handle', $objRequest->input('campaignPlan'))
                ->where('is_available', 1)
                ->first();

            $objCampaignPlanOrder = new CampaignPlanOrder(self::getUserId(), 'NEW', $objCProductPlan['price']);
            $objCampaignPlanOrder->addItem($objCProductPlan['product_plan_id']);
            $objCampaignPlanOrder->setTransactionNote("Place a campaign ({$objCProductPlan['name']}) plan order for {$objCProductPlan['price']}");
            $objCampaignPlanOrder->create();

            $objUser = AuthServiceProvider::getUserAuth();

            $objCurl = curl_init(env('PAYPAL_URL'));

            $arrPostField = array(
                'cmd' => '_xclick',
                'business' => env('PAYPAL_MERCHANT_ID'),
                'currency_code' => env('PAYPAL_MERCHANT_CURRENCY', 'PHP'),
                'charset' => 'utf-8',
                'return' => getenv('APP_URL') . '/#/payment/success',
                'cancel_return' => getenv('APP_URL') . '/campaign/pricing?cancel_order=1',
                'item_name' => $objCProductPlan['name'],
                'item_number' => $objCampaignPlanOrder->getOrderId(),
                'amount' => $objCProductPlan['price'],
                'no_note' => '1',
                'notify_url' => env('APP_URL') . '/payment/paypal/ipn',
                'paymentaction' => 'sale', //authorization
                'custom' => $objCampaignPlanOrder->getOrderId(),

                'first_name' => $objUser->getFirstName(),
                'last_name' => $objUser->getLastName(),
                'address1' => $objUser->getAddress(),
                'city' => $objUser->getCity(),
                'state' => $objUser->getState(),
                'zip' => $objUser->getZip(),
                'country' => $objUser->getCountryCode()
            );

            curl_setopt_array($objCurl, array(
                CURLOPT_SSL_VERIFYPEER => false,
                CURLOPT_SSL_VERIFYHOST => false,
                CURLOPT_RETURNTRANSFER => true,
                CURLOPT_HEADER => true,
                CURLOPT_POSTFIELDS => http_build_query($arrPostField)
            ));

            curl_exec($objCurl);
            
            $arrCurlInfo = curl_getinfo($objCurl);

            curl_close($objCurl);

            if (isset($arrCurlInfo['redirect_url']))
            {
                DB::commit();

                header("Location: {$arrCurlInfo['redirect_url']}");
                
                exit;
            }
            else
            {
                throw new \Exception('Redirect url does not exit');
            }
        }
        catch (\Exception $objError)
        {
            DB::rollBack();

            syslog(LOG_CRIT, __FILE__ . ":" . __METHOD__ . ":" . __LINE__ . " => {$objError->getMessage()}");

            return redirect('/#/500');
        }
    }
}