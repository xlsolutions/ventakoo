<?php

namespace App\Http\Controllers\Web;

use App\Models\Users\UserRole;
use App\Providers\AuthServiceProvider;
use App\Events\VerifyRegistrationEmailEvent;
use App\Http\Controllers\Controller;
use App\Models\Constants\CActivationType;
use App\Models\Users\TUserActivation;
use App\Providers\FlashMessageServiceProvider;
use Illuminate\Http\Request;
use Illuminate\Support\Facades\Hash;
use Illuminate\Support\Facades\Session;
use Illuminate\Support\Facades\View;
use App\Models\Users\TUser;
use App\Models\Users\TUserContact;
use App\Models\Users\TUserContactAttribute;
use App\Models\Users\TUserInformation;
use App\Models\Users\TUserLocation;
use App\Models\Constants\CUserContactInformation;
use App\Models\Constants\CCountry;
use App\Models\Constants\CState;
use App\Models\Constants\CCity;
use DB;
use Intervention\Image\ImageManagerStatic as Image;

class UserUpdateInformationPageController extends Controller
{
    private $strAccountType;

    private $intUserID;

    public function __construct()
    {
        try
        {
            $this->intUserID = AuthServiceProvider::getUserAuth()->getUserId();

            $this->strAccountType = UserRole::getRoleByUserId($this->intUserID);

            if(!@file_exists('../resources/sysclient/images/user'))
            {
				mkdir("../resources/sysclient/images/user",0755);
			}
        }
        catch (\Exception $objError)
        {
            syslog(LOG_CRIT, __CLASS__ . ' > ' . __METHOD__ . ' : ' . $objError->getMessage());

            return redirect('/#/500');
        }
    }
    /**
     * Used to get the Country, State and City for the UserUpdateInformation Form
     *
     * @param $objRequest Request
     * 
     * @return array to User Update Information Page View
     *
     * */
    public function getMethod(Request $objRequest)
    {
        try
        {
            $arrData = array();

            $objTUser = new TUser();

            $objUser = $objTUser->select()->where('t_user.user_id',$this->intUserID)->first();

            $arrCCountry = CCountry::all()->sortBy('country_name')->toArray();

            $arrData['countries'] = $arrCCountry;
            $arrData['user_information'] = $objUser;
            $arrData['user_type'] = $this->strAccountType;
            $arrData['firstname'] = $objRequest->old('firstname') ?: $objUser['firstname'];
            $arrData['lastname'] = $objRequest->old('lastname') ?: $objUser['lastname'];
            $arrData['email_address'] = $objRequest->old('email_address') ?: $objUser['email_address'];
            $arrData['mobile_number'] = $objRequest->old('mobile_number') ?: $objUser['mobile'];
            $arrData['phone_number'] = $objRequest->old('phone_number') ?: $objUser['phone_number'];
            $arrData['country'] = $objRequest->old('country') ?: $objUser['country_code'];
            $arrData['state'] = $objRequest->old('state') ?: $objUser['state_id'];
            $arrData['city'] = $objRequest->old('city') ?: $objUser['city_id'];
            $arrData['street'] = $objRequest->old('street') ?: $objUser['address'];

            if ($objRequest->input('campaignPlan'))
            {
                Session::put('UserUpdateInformationPageController_getMethod_isFromPricingPage', true);
                Session::save();
            }
            
            $strPhoto = AuthServiceProvider::getUserAuth()->getProfilePhoto();

            if (!$strPhoto)
            {
                //@readme: buyer does not have firstname by default so let's use the email address if not set
                $strLetter = isset($objUser['firstname'][0]) ? $objUser['firstname'][0] : $objUser['email_address'][0];

                $strPhoto = self::getLetterPhotoByLetter($strLetter);
            }

            $arrData['strImageSource'] = $strPhoto;

            return View::make('auth/pages/user_update_information', $arrData);
        }
        catch (\Exception $objError)
        {
            syslog(LOG_CRIT, __FILE__ . ":" . __METHOD__ . ":" . __LINE__ . " => {$objError->getMessage()}");

            return redirect('/#/500');
        }
    }

    /**
     * Used to update user account information
     *
     * @param $objRequest Request
     *
     * @return view Verify email page
     * */
    public function postMethod(Request $objRequest)
    {
        try
        {
            $strHash = env('AUTH_HASH_PASSWORD');

            $objTUserInformation = TUserInformation::where('user_id',$this->intUserID)->first();

            if ($objRequest->input('old_password'))
            {
                if (!Hash::check($strHash . $objRequest->input('old_password'), $objTUserInformation['password']))
                {
                    FlashMessageServiceProvider::danger('Incorrect current password. Please try again.');

                    return redirect('/account');
                }
            }

            DB::beginTransaction();

            if($objRequest->file("user_image"))
            {
                $file = $objRequest->file("user_image");

                $strImageName = $this->intUserID. '.jpg';

                $strPath = '../resources/sysclient/images/user/';

                $file->move($strPath,$strImageName);

                $objImage = new Image();
                $objImage->make($strPath.$strImageName)->resize(170,170)->save();
            }

            $strDate = date("Y-m-d H:i:s");

            $objCCountry = CCountry::where('country_code', $objRequest->input('country'))->first();

            $objCState = CState::where([
                ['country_id', '=', $objCCountry['country_id']],
                ['state_id', '=', $objRequest->input('state')]
            ])
                ->first();

            $objCCity = CCity::where([
                ['state_id', '=', $objCState['state_id']],
                ['city_id', '=', $objRequest->input('city')]
            ])
                ->first();

            $strPassword = $objTUserInformation['password'];

            if(trim($objRequest->input('new_password')))
            {
                $strPassword =  Hash::make($strHash . $objRequest->input('new_password'));
            }

            $objUserInfo = TUserInformation::find($objTUserInformation['user_information_id']);
            $objUserInfo->firstname = $objRequest->input('firstname');
            $objUserInfo->lastname = $objRequest->input('lastname');
            $objUserInfo->password = $strPassword;
            $objUserInfo->validated = (int)($objRequest->input('type') !== 'SELLER');
            $objUserInfo->save();

            $objTUserContact = TUserContact::where('user_information_id',$objTUserInformation['user_information_id'])->first();

            TUserContact::where('user_information_id',$objTUserInformation['user_information_id'])
                ->update([
                    'updated_at' => $strDate,
                ]);

            $objTUserLocation = TUserLocation::where('user_location_id', $objTUserContact['user_location_id'])->first();

            TUserLocation::where('user_location_id', $objTUserLocation['user_location_id'])
                ->update([
                    'updated_at' => $strDate,
                    'country_id' => $objCCountry['country_id'],
                    'state_id' => $objCState['state_id'],
                    'city_id' => $objCCity['city_id']
                ]);

            $strRegistrationAttribute = ["MOBILE_NUMBER", "PHONE_NUMBER", "ADDRESS"];

            $arrCUserContactInformation = CUserContactInformation::whereIn('handle', $strRegistrationAttribute)->get()->toArray();

            foreach ($arrCUserContactInformation as $userContactInformation)
            {
                if ($userContactInformation['handle'] == 'MOBILE_NUMBER')
                {
                    TUserContactAttribute::where([
                        ['user_contact_id','=', $objTUserContact['user_contact_id']],
                        ['user_contact_information_id','=', $userContactInformation['user_contact_information_id']]
                    ])
                        ->update([
                            'updated_at' => $strDate,
                            'value' => $objRequest->input('mobile_number'),
                        ]);
                }

                if ($userContactInformation['handle'] == 'PHONE_NUMBER')
                {
                    TUserContactAttribute::where([
                        ['user_contact_id','=', $objTUserContact['user_contact_id']],
                        ['user_contact_information_id','=', $userContactInformation['user_contact_information_id']]
                    ])
                        ->update([
                            'updated_at' => $strDate,
                            'value' => $objRequest->input('phone_number'),
                        ]);
                }

                if ($userContactInformation['handle'] == 'ADDRESS')
                {
                    TUserContactAttribute::where([
                        ['user_contact_id','=', $objTUserContact['user_contact_id']],
                        ['user_contact_information_id','=', $userContactInformation['user_contact_information_id']]
                    ])
                        ->update([
                            'updated_at' => $strDate,
                            'value' => $objRequest->input('street'),
                        ]);
                }
            }

            $objCActivationType = CActivationType::where('handle', 'VERIFY_ACCOUNT_EMAIL')->first();

            if ($objRequest->input('convert_type') && $this->strAccountType === 'BUYER')
            {
                $strCode = $this->generateVerificationCode();

                TUserActivation::updateOrCreate(
                    [
                        'user_contact_id' => $objTUserContact['user_contact_id'],
                        'activation_type_id' => $objCActivationType['activation_type_id']
                    ],
                    [
                        'activation_code' => $strCode,
                        'activation_type_id' => $objCActivationType['activation_type_id'],
                        'user_contact_id' => $objTUserContact['user_contact_id']
                    ]
                );

                $bolPricingPlan = Session::get('UserUpdateInformationPageController_getMethod_isFromPricingPage');

                if ($bolPricingPlan)
                {
                    Session::forget('UserUpdateInformationPageController_getMethod_isFromPricingPage');
                    Session::save();
                }

                $objVerifyRegistrationEmailEvent = new VerifyRegistrationEmailEvent();
                $objVerifyRegistrationEmailEvent->setEmail($objTUserContact['email_address']);
                $objVerifyRegistrationEmailEvent->setPricingPageSession($bolPricingPlan);
                $objVerifyRegistrationEmailEvent->setCode($strCode);

                event($objVerifyRegistrationEmailEvent);

                $objView = view('auth/pages/verify_email', [
                    'email' => $objTUserContact['email_address']
                ]);
            }
            else
            {
                $objView = redirect("/account");
            }

            DB::commit();

            FlashMessageServiceProvider::success('Account successfully updated');
            
            return $objView;
        }
        catch (\Exception $objError)
        {
            DB::rollBack();

            syslog(LOG_CRIT, __FILE__ . ":" . __METHOD__ . ":" . __LINE__ . " => {$objError->getMessage()}");

            return redirect('/#/500');
        }
    }

    private function generateVerificationCode()
    {
        $strCode = mt_rand (100000,999999);

        $objTUserActivation = TUserActivation::where('activation_code', $strCode)->first();

        if (!$objTUserActivation)
        {
            return $strCode;
        }
        else
        {
            return $this->generateVerificationCode();
        }
    }
}
