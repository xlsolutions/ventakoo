<?php

namespace App\Http\Controllers\Web;

use App\Events\ForgotPasswordEmailEvent;
use App\Http\Controllers\Controller;
use App\Models\Constants\CActivationType;
use App\Models\Users\TUserActivation;
use Illuminate\Support\Facades\View;
use Illuminate\Http\Request;
use App\Models\Users\TUserContact;
use DB;

class ForgotPasswordPageController extends Controller
{
    /**
     * For routing to forgot password page e.g. /verify_forgot
     *
     * @return view
     *
     * */
    public function getMethod()
    {
        try
        {
            return View::make('auth.pages.forgot_password', [
                'change_password' => false
            ]);
        }
        catch (\Exception $objError)
        {
            syslog(LOG_CRIT, __FILE__ . ":" . __METHOD__ . ":" . __LINE__ . " => {$objError->getMessage()}");

            return redirect('/#/500');
        }
    }

    /**
     *
     * Re/Send forgot password verification code
     *
     * @param $objRequest Request
     *
     * @return view
     * */
    public function postMethod(Request $objRequest)
    {
        try
        {
            DB::beginTransaction();

            $objTUserContact = TUserContact::where('email_address', $objRequest->input('email_address'))->first();
            $intUserContactID = $objTUserContact['user_contact_id'];

            $objCActivationType = CActivationType::where('handle', 'VERIFY_FORGOT_EMAIL')->first();
            $intActivationTypeID = $objCActivationType['activation_type_id'];

            $strCode = $this->generateVerificationCode();

            $objTUserActivation = TUserActivation::where('activation_code', $strCode)->where('activation_type_id', $intActivationTypeID)->where('user_contact_id', $intUserContactID)->first();

            if ($objTUserActivation)
            {
                $objTUserActivation->activation_code = $strCode;
                $objTUserActivation->save();
            }
            else
            {
                TUserActivation::create([
                    'activation_code' => $strCode,
                    'activation_type_id' => $intActivationTypeID,
                    'user_contact_id' => $intUserContactID
                ]);
            }

            $objForgotPasswordEmailEvent = new ForgotPasswordEmailEvent();
            $objForgotPasswordEmailEvent->setEmail($objTUserContact['email_address']);
            $objForgotPasswordEmailEvent->setCode($strCode);

            event($objForgotPasswordEmailEvent);

            DB::commit();

            return View::make('auth.pages.forgot_password_sent');
        }
        catch (\Exception $objError)
        {
            DB::rollBack();

            syslog(LOG_CRIT, __FILE__ . ":" . __METHOD__ . ":" . __LINE__ . " => {$objError->getMessage()}");

            return redirect('/#/500');
        }
    }

    private function generateVerificationCode()
    {
        $strCode = mt_rand (100000,999999);

        $objTUserActivation = TUserActivation::where('activation_code', $strCode)->first();

        if (!$objTUserActivation)
        {
            return $strCode;
        }
        else
        {
            return $this->generateVerificationCode();
        }
    }
}