<?php

namespace App\Http\Controllers\Web;

use App\Http\Controllers\Controller;
use App\Models\Constants\CActivationType;
use App\Models\Users\TUserActivation;
use App\Models\Users\TUserContact;
use App\Models\Users\TUserInformation;
use Illuminate\Support\Facades\Hash;
use Illuminate\Support\Facades\View;
use Illuminate\Http\Request;
use DB;

class ForgotPasswordVerifyPageController extends Controller
{
    /**
     * For routing to forgot password verification page with code verification query param e.g. /forgot_password_verify?code=XXXXXX
     *
     * @param $objRequest Request
     *
     * @return view
     * */
    public function getMethod(Request $objRequest)
    {
        try
        {
            return View::make('auth.pages.forgot_password', [
                'code' => $objRequest->input('code'),
                'change_password' => true
            ]);
        }
        catch (\Exception $objError)
        {
            syslog(LOG_CRIT, __FILE__ . ":" . __METHOD__ . ":" . __LINE__ . " => {$objError->getMessage()}");

            return redirect('/#/500');
        }
    }

    /**
     * Change password with verification code
     *
     * @param $objRequest Request
     *
     * @return view
     * */
    public function postMethod(Request $objRequest)
    {
        try
        {
            DB::beginTransaction();

            $objCActivationType = CActivationType::where('handle', 'VERIFY_FORGOT_EMAIL')->first();
            $intActivationTypeID = $objCActivationType['activation_type_id'];

            $objTUserActivation = TUserActivation::where('activation_code', $objRequest->input('code'))->where('activation_type_id', $intActivationTypeID)->first();
            $intUserContactID = $objTUserActivation['user_contact_id'];

            $objTUserContact = TUserContact::find($intUserContactID);
            $intUserInformationID = $objTUserContact['user_information_id'];

            $strHash = env('AUTH_HASH_PASSWORD');

            $objTUserInformation = TUserInformation::where('user_information_id', $intUserInformationID)->first();
            $objTUserInformation->password = Hash::make($strHash.$objRequest->input('password'));
            $objTUserInformation->save();

            $objTUserActivation->forceDelete();

            DB::commit();

            return View::make('auth.pages.forgot_password_completed');
        }
        catch (\Exception $objError)
        {
            DB::rollBack();

            syslog(LOG_CRIT, __FILE__ . ":" . __METHOD__ . ":" . __LINE__ . " => {$objError->getMessage()}");

            return redirect('/#/500');
        }
    }
}