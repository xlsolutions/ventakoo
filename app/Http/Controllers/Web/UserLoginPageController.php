<?php

namespace App\Http\Controllers\Web;

use App\Events\UserLoginEvent;
use App\Http\Controllers\Controller;
use App\Providers\AuthServiceProvider;
use App\Providers\FlashMessageServiceProvider;
use Illuminate\Support\Facades\View;
use Illuminate\Http\Request;
use Illuminate\Support\Facades\Hash;
use App\Models\Users\TUserContact;
use App\Models\Users\TUserInformation;
use Illuminate\Support\Facades\Session;

class UserLoginPageController extends Controller
{
    /**
     * For routing to Login Page
     *
     * @return view Login
     *
     * */
    public function getMethod(Request $objRequest)
    {
        try
        {
            Session::put('redirect', $objRequest->input('redirect'));
            Session::save();
            
            return View::make('auth.pages.login');
        }
        catch (\Exception $objError)
        {
            syslog(LOG_CRIT, __FILE__ . ":" . __METHOD__ . ":" . __LINE__ . " => {$objError->getMessage()}");

            return redirect('/#/500');
        }
    }

    /**
     * Used to handle
     * - when user is not yet login it will execute all process
     * - when user is already login and wants to login again it will just redirect to home page
     *
     * @param $objRequest Request
     *
     * @return redirect
     * */
    public function postMethod(Request $objRequest)
    {
        try
        {
            $strHash = env('AUTH_HASH_PASSWORD');

            $objTUserContact = TUserContact::where('email_address', $objRequest->input('email_address'))->first();

            $bolLoginFlag = false;
            $bolAccountActivated = true;

            if ($objTUserContact)
            {
                $objTUserInformation = TUserInformation::where('user_information_id', $objTUserContact['user_information_id'])->where('validated', 1)->first();

                if ($objTUserInformation)
                {
                    if (!$objRequest->session()->has('SECRET_AUTH'))
                    {
                        if (Hash::check($strHash . $objRequest->input('password'), $objTUserInformation['password']))
                        {
                            event(new UserLoginEvent($objTUserInformation));

                            $bolLoginFlag = true;
                        }
                    }
                    else
                    {
                        $bolLoginFlag = AuthServiceProvider::bolLogin();
                    }
                }
                else
                {
                    $bolAccountActivated = false;
                }
            }

            if ($bolLoginFlag === true)
            {
                $objView = redirect('/#/product');
                $strRedirect = Session::get('redirect');
                $strRedirectPhp = Session::get('redirectPhp');
                
                if ($strRedirect)
                {
                    Session::forget('redirect');
                    Session::save();

                    $objView = $strRedirect ? redirect("/#/{$strRedirect}") : $objView;
                }
                else if ($strRedirectPhp)
                {
                    Session::forget('redirectPhp');
                    Session::save();

                    $objView = $strRedirectPhp ? redirect("/{$strRedirect}") : $objView;
                }

                return $objView;
            }

            if (!$bolAccountActivated)
            {
                FlashMessageServiceProvider::danger('Account not yet activated please check your email');
            }
            else
            {
                FlashMessageServiceProvider::danger('Incorrect combination of username and password');
            }

            return redirect('/login');
        }
        catch (\Exception $objError)
        {
            syslog(LOG_CRIT, __FILE__ . ":" . __METHOD__ . ":" . __LINE__ . " => {$objError->getMessage()}");

            return redirect('/#/500');
        }
    }
}