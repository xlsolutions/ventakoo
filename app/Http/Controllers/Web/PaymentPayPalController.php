<?php

namespace App\Http\Controllers\Web;

use App\Events\PayPal\PayPalIPNEvent;
use App\Models\Order\TOrder;
use Illuminate\Http\Request;
use App\Http\Controllers\Controller;

class PaymentPayPalController extends Controller
{
    /**
     * handle payment paypal processes
     *
     * @param $objRequest Request
     * 
     */
    public function processAction(Request $objRequest)
    {
        syslog(LOG_DEBUG, __CLASS__ . ':' . __METHOD__ . ': incoming paypal ipn');

        $objTOrder = TOrder::findOrFail($objRequest->input('custom'));

        $objPayPalIPNEvent = new PayPalIPNEvent();
        $objPayPalIPNEvent->setOrderId($objTOrder->order_id);
        $objPayPalIPNEvent->setIntUserId($objTOrder->user_id);
        $objPayPalIPNEvent->setOrderGrossAmount($objTOrder->price);

        event($objPayPalIPNEvent);
    }
}
