<?php

namespace App\Http\Controllers\Web;

use App\Events\SetupSellerAccountEvent;
use App\Events\UserLoginEvent;
use App\Events\VerifyRegistrationEmailEvent;
use App\Http\Controllers\Controller;
use App\Models\Users\TUserActivation;
use App\Models\Users\UserRole;
use App\Providers\AuthServiceProvider;
use Illuminate\Support\Facades\Session;
use Illuminate\Support\Facades\View;
use Illuminate\Http\Request;
use App\Models\Users\TUserContact;
use App\Models\Users\TUserInformation;
use DB;;

/**
 * 
 * This serves only for SELLER account, no need to validate the email of NON SELLER account
 * 
 * */
class VerifyEmailPageController extends Controller
{
    /**
     * For routing to account verification page
     *
     * @param $objRequest Request
     * 
     * @return view
     *
     * */
    public function getMethod(Request $objRequest)
    {
        try
        {
            DB::beginTransaction();

            if (Session::get('errors'))
            {
                DB::rollBack();

                return redirect('/#/404');
            }
            else
            {
                $objTUserActivation = TUserActivation::where('activation_code', $objRequest->input('code'))->first();

                if (!$objTUserActivation)
                {
                    return redirect('/#/404');
                }

                $intUserContactID = $objTUserActivation['user_contact_id'];

                $objTUserContact = TUserContact::find($intUserContactID);

                $intUserInformationID = $objTUserContact['user_information_id'];

                $objTUserInformation = TUserInformation::find($intUserInformationID);

                $objTUserInformation->validated = 1;

                $objTUserInformation->save();
                $objTUserActivation->forceDelete();

                $objSetupSellerAccountEvent = new SetupSellerAccountEvent();
                $objSetupSellerAccountEvent->setIntUserId($objTUserInformation['user_id']);

                event($objSetupSellerAccountEvent);

                if ($objRequest->input('campaignPlan'))
                {
                    if (AuthServiceProvider::bolLogin())
                    {
                        UserRole::loadPermsByUserId($objTUserInformation['user_id'], true);
                        
                        event(new UserLoginEvent($objTUserInformation));
                        
                        DB::commit();
                        
                        return redirect('/campaign/pricing');
                    }
                    else
                    {
                        Session::put('redirectPhp', 'campaign/pricing');
                        Session::save();
                    }
                }
                
                DB::commit();

                return View::make('auth.pages.activated');
            }
        }
        catch (\Exception $objError)
        {
            DB::rollBack();

            syslog(LOG_CRIT, __FILE__ . ":" . __METHOD__ . ":" . __LINE__ . " => {$objError->getMessage()}");

            return redirect('/#/500');
        }
    }

    /**
     * Resend verification code
     *
     * @param $objRequest Request
     *
     * @return view
     * */
    public function postMethod(Request $objRequest)
    {
        try
        {
            DB::beginTransaction();

            $objTUserContact = TUserContact::where('email_address', $objRequest->input('email'))->first();
            $intUserContactID = $objTUserContact['user_contact_id'];
            $intUserInformationID = $objTUserContact['user_information_id'];

            $objTUserInformation = TUserInformation::find($intUserInformationID);

            if ($objTUserInformation->validated)
            {
                throw new \Exception("Email ({$objTUserContact['email_address']}) already validated");
            }

            $strCode = $this->generateVerificationCode();

            $objTUserActivation = TUserActivation::where('user_contact_id', $intUserContactID)->first();
            $objTUserActivation->activation_code = $strCode;
            $objTUserActivation->save();

            $objVerifyRegistrationEmailEvent = new VerifyRegistrationEmailEvent();
            $objVerifyRegistrationEmailEvent->setEmail($objTUserContact['email_address']);
            $objVerifyRegistrationEmailEvent->setCode($strCode);

            event($objVerifyRegistrationEmailEvent);

            DB::commit();

            return View::make('auth.pages.verify_email', [
                'email' => $objTUserContact['email_address']
            ]);
        }
        catch (\Exception $objError)
        {
            DB::rollBack();

            syslog(LOG_CRIT, __FILE__ . ":" . __METHOD__ . ":" . __LINE__ . " => {$objError->getMessage()}");

            return redirect('/#/500');
        }
    }

    private function generateVerificationCode()
    {
        $strCode = mt_rand (100000,999999);

        $objTUserActivation = TUserActivation::where('activation_code', $strCode)->first();

        if (!$objTUserActivation)
        {
            return $strCode;
        }
        else
        {
            return $this->generateVerificationCode();
        }
    }
}