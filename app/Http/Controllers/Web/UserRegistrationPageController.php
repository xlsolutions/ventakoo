<?php

namespace App\Http\Controllers\Web;

use App\Events\SetupBuyerAccountEvent;
use App\Events\UserLoginEvent;
use App\Events\VerifyRegistrationEmailEvent;
use App\Http\Controllers\Controller;
use App\Models\Constants\CActivationType;
use App\Models\Users\TUserActivation;
use Illuminate\Http\Request;
use Illuminate\Support\Facades\Hash;
use Illuminate\Support\Facades\View;
use App\Models\Users\TUser;
use App\Models\Users\TUserContact;
use App\Models\Users\TUserContactAttribute;
use App\Models\Users\TUserInformation;
use App\Models\Users\TUserLocation;
use App\Models\Constants\CUserContactInformation;
use App\Models\Constants\CCountry;
use App\Models\Constants\CState;
use App\Models\Constants\CCity;
use DB;
use Illuminate\Support\Facades\Session;


class UserRegistrationPageController extends Controller
{
    /**
     * Used to get the Country, State and City for the UserRegistration Form
     *
     * @param $objRequest Request
     * 
     * @return array to Registration Page View
     *
     * */
    public function getMethod(Request $objRequest)
    {
        try
        {
            $arrData = array();
            
            $arrCCountry = CCountry::all()->sortBy('country_name')->toArray();

            $arrData['countries'] = $arrCCountry;
            $arrData['registration_type'] = $objRequest->input('type');
            $arrData['firstname'] = $objRequest->old('firstname');
            $arrData['lastname'] = $objRequest->old('lastname');
            $arrData['email_address'] = $objRequest->old('email_address');
            $arrData['mobile_number'] = $objRequest->old('mobile_number');
            $arrData['phone_number'] = $objRequest->old('phone_number');
            $arrData['country'] = $objRequest->old('country');
            $arrData['state'] = $objRequest->old('state');
            $arrData['city'] = $objRequest->old('city');
            $arrData['street'] = $objRequest->old('street');
            
            Session::put('UserRegistrationPageController_getMethod_isFromPricingPage', $objRequest->input('campaignPlan'));
            Session::put('redirect', $objRequest->input('redirect'));
            Session::save();

            return View::make('auth/pages/register', $arrData);
        }
        catch (\Exception $objError)
        {
            syslog(LOG_CRIT, __FILE__ . ":" . __METHOD__ . ":" . __LINE__ . " => {$objError->getMessage()}");

            return redirect('/#/500');
        }
    }

    /**
     * Used to create or register a new user account
     *
     * @param $objRequest Request
     *
     * @return view Verify email page
     * */
    public function postMethod(Request $objRequest)
    {
        try
        {
            DB::beginTransaction();

            $strHash = env('AUTH_HASH_PASSWORD');

            $strDate =  date("Y-m-d H:i:s");

            $objCCountry = CCountry::where('country_code', $objRequest->input('country'))->first();

            $objCState = CState::where([
                ['country_id','=',$objCCountry['country_id']],
                ['state_id','=',$objRequest->input('state')]
            ])
                ->first();

            $objCCity = CCity::where([
                ['state_id','=', $objCState['state_id']],
                ['city_id','=',$objRequest->input('city')]
            ])
                ->first();

            $intUserID = TUser::insertGetId([]);

            $intUserInformationID = TUserInformation::insertGetId([
                'created_at' => $strDate,
                'updated_at' => $strDate,
                'user_id' => $intUserID,
                'firstname' => $objRequest->input('firstname'),
                'lastname' => $objRequest->input('lastname'),
                'password' => Hash::make($strHash.$objRequest->input('password')),
                'validated' => (int)($objRequest->input('type') !== 'SELLER')
            ]);

            $intUserLocationID = TUserLocation::insertGetId([
                'created_at' => $strDate,
                'updated_at' => $strDate,
                'country_id' => $objCCountry['country_id'],
                'state_id' => $objCState['state_id'],
                'city_id' => $objCCity['city_id']
            ]);

            $intUserContactID = TUserContact::insertGetId([
                'created_at' => $strDate,
                'updated_at' => $strDate,
                'user_information_id' => $intUserInformationID,
                'email_address' => $objRequest->input('email_address'),
                'user_location_id' => $intUserLocationID
            ]);

            $strRegistrationAttribute = ["MOBILE_NUMBER","PHONE_NUMBER","ADDRESS"];

            $arrCUserContactInformation= CUserContactInformation::whereIn('handle',$strRegistrationAttribute)->get()->toArray();

            foreach ($arrCUserContactInformation as $userContactInformation)
            {
                if($userContactInformation['handle'] == 'MOBILE_NUMBER')
                {
                    TUserContactAttribute::create([
                        'user_contact_id' => $intUserContactID,
                        'user_contact_information_id' => $userContactInformation['user_contact_information_id'],
                        'value' => $objRequest->input('mobile_number'),
                    ]);
                }

                if($userContactInformation['handle'] == 'PHONE_NUMBER')
                {
                    TUserContactAttribute::create([
                        'user_contact_id' => $intUserContactID,
                        'user_contact_information_id' => $userContactInformation['user_contact_information_id'],
                        'value' => $objRequest->input('phone_number'),
                    ]);
                }

                if($userContactInformation['handle'] == 'ADDRESS')
                {
                    TUserContactAttribute::create([
                        'user_contact_id' => $intUserContactID,
                        'user_contact_information_id' => $userContactInformation['user_contact_information_id'],
                        'value' => $objRequest->input('street'),
                    ]);
                }
            }

            if ($objRequest->input('type') === 'SELLER')
            {
                $strCode = $this->generateVerificationCode();

                $objCActivationType = CActivationType::where('handle','VERIFY_ACCOUNT_EMAIL')->first();

                TUserActivation::create([
                    'activation_code' => $strCode,
                    'activation_type_id' => $objCActivationType['activation_type_id'],
                    'user_contact_id' => $intUserContactID
                ]);

                $bolPricingPage = Session::get('UserRegistrationPageController_getMethod_isFromPricingPage');
                
                if ($bolPricingPage)
                {
                    Session::forget('UserRegistrationPageController_getMethod_isFromPricingPage');
                    Session::save();
                }
                
                $objVerifyRegistrationEmailEvent = new VerifyRegistrationEmailEvent();
                $objVerifyRegistrationEmailEvent->setEmail($objRequest->input('email_address'));
                $objVerifyRegistrationEmailEvent->setPricingPageSession($bolPricingPage);
                $objVerifyRegistrationEmailEvent->setCode($strCode);

                event($objVerifyRegistrationEmailEvent);

                $objView = view('auth/pages/verify_email', [
                    'email' => $objRequest->input('email_address')
                ]);
            }
            else
            {
                $strRedirect = Session::get('redirect');
                
                if ($strRedirect)
                {
                    Session::forget('redirect');
                    Session::save();
                }
                
                $objView = $strRedirect ? redirect("/#/{$strRedirect}") : redirect('/#/product');
                
                /**
                 * @readme: Auto login user since this is only a buyer account
                 * 
                 * */
                $objTUserInformation = TUserInformation::where('user_information_id', $intUserInformationID)->first();
                
                event(new UserLoginEvent($objTUserInformation));
            }

            $objSetupBuyerAccountEvent = new SetupBuyerAccountEvent();
            $objSetupBuyerAccountEvent->setIntUserId($intUserInformationID);

            event($objSetupBuyerAccountEvent);

            DB::commit();
            
            return $objView;
        }
        catch (\Exception $objError)
        {
            DB::rollBack();

            syslog(LOG_CRIT, __FILE__ . ":" . __METHOD__ . ":" . __LINE__ . " => {$objError->getMessage()}");

            return redirect('/#/500');
        }
    }

    public function getStatesByCountryCode(Request $objRequest)
    {
        $objCCountry = CCountry::where('country_code',$objRequest->input('strCountryCode'))->first();

        $objCState = CState::where('country_id',$objCCountry->country_id)->get()->sortBy('state_name')->toArray();

        return json_encode($objCState);
    }

    public function getCitiesByStateID(Request $objRequest)
    {
        $objCCity = CCity::where('state_id',$objRequest->input('intStateID'))->get()->sortBy('city_name')->toArray();

        return json_encode($objCCity);
    }

    private function generateVerificationCode()
    {
        $strCode = mt_rand (100000,999999);

        $objTUserActivation = TUserActivation::where('activation_code', $strCode)->first();

        if (!$objTUserActivation)
        {
            return $strCode;
        }
        else
        {
            return $this->generateVerificationCode();
        }
    }
}
