<?php

/**
 * @package: Events
 * @class: UserLogoutEvent
 * @description: Route for User Logout
 * @uri:
 * - /logout: GET
 *
 * */
use App\Providers\AuthServiceProvider;
use App\Events\UserLogoutEvent;

Route::get('/logout', function() {
    try
    {
        event(new UserLogoutEvent(AuthServiceProvider::getUserAuth()));
    }
    catch (\Exception $objError)
    {
        syslog(LOG_CRIT, "Logout brute access. Got message: {$objError->getMessage()}");
    }
    
    return redirect('/#/product');
});