<?php

use App\Http\Requests\RequestVerifyEmail;
use App\Http\Controllers\Web\VerifyEmailPageController;

/**
 * @package: Controllers
 * @class: VerifyEmailPageController
 * @description: Confirm account using email verification code
 * @uri:
 * - /verify_email: GET | POST
 *
 * */
Route::get('/verify_email', 'VerifyEmailPageController@getMethod')->middleware('guest');

Route::post('/verify_email', function(VerifyEmailPageController $objVerifyEmailPageController, RequestVerifyEmail $objRequestVerifyEmail) {
    return $objVerifyEmailPageController->postMethod($objRequestVerifyEmail);
})->middleware('guest');