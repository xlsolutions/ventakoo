<?php

use App\Http\Requests\RequestPaymentPayPal;
use App\Http\Controllers\Web\PaymentPayPalController;

/**
 * @package: Controllers
 * @class: PaymentPayPalController
 * @description: Payment PayPal processes
 * @uri:
 * 
 * - /payment/paypal/ipn: POST
 *
 * */
Route::post('/payment/paypal/ipn', function(PaymentPayPalController $objPaymentPayPalController, RequestPaymentPayPal $objRequestPaymentPayPal) {
    return $objPaymentPayPalController->processAction($objRequestPaymentPayPal);
});