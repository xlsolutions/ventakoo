<?php

use App\Http\Requests\RequestUserLogin;
use App\Http\Controllers\Web\UserLoginPageController;

/**
 * @package: Controllers
 * @class: UserLoginPageController
 * @description: Page for User Login
 * @uri:
 * - /login: GET | POST
 *
 * */
Route::get('/login','UserLoginPageController@getMethod')->middleware('guest');

Route::post('/login',function(UserLoginPageController $objUserLoginPageController, RequestUserLogin $objRequestUserLogin) {
    return $objUserLoginPageController->postMethod($objRequestUserLogin);
})->middleware('guest');