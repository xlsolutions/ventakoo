<?php

use App\Http\Requests\RequestUserUpdateInformation;
use App\Http\Controllers\Web\UserUpdateInformationPageController;

/**
 * @package: Controllers
 * @class: UserUpdateInformationPageController
 * @description: Page for User Update Information
 * @uri:
 * - /account: GET
 * - /account/update: POST
 *
 * */
Route::get('/account',function(UserUpdateInformationPageController $objUserUpdateInformationPageController, \Illuminate\Http\Request $objRequest){
	return $objUserUpdateInformationPageController->getMethod($objRequest);
});

Route::post('/account/update',function(UserUpdateInformationPageController $objUserUpdateInformationPageController, RequestUserUpdateInformation $objRequestUserUpdateInformation) {
    return $objUserUpdateInformationPageController->postMethod($objRequestUserUpdateInformation);
});
