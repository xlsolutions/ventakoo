<?php

use App\Http\Requests\RequestForgotPasswordVerify;
use App\Http\Controllers\Web\ForgotPasswordVerifyPageController;

/**
 * @package: Controllers
 * @class: ForgotPasswordVerifyPageController
 * @description: Forgot password verification using email verification code
 * @uri:
 * - /forgot_password_verify: GET | POST
 *
 * */
Route::get('/forgot_password_verify', function(ForgotPasswordVerifyPageController $objForgotPasswordVerifyPageController, RequestForgotPasswordVerify $objRequestForgotPasswordVerify) {
    return $objForgotPasswordVerifyPageController->getMethod($objRequestForgotPasswordVerify);
})->middleware('guest');

Route::post('/forgot_password_verify', function(ForgotPasswordVerifyPageController $objForgotPasswordVerifyPageController, RequestForgotPasswordVerify $objRequestForgotPasswordVerify) {
    return $objForgotPasswordVerifyPageController->postMethod($objRequestForgotPasswordVerify);
})->middleware('guest');