<?php

use App\Http\Requests\RequestCampaignPricing;
use App\Http\Controllers\Web\CampaignPricingPageController;

/**
 * @package: Controllers
 * @class: CampaignPlanPageController
 * @description: Campaign plan page
 * @uri:
 * - /campaign/plan: GET | POST
 *
 * */
Route::get('/campaign/pricing', 'CampaignPricingPageController@indexAction');

Route::post('/campaign/pricing', function(CampaignPricingPageController $objCampaignPricingPageController, RequestCampaignPricing $objRequestCampaignPricing) {
    return $objCampaignPricingPageController->postMethod($objRequestCampaignPricing);
});