<?php

use App\Http\Requests\RequestUserRegistration;
use App\Http\Controllers\Web\UserRegistrationPageController;
use App\Http\Requests\RequestCities;
use App\Http\Requests\RequestStates;

/**
 * @package: Controllers
 * @class: UserRegistrationPageController
 * @description: Page for User Registration
 * @uri:
 * - /register: GET | POST
 *
 * */
Route::get('/register','UserRegistrationPageController@getMethod')->middleware('guest');

Route::get('/register/getStates', function(UserRegistrationPageController $objUserRegistrationPageController,RequestStates $objRequestStates) {
    return $objUserRegistrationPageController->getStatesByCountryCode($objRequestStates);
})->middleware('guest');

Route::get('/register/getCities', function(UserRegistrationPageController $objUserRegistrationPageController,RequestCities $objRequestCities) {
    return $objUserRegistrationPageController->getCitiesByStateID($objRequestCities);
})->middleware('guest');

Route::post('/register',function(UserRegistrationPageController $objUserRegistrationPageController, RequestUserRegistration $objRequestUserRegistration) {
    return $objUserRegistrationPageController->postMethod($objRequestUserRegistration);
})->middleware('guest');
