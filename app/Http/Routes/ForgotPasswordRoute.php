<?php

use App\Http\Requests\RequestForgotPassword;
use App\Http\Controllers\Web\ForgotPasswordPageController;

/**
 * @package: Controllers
 * @class: ForgotPasswordPageController
 * @description: Forgot password account using email verification code
 * @uri:
 * - /verify_email: GET | POST
 *
 * */
Route::get('/forgot_password', 'ForgotPasswordPageController@getMethod')->middleware('guest');

Route::post('/forgot_password', function(ForgotPasswordPageController $objForgotPasswordPageController, RequestForgotPassword $objRequestForgotPassword) {
    return $objForgotPasswordPageController->postMethod($objRequestForgotPassword);
})->middleware('guest');