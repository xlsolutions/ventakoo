<?php

use App\Http\Requests\Resource\v1\RequestProductCampaignResource;
use App\Http\Controllers\Resource\v1\ProductCampaignResource;
use App\Http\Requests\Resource\v1\RequestAdPostResource;
use App\Http\Controllers\Resource\v1\AdPostResource;

/**
 * @package: Controllers\Resource\v1 - Version 1
 * @class: ProductCampaignResource
 * @description: API for product
 * @headers:
 * - X-Requested-With: XMLHttpRequest
 * - Content-Type: application/json
 *
 * @uri:
 * - /rest/1/product : GET - all products with pagination
 *
 * - /rest/1/product/count: GET - count all products
 *
 * - /rest/1/product/{handle} : GET product
 *
 * */

Route::get('1/product/campaign', function(ProductCampaignResource $objProductCampaignResource, RequestProductCampaignResource $objRequestProductCampaignResource) {
    return $objProductCampaignResource->getAllMethod($objRequestProductCampaignResource);
});

Route::get('1/user/{user_id}/product/campaign', function(ProductCampaignResource $objProductCampaignResource, RequestProductCampaignResource $objRequestProductCampaignResource, $intUserId) {
    $objProductCampaignResource->intUserId = $intUserId;

    return $objProductCampaignResource->getAllMethod($objRequestProductCampaignResource);
});

Route::get('1/product/campaign/count', function(ProductCampaignResource $objProductCampaignResource, RequestProductCampaignResource $objRequestProductCampaignResource) {
    return $objProductCampaignResource->getCountMethod($objRequestProductCampaignResource);
});

Route::get('1/user/{user_id}/product/campaign/count', function(ProductCampaignResource $objProductCampaignResource, RequestProductCampaignResource $objRequestProductCampaignResource, $intUserId) {
    $objProductCampaignResource->intUserId = $intUserId;

    return $objProductCampaignResource->getCountMethod($objRequestProductCampaignResource);
});

Route::get('1/product/campaign/{product_handle}', function(ProductCampaignResource $objProductCampaignResource, RequestProductCampaignResource $objRequestProductCampaignResource, $strHandle) {
    return $objProductCampaignResource->getMethod($objRequestProductCampaignResource, $strHandle);
});

/**
 * @package: Controllers\Resource\v1 - Version 1
 * @class: AdPostResource
 * @description: API for ad posting
 * @headers:
 * - X-Requested-With: XMLHttpRequest
 * - Content-Type: application/json
 *
 * @uri:
 * - /rest/1/product/campaign : POST | UPDATE
 *
 * */

Route::post('1/product/campaign', function(AdPostResource $objAdPostResource, RequestAdPostResource $objRequestAdPostResource) {
    $intAdPostId = $objRequestAdPostResource->json('campaignId');

    if ($intAdPostId)
    {
        return $objAdPostResource->postUpdateMethod($objRequestAdPostResource, $intAdPostId);
    }
    else
    {
        return $objAdPostResource->postMethod($objRequestAdPostResource);
    }
});