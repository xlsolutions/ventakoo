<?php

use App\Http\Requests\Resource\v1\RequestProductAttributeResource;
use App\Http\Controllers\Resource\v1\ProductAttributeResource;

/**
 * @package: Controllers\Resource\v1 - Version 1
 * @class: ProductAttributeResource
 * @description: API for product attribute
 * @headers:
 * - X-Requested-With: XMLHttpRequest
 * - Content-Type: application/json
 *
 * @uri:
 * - /rest/1/product/attribute : GET - all product attributes
 * - /rest/1/product/attribute/{handle} : GET | POST | DELETE
 * - /rest/1/product/attribute/{product_handle}/{handle} : DELETE - delete one attribute only
 *
 * */

Route::get('1/product/{product_handle}/attribute/{product_attribute_handle}', function(ProductAttributeResource $objProductAttributeResource, RequestProductAttributeResource $objRequestProductAttributeResource, $strProductHandle, $strProductAttributeHandle) {
    $objProductAttributeResource->strProductHandle = $strProductHandle;
    
    return $objProductAttributeResource->getMethod($objRequestProductAttributeResource, $strProductAttributeHandle);
});

Route::get('1/product/{product_handle}/attribute', function(ProductAttributeResource $objProductAttributeResource, RequestProductAttributeResource $objRequestProductAttributeResource, $strProductHandle) {
    $objProductAttributeResource->strProductHandle = $strProductHandle;
    
    return $objProductAttributeResource->getAllMethod($objRequestProductAttributeResource);
});

Route::post('1/product/{product_handle}/attribute', function(ProductAttributeResource $objProductAttributeResource, RequestProductAttributeResource $objRequestProductAttributeResource, $strProductHandle) {
    $objProductAttributeResource->strProductHandle = $strProductHandle;

    return $objProductAttributeResource->postMethod($objRequestProductAttributeResource);
});

Route::post('1/product/{product_handle}/attribute/{product_attribute_handle}', function(ProductAttributeResource $objProductAttributeResource, RequestProductAttributeResource $objRequestProductAttributeResource, $strProductHandle, $strProductAttributeHandle) {
    $objProductAttributeResource->strProductHandle = $strProductHandle;

    return $objProductAttributeResource->postUpdateMethod($objRequestProductAttributeResource, $strProductAttributeHandle);
});

Route::delete('1/product/{product_handle}/attribute', function(ProductAttributeResource $objProductAttributeResource, RequestProductAttributeResource $objRequestProductAttributeResource, $strProductHandle) {
    return $objProductAttributeResource->deleteAllMethod($objRequestProductAttributeResource, $strProductHandle);
});

Route::delete('1/product/{product_handle}/attribute/{product_attribute_handle}', function(ProductAttributeResource $objProductAttributeResource, RequestProductAttributeResource $objRequestProductAttributeResource, $strProductHandle, $strProductAttributeHandle) {
    $objProductAttributeResource->strProductHandle = $strProductHandle;
    
    return $objProductAttributeResource->deleteMethod($objRequestProductAttributeResource, $strProductAttributeHandle);
});