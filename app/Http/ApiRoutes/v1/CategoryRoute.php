<?php

use App\Http\Requests\Resource\v1\RequestCategoryResource;
use App\Http\Controllers\Resource\v1\CategoryResource;

/**
 * @package: Controllers\Resource\v1 - Version 1
 * @class: CategoryResource
 * @description: API for category
 * @headers:
 * - X-Requested-With: XMLHttpRequest
 * - Content-Type: application/json
 *
 * @uri:
 * - /rest/1/category : GET - all categories
 * - /rest/1/category/{handle} : GET | POST | DELETE
 *
 * */

Route::get('1/category/{handle}', function(CategoryResource $objCategoryResource, RequestCategoryResource $objRequestCategoryResource, $strHandle) {
    return $objCategoryResource->getMethod($objRequestCategoryResource, $strHandle);
});

Route::get('1/category', function(CategoryResource $objCategoryResource, RequestCategoryResource $objRequestCategoryResource) {
    return $objCategoryResource->getAllMethod($objRequestCategoryResource);
});

Route::post('1/category', function(CategoryResource $objCategoryResource, RequestCategoryResource $objRequestCategoryResource) {
    return $objCategoryResource->postMethod($objRequestCategoryResource);
});

Route::post('1/category/{handle}', function(CategoryResource $objCategoryResource, RequestCategoryResource $objRequestCategoryResource, $strHandle) {
    return $objCategoryResource->postUpdateMethod($objRequestCategoryResource, $strHandle);
});

Route::delete('1/category/{handle}', function(CategoryResource $objCategoryResource, RequestCategoryResource $objRequestCategoryResource, $strHandle) {
    return $objCategoryResource->deleteMethod($objRequestCategoryResource, $strHandle);
});