<?php

use App\Http\Requests\Resource\v1\RequestProductAttributeResource;
use App\Http\Controllers\Resource\v1\ProductAttributeResource;

/**
 * @package: Controllers\Resource\v1 - Version 1
 * @class: ProductAttributeResource
 * @description: API for product attribute
 * @headers:
 * - X-Requested-With: XMLHttpRequest
 * - Content-Type: application/json
 *
 * @uri:
 * - 1/user/{user_id}/product/attribute : GET - all product attributes by user id
 *
 * */

Route::get('1/user/{user_id}/product/attribute', function(ProductAttributeResource $objProductAttributeResource, RequestProductAttributeResource $objRequestProductAttributeResource, $intUserId) {
    $objProductAttributeResource->intUserId = $intUserId;

    return $objProductAttributeResource->getAllByUserMethod($objRequestProductAttributeResource);
});