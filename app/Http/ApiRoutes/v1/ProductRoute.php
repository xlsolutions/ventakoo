<?php

use App\Http\Requests\Resource\v1\RequestProductResource;
use App\Http\Controllers\Resource\v1\ProductResource;

/**
 * @package: Controllers\Resource\v1 - Version 1
 * @class: ProductResource
 * @description: API for product
 * @headers:
 * - X-Requested-With: XMLHttpRequest
 * - Content-Type: application/json
 *
 * @uri:
 * - /rest/1/product : GET - all products with pagination
 * 
 * - /rest/1/product : POST - create new or update product
 * 
 * - /rest/1/product/count: GET - count all products
 * 
 * - /rest/1/product/{handle} : GET | DELETE
 *
 * */

Route::get('1/product', function(ProductResource $objProductResource, RequestProductResource $objRequestProductResource) {
    return $objProductResource->getAllMethod($objRequestProductResource);
});

Route::get('1/user/{user_id}/product', function(ProductResource $objProductResource, RequestProductResource $objRequestProductResource, $intUserId) {
    $objProductResource->intUserId = $intUserId;
        
    return $objProductResource->getAllMethod($objRequestProductResource);
});

Route::get('1/product/count', function(ProductResource $objProductResource, RequestProductResource $objRequestProductResource) {
    return $objProductResource->getCountMethod($objRequestProductResource);
});

Route::get('1/user/{user_id}/product/count', function(ProductResource $objProductResource, RequestProductResource $objRequestProductResource, $intUserId) {
    $objProductResource->intUserId = $intUserId;

    return $objProductResource->getCountMethod($objRequestProductResource);
});

Route::get('1/product/{product_handle}', function(ProductResource $objProductResource, RequestProductResource $objRequestProductResource, $strHandle) {
    return $objProductResource->getMethod($objRequestProductResource, $strHandle);
});

Route::get('1/sell/product/{product_handle}', function(ProductResource $objProductResource, RequestProductResource $objRequestProductResource, $strHandle) {
    return $objProductResource->getSellMethod($objRequestProductResource, $strHandle);
});

Route::post('1/product', function(ProductResource $objProductResource, RequestProductResource $objRequestProductResource) {
    $strHandle = $objRequestProductResource->json('handle');

    if ($strHandle)
    {
        return $objProductResource->postUpdateMethod($objRequestProductResource, $strHandle);
    }
    else
    {
        return $objProductResource->postMethod($objRequestProductResource);
    }
});

Route::delete('1/product/{product_handle}', function(ProductResource $objProductResource, RequestProductResource $objRequestProductResource, $strHandle) {
    return $objProductResource->deleteMethod($objRequestProductResource, $strHandle);
});