<?php

use App\Http\Requests\Resource\v1\RequestOrderResource;
use App\Http\Controllers\Resource\v1\OrderResource;

use App\Http\Requests\Resource\v1\RequestOrderItemResource;
use App\Http\Controllers\Resource\v1\OrderItemResource;

use App\Http\Requests\Resource\v1\RequestPayPalOrderResource;
use App\Http\Controllers\Resource\v1\PayPalOrderResource;

/**
 * @package: Controllers\Resource\v1 - Version 1
 * @class: OrderResource
 * @description: API for order
 * @headers:
 * - X-Requested-With: XMLHttpRequest
 * - Content-Type: application/json
 *
 * @uri:
 * - /rest/1/order : GET - list of all order with information
 * 
 * - /rest/1/order/{order_id} : GET - order information
 *
 * - /rest/1/user/{user_id}/order/{order_id}/item : GET - list of all ordered items
 * 
 * - /rest/1/user/{user_id}/order/{order_id}/paypal : GET - object of all transaction
 *
 * */

Route::get('1/user/{user_id}/order/count', function(OrderResource $objOrderResource, RequestOrderResource $objRequestOrderResource, $intUserId) {
    $objOrderResource->intUserId = $intUserId;

    return $objOrderResource->getCountMethod($objRequestOrderResource);
});

Route::get('1/user/{user_id}/order', function(OrderResource $objOrderResource, RequestOrderResource $objRequestOrderResource, $intUserId) {
    $objOrderResource->intUserId = $intUserId;
    
    return $objOrderResource->getAllMethod($objRequestOrderResource);
});

Route::get('1/user/{user_id}/order/{order_id}/item', function(OrderItemResource $objOrderItemResource, RequestOrderItemResource $objRequestOrderItemResource, $intUserId, $intOrderId) {
    $objOrderItemResource->intUserId = $intUserId;
    $objOrderItemResource->intOrderId = $intOrderId;

    return $objOrderItemResource->getAllMethod($objRequestOrderItemResource);
});

Route::get('1/user/{user_id}/order/{order_id}/paypal', function(PayPalOrderResource $objPayPalOrderResource, RequestPayPalOrderResource $objRequestPayPalOrderResource, $intUserId, $intOrderId) {
    $objPayPalOrderResource->intUserId = $intUserId;

    return $objPayPalOrderResource->getMethod($objRequestPayPalOrderResource, $intOrderId);
});

Route::get('1/order/{order_id}', function(OrderResource $objOrderResource, RequestOrderResource $objRequestOrderResource, $intOrderId) {
    return $objOrderResource->getMethod($objRequestOrderResource, $intOrderId);
});