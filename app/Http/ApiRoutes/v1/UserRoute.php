<?php

use App\Http\Requests\Resource\v1\RequestUserResource;
use App\Http\Controllers\Resource\v1\UserResource;

/**
 * @package: Controllers\Resource\v1 - Version 1
 * @class: UserResource
 * @description: API for user
 * @headers:
 * - X-Requested-With: XMLHttpRequest
 * - Content-Type: application/json
 *
 * @uri:
 * - /rest/1/user/{user_id} : GET - user information
 *
 * */

Route::get('1/user/{user_id}', function(UserResource $objUserResource, RequestUserResource $objRequestUserResource, $intUserID) {
    return $objUserResource->getMethod($objRequestUserResource, $intUserID);
});