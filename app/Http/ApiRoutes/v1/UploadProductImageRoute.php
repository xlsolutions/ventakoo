<?php

use App\Http\Requests\Resource\v1\RequestProductImageResource;
use App\Http\Controllers\Resource\v1\ProductImageResource;

/**
 * @package: Controllers\Resource\v1 - Version 1
 * @class: ProductImageResource
 * @description: API for product image
 *
 * @uri:
 * - /rest/1/upload/product : POST - return the uploaded temporary image
 *
 * */

Route::post('1/upload/product/image', function(ProductImageResource $objProductImageResource, RequestProductImageResource $objRequestProductImageResource) {
    return $objProductImageResource->postMethod($objRequestProductImageResource);
});