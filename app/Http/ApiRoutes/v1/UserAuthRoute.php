<?php

use App\Providers\AuthServiceProvider;

/**
 * @package: Providers
 * @class: AuthServiceProvider
 * @description: API for auth user checking
 * @headers:
 * - X-Requested-With: XMLHttpRequest
 * - Content-Type: application/json
 *
 * @uri:
 * - /rest/1/auth/user : GET - auth user information
 *
 * */

Route::get('1/auth/user', function() {

    try
    {
        $intUserId = AuthServiceProvider::getUserAuth()->getUserId();
        $strRole = AuthServiceProvider::getUserAuth()->getUserRole();
        $flBalance = AuthServiceProvider::getAccountBalanceByUserId($intUserId);
    }
    catch (\Exception $objError)
    {
        $intUserId = 0;
        $strRole = null;
        $flBalance = 0;
    }

    return Response::json([
        'user_id' => $intUserId,
        'role' => $strRole,
        'accountBalance' => $flBalance
    ]);
});