<?php

namespace App\Http\Requests;

use App\Models\Users\UserRole;
use App\Providers\AuthServiceProvider;
use App\Providers\FlashMessageServiceProvider;
use Illuminate\Contracts\Validation\Validator;
use Illuminate\Support\Facades\Hash;
use Illuminate\Validation\Rule;

class RequestUserUpdateInformation extends Request
{
    /**
     * Get the validation rules that apply to the request.
     *
     * @return array
     */
    public function rules()
    {
        $strRole = UserRole::getRoleByUserId(AuthServiceProvider::getUserAuth()->getUserId());

        switch($this->method())
        {
            case "GET": return [];
            case "POST":

                $arrRule = [
                    'new_password' => 'between:8,100',
                    'confirm_password' => 'between:8,100|same:new_password',
                    'user_image' => 'image|mimes:jpg,jpeg',
                    'country' => 'exists:c_country,country_code',
                    'state' => 'exists:c_state,state_id',
                    'city' => 'exists:c_city,city_id',
                    'mobile_number' => 'numeric',
                    'phone_number' => 'numeric',
                    'street' => 'between:5,130'
                ];

				if ($this->input('type') === 'SELLER' || ($this->input('convert_type') && $strRole === 'BUYER'))
				{
                    $arrRule['firstname'] = 'required';
                    $arrRule['lastname'] = 'required';
                    $arrRule['mobile_number'] = 'required|' . $arrRule['mobile_number'];
                    $arrRule['phone_number'] = 'required|' . $arrRule['phone_number'];
                    $arrRule['country'] = 'required|' . $arrRule['country'];
                    $arrRule['state'] = 'required|' . $arrRule['state'];
                    $arrRule['city'] = 'required|' . $arrRule['city'];
                    $arrRule['street'] = 'required|' . $arrRule['street'];

                    if (!AuthServiceProvider::getUserAuth()->getProfilePhoto())
                    {
                        $arrRule['user_image'] = 'required|image|mimes:jpg,jpeg';
                    }
				}
				else
				{
                    $arrRule['mobile_number'] = 'numeric';
                    $arrRule['phone_number'] = 'numeric';
				}

                return $arrRule;
        }
    }

    /**
     * Get the sanitized input for the request.
     *
     * @return array
     */
    public function sanitize()
    {
        return $this->all();
    }

    /**
     * Get the error messages for the defined validation rules.
     *
     * @return array
     */
    public function messages()
    {
        return [
            '*.required' => ":attribute is required",
            '*.min' => ":attribute minimum value is :min",
            '*.numeric' => ":attribute is not a valid number",
            '*.size' => ":attribute maximum character limit is :size",
            '*.exists' => ":attribute does not exists",
            '*.same' => ":attribute does not match with :other",
            '*.between' => ":attribute must be between :min - :max",
            '*.unique' => ":attribute already exist",
            '*.email' => ":attribute is not a valid email",
            '*.image' => ":attribute is not an image",
            '*.mimes' => ":attribute is not in jpg/jpeg format"
        ];
    }

    /**
     * Get custom attributes for validator errors.
     *
     * @return array
     */
    public function attributes()
    {
        return [
            'firstname' => "First Name",
            'lastname' => "Last Name",
            'mobile_number' => "Mobile Number",
            'phone_number' => "Phone Number",
            'email_address' => "Email Address",
            'country' => "Country",
            'state' => "State",
            'city' => "City",
            'street' => "Street",
            'old_password' => "Old Password",
            'new_password' => "New Password",
            'confirm_password' => "Confirm New Password",
            'user_image' => "User Image Upload"
        ];
    }

    /** 
     * Format validation errors
     *
     * @param Validator $validator
     * 
     * @return array
     */
    protected function formatErrors(Validator $validator)
    {
        $arrError = $validator->errors()->all();

        foreach($arrError as $strError)
        {
            FlashMessageServiceProvider::danger($strError);
        }

        return $arrError;
    }
}
