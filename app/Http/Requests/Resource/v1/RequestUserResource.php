<?php

namespace App\Http\Requests\Resource\v1;

use App\Http\Requests\ResourceRequest;

class RequestUserResource extends ResourceRequest
{
    /**
     * Get the validation rules that apply to the request.
     *
     * @return array
     */
    public function rules()
    {
        switch($this->method())
        {
            case "GET": 
            case "POST": 
            case "DELETE": 
                
                return [];
        }
    }
}
