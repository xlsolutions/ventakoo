<?php

namespace App\Http\Requests\Resource\v1;

use App\Http\Requests\ResourceRequest;

class RequestOrderResource extends ResourceRequest
{
    /**
     * Get all of the input and files for the request.
     *
     * @return array
     */
    public function all()
    {
        $arrRequestAll = parent::all();

        if ($this->route('order_id'))
        {
            $arrRequestAll['orderId'] = $this->route('order_id');
        }

        if ($this->route('user_id'))
        {
            $arrRequestAll['userId'] = $this->route('user_id');
        }

        return $arrRequestAll;
    }
    
    /**
     * Get the validation rules that apply to the request.
     *
     * @return array
     */
    public function rules()
    {
        switch($this->method())
        {
            case "GET": return [
                'orderId' => 'numeric|exists:t_order,order_id',
                'userId' => 'numeric|exists:t_user,user_id'
            ];

            case "POST":
            case "DELETE": return [];
        }
    }

    /**
     * Get the error messages for the defined validation rules.
     *
     * @return array
     */
    public function messages()
    {
        return [
            '*.numeric' => ":attribute is not a valid number",
            '*.exists' => ":attribute does not exists"
        ];
    }

    /**
     * Get custom attributes for validator errors.
     *
     * @return array
     */
    public function attributes()
    {
        return [
            'orderId' => "Order",
            'userId' => "User"
        ];
    }

}
