<?php

namespace App\Http\Requests\Resource\v1;

use App\Events\AccountBalanceEvent;
use App\Events\AdPostReviewStatus;
use App\Http\Requests\ResourceRequest;
use App\Models\AdPost\TAdPost;
use App\Models\AdPost\TAdPostNote;
use App\Models\Constants\CProductPlan;
use App\Models\Products\TProduct;
use App\Models\Users\UserRole;
use App\Providers\AuthServiceProvider;
use Illuminate\Validation\Rule;

class RequestAdPostResource extends ResourceRequest
{
    private $strCurrentCampaignPlan;
    private $intCampaignPostPerPrice;
    private $bolFreeCampaignPost = false;
    private $objAccountBalanceEvent;
    
    private $strCampaignPreStateHandle;

    /**
     * @return string
     */
    public function getCurrentCampaignPlan()
    {
        return $this->strCurrentCampaignPlan;
    }

    /**
     * @return integer
     */
    public function getCampaignPostPerPrice()
    {
        return $this->intCampaignPostPerPrice;
    }

    /**
     * @return string
     */
    public function getCampaignPreStateHandle()
    {
        return $this->strCampaignPreStateHandle;
    }
    
    /**
     * @return boolean
     */
    public function getFreeCampaignPost()
    {
        return $this->bolFreeCampaignPost;
    }

    /**
     * @return AccountBalanceEvent
     */
    public function getAccountBalanceEvent()
    {
        return $this->objAccountBalanceEvent;
    }
    
    /**
     * Determine if the user is authorized to make this request.
     *
     * @return bool
     */
    public function authorize()
    {
        $intUserId = AuthServiceProvider::getUserAuth()->getUserId();

        //@todo: this should be a Middleware to reused globally
        if ($this->method() === 'POST' && UserRole::getRoleByUserId($intUserId) === 'BUYER')
        {
            syslog(LOG_CRIT, __CLASS__ . ':' . __METHOD__ . ' User does not have the permission to post a campaign and yet forcing to post one. User Id: ' . $intUserId);

            return false;
        }

        if ($this->method() === 'POST')
        {
            $objTProduct = TProduct::where('handle', $this->json('productId'))->get()->first();

            if ($objTProduct && ($objTProduct['user_id'] != $intUserId))
            {
                syslog(LOG_DEBUG, "User Id ({$objTProduct['user_id']}) is not the owner of this product. Current User Id: " . $intUserId);

                return false;
            }

            if (!$this->isValidCampaignState())
            {
                syslog(LOG_DEBUG, "Invalid product campaign post state: {$this->json('post_status')}. User ID: " . $intUserId);

                return false;
            }

            $intPost = TAdPost::join('t_product', 't_product.product_id', '=', 't_ad_post.product_id')
                ->where('t_product.user_id', $intUserId)
                ->where('t_ad_post.cancel_ad_post', 0)
                ->count();

            $this->strCurrentCampaignPlan = AuthServiceProvider::getCampaignPlanByUserId($intUserId);

            //@readme: paid plan so check the account balance if have sufficient fund
            if ($this->strCurrentCampaignPlan != 'FREE_AD_POST_CAMPAIGN')
            {
                if ($intPost >= 5)
                {
                    $intAccountBalance = AuthServiceProvider::getAccountBalanceByUserId($intUserId);

                    switch($this->strCurrentCampaignPlan)
                    {
                        case 'PRO_AD_POST_CAMPAIGN';
                            $intPricePerPost = CProductPlan::PRO_PER_POST;
                            break;

                        case 'BUSINESS_AD_POST_CAMPAIGN':
                            $intPricePerPost = CProductPlan::BUSINESS_PER_POST;
                            break;

                        case 'STARTER_AD_POST_CAMPAIGN':
                        default:
                            $intPricePerPost = CProductPlan::STARTER_PER_POST;
                    }

                    if ($this->strCampaignPreStateHandle == AdPostReviewStatus::DRAFT || $this->json('post_status') == AdPostReviewStatus::FOR_REVIEW)
                    {
                        if (($intAccountBalance - $intPricePerPost) < 0)
                        {
                            syslog(LOG_DEBUG, "Error: Insufficient funds. Plan: {$this->strCurrentCampaignPlan}, UserId: " . $intUserId);

                            return $this->bolFreeCampaignPost;
                        }
                    }

                    $this->intCampaignPostPerPrice = $intPricePerPost;

                    $this->objAccountBalanceEvent = new AccountBalanceEvent();
                    $this->objAccountBalanceEvent->setIntUserId($intUserId);

                    if ($this->json('post_status') === AdPostReviewStatus::CANCELLED)
                    {
                        $this->objAccountBalanceEvent->setCredit(true);
                    }
                    else
                    {
                        $this->objAccountBalanceEvent->setDebit(true);
                        $this->objAccountBalanceEvent->setBalance($intPricePerPost);
                    }
                }
                else
                {
                    syslog(LOG_DEBUG, "Free: All 5 free campaign post aren't used so its still free");

                    $this->bolFreeCampaignPost = true;
                    
                    return true;
                }
            }
            else if ($intPost > 5) //@readme: this is a free plan account
            {
                syslog(LOG_DEBUG, "Free account plan can post only up to 5 ad campaign. UserId: " . $intUserId);

                return false;
            }
            else
            {
                syslog(LOG_DEBUG, "Free Plan: All 5 free campaign post aren't used so its still free");

                $this->bolFreeCampaignPost = true;

                return true;
            }
        }

        return true;
    }

    /**
     * Get the validation rules that apply to the request.
     *
     * @return array
     */
    public function rules()
    {
        switch($this->method())
        {
            case "POST":

                $arrValidate = [
                    'productId' => 'required',
                    'auto_publish' => 'required|numeric|min:0|max:1',
                    'post_status' => [
                        'required',
                        Rule::in(['FOR_REVIEW','CANCELLED','START','PAUSED'])
                    ]
                ];

                if ($this->json('campaignId'))
                {
                    $arrValidate['campaignId'] = 'exists:t_ad_post,ad_post_id';
                }

                return $arrValidate;

            case "GET":

                $arrValidate = [];

                return $arrValidate;
        }
    }

    /**
     * Get the error messages for the defined validation rules.
     *
     * @return array
     */
    public function messages()
    {
        return [
            '*.required' => ":attribute is required",
            '*.min' => ":attribute minimum value is :min",
            '*.max' => ":attribute maximum value is :max",
            '*.numeric' => ":attribute is not a valid number",
            '*.in' => ":attribute is not a valid post state",
            '*.exists' => ":attribute does not exists"
        ];
    }

    /**
     * Get custom attributes for validator errors.
     *
     * @return array
     */
    public function attributes()
    {
        return [
            'auto_publish' => "Should auto publish upon approval",
            'post_status' => "Campaign state",
            'campaignId' => "Product campaign",
        ];
    }

    private function isValidCampaignState()
    {
        $objTAdPostNote = TAdPostNote::query()
            ->select('c_ad_post_status.handle')
            ->join('c_ad_post_status', 'c_ad_post_status.ad_post_status_id', '=', 't_ad_post_note.ad_post_status_id')
            ->join('t_ad_post', 't_ad_post.ad_post_id', '=', 't_ad_post_note.ad_post_id')
            ->join('t_product', 't_product.product_id', '=', 't_ad_post.product_id')
            ->where('t_product.handle', $this->json('productId'))
            ->orderBy('t_ad_post_note.ad_post_note_id', 'desc')
            ->limit(1)
            ->get()
            ->first();

        if ($objTAdPostNote)
        {
            $strCampaignState = $objTAdPostNote['handle'];
        }
        else
        {
            $strCampaignState = 'DRAFT';
        }

        $this->strCampaignPreStateHandle = $strCampaignState;
        
        $bolStateError = true;

        switch($this->json('post_status'))
        {
            case 'FOR_REVIEW':
                $bolStateError = !in_array($strCampaignState, ['DRAFT','DECLINED','CANCELLED']);
                break;

            case 'START':
                $bolStateError = !in_array($strCampaignState, ['APPROVED', 'PAUSED']);
                break;

            case 'PAUSED':
                $bolStateError = !in_array($strCampaignState, ['START']);
                break;

            case 'CANCELLED':
                $bolStateError = !in_array($strCampaignState, ['FOR_REVIEW', 'APPROVED']);
                break;
        }

        syslog(LOG_DEBUG, __CLASS__ . ':' . __METHOD__ . ': Product campaign pre state: ' . $strCampaignState . '. | User Id: ' . AuthServiceProvider::getUserAuth()->getUserId());

        return $bolStateError === false;
    }
}
