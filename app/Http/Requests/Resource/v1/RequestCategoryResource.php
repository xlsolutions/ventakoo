<?php

namespace App\Http\Requests\Resource\v1;

use App\Http\Requests\ResourceRequest;

class RequestCategoryResource extends ResourceRequest
{
    /**
     * Get the validation rules that apply to the request.
     *
     * @return array
     */
    public function rules()
    {
        switch($this->method())
        {
            case "GET": return [];

            case "POST":

                return [
                    'handle' => 'exists:t_category,handle|max:100',
                    'name' => 'required|max:100'
                ];

            case "DELETE": return [];
        }
    }

    /**
     * Get the error messages for the defined validation rules.
     *
     * @return array
     */
    public function messages()
    {
        return [
            '*.required' => ":attribute is required",
            '*.exists' => ":attribute does not exists",
            '*.size' => ":attribute maximum character limit is :size"
        ];
    }

    /**
     * Get custom attributes for validator errors.
     *
     * @return array
     */
    public function attributes()
    {
        return [
            'handle' => "Category handle",
            'name' => "Category name"
        ];
    }

}
