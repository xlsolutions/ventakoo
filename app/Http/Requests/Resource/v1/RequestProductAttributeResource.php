<?php

namespace App\Http\Requests\Resource\v1;

use App\Http\Requests\ResourceRequest;
use App\Models\AdPost\TAdPost;
use App\Models\AdPost\TAdPostNote;
use App\Models\Users\UserRole;
use App\Providers\AuthServiceProvider;

class RequestProductAttributeResource extends ResourceRequest
{
    /**
     * Determine if the user is authorized to make this request.
     * @readme: only DECLINED or CANCELLED product ad post can be edited
     *
     * @return bool
     */
    public function authorize()
    {
        if ($this->method() === 'GET')
        {
            return true;
        }

        $intUserId = AuthServiceProvider::getUserAuth()->getUserId();

        //@todo: this should be a Middleware to reused globally
        if ($this->method() === 'POST' && UserRole::getRoleByUserId($intUserId) === 'BUYER')
        {
            syslog(LOG_CRIT, __CLASS__ . ':' . __METHOD__ . ' User does not have the permission to create a product attribute and yet forcing to create one. User Id: ' . $intUserId);

            return false;
        }
        
        if ($this->method() === 'POST')
        {
            if ($this->json('handle'))
            {
                $intPost = TAdPost::join('t_product', 't_product.product_id', '=', 't_ad_post.product_id')
                    ->where('t_product.user_id', $intUserId)
                    ->where('t_product.handle', $this->json('handle'))
                    ->count();

                $intPostStatus = TAdPostNote::join('c_ad_post_status', 'c_ad_post_status.ad_post_status_id', '=', 't_ad_post_note.ad_post_status_id')
                    ->join('t_ad_post', 't_ad_post.ad_post_id', '=', 't_ad_post_note.ad_post_id')
                    ->join('t_product', 't_product.product_id', '=', 't_ad_post.product_id')
                    ->whereIn('c_ad_post_status.handle', ['DECLINED', 'CANCELLED'])
                    ->where('t_product.user_id', $intUserId)
                    ->where('t_product.handle', $this->json('handle'))
                    ->orderBy('t_ad_post_note.ad_post_note_id', 'desc')
                    ->limit(1)
                    ->count();

                if ($intPostStatus === 0 && $intPost > 0)
                {
                    return false;
                }
            }
        }

        return true;
    }

    /**
     * Get the validation rules that apply to the request.
     *
     * @return array
     */
    public function rules()
    {
        switch($this->method())
        {
            case "GET":
            case "POST":
            case "DELETE": return [];
        }
    }
}
