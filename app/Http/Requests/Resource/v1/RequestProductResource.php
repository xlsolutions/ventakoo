<?php

namespace App\Http\Requests\Resource\v1;

use App\Http\Requests\ResourceRequest;
use App\Models\AdPost\TAdPost;
use App\Models\AdPost\TAdPostNote;
use App\Models\Users\UserRole;
use App\Providers\AuthServiceProvider;
use Illuminate\Http\JsonResponse;

class RequestProductResource extends ResourceRequest
{
    /**
     * Determine if the user is authorized to make this request.
     * @readme: only DECLINED or CANCELLED product ad post can be edited
     *
     * @return bool
     */
    public function authorize()
    {
        if ($this->method() === 'GET')
        {
            return true;
        }

        $intUserId = AuthServiceProvider::getUserAuth()->getUserId();
        
        //@todo: this should be a Middleware to reused globally
        if (($this->method() === 'POST' || $this->method() === 'DELETE') && UserRole::getRoleByUserId($intUserId) === 'BUYER')
        {
            syslog(LOG_CRIT, __CLASS__ . ':' . __METHOD__ . ' User does not have the permission to create a campaign and yet forcing to create one. User Id: ' . $intUserId);   
            
            return false;
        }
        
        if ($this->method() === 'POST')
        {
            if ($this->json('handle'))
            {
                $intPost = TAdPost::join('t_product', 't_product.product_id', '=', 't_ad_post.product_id')
                    ->where('t_product.user_id', $intUserId)
                    ->where('t_product.handle', $this->json('handle'))
                    ->count();
                
                $intPostStatus = TAdPostNote::join('c_ad_post_status', 'c_ad_post_status.ad_post_status_id', '=', 't_ad_post_note.ad_post_status_id')
                    ->join('t_ad_post', 't_ad_post.ad_post_id', '=', 't_ad_post_note.ad_post_id')
                    ->join('t_product', 't_product.product_id', '=', 't_ad_post.product_id')
                    ->whereIn('c_ad_post_status.handle', ['DECLINED', 'CANCELLED'])
                    ->where('t_product.user_id', $intUserId)
                    ->where('t_product.handle', $this->json('handle'))
                    ->orderBy('t_ad_post_note.ad_post_note_id', 'desc')
                    ->limit(1)
                    ->count();

                if ($intPostStatus === 0 && $intPost > 0)
                {
                    return false;
                }
            }
        }
        else if ($this->method() === 'DELETE')
        {
            if ($this->json('handle'))
            {
                $intPost = TAdPost::join('t_product', 't_product.product_id', '=', 't_ad_post.product_id')
                    ->where('t_product.user_id', $intUserId)
                    ->where('t_product.handle', $this->json('handle'))
                    ->count();

                $intPostStatus = TAdPostNote::join('c_ad_post_status', 'c_ad_post_status.ad_post_status_id', '=', 't_ad_post_note.ad_post_status_id')
                    ->join('t_ad_post', 't_ad_post.ad_post_id', '=', 't_ad_post_note.ad_post_id')
                    ->join('t_product', 't_product.product_id', '=', 't_ad_post.product_id')
                    ->whereIn('c_ad_post_status.handle', ['DECLINED', 'CANCELLED', 'FOR_REVIEW'])
                    ->where('t_product.user_id', $intUserId)
                    ->where('t_product.handle', $this->json('handle'))
                    ->orderBy('t_ad_post_note.ad_post_note_id', 'desc')
                    ->limit(1)
                    ->count();

                if ($intPostStatus === 0 && $intPost > 0)
                {
                    return false;
                }
            }
        }

        return true;
    }

    /**
     * Get the validation rules that apply to the request.
     *
     * @return array
     */
    public function rules()
    {
        switch($this->method())
        {
            case "GET": return [];

            case "POST":

                $arrValidate = [
                    'category' => 'required|exists:t_category,handle',
                    'visibility' => 'required|exists:c_status,handle',
                    'quantity' => 'required|numeric|min:0',
                    'price' => 'required|numeric|min:0',
                    'name' => 'required|max:60'
                ];

                if ($this->json('handle'))
                {
                    $arrValidate['handle'] = 'exists:t_product,handle';
                }

                return $arrValidate;

            case "DELETE": return [];
        }
    }

    /**
     * Get the proper failed validation response for the request.
     *
     * @param  array  $errors
     * @return \Symfony\Component\HttpFoundation\Response
     */
    public function response(array $errors)
    {
        if (($this->ajax() && ! $this->pjax()) || $this->wantsJson())
        {
            $arrError = [];

            foreach($errors as $handles)
            {
                foreach($handles as $message)
                {
                    $arrError[] = $message;
                }
            }

            return new JsonResponse($arrError, 422);
        }
        else
        {
            exit('Not a valid http request');
        }
    }

    /**
     * Get the error messages for the defined validation rules.
     *
     * @return array
     */
    public function messages()
    {
        return [
            '*.required' => ":attribute is required",
            '*.min' => ":attribute minimum value is :min",
            '*.numeric' => ":attribute is not a valid number",
            '*.size' => ":attribute maximum character limit is :size",
            '*.exists' => ":attribute does not exists"
        ];
    }

    /**
     * Get custom attributes for validator errors.
     *
     * @return array
     */
    public function attributes()
    {
        return [
            'handle' => "Product handle",
            'category' => "Product category",
            'visibility' => "Product status",
            'quantity' => "Product quantity",
            'price' => "Product price",
            'name' => "Product name"
        ];
    }

}
