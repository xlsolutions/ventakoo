<?php

namespace App\Http\Requests;

use App\Models\Users\UserRole;
use App\Providers\AuthServiceProvider;

class RequestCampaignPricing extends Request
{
    /**
     * Determine if the user is authorized to make this request.
     *
     * @return bool
     */
    public function authorize()
    {
        if (AuthServiceProvider::bolLogin())
        {
            $intUserId = AuthServiceProvider::getUserAuth()->getUserId();
            
            //@todo: this should be a Middleware to reused globally
            if ($this->method() === 'POST' && UserRole::getRoleByUserId($intUserId) === 'BUYER')
            {
                syslog(LOG_CRIT, __CLASS__ . ':' . __METHOD__ . ' User does not have the permission to buy a campaign plan and yet forcing to do so. User Id: ' . $intUserId);

                return false;
            }   
        }
        
        return true;
    }
    
    /**
     * Get the validation rules that apply to the request.
     *
     * @return array
     */
    public function rules()
    {
        switch($this->method())
        {
            case "GET": return [];
            case "POST": return [
                'campaignPlan' => 'required|exists:c_product_plan,handle'
            ];
        }
    }

    /**
     * Get the error messages for the defined validation rules.
     *
     * @return array
     */
    public function messages()
    {
        return [
            '*.required' => ":attribute plan is required",
            '*.exists' => ":attribute plan does not exists"
        ];
    }

    /**
     * Get custom attributes for validator errors.
     *
     * @return array
     */
    public function attributes()
    {
        return [
            'campaignPlan' => "Pricing"
        ];
    }

}
