<?php

namespace App\Http\Requests;

class RequestPaymentPayPal extends Request
{
    /**
     * Get the validation rules that apply to the request.
     *
     * @return array
     */
    public function rules()
    {
        if ($this->method() != 'POST')
        {
            syslog(LOG_CRIT, "Http request is not POST. Someone is trying to submit in PayPal IPN process");
        }
        
        return [];
    }
}
