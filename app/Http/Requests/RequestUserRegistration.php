<?php

namespace App\Http\Requests;

use App\Providers\FlashMessageServiceProvider;
use Illuminate\Contracts\Validation\Validator;

class RequestUserRegistration extends Request
{
    /**
     * Get the validation rules that apply to the request.
     *
     * @return array
     */
    public function rules()
    {
        switch($this->method())
        {
            case "POST":

                if ($this->input('type') === 'SELLER')
                {
                    return [
                        'firstname' => 'required',
                        'lastname' => 'required',
                        'mobile_number' => 'required|numeric',
                        'phone_number' => 'required|numeric',
                        'email_address' => 'required|email|unique:t_user_contact,email_address',
                        'country' => 'required|exists:c_country,country_code',
                        'state' => 'required|exists:c_state,state_id',
                        'city' => 'required|exists:c_city,city_id',
                        'street' => 'required|between:5,130',
                        'password' => 'required|between:8,100',
                        'confirm_password' => 'required|between:8,100|same:password'
                    ];
                }
                else
                {
                    return [
                        'email_address' => 'required|email|unique:t_user_contact,email_address',
                        'password' => 'required|between:8,100',
                        'confirm_password' => 'required|between:8,100|same:password',
                        'redirect' => 'string'
                    ];
                }
        }
    }
    
    /**
     * Get the error messages for the defined validation rules.
     *
     * @return array
     */
    public function messages()
    {
        return [
            '*.required' => ":attribute is required",
            '*.min' => ":attribute minimum value is :min",
            '*.numeric' => ":attribute is not a valid number",
            '*.size' => ":attribute maximum character limit is :size",
            '*.exists' => ":attribute does not exists",
            '*.same' => ":attribute does not match with :other",
            '*.between' => ":attribute must be between :min - :max",
            '*.unique' => ":attribute already exist",
            '*.email' => ":attribute is not a valid email"
        ];
    }

    /**
     * Get custom attributes for validator errors.
     *
     * @return array
     */
    public function attributes()
    {
        return [
            'firstname' => "First Name",
            'lastname' => "Last Name",
            'mobile_number' => "Mobile Number",
            'phone_number' => "Phone Number",
            'email_address' => "Email Address",
            'country' => "Country",
            'state' => "State",
            'city' => "City",
            'street' => "Street",
            'password' => "Password",
            'confirm_password' => "Confirm Password"
        ];
    }

    /** 
     * Format validation errors
     *
     * @param Validator $validator
     * 
     * @return array
     */
    protected function formatErrors(Validator $validator)
    {
        $arrError = $validator->errors()->all();

        foreach($arrError as $strError)
        {
            FlashMessageServiceProvider::danger($strError);
        }

        return $arrError;
    }
}
