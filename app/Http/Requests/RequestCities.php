<?php

namespace App\Http\Requests;

class RequestCities extends Request
{
    /**
     * Get the validation rules that apply to the request.
     *
     * @return array
     */
    public function rules()
    {
        switch($this->method())
        {
            case "GET": return [
                'intStateID' => 'required|exists:c_state,state_id',
            ];
        }
    }

    /**
     * Get the error messages for the defined validation rules.
     *
     * @return array
     */
    public function messages()
    {
        return [
            '*.required' => ":attribute is required",
            '*.exists' => ":attribute does not exists"
        ];
    }

    /**
     * Get custom attributes for validator errors.
     *
     * @return array
     */
    public function attributes()
    {
        return [
            'intStateID' => "User account state",
        ];
    }

}
