<?php

namespace App\Http\Requests;

use App\Providers\FlashMessageServiceProvider;
use Illuminate\Contracts\Validation\Validator;

class RequestForgotPasswordVerify extends Request
{
    /**
     * Get the validation rules that apply to the request.
     *
     * @return array
     */
    public function rules()
    {
        switch($this->method())
        {
            case "GET":
                
                return [
                    'code' => 'required|exists:t_user_activation,activation_code'
                ];
            
            case "POST":

                return [
                    'code' => 'required|exists:t_user_activation,activation_code',
                    'password' => 'required|between:8,100',
                    'confirm_password' => 'required|between:8,100|same:password',
                ];
        }
    }

    /**
     * Get the error messages for the defined validation rules.
     *
     * @return array
     */
    public function messages()
    {
        return [
            '*.required' => ":attribute is required",
            'code.exists' => "Unknown :attribute or already expired",
            '*.same' => ":attribute does not match with :other",
            '*.between' => ":attribute must be between :min - :max"
        ];
    }

    /**
     * Get custom attributes for validator errors.
     *
     * @return array
     */
    public function attributes()
    {
        return [
            'code' => "request",
            'password' => "Password",
            'confirm_password' => "Confirm Password"
        ];
    }

    /**
     * Format validation errors
     *
     * @param Validator $validator
     *
     * @return array
     */
    protected function formatErrors(Validator $validator)
    {
        $arrError = $validator->errors()->all();

        foreach($arrError as $strError)
        {
            FlashMessageServiceProvider::danger($strError);
        }

        return $arrError;
    }
}
