<?php

namespace App\Http\Requests;

use Illuminate\Http\JsonResponse;

/*
 * Used for API requests
 * */
abstract class ResourceRequest extends Request
{
    /**
     * Get the proper failed validation response for the request.
     *
     * @param  array  $errors
     * @return \Symfony\Component\HttpFoundation\Response
     */
    public function response(array $errors)
    {
        if (($this->ajax() && ! $this->pjax()) || $this->wantsJson())
        {
            return new JsonResponse($errors, 422);
        }
        else
        {
            exit('Not a valid http request');
        }
    }
}
