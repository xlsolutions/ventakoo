<?php

namespace App\Http\Requests;

use App\Providers\FlashMessageServiceProvider;
use Illuminate\Contracts\Validation\Validator;

class RequestUserLogin extends Request
{
    /**
     * Get the validation rules that apply to the request.
     *
     * @return array
     */
    public function rules()
    {
        switch($this->method())
        {
            case "POST":
                return [
                    'email_address' => 'required|email|exists:t_user_contact,email_address',
                    'password' => 'required',
                ];
        }
    }
    
    /**
     * Get the error messages for the defined validation rules.
     *
     * @return array
     */
    public function messages()
    {
        return [
            '*.required' => "Incorrect email or password.",
            '*.exists' => "Incorrect email or password.",
            '*.email' => "Incorrect email or password."
        ];
    }

    /**
     * Format validation errors
     *
     * @param Validator $validator
     *
     * @return array
     */
    protected function formatErrors(Validator $validator)
    {
        $arrError = $validator->errors()->all();

        foreach($arrError as $strError)
        {
            FlashMessageServiceProvider::danger($strError);
        }

        return $arrError;
    }
}
