<?php

namespace App\Listeners;

use App\Events\Event;
use App\Providers\AuthServiceProvider;
use Illuminate\Support\Facades\Session;

class UserLogoutListener extends Listener
{
    protected $strClassEvent = 'App\Events\UserLogoutEvent';

    public function handle(Event $objEvent)
    {
        try
        {
            $this->checkEventInstance($objEvent);
            
            if (AuthServiceProvider::getUserAuth()->getUserId())
            {
                Session::forget('SECRET_AUTH');
                Session::forget('user_hash_id');
                Session::save();
            }
            else
            {
                throw new \Exception("Secret auth user_hash_id does not match user_hash_id session");
            }
        }
        catch (\Exception $objError)
        {
            syslog(LOG_CRIT, __CLASS__ . ':' . __METHOD__ . ': ' . $objError->getMessage());
        }
    }
}