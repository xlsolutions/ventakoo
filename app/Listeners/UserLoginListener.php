<?php

namespace App\Listeners;

use App\Classes\UserAuth;
use App\Events\CancelAbandonedOrderEvent;
use App\Events\Event;
use App\Models\Constants\CCity;
use App\Models\Constants\CCountry;
use App\Models\Constants\CState;
use App\Models\Constants\CUserContactInformation;
use App\Models\Users\TUserContact;
use App\Models\Users\TUserContactAttribute;
use App\Models\Users\TUserInformation;
use App\Models\Users\TUserLocation;
use App\Models\Users\UserRole;
use Illuminate\Support\Facades\Crypt;
use DB;
use Illuminate\Support\Facades\Session;

class UserLoginListener extends Listener
{
    protected $strClassEvent = 'App\Events\UserLoginEvent';

    public function handle(Event $objEvent)
    {
        try
        {
            DB::beginTransaction();

            $this->checkEventInstance($objEvent);

            $objTUserInformation = $objEvent->getTUserInformation();

            if (!$objEvent->getRefreshAuth())
            {
                $objTUserInformation->last_login = date('Y-m-d H:i:s');
                $objTUserInformation->save();   
            }

            UserRole::loadPermsByUserId($objTUserInformation['user_id']);

            if (UserRole::getRoleByUserId($objTUserInformation['user_id']) === 'SELLER')
            {
                $objUserAuth = $this->initializeSeller($objTUserInformation);
            }
            else
            {
                $objUserAuth = $this->initializeBuyer($objTUserInformation);
            }

            Session::put('SECRET_AUTH', Crypt::encrypt(serialize($objUserAuth)));
            Session::put('user_hash_id', Crypt::encrypt($objTUserInformation['user_id']));
            Session::save();

            DB::commit();

            $objCancelAbandonedOrderEvent = new CancelAbandonedOrderEvent();
            $objCancelAbandonedOrderEvent->setIntUserId($objTUserInformation['user_id']);
            event($objCancelAbandonedOrderEvent);
        }
        catch (\Exception $objError)
        {
            DB::rollBack();

            syslog(LOG_CRIT, __CLASS__ . ':' . __METHOD__ . ': ' . $objError->getMessage());
        }
    }
    
    private function initializeSeller(TUserInformation $objTUserInformation)
    {
        $intUserId = $objTUserInformation['user_id'];
        
        $objTUserContact = TUserContact::where('user_information_id', $objTUserInformation['user_information_id'])->first();

        $objTUserLocation = TUserLocation::find($objTUserContact['user_location_id']);
        $objCCountry = CCountry::find($objTUserLocation->country_id);
        $objCState = CState::find($objTUserLocation->state_id);
        $objCCity = CCity::find($objTUserLocation->city_id);

        $objCUserContactInformation = CUserContactInformation::where('handle', 'ADDRESS')->first();
        $intContactAddressId = $objCUserContactInformation['user_contact_information_id'];

        $objTUserContactAddress = TUserContactAttribute::where('user_contact_information_id', $intContactAddressId)
            ->where('user_contact_id', $objTUserContact['user_contact_id'])
            ->first();

        $objCUserContactInformation = CUserContactInformation::where('handle', 'ZIP')->first();
        $intContactZipId = $objCUserContactInformation['user_contact_information_id'];

        $objTUserContactZip = TUserContactAttribute::where('user_contact_information_id', $intContactZipId)
            ->where('user_contact_id', $objTUserContact['user_contact_id'])
            ->first();

        return new UserAuth(
            $intUserId,
            UserRole::getRoleByUserId($intUserId),
            $objTUserInformation['firstname'],
            $objTUserInformation['lastname'],
            $objTUserContactAddress['value'],
            $objCCity->city_name,
            $objCState->state_name,
            $objTUserContactZip['value'],
            $objCCountry->country_code
        );
    }
    
    private function initializeBuyer(TUserInformation $objTUserInformation)
    {
        return new UserAuth(
            $objTUserInformation['user_id'],
            UserRole::getRoleByUserId($objTUserInformation['user_id']),
            $objTUserInformation['firstname'],
            $objTUserInformation['lastname']
        );
    }
}