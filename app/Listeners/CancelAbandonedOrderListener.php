<?php

namespace App\Listeners;

use App\Events\Event;
use App\Models\Constants\COrderStatus;
use App\Models\Order\TOrder;
use DB;

class CancelAbandonedOrderListener extends Listener
{
    protected $strClassEvent = 'App\Events\CancelAbandonedOrderEvent';

    public function handle(Event $objEvent)
    {
        try
        {
            $this->checkEventInstance($objEvent);

            DB::beginTransaction();

            $arrTOrder = TOrder::where('user_id', $objEvent->getIntUserId())
                ->where('order_status_id', DB::raw("(SELECT order_status_id FROM c_order_status WHERE handle = 'NEW')"))
                ->get();

            if ($arrTOrder)
            {
                $objCOrderStatus = COrderStatus::where('handle', 'CANCELLED')
                    ->first();

                foreach($arrTOrder as $objTOrder)
                {
                    if (date('Y-m-d H:i:s', strtotime($objTOrder->created_at . ' +1 hour')) <= date('Y-m-d H:i:s'))
                    {
                        $objTOrder->order_status_id = $objCOrderStatus['order_status_id'];
                        $objTOrder->save();
                    }
                }
            }

            DB::commit();
        }
        catch (\Exception $objError)
        {
            DB::rollBack();

            syslog(LOG_CRIT, __CLASS__ . ':' . __METHOD__ . ': ' . $objError->getMessage());
        }
    }
}