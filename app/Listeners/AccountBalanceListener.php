<?php

namespace App\Listeners;

use App\Events\Event;
use App\Models\Users\TAccountBalance;
use App\Providers\AuthServiceProvider;
use DB;

class AccountBalanceListener extends Listener
{
    protected $strClassEvent = 'App\Events\AccountBalanceEvent';
    
    public function handle(Event $objEvent)
    {
        try
        {
            $this->checkEventInstance($objEvent);

            DB::beginTransaction();

            $intCurrentBalanceAmount = AuthServiceProvider::getAccountBalanceByUserId($objEvent->getIntUserId());

            $intBalance = $objEvent->getBalance();
                
            if ($objEvent->getCredit() === true && $objEvent->getDebit() === false)
            {
                $intBalance += $intCurrentBalanceAmount;
            }

            if ($objEvent->getDebit() === true && $objEvent->getCredit() === false)
            {
                $intBalance = $intCurrentBalanceAmount - $intBalance;
                
                if ($intBalance < 0)
                {
                    throw new \Exception("Balance amount is negative. Please check finance transaction table for User Id: {$objEvent->getIntUserId()}");
                }
            }
            
            TAccountBalance::updateOrCreate(
                [
                    'user_id' => $objEvent->getIntUserId()
                ],
                [
                    'user_id' => $objEvent->getIntUserId(),
                    'amount' => $intBalance
                ]
            );

            DB::commit();
        }
        catch (\Exception $objError)
        {
            DB::rollBack();

            syslog(LOG_CRIT, __CLASS__ . ':' . __METHOD__ . ': ' . $objError->getMessage());
        }
    }
}