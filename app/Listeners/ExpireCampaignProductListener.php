<?php

namespace App\Listeners;

use App\Events\AdPostPublishEvent;
use App\Events\Event;
use App\Models\AdPost\TAdPostExpiration;

class ExpireCampaignProductListener extends Listener
{
    protected $strClassEvent = 'App\Events\ExpireCampaignProductEvent';

    public function handle(Event $objEvent)
    {
        try
        {
            $this->checkEventInstance($objEvent);

            $objTAdPostExpiration = TAdPostExpiration::where('ad_post_id', $objEvent->getCampaignProductId())->first();

            if ($objTAdPostExpiration)
            {
                if (is_null($objTAdPostExpiration->time_end) && $objTAdPostExpiration->time_duration > 0)
                {
                    $intDateTimeNow = strtotime(date('Y-m-d H:i:s'));

                    $intRunningDuration = $intDateTimeNow - strtotime($objTAdPostExpiration->time_start);

                    if($intRunningDuration >= $objTAdPostExpiration->time_duration)
                    {
                        $objAdPostPublishEvent = new AdPostPublishEvent($objEvent->getCampaignProductId());
                        $objAdPostPublishEvent->setForceExpireCampaign(true);
                        event($objAdPostPublishEvent);
                    }
                }
            }
        }
        catch (\Exception $objError)
        {
            syslog(LOG_CRIT, __CLASS__ . ':' . __METHOD__ . ': ' . $objError->getMessage());
        }
    }
}