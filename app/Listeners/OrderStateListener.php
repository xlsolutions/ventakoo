<?php

namespace App\Listeners;

use App\Events\Event;
use App\Models\Finance\TFinanceTransaction;
use App\Models\Order\TOrder;
use DB;

class OrderStateListener extends Listener
{
    protected $strClassEvent = 'App\Events\OrderStateEvent';

    public function handle(Event $objEvent)
    {
        try
        {
            DB::beginTransaction();

            $this->checkEventInstance($objEvent);

            $objTOrder = TOrder::find($objEvent->getOrderId());
            $objTOrder->order_status_id = $objEvent->getOrderStatusId();
            $objTOrder->save();

            TFinanceTransaction::create([
                'order_id' => $objEvent->getOrderId(),
                'note' => "Updating order state from {$objEvent->getCurrentState()} to {$objEvent->getNewState()}"
            ]);
            
            DB::commit();
        }
        catch (\Exception $objError)
        {
            DB::rollBack();

            syslog(LOG_CRIT, __CLASS__ . ':' . __METHOD__ . ': ' . $objError->getMessage());
        }
    }
}