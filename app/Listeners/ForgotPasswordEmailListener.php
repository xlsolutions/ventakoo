<?php

namespace App\Listeners;

use App\Events\Event;
use Illuminate\Support\Facades\Mail;

class ForgotPasswordEmailListener extends Listener
{
    protected $strClassEvent = 'App\Events\ForgotPasswordEmailEvent';

    /**
     * Handle the event
     *
     * @param $objEvent Event must be an instance of $strClassEvent
     *
     * @return void
     *
     * */
    public function handle(Event $objEvent)
    {
        try
        {
            $this->checkEventInstance($objEvent);

            $arrData = [
                'email' => $objEvent->getEmail(),
                'code' => $objEvent->getCode()
            ];

            Mail::send(['email.html.forgot_password', 'email.text.forgot_password'], $arrData, function($objMessage) use ($objEvent) {
                $objMessage->from($objEvent->getFromAddress(), $objEvent->getFrom());
                $objMessage->sender($objEvent->getFromAddress(), $objEvent->getFrom());
                $objMessage->to($objEvent->getEmail(), $objEvent->getEmail());
                $objMessage->subject('Forgot password request');
            });
        }
        catch(\Exception $objError)
        {
            syslog(LOG_CRIT, __CLASS__ . ':' . __METHOD__ . ':' . $objError->getMessage());
        }
    }
}