<?php

namespace App\Listeners;

use App\Events\Event;

abstract class Listener
{
    protected $strClassEvent = 'App\Events\Event';
    
    abstract public function handle(Event $objEvent);
    
    protected function checkEventInstance(Event $objEvent)
    {
        if (!$objEvent instanceof $this->strClassEvent)
        {
            throw new \Exception("Event is not an instance of {$this->strClassEvent}");
        }
    }
}
