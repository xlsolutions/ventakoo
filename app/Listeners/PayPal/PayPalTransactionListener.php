<?php

namespace App\Listeners\PayPal;

use App\Events\Event;
use App\Listeners\Listener;
use App\Models\PayPal\TPayPalBuyer;
use App\Models\PayPal\TPayPalBuyerInfo;
use App\Models\PayPal\TPayPalTrans;
use App\Models\PayPal\TPayPalTransAddress;
use App\Models\PayPal\TPayPalTransAmount;
use App\Models\PayPal\TPayPalTransItem;
use App\Models\PayPal\TPayPalTransPayerInfo;
use DB;

class PayPalTransactionListener extends Listener
{
    protected $strClassEvent = 'App\Events\PayPal\PayPalTransactionEvent';

    public function handle(Event $objEvent)
    {
        try
        {
            $this->checkEventInstance($objEvent);

            $objPayPalTrans = $objEvent->getPayPalTrans();

            DB::beginTransaction();

            $objTPayPalTrans = TPayPalTrans::create([
                'txn_id' => $objPayPalTrans->getTransactionId(),
                'order_id' => $objPayPalTrans->getOrderId(),
                'ipn_track_id' => $objPayPalTrans->getIpnTrackId(),
                'protection_eligibility' => $objPayPalTrans->getProtectionEligibility(),
                'payment_date' => $objPayPalTrans->getPaymentDate(),
                'payment_status' => $objPayPalTrans->getPaymentStatus(),
                'pending_reason' => $objPayPalTrans->getPendingReason(),
                'reason_code' => $objPayPalTrans->getReasonCode(),
                'verify_sign' => $objPayPalTrans->getVerifySign(),
                'payment_type' => $objPayPalTrans->getPaymentType(),
                'txn_type' => $objPayPalTrans->getTransactionType(),
                'quantity' => $objPayPalTrans->getQuanity()
            ]);

            $intPayPalTransId = $objTPayPalTrans['paypal_trans_id'];

            $objPayPalTransItem = $objEvent->getPayPalTransItem();

            TPayPalTransItem::create([
                'paypal_trans_id' => $intPayPalTransId,
                'item_name' => $objPayPalTransItem->getItemName(),
                'item_number' => $objPayPalTransItem->getItemNumber()
            ]);

            $objPayPalTransAmount = $objEvent->getPayPalTransAmount();
            
            TPayPalTransAmount::create([
                'paypal_trans_id' => $intPayPalTransId,
                'mc_currency' => $objPayPalTransAmount->getMcCurrency(),
                'mc_gross' => $objPayPalTransAmount->getMcGross(),
                'exchange_rate' => $objPayPalTransAmount->getExchangeRate(),
                'settle_amount' => $objPayPalTransAmount->getSettleAmount(),
                'settle_currency' => $objPayPalTransAmount->getSettleCurrency(),
                'mc_fee' => $objPayPalTransAmount->getMcFee()
            ]);
            
            $objPayPalTransPayerInfo = $objEvent->getPayPalTransPayerInfo();

            TPayPalTransPayerInfo::create([
                'paypal_trans_id' => $intPayPalTransId,
                'first_name' => $objPayPalTransPayerInfo->getFirstName(),
                'last_name' => $objPayPalTransPayerInfo->getLastName(),
                'payer_status' => $objPayPalTransPayerInfo->getPayerStatus(),
                'residence_country' => $objPayPalTransPayerInfo->getResidenceCountry()
            ]);
            
            $objPayPalTransAddress = $objEvent->getPayPalTransAddress();

            TPayPalTransAddress::create([
                'paypal_trans_id' => $intPayPalTransId,
                'address_status' => $objPayPalTransAddress->getStatus(),
                'address_street' => $objPayPalTransAddress->getStreet(),
                'address_zip' => $objPayPalTransAddress->getZip(),
                'address_country_code' => $objPayPalTransAddress->getCountryCode(),
                'address_name' => $objPayPalTransAddress->getName(),
                'address_country' => $objPayPalTransAddress->getCountry(),
                'address_city' => $objPayPalTransAddress->getCity(),
                'address_state' => $objPayPalTransAddress->getState()
            ]);

            $objTPayPalBuyer = TPayPalBuyer::updateOrCreate([
                'user_id' => $objEvent->getIntUserId()
            ]);

            $intBuyerId = $objTPayPalBuyer['buyer_id'];
            
            $objPayPalBuyerInfo = $objEvent->getPayPalBuyerInfo();
            
            TPayPalBuyerInfo::updateOrCreate([
                'buyer_id' => $intBuyerId,
                'payer_email' => $objPayPalBuyerInfo->getPayerEmail(),
                'payer_id' => $objPayPalBuyerInfo->getPayerId()
            ]);

            DB::commit();
        }
        catch (\Exception $objError)
        {
            DB::rollBack();

            syslog(LOG_CRIT, __CLASS__ . ':' . __METHOD__ . ': ' . $objError->getMessage());
        }
    }
}