<?php

namespace App\Listeners\PayPal;

use App\Classes\PayPal\PayPalBuyer;
use App\Classes\PayPal\PayPalBuyerInfo;
use App\Classes\PayPal\PayPalTrans;
use App\Classes\PayPal\PayPalTransAddress;
use App\Classes\PayPal\PayPalTransAmount;
use App\Classes\PayPal\PayPalTransItem;
use App\Classes\PayPal\PayPalTransPayerInfo;
use App\Events\AccountBalanceEvent;
use App\Events\Event;
use App\Events\OrderState;
use App\Events\OrderStateEvent;
use App\Events\PayPal\PayPalTransactionEvent;
use App\Listeners\Listener;
use App\Models\Constants\COrderStatus;
use App\Models\Finance\TFinanceTransaction;
use App\Models\Order\TOrder;
use App\Models\Users\TProductPlan;
use App\Providers\AuthServiceProvider;

class PayPalIPNListener extends Listener
{
    protected $strClassEvent = 'App\Events\PayPal\PayPalIPNEvent';

    private $intOrderId;
    private $intUserId;
    private $decGrossAmount;
    private $strRequestParam;

    public function handle(Event $objEvent)
    {
        syslog(LOG_DEBUG, __CLASS__ . ':' . __METHOD__ . ': START HANDLE IPN LISTENER');

        $arrRequest = $objEvent->getRequest();

        $this->intOrderId = $objEvent->getOrderId();
        
        $this->intUserId = $objEvent->getIntUserId();
        
        $decIpnGrossAmount = (float)$arrRequest['mc_gross'];
        
        $this->decGrossAmount = $objEvent->getOrderGrossAmount();
        
        $this->strRequestParam = $objEvent->getRequestParam();
        
        try
        {
            $this->checkEventInstance($objEvent);

            event(new OrderStateEvent($this->intOrderId, $this->getOrderCurrentState(), OrderState::INITIATING_PAYMENT));

            // Post the data back to PayPal, using curl. Throw exceptions if errors occur.
            $objCurl = curl_init(env('PAYPAL_IPN_URL'));

            curl_setopt_array($objCurl, [
                CURLOPT_HTTP_VERSION => CURL_HTTP_VERSION_1_1,
                CURLOPT_POST => 1,
                CURLOPT_RETURNTRANSFER => 1,
                CURLOPT_POSTFIELDS => $objEvent->getRequestParam(),
                CURLOPT_SSLVERSION => 6,
                CURLOPT_SSL_VERIFYPEER => FALSE, //@todo: buy ssl
                CURLOPT_SSL_VERIFYHOST => FALSE, //@todo: buy ssl
                CURLOPT_FORBID_REUSE => 1,
                CURLOPT_CONNECTTIMEOUT => 120,
                CURLOPT_HTTPHEADER => [
                    'Connection: Close'
                ]
            ]);

            $strResponse = curl_exec($objCurl);

            $bolError = false;

            if (!$strResponse)
            {
                $bolError = true;

                $intError = curl_errno($objCurl);
                $strError = curl_error($objCurl);

                syslog(LOG_CRIT, __CLASS__ . ':' . __METHOD__ . ": PAYPAL_CURL_ERROR: {$strError}, ERRNO: {$intError} | Changing payment to 'REVIEW' state. Check Order #: {$this->intOrderId}");
            }
            else
            {
                $arrInfo = curl_getinfo($objCurl);

                if ($arrInfo['http_code'] != 200)
                {
                    $bolError = true;

                    syslog(LOG_CRIT, __CLASS__ . ':' . __METHOD__ . ": HTTP_RESPONSE_ERROR_CODE: {$arrInfo['http_code']} | Changing payment to 'REVIEW' state. Check Order #: {$this->intOrderId}");
                }
                else if ($strResponse != "VERIFIED")
                {
                    $bolError = true;

                    syslog(LOG_CRIT, __CLASS__ . ':' . __METHOD__ . ": RESPONSE_INVALID: {$strResponse} | Changing payment to 'REVIEW' state. Check Order #: {$this->intOrderId} | Request: {$this->strRequestParam}");
                }
                else if ($decIpnGrossAmount != $this->decGrossAmount)
                {
                    $bolError = true;

                    syslog(LOG_CRIT, __CLASS__ . ':' . __METHOD__ . ": DIFF_AMOUNT: Order({$this->decGrossAmount}), IPN({$decIpnGrossAmount}) | Changing payment to 'REVIEW' state. Check Order #: {$this->intOrderId} | Request: {$this->strRequestParam}");
                }
                else if ($arrRequest['receiver_id'] != getenv('PAYPAL_MERCHANT_ID'))
                {
                    $bolError = true;

                    syslog(LOG_CRIT, __CLASS__ . ':' . __METHOD__ . ": INVALID_MERCHANT: Receiver {$arrRequest['receiver_id']} | Changing payment to 'REVIEW' state. Check Order #: {$this->intOrderId} | Request: {$this->strRequestParam}");
                }
            }

            $this->recordTransaction($arrRequest);

            if ($bolError === false)
            {
                $this->checkPaymentStatus($arrRequest['payment_status']);
            }
            else
            {
                event(new OrderStateEvent($this->intOrderId, $this->objOrderStatus->handle, OrderState::REVIEW));
            }

            curl_close($objCurl);
        }
        catch (\Exception $objError)
        {
            syslog(LOG_CRIT, __CLASS__ . ':' . __METHOD__ . ': PAYPAL_EXCEPTION: ' . $objError->getMessage() . " | Changing payment to 'REVIEW' state. Check Order #: {$this->intOrderId}");

            event(new OrderStateEvent($this->intOrderId, $this->objOrderStatus->handle, OrderState::REVIEW));
        }

        syslog(LOG_DEBUG, __CLASS__ . ':' . __METHOD__ . ': END HANDLE IPN LISTENER');
    }

    /**
     * @return string
     * */
    private function getOrderCurrentState()
    {
        $objTOrder = TOrder::findOrFail($this->intOrderId);
        $this->objOrderStatus = COrderStatus::findOrFail($objTOrder->order_status_id);

        return $this->objOrderStatus->handle;
    }
    
    /**
     * @readme:
     *   The status of the payment:
     *   Canceled_Reversal: A reversal has been canceled. For example, you won a dispute with the customer, and the funds for the transaction that was reversed have been returned to you.
     *   Completed: The payment has been completed, and the funds have been added successfully to your account balance.
     *   Created: A German ELV payment is made using Express Checkout.
     *   Denied: The payment was denied. This happens only if the payment was previously pending because of one of the reasons listed for the pending_reason variable or the Fraud_Management_Filters_x variable.
     *   Expired: This authorization has expired and cannot be captured.
     *   Failed: The payment has failed. This happens only if the payment was made from your customer's bank account.
     *   Pending: The payment is pending. See pending_reason for more information.
     *   Refunded: You refunded the payment.
     *   Reversed: A payment was reversed due to a chargeback or other type of reversal. The funds have been removed from your account balance and returned to the buyer. The reason for the reversal is specified in the ReasonCode element.
     *   Processed: A payment has been accepted.
     *   Voided: This authorization has been voided.
     *
     * @param $strPaymentStatus string
     *
     * @return void
     *
     * */
    private function checkPaymentStatus($strPaymentStatus)
    {
        syslog(LOG_DEBUG, __CLASS__ . ':' . __METHOD__ . ': Got payment status: ' . $strPaymentStatus . ' for order id ' . $this->intOrderId);

        switch ($strPaymentStatus)
        {
            case 'Refunded':
                event(new OrderStateEvent($this->intOrderId, $this->getOrderCurrentState(), OrderState::COMPLETED));

                $objAccountBalanceEvent = new AccountBalanceEvent();
                $objAccountBalanceEvent->setIntUserId($this->intUserId);
                $objAccountBalanceEvent->setDebit(true);
                $objAccountBalanceEvent->setBalance($this->decGrossAmount);

                break;

            case 'Canceled_Reversal':
            case 'Completed':
                event(new OrderStateEvent($this->intOrderId, $this->getOrderCurrentState(), OrderState::COMPLETED));
                
                $objOrder = TOrder::select(
                    't_order_type.plan_id',
                    'c_product_plan.name AS planName',
                    'c_product_plan.handle AS planHandle'
                )
                    ->join('t_order_item', 't_order_item.order_id', '=', 't_order.order_id')
                    ->join('t_order_type', 't_order_type.order_type_id', '=', 't_order_item.order_type_id')
                    ->join('c_product_plan', 'c_product_plan.product_plan_id', '=', 't_order_type.plan_id')
                    ->find($this->intOrderId);

                $objTProductPlan = TProductPlan::where('user_id', $this->intUserId)
                    ->where('product_plan_id', $objOrder->plan_id)
                    ->first();

                $objCurrentCampaignPlan = AuthServiceProvider::getCampaignPlanModelByUserId($this->intUserId);

                if (!$objTProductPlan)
                {
                    syslog(LOG_DEBUG, __CLASS__ . ':' . __METHOD__ . ": SWITCH PLAN ({$objOrder->planHandle}) | Order Id: " . $this->intOrderId);

                    TProductPlan::create([
                        'user_id' => $this->intUserId,
                        'product_plan_id' => $objOrder->plan_id
                    ]);

                    TFinanceTransaction::create([
                        'order_id' => $this->intOrderId,
                        'note' => "Account switch to ({$objOrder->planName}) plan"
                    ]);
                }
                else if ($objCurrentCampaignPlan['handle'] == $objOrder->planHandle)
                {
                    syslog(LOG_DEBUG, __CLASS__ . ':' . __METHOD__ . ": EXTEND PLAN ({$objCurrentCampaignPlan['handle']}) | Order Id: " . $this->intOrderId);

                    TFinanceTransaction::create([
                        'order_id' => $this->intOrderId,
                        'note' => "Account ({$objOrder->planName}) plan extended"
                    ]);
                }
                else if ($objTProductPlan)
                {
                    syslog(LOG_DEBUG, __CLASS__ . ':' . __METHOD__ . "': SWITCH PLAN from {$objCurrentCampaignPlan['handle']} to {$objOrder->planHandle} | Order Id: '" . $this->intOrderId);

                    $objTProductPlan->delete();

                    TProductPlan::create([
                        'user_id' => $this->intUserId,
                        'product_plan_id' => $objOrder->plan_id
                    ]);

                    TFinanceTransaction::create([
                        'order_id' => $this->intOrderId,
                        'note' => "Account switch from {$objCurrentCampaignPlan['name']} to {$objOrder->planName} plan"
                    ]);
                }

                $objAccountBalanceEvent = new AccountBalanceEvent();
                $objAccountBalanceEvent->setIntUserId($this->intUserId);
                $objAccountBalanceEvent->setCredit(true);
                $objAccountBalanceEvent->setBalance($this->decGrossAmount);
                
                event($objAccountBalanceEvent);
                
                break;

            case 'Processed':
            case 'Created':
            case 'Reversed':
                event(new OrderStateEvent($this->intOrderId, $this->getOrderCurrentState(), OrderState::REVIEW));

                break;

            case 'Failed':
            case 'Expired':
            case 'Voided':
                event(new OrderStateEvent($this->intOrderId, $this->getOrderCurrentState(), OrderState::CANCELLED));

                break;

            case 'Denied':
                event(new OrderStateEvent($this->intOrderId, $this->getOrderCurrentState(), OrderState::DECLINED));

                break;
        }
    }

    /**
     * @param $arrRequest array
     *
     * @return void
     * */
    private function recordTransaction(Array $arrRequest)
    {
        syslog(LOG_DEBUG, __CLASS__ . ':' . __METHOD__ . ': Saving transaction record for order id (' . $this->intOrderId . '): ' . "\n" . $this->strRequestParam);

        event(new OrderStateEvent($this->intOrderId, $this->getOrderCurrentState(), OrderState::PROCESSING));

        $objPayPalTrans = new PayPalTrans(
            $arrRequest['txn_id'],
            $this->intOrderId,
            $arrRequest['ipn_track_id'],
            $arrRequest['protection_eligibility'],
            $arrRequest['payment_date'],
            $arrRequest['payment_status'],
            $arrRequest['verify_sign'],
            $arrRequest['payment_type'],
            isset($arrRequest['pending_reason']) ? $arrRequest['pending_reason'] : null,
            isset($arrRequest['reason_code']) ? $arrRequest['reason_code'] : null,
            $arrRequest['txn_type'],
            $arrRequest['quantity']
        );

        $objPayPalTransItem = new PayPalTransItem($arrRequest['item_name'], $arrRequest['item_number']);

        $objPayPalTransAmount = new PayPalTransAmount(
            $arrRequest['mc_currency'],
            $arrRequest['mc_gross'],
            isset($arrRequest['exchange_rate']) ? $arrRequest['exchange_rate'] : null,
            isset($arrRequest['settle_amount']) ? $arrRequest['settle_amount'] : null,
            isset($arrRequest['settle_currency']) ? $arrRequest['settle_currency'] : null,
            isset($arrRequest['mc_fee']) ? $arrRequest['mc_fee'] : null
        );

        $objPayPalTransPayerInfo = new PayPalTransPayerInfo(
            $arrRequest['first_name'],
            $arrRequest['last_name'],
            $arrRequest['payer_status'],
            $arrRequest['residence_country']
        );

        $objPayPalTransAddress = new PayPalTransAddress(
            $arrRequest['address_status'],
            $arrRequest['address_street'],
            $arrRequest['address_zip'],
            $arrRequest['address_country_code'],
            $arrRequest['address_name'],
            $arrRequest['address_country'],
            $arrRequest['address_city'],
            $arrRequest['address_state']
        );

        $objPayPalBuyerInfo = new PayPalBuyerInfo($arrRequest['payer_email'], $arrRequest['payer_id']);

        $objPayPalTransactionEvent = new PayPalTransactionEvent();
        $objPayPalTransactionEvent->setIntUserId($this->intUserId);
        $objPayPalTransactionEvent->setPayPalTrans($objPayPalTrans);
        $objPayPalTransactionEvent->setPayPalTransItem($objPayPalTransItem);
        $objPayPalTransactionEvent->setPayPalTransAmount($objPayPalTransAmount);
        $objPayPalTransactionEvent->setPayPalTransPayerInfo($objPayPalTransPayerInfo);
        $objPayPalTransactionEvent->setPayPalTransAddress($objPayPalTransAddress);
        $objPayPalTransactionEvent->setPayPalBuyerInfo($objPayPalBuyerInfo);
        
        event($objPayPalTransactionEvent);
    }
}