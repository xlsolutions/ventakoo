<?php

namespace App\Listeners;

use App\Events\AdPostReviewStatus;
use App\Events\Event;
use App\Models\AdPost\TAdPostNote;
use App\Models\Constants\CAdPostStatus;
use DB;

class AdPostReviewListener extends Listener
{
    protected $strClassEvent = 'App\Events\AdPostReviewEvent';

    public function handle(Event $objEvent)
    {
        try
        {
            DB::beginTransaction();

            $this->checkEventInstance($objEvent);

            if ($objEvent->getReviewStatus() === AdPostReviewStatus::FOR_REVIEW)
            {
                $strNote = 'Your product campaign has been submitted for review.';
            }
            else if ($objEvent->getReviewStatus() === AdPostReviewStatus::REVIEWING)
            {
                $strNote = 'Your product campaign is being review.';
            }
            else if ($objEvent->getReviewStatus() === AdPostReviewStatus::DECLINED)
            {
                $strNote = 'Your product campaign has been declined.';
            }
            else if ($objEvent->getReviewStatus() === AdPostReviewStatus::APPROVED)
            {
                $strNote = 'Your product campaign has been approved.';
            }
            else if ($objEvent->getReviewStatus() === AdPostReviewStatus::CANCELLED)
            {
                $strNote = 'Your product campaign has been cancelled.';
            }
            else if ($objEvent->getReviewStatus() === AdPostReviewStatus::START)
            {
                $strNote = 'Your product campaign has been started.';
            }
            else if ($objEvent->getReviewStatus() === AdPostReviewStatus::PAUSE)
            {
                $strNote = 'Your product campaign has been paused.';
            }
            else if ($objEvent->getReviewStatus() === AdPostReviewStatus::COMPLETED)
            {
                $strNote = 'Your product campaign has been completed.';
            }

            $objCAdPostStatus = CAdPostStatus::where('handle', $objEvent->getReviewStatus())->first();

            TAdPostNote::create([
                'ad_post_id' => $objEvent->getAdPostId(),
                'ad_post_status_id' => $objCAdPostStatus['ad_post_status_id'],
                'user_id' => $objEvent->getIntUserId(), //if null it was processed by the system
                'note' => $strNote
            ]);

            DB::commit();
        }
        catch (\Exception $objError)
        {
            DB::rollBack();

            syslog(LOG_CRIT, __CLASS__ . ':' . __METHOD__ . ': ' . $objError->getMessage());
        }
    }
}