<?php

namespace App\Listeners;

use App\Events\Event;
use App\Models\Constants\COrderStatus;
use App\Models\Order\TOrder;
use DB;

class CancelRecentOrderListener extends Listener
{
    protected $strClassEvent = 'App\Events\CancelRecentOrderEvent';

    public function handle(Event $objEvent)
    {
        try
        {
            $this->checkEventInstance($objEvent);

            DB::beginTransaction();

            $objTOrder = TOrder::where('user_id', $objEvent->getIntUserId())
                ->where('order_status_id', DB::raw("(SELECT order_status_id FROM c_order_status WHERE handle = 'NEW')"))
                ->orderBy('order_id', 'desc')
                ->limit(1)
                ->first();

            $objCOrderStatus = COrderStatus::where('handle', 'CANCELLED')
                ->first();
            
            $objTOrder->order_status_id = $objCOrderStatus['order_status_id'];
            $objTOrder->save();

            DB::commit();
        }
        catch (\Exception $objError)
        {
            DB::rollBack();

            syslog(LOG_CRIT, __CLASS__ . ':' . __METHOD__ . ': ' . $objError->getMessage());
        }
    }
}