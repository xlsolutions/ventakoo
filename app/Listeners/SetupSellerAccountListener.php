<?php

namespace App\Listeners;

use App\Events\Event;
use App\Models\Constants\CPerm;
use App\Models\Constants\CProductInformation;
use App\Models\Constants\CProductPlan;
use App\Models\Constants\CServiceInformation;
use App\Models\Users\TPermUser;
use App\Models\Users\TProductPlan;
use App\Models\Users\TUserProductAndServiceLinker;
use DB;

class SetupSellerAccountListener extends Listener
{
    protected $strClassEvent = 'App\Events\SetupSellerAccountEvent';

    /**
     * Handle the event
     *
     * @param $objEvent Event must be an instance of $strClassEvent
     *
     * @return void
     *
     * */
    public function handle(Event $objEvent)
    {
        try
        {
            DB::beginTransaction();
            
            $this->checkEventInstance($objEvent);
            
            $this->setProductAndServiceDefaultAttributes($objEvent->getIntUserId());
            $this->setPermissions($objEvent->getIntUserId());
            $this->setDefaultAccountPlan($objEvent->getIntUserId());

            DB::commit();
        }
        catch(\Exception $objError)
        {
            DB::rollBack();
            
            syslog(LOG_CRIT, __CLASS__ . ':' . __METHOD__ . ": Failed to setup account for user id ({$objEvent->getIntUserId()}), got exception: {$objError->getMessage()}");
        }
    }

    /**
     * Default product and service attribute for a seller account
     *
     * @param $intUserId integer
     *
     * @return void
     *
     * */
    private function setProductAndServiceDefaultAttributes($intUserId)
    {
        $arrDefaultAttributeHandles = [
            'COLOR',
            'DESCRIPTION',
            'NAME',
            'SIZE',
            'IMAGE_THUMBNAIL',
            'IMAGE_ORIGINAL',
            'IMAGE_GALLERY',
        ];

        $objCProductInformationSet = CProductInformation::whereIn('handle', $arrDefaultAttributeHandles)->where('status', 1)->get()->toArray();

        if ($objCProductInformationSet)
        {
            foreach($objCProductInformationSet as $objCProductInformation)
            {

                TUserProductAndServiceLinker::create([
                    'user_id' => $intUserId,
                    'product_information_id' => $objCProductInformation['product_information_id']
                ]);
            }
        }

        $objCServiceInformationSet = CServiceInformation::whereIn('handle', $arrDefaultAttributeHandles)->where('status', 1)->get()->toArray();

        if ($objCServiceInformationSet)
        {
            foreach($objCServiceInformationSet as $objCServiceInformation)
            {

                TUserProductAndServiceLinker::create([
                    'user_id' => $intUserId,
                    'service_information_id' => $objCServiceInformation['service_information_id']
                ]);
            }
        }
    }

    /**
     * Specific perms for a seller account
     *
     * @param $intUserId integer
     *
     * @return void
     *
     * */
    private function setPermissions($intUserId)
    {
        $arrPerm = [
            'user.account.view',
            'user.account.edit',
            'user.product',
            'user.service',
            'user.order',
            'user.message'
        ];

        foreach($arrPerm as $strPerm)
        {
            $objCPerm = CPerm::where('handle', $strPerm)->first();

            $intPermId = $objCPerm['perm_id'];

            TPermUser::create([
                'user_id' => $intUserId,
                'perm_id' => $intPermId
            ]);
        }
    }

    /**
     * Set the default account plan
     *
     * @param $intUserId integer
     *
     * @return void
     *
     * */
    private function setDefaultAccountPlan($intUserId)
    {
        $objCProductPlan = CProductPlan::where('handle', 'FREE_AD_POST_CAMPAIGN')->first();

        $intProductPlanId = $objCProductPlan['product_plan_id'];

        TProductPlan::create([
            'product_plan_id' => $intProductPlanId,
            'user_id' => $intUserId
        ]);
    }
}