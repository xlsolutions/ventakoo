<?php

namespace App\Listeners;

use App\Events\Event;
use Illuminate\Support\Facades\Mail;

class VerifyRegistrationEmailListener extends Listener
{
    protected $strClassEvent = 'App\Events\VerifyRegistrationEmailEvent';
    
    public function handle(Event $objEvent)
    {
        try
        {
            $this->checkEventInstance($objEvent);

            $arrData = [
                'email' => $objEvent->getEmail(),
                'code' => $objEvent->getCode(),
                'campaignPlan' => (int)$objEvent->getPricingPageSession() //if true, after validating the email, redirect to campaign pricing page upon login 
            ];

            Mail::send(['email.html.verify_email', 'email.text.verify_email'], $arrData, function($objMessage) use ($objEvent) {
                $objMessage->from($objEvent->getFromAddress(), $objEvent->getFrom());
                $objMessage->sender($objEvent->getFromAddress(), $objEvent->getFrom());
                $objMessage->to($objEvent->getEmail(), $objEvent->getEmail());
                $objMessage->subject('Confirm your Ventakoo account');
            });
        }
        catch(\Exception $objError)
        {
            syslog(LOG_CRIT, __CLASS__ . ':' . __METHOD__ . ':' . $objError->getMessage());
        }
    }
}