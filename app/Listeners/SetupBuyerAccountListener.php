<?php

namespace App\Listeners;

use App\Events\Event;
use App\Models\Constants\CPerm;
use App\Models\Users\TPermUser;
use DB;

class SetupBuyerAccountListener extends Listener
{
    protected $strClassEvent = 'App\Events\SetupBuyerAccountEvent';

    /**
     * Handle the event
     *
     * @param $objEvent Event must be an instance of $strClassEvent
     *
     * @return void
     *
     * */
    public function handle(Event $objEvent)
    {
        try
        {
            DB::beginTransaction();

            $this->checkEventInstance($objEvent);
            
            $this->setPermissions($objEvent->getIntUserId());

            DB::commit();
        }
        catch(\Exception $objError)
        {
            DB::rollBack();

            syslog(LOG_CRIT, __CLASS__ . ':' . __METHOD__ . ": Failed to setup account for user id ({$objEvent->getIntUserId()}), got exception: {$objError->getMessage()}");
        }
    }

    /**
     * Specific perms for a seller account
     *
     * @param $intUserId integer
     * 
     * @return void
     * 
     * */
    private function setPermissions($intUserId)
    {
        $arrPerm = [
            'user.account.view',
            'user.account.edit',
            'user.product.view',
            'user.service.view',
            'user.order',
            'user.message'
        ];

        foreach($arrPerm as $strPerm)
        {
            $objCPerm = CPerm::where('handle', $strPerm)->first();

            $intPermId = $objCPerm['perm_id'];

            TPermUser::create([
                'user_id' => $intUserId,
                'perm_id' => $intPermId
            ]);   
        }
    }
}