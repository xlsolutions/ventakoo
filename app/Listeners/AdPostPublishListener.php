<?php

namespace App\Listeners;

use App\Events\AdPostReviewStatus;
use App\Events\Event;
use App\Models\AdPost\TAdPostExpiration;
use App\Events\AdPostReviewEvent;
use App\Providers\AuthServiceProvider;
use DB;

class AdPostPublishListener extends Listener
{
    protected $strClassEvent = 'App\Events\AdPostPublishEvent';

    public function handle(Event $objEvent)
    {
        try
        {
            DB::beginTransaction();

            $this->checkEventInstance($objEvent);

            $objTAdPostExpiration = TAdPostExpiration::where('ad_post_id', $objEvent->getAdPostId())->first();

            if ($objEvent->getStart() === true && $objEvent->getPause() === false)
            {
                if (!$objTAdPostExpiration)
                {
                    TAdPostExpiration::create([
                        'ad_post_id' => $objEvent->getAdPostId(),
                        'time_start' => date('Y-m-d H:i:s'),
                        'time_duration' => $objEvent->getExpiration()
                    ]);
                }
                else
                {
                    $objTAdPostExpiration->time_start = date('Y-m-d H:i:s');
                    $objTAdPostExpiration->time_end = null;
                }
            }

            if ($objEvent->getStart() === false && $objEvent->getPause() === true)
            {
                $objTAdPostExpiration->time_end = date('Y-m-d H:i:s');

                if (is_null($objTAdPostExpiration->time_duration))
                {
                    //unlimited plan so nothing to do here
                }
                else
                {
                    $intDuration = strtotime($objTAdPostExpiration->time_end) - strtotime($objTAdPostExpiration->time_start);

                    if ($objTAdPostExpiration->time_duration - $intDuration < 0)
                    {
                        syslog(LOG_CRIT, 'duration exceeded, expired the campaign immediately');

                        $objTAdPostExpiration->time_duration = 0;

                        $objAdPostReviewEvent = new AdPostReviewEvent($objEvent->getAdPostId());
                        $objAdPostReviewEvent->setIntUserId(AuthServiceProvider::getUserAuth()->getUserId());
                        $objAdPostReviewEvent->setReviewStatus(AdPostReviewStatus::COMPLETED);
                        event($objAdPostReviewEvent);
                    }
                    else
                    {
                        $objTAdPostExpiration->time_duration -= $intDuration;
                    }
                }
            }

            if ($objEvent->getForceExpireCampaign() === true)
            {
                syslog(LOG_CRIT, 'forcing to expire the campaign immediately');

                $objTAdPostExpiration->time_duration = 0;

                $objAdPostReviewEvent = new AdPostReviewEvent($objEvent->getAdPostId());
                $objAdPostReviewEvent->setIntUserId($objAdPostReviewEvent->getUserIdByAdPostId());
                $objAdPostReviewEvent->setReviewStatus(AdPostReviewStatus::COMPLETED);
                event($objAdPostReviewEvent);
            }

            if ($objTAdPostExpiration)
            {
                $objTAdPostExpiration->save();
            }

            DB::commit();
        }
        catch (\Exception $objError)
        {
            DB::rollBack();

            syslog(LOG_CRIT, __CLASS__ . ':' . __METHOD__ . ': ' . $objError->getMessage());
        }
    }
}