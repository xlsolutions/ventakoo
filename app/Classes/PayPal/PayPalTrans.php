<?php

namespace App\Classes\PayPal;

interface IPayPalTrans {

    function getTransactionId();
    
    function getOrderId();
    
    function getIpnTrackId();
    
    function getProtectionEligibility();
    
    function getPaymentDate();
    
    function getPaymentStatus();
    
    function getVerifySign();
    
    function getPaymentType();
    
    function getPendingReason();
    
    function getReasonCode();
    
    function getTransactionType();
    
    function getQuanity();
}

class PayPalTrans implements IPayPalTrans {

    private $strTransactionId;
    private $intOrderId;
    private $strIpnTrackId;
    private $strProtectionEligibility;
    private $strPaymentDate;
    private $strPaymentStatus;
    private $strVerifySign;
    private $strPaymentType;
    private $strPendingReason;
    private $strReasonCode;
    private $strTransactionType;
    private $intQuantity;

    public function __construct(
        $strTransactionId = null,
        $intOrderId = null,
        $strIpnTrackId = null,
        $strProtectionEligibility = null,
        $strPaymentDate = null,
        $strPaymentStatus = null,
        $strVerifySign = null,
        $strPaymentType = null,
        $strPendingReason = null,
        $strReasonCode = null,
        $strTransactionType = null,
        $intQuantity = null
    )
    {
        $this->strTransactionId = (string)$strTransactionId;
        $this->intOrderId = (int)$intOrderId;
        $this->strIpnTrackId = (string)$strIpnTrackId;
        $this->strProtectionEligibility = (string)$strProtectionEligibility;
        $this->strPaymentDate = (string)$strPaymentDate;
        $this->strPaymentStatus = (string)$strPaymentStatus;
        $this->strVerifySign = (string)$strVerifySign;
        $this->strPaymentType = (string)$strPaymentType;
        $this->strPendingReason = (string)$strPendingReason;
        $this->strReasonCode = (string)$strReasonCode;
        $this->strTransactionType = (string)$strTransactionType;
        $this->intQuantity = (int)$intQuantity;
    }

    public function getTransactionId()
    {
        return $this->strTransactionId;
    }

    public function getOrderId()
    {
        return $this->intOrderId;
    }

    public function getIpnTrackId()
    {
        return $this->strIpnTrackId;
    }

    public function getProtectionEligibility()
    {
        return $this->strProtectionEligibility;
    }

    public function getPaymentDate()
    {
        return $this->strPaymentDate;
    }

    public function getPaymentStatus()
    {
        return $this->strPaymentStatus;
    }

    public function getVerifySign()
    {
        return $this->strVerifySign;
    }

    public function getPaymentType()
    {
        return $this->strPaymentType;
    }

    public function getPendingReason()
    {
        return $this->strPendingReason;
    }

    public function getReasonCode()
    {
        return $this->strReasonCode;
    }

    public function getTransactionType()
    {
        return $this->strTransactionType;
    }

    public function getQuanity()
    {
        return $this->intQuantity;
    }
}