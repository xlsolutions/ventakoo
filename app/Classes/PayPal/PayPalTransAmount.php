<?php

namespace App\Classes\PayPal;

interface IPayPalTransAmount {
    
    function getMcCurrency();

    function getMcGross();
    
    function getExchangeRate();
    
    function getSettleAmount();
    
    function getSettleCurrency();
    
    function getMcFee();
}

class PayPalTransAmount implements IPayPalTransAmount {
    
    private $strMcCurrency;
    private $decMcGross;
    private $decExchangeRate;
    private $decSettleAmount;
    private $strSettleCurrency;
    private $decMcFee;

    public function __construct(
        $strMcCurrency = null,
        $decMcGross = null,
        $decExchangeRate = null,
        $decSettleAmount = null,
        $strSettleCurrency = null,
        $decMcFee = null
    )
    {
        $this->strMcCurrency = (string)$strMcCurrency;
        $this->decMcGross = (float)$decMcGross;
        $this->decExchangeRate = (float)$decExchangeRate;
        $this->decSettleAmount = (float)$decSettleAmount;
        $this->strSettleCurrency = (string)$strSettleCurrency;
        $this->decMcFee = (float)$decMcFee;
    }

    public function getMcCurrency()
    {
        return $this->strMcCurrency;
    }

    public function getMcGross()
    {
        return $this->decMcGross;
    }
    
    public function getExchangeRate()
    {
        return $this->decExchangeRate;
    }
    
    public function getSettleAmount()
    {
        return $this->decSettleAmount;
    }
    
    public function getSettleCurrency()
    {
        return $this->strSettleCurrency;
    }
    
    public function getMcFee()
    {
        return $this->decMcFee;
    }
}