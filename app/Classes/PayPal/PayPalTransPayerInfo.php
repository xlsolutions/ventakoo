<?php

namespace App\Classes\PayPal;

interface IPayPalTransPayerInfo {

    function getFirstName();

    function getLastName();
    
    function getPayerStatus();
    
    function getResidenceCountry();
}

class PayPalTransPayerInfo implements IPayPalTransPayerInfo {

    private $strFirstName;
    private $strLastName;
    private $strPayerStatus;
    private $strResidenceCountry;

    public function __construct(
        $strFirstName = null,
        $strLastName = null,
        $strPayerStatus = null,
        $strResidenceCountry = null
    )
    {
        $this->strFirstName = (string)$strFirstName;
        $this->strLastName = (string)$strLastName;
        $this->strPayerStatus = (string)$strPayerStatus;
        $this->strResidenceCountry = (string)$strResidenceCountry;
    }

    public function getFirstName()
    {
        return $this->strFirstName;
    }

    public function getLastName()
    {
        return $this->strLastName;
    }
    
    public function getPayerStatus()
    {
        return $this->strPayerStatus;
    }
    
    public function getResidenceCountry()
    {
        return $this->strResidenceCountry;
    }
}