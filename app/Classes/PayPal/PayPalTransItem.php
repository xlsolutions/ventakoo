<?php

namespace App\Classes\PayPal;

interface IPayPalTransItem {

    function getItemName();
    
    function getItemNumber();
}

class PayPalTransItem implements IPayPalTransItem {

    private $strItemName;
    private $strItemNumber;

    public function __construct(
        $strItemName = null,
        $strItemNumber = null
    )
    {
        $this->strItemName = (string)$strItemName;
        $this->strItemNumber = (string)$strItemNumber;
    }

    public function getItemName()
    {
        return $this->strItemName;
    }

    public function getItemNumber()
    {
        return $this->strItemNumber;
    }
}