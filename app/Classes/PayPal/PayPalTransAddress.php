<?php

namespace App\Classes\PayPal;

interface IPayPalTransAddress {

    function getStatus();

    function getStreet();

    function getZip();

    function getCountryCode();
    
    function getName();
    
    function getCountry();
    
    function getCity();
    
    function getState();
}

class PayPalTransAddress implements IPayPalTransAddress {

    private $strStatus;
    private $strStreet;
    private $strZip;
    private $strCountryCode;
    private $strName;
    private $strCountry;
    private $strCity;
    private $strState;

    public function __construct(
        $strStatus = null,
        $strStreet = null,
        $strZip = null,
        $strCountryCode = null,
        $strName = null,
        $strCountry = null,
        $strCity = null,
        $strState = null
    )
    {
        $this->strStatus = (string)$strStatus;
        $this->strStreet = (string)$strStreet;
        $this->strZip = (string)$strZip;
        $this->strCountryCode = (string)$strCountryCode;
        $this->strName = (string)$strName;
        $this->strCountry = (string)$strCountry;
        $this->strCity = (string)$strCity;
        $this->strState = (string)$strState;
    }

    public function getStatus()
    {
        return $this->strStatus;
    }

    public function getStreet()
    {
        return $this->strStreet;
    }

    public function getZip()
    {
        return $this->strZip;
    }

    public function getCountryCode()
    {
        return $this->strCountryCode;
    }
    
    public function getName()
    {
        return $this->strName;
    }
    
    public function getCountry()
    {
        return $this->strCountry;
    }
    
    public function getCity()
    {
        return $this->strCity;
    }
    
    public function getState()
    {
        return $this->strState;
    }
}