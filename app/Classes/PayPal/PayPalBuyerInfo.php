<?php

namespace App\Classes\PayPal;

interface IPayPalBuyerInfo {

    function getPayerEmail();
    
    function getPayerId();
}

class PayPalBuyerInfo implements IPayPalBuyerInfo {

    private $strPayerEmail;
    private $strPayerId;

    public function __construct(
        $strPayerEmail = null,
        $strPayerId = null
    )
    {
        $this->strPayerEmail = (string)$strPayerEmail;
        $this->strPayerId = (string)$strPayerId;
    }

    public function getPayerEmail()
    {
        return $this->strPayerEmail;
    }
    
    public function getPayerId()
    {
        return $this->strPayerId;
    }
}