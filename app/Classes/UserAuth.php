<?php

namespace App\Classes;

class UserAuth implements IUserAuth {

    private $intUserId;
    private $strRole;
    private $strFirstName;
    private $strLastName;
    private $strAddress;
    private $strCity;
    private $strState;
    private $strZip;
    private $strCountryCode;
    
    public function __construct(
        $intUserId = null,
        $strRole = null,
        $strFirstName = null,
        $strLastName = null,
        $strAddress = null,
        $strCity = null,
        $strState = null,
        $strZip = null,
        $strCountryCode = null
    )
    {
        $this->intUserId = (int)$intUserId;
        $this->strRole = (string)$strRole;
        $this->strFirstName = (string)$strFirstName;
        $this->strLastName = (string)$strLastName;
        $this->strAddress = (string)$strAddress;
        $this->strCity = (string)$strCity;
        $this->strState = (string)$strState;
        $this->strZip = (string)$strZip;
        $this->strCountryCode = (string)$strCountryCode;
    }

    public function getUserId()
    {
        return $this->intUserId;
    }

    public function getUserRole()
    {
        return $this->strRole;
    }

    public function getFirstName()
    {
        return $this->strFirstName;
    }

    public function getLastName()
    {
        return $this->strLastName;
    }

    public function getAddress()
    {
        return $this->strAddress;
    }

    public function getCity()
    {
        return $this->strCity;
    }

    public function getState()
    {
        return $this->strState;
    }

    public function getZip()
    {
        return $this->strZip;
    }

    public function getCountryCode()
    {
        return $this->strCountryCode;
    }
    
    public function getProfilePhoto()
    {
        if(@file_exists("../resources/sysclient/images/user/{$this->intUserId}.jpg"))
        {
            $strPhoto = "data:image/jpeg;base64," . base64_encode(file_get_contents("../resources/sysclient/images/user/{$this->intUserId}.jpg"));
        }
        else
        {
            $strPhoto = null;
        }
        
        return $strPhoto;
    }
}