<?php

namespace App\Classes\Order;

use App\Models\Order\TOrderType;

class CampaignPlanOrder extends AOrderItem {

    private $intOrderTypeId;
    private $strTransactionNote;

    public function getOrderTypeId()
    {
        return $this->intOrderTypeId;
    }

    /**
     * @param integer $intItemId
     * @param integer | null $intPrice
     *
     * @return void
     */
    public function addItem($intItemId, $intPrice = null)
    {
        $objTOrderType = TOrderType::create([
            'ad_post_id' => null,
            'plan_id' => $intItemId,
            'service_id' => null
        ]);

        $this->intOrderTypeId = $objTOrderType['order_type_id'];

        if (is_null($intPrice))
        {
            $intPrice = $this->getPrice();
        }

        $this->setPrice($intPrice);
    }

    /**
     * @param string $strTransactionNote
     *
     * @return void
     */
    public function setTransactionNote($strTransactionNote)
    {
        $this->strTransactionNote = $strTransactionNote;
    }
    
    /**
     * @return string
     */
    public function getTransactionNote()
    {
        return $this->strTransactionNote;
    }
}