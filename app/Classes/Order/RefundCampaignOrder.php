<?php

namespace App\Classes\Order;

use App\Models\Order\TOrder;
use App\Models\Order\TOrderType;

class RefundCampaignOrder extends AOrderItem {

    private $intOrderTypeId;
    private $strTransactionNote;

    public function getOrderTypeId()
    {
        return $this->intOrderTypeId;
    }

    /**
     * @param integer $intItemId
     * @param integer | null $intPrice
     *
     * @return void
     */
    public function addItem($intItemId, $intPrice = null)
    {
        //@readme: get the refund price via previous order
        $objPreviousOrderType = TOrderType::select('t_order.price')
            ->join('t_order_item', 't_order_item.order_type_id', '=', 't_order_type.order_type_id')
            ->join('t_order', 't_order.order_id', '=', 't_order_item.order_id')
            ->join('c_order_status', 'c_order_status.order_status_id', '=', 't_order.order_status_id')
            ->where('c_order_status.handle', 'COMPLETED')
            ->where('t_order_type.ad_post_id', $intItemId)
            ->orderBy('t_order_type.order_type_id', 'desc')
            ->limit(1)
            ->first();
        
        $this->setPrice($objPreviousOrderType['price']);
        
        $objTOrderType = TOrderType::create([
            'ad_post_id' => $intItemId,
            'plan_id' => null,
            'service_id' => null
        ]);
        
        $this->intOrderTypeId = $objTOrderType['order_type_id'];
    }

    /**
     * @param string $strTransactionNote
     *
     * @return void
     */
    public function setTransactionNote($strTransactionNote)
    {
        $this->strTransactionNote = $strTransactionNote;
    }
    
    /**
     * @return string
     */
    public function getTransactionNote()
    {
        return $this->strTransactionNote;
    }

    /**
     * @return integer
     */
    public function getRefundAmount()
    {
        return $this->getPrice();
    }
}