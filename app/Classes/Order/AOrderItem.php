<?php

namespace App\Classes\Order;

use App\Models\Order\TOrderItem;

abstract class AOrderItem extends AOrder {

    public function create()
    {
        if (!$this->getOrderId())
        {
            parent::create();
        }

        TOrderItem::create([
            'order_id' => $this->getOrderId(),
            'order_type_id' => $this->getOrderTypeId(),
            'price' => $this->getPrice()
        ]);
    }

    abstract public function getOrderTypeId();

    abstract public function addItem($intItemId, $intPrice = null);
}