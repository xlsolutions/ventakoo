<?php

namespace App\Classes\Order;

use App\Models\Constants\COrderStatus;
use App\Models\Finance\TFinanceTransaction;
use App\Models\Order\TOrder;

abstract class AOrder {

    private $intOrderId;

    private $intUserId;
    private $strOrderStatus;
    private $intPrice;

    final public function __construct($intUserId = null, $strOrderStatus = null, $intPrice = null)
    {
        $this->intUserId = $intUserId;
        $this->strOrderStatus = $strOrderStatus;
        $this->intPrice = $intPrice;
    }

    abstract public function setTransactionNote($strTransactionNote);
    
    abstract public function getTransactionNote();
    
    /**
     * Public methods
     * */
    public function create()
    {
        $objTOrder = TOrder::create([
            'user_id' => $this->getUserId(),
            'order_status_id' => $this->getOrderStatusIdByHandle(),
            'price' => $this->getPrice()
        ]);

        $this->intOrderId = $objTOrder['order_id'];
        
        TFinanceTransaction::create([
            'order_id' => $this->intOrderId,
            'note' => $this->getTransactionNote()
        ]);
    }

    public function getOrderId()
    {
        return $this->intOrderId;
    }

    /**
     * Protected methods
     * */
    protected function getUserId()
    {
        return $this->intUserId;
    }

    protected function getOrderStatus()
    {
        return $this->strOrderStatus;
    }

    protected function getOrderStatusIdByHandle()
    {
        $objCOrderStatus = COrderStatus::where('handle', $this->strOrderStatus)->first();

        return $objCOrderStatus['order_status_id'];
    }

    protected function setPrice($intPrice)
    {
        $this->intPrice = (int)$intPrice;
    }

    protected function getPrice()
    {
        return $this->intPrice;
    }
}