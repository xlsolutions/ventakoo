<?php

namespace App\Classes;

interface IUserAuth {

    function getUserId();
    function getUserRole();
    function getFirstName();
    function getLastName();
    function getAddress();
    function getCity();
    function getState();
    function getZip();
    function getCountryCode();
    function getProfilePhoto();
}