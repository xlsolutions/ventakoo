<?php

namespace App\Models\AdPost;

use Illuminate\Database\Eloquent\Model;

/**
 * Class TAdPostNote
 */
class TAdPostNote extends Model
{
    protected $table = 't_ad_post_note';

    protected $primaryKey = 'ad_post_note_id';

    protected $fillable = [
        'ad_post_id',
        'ad_post_status_id',
        'user_id',
        'note'
    ];

    public function select()
    {
        return $this->defaultJoins()
            ->select(
                't4.handle AS post_status',
                "COALESCE(CONCAT(t2.firstname, ' ', t2.lastname), 'System') AS submitted_by",
                't_ad_post_note.ad_post_id AS campaignId',
                't_ad_post_note.note',
                't_ad_post_note.created_at AS date_created'
            );
    }

    private function defaultJoins()
    {
        return self::query()
            ->leftJoin('t_user AS t1', 't1.user_id', '=', 't_ad_post_note.user_id')
            ->leftJoin('t_user_information AS t2', 't2.user_id', '=', 't_ad_post_note.user_id')
            ->join('t_ad_post AS t3', 't3.ad_post_id', '=', 't_ad_post_note.ad_post_id')
            ->join('c_ad_post_status AS t4', 't4.ad_post_status_id', '=', 't_ad_post_note.ad_post_status_id')
            ->join('t_product AS t5', 't5.product_id', '=', 't_ad_post.product_id')
            ->join('t_user AS t6', 't6.user_id', '=', 't_product.user_id');
    }
}