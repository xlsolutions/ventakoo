<?php

namespace App\Models\AdPost;

use Illuminate\Database\Eloquent\Model;

/**
 * Class TAdPost
 */
class TAdPost extends Model
{
    protected $table = 't_ad_post';

    protected $primaryKey = 'ad_post_id';
    
    public $timestamps = false;

    public function select()
    {
        return $this->defaultJoins()
            ->select(
                't_ad_post.ad_post_id AS id',
                't_ad_post_note.note AS status_note',
                'c_ad_post_status.handle AS status_handle',
                't_ad_post_expiration.time_start',
                't_ad_post_expiration.time_end',
                "IF(t_ad_post_expiration.ad_post_expiration_id IS NULL, 'NOT_YET_PUBLISHED', t_ad_post_expiration.time_duration)"
            )
            ->where('t_ad_post.cancel_ad_post', 0)
            ->orderBy('t_ad_post_note.ad_post_note_id', 'desc')
            ->limit(1);
    }

    private function defaultJoins()
    {
        return self::query()
            ->leftJoin('t_ad_post_expiration', 't_ad_post_expiration.ad_post_id', '=', 't_ad_post.ad_post_id')
            ->leftJoin('t_ad_post_note', 't_ad_post_note.ad_post_id', '=', 't_ad_post.ad_post_id')
            ->leftJoin('c_ad_post_status', 'c_ad_post_status.ad_post_status_id', '=', 't_ad_post_note.ad_post_status_id')
            ->join('t_product', 't_product.product_id', '=', 't_ad_post.product_id')
            ->join('t_user', 't_user.user_id', '=', 't_product.user_id');
    }
}