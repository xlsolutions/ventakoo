<?php

namespace App\Models\AdPost;

use Illuminate\Database\Eloquent\Model;

/**
 * Class TAdPostExpiration
 */
class TAdPostExpiration extends Model
{
    protected $table = 't_ad_post_expiration';

    protected $primaryKey = 'ad_post_expiration_id';

    protected $fillable = [
        'ad_post_id',
        'time_start',
        'time_duration'
    ];
}