<?php

namespace App\Models\Products;

use Illuminate\Database\Eloquent\Model;
use Illuminate\Database\Eloquent\SoftDeletes;

/**
 * Class TProductAttribute
 */
class TProductAttribute extends Model
{
    use SoftDeletes;

    protected $table = 't_product_attribute';

    protected $primaryKey = 'product_attribute_id';

    /**
     * The attributes that should be mutated to dates.
     *
     * @var array
     */
    protected $dates = ['deleted_at'];

    protected $fillable = [
        'product_id',
        'product_information_id',
        'value'
    ];

    protected $hidden = ['pivot'];

    protected $guarded = [];
    
    public function select()
    {
        return self::query()
            ->select(
                'c_product_information.product_attribute_type as description',
                'c_product_information.handle as handle',
                't_product_attribute.value as value'
            )
            ->join('t_product', 't_product.product_id', '=', 't_product_attribute.product_id')
            ->join('c_product_information', 'c_product_information.product_information_id', '=', 't_product_attribute.product_information_id');
    }

    public function selectWithID()
    {
        return self::query()
            ->select(
                't_product_attribute.product_attribute_id as id',
                'c_product_information.product_attribute_type as type',
                'c_product_information.handle as handle',
                't_product_attribute.value as value'
            )
            ->join('t_product', 't_product.product_id', '=', 't_product_attribute.product_id')
            ->join('c_product_information', 'c_product_information.product_information_id', '=', 't_product_attribute.product_information_id');
    }
}