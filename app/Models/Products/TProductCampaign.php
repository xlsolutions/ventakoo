<?php

namespace App\Models\Products;

use DB;

/**
 * Class TProductCampaign
 */
class TProductCampaign extends TProduct
{
    public function select()
    {
        $strQuery = " SELECT MAX(t1.ad_post_note_id) AS recentNoteId, t1.ad_post_id, t2.product_id, t4.user_id FROM t_ad_post_note AS t1";
        $strQuery .= " INNER JOIN t_ad_post AS t2 ON t2.ad_post_id = t1.ad_post_id";
        $strQuery .= " INNER JOIN c_ad_post_status AS t3 ON t3.ad_post_status_id = t1.ad_post_status_id";
        $strQuery .= " INNER JOIN t_product AS t4 ON t4.product_id = t2.product_id";
        $strQuery .= " GROUP BY t1.ad_post_id, t2.product_id, t4.user_id";

        return self::query()
            ->select(
                't_product.handle',
                DB::raw("CONCAT(t_user_information.firstname, ' ', t_user_information.lastname) AS owner"),
                't_user_information.user_id AS ownerId',
                't_product.quantity',
                't_product.price',
                DB::raw('COALESCE(t_ad_post_expiration.updated_at, t_product.created_at) AS datePosted'),
                't_category.handle AS category',
                't_ad_post_expiration.time_start AS timeStart',
                't_ad_post_expiration.time_end AS timeEnd',
                't_ad_post_expiration.time_duration AS timeDuration'
            )
            ->join('t_category', 't_category.category_id', '=', 't_product.category_id')
            ->join('c_status', 'c_status.status_id', '=', 't_product.status_id')
            ->join('t_user_information', 't_user_information.user_id', '=', 't_product.user_id')
            ->join('t_ad_post', 't_ad_post.product_id', '=', 't_product.product_id')
            ->join('t_ad_post_expiration', 't_ad_post_expiration.ad_post_id', '=', 't_ad_post.ad_post_id')
            ->join(DB::raw("({$strQuery}) AS recent_note"), function($objJoin) {
                $objJoin->on('recent_note.ad_post_id', '=', 't_ad_post.ad_post_id');
                $objJoin->on('recent_note.user_id', '=', 't_product.user_id');
                $objJoin->on('recent_note.product_id', '=', 't_product.product_id');
            })
            ->join('t_ad_post_note', 't_ad_post_note.ad_post_note_id', '=', 'recent_note.recentNoteId')
            ->join('c_ad_post_status', 'c_ad_post_status.ad_post_status_id', '=', 't_ad_post_note.ad_post_status_id')
            ->where('c_ad_post_status.handle', '=', 'START')
            ->orderBy('t_product.product_id', 'desc');

        //        $strQuery .= " WHERE t3.handle = 'START'";

    }
}