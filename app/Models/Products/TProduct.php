<?php

namespace App\Models\Products;

use Illuminate\Database\Eloquent\Model;
use Illuminate\Database\Eloquent\SoftDeletes;
use Illuminate\Support\Facades\DB;

/**
 * Class TProduct
 */
class TProduct extends Model
{
    use SoftDeletes;

    protected $table = 't_product';

    protected $primaryKey = 'product_id';

    /**
     * The attributes that should be mutated to dates.
     *
     * @var array
     */
    protected $dates = ['deleted_at'];
    
    protected $fillable = [
        'category_id',
        'handle',
        'user_id',
        'quantity',
        'price',
        'status_id'
    ];

    protected $guarded = ['product_id'];
    
    public function select()
    {        
        return self::query()
            ->select(
                't_product.handle',
                DB::raw("CONCAT(t_user_information.firstname, ' ', t_user_information.lastname) AS owner"),
                't_user_information.user_id AS ownerId',
                't_product.quantity',
                't_product.price',
                DB::raw('COALESCE(t_ad_post_expiration.updated_at, t_product.created_at) AS datePosted'),
                't_category.handle AS category',
                'c_status.handle AS visibility',
                't_ad_post.ad_post_id AS campaignId',
                't_ad_post_expiration.time_start AS timeStart',
                't_ad_post_expiration.time_end AS timeEnd',
                't_ad_post_expiration.time_duration AS timeDuration'
            )
            ->join('t_category', 't_category.category_id', '=', 't_product.category_id')
            ->join('c_status', 'c_status.status_id', '=', 't_product.status_id')
            ->join('t_user_information', 't_user_information.user_id', '=', 't_product.user_id')
            ->leftJoin('t_ad_post', 't_ad_post.product_id', '=', 't_product.product_id')
            ->leftJoin('t_ad_post_expiration', 't_ad_post_expiration.ad_post_id', '=', 't_ad_post.ad_post_id')
            ->orderBy('t_product.product_id', 'desc');
    }

    public function selectAttribute()
    {
        return self::query()
            ->select(
                't_product.handle AS product_handle',
                't_product_attribute.product_attribute_id',
                't_product_attribute.value',
                'c_product_information.handle'
            )
            ->join('t_product_attribute', 't_product.product_id', '=', 't_product_attribute.product_id')
            ->join('c_product_information', 'c_product_information.product_information_id', '=', 't_product_attribute.product_information_id')
            ->where('c_product_information.status', 1)
            ->whereIn('c_product_information.handle', ["NAME","DESCRIPTION", "IMAGE_THUMBNAIL", "IMAGE_ORIGINAL"]);
    }
}