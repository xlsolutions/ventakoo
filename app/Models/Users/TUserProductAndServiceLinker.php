<?php

namespace App\Models\Users;

use Illuminate\Database\Eloquent\Model;
use Illuminate\Database\Eloquent\SoftDeletes;

/**
 * Class TUserProductAndServiceLinker
 */
class TUserProductAndServiceLinker extends Model
{
    use SoftDeletes;
    
    protected $table = 't_user_product_and_service_linker';

    protected $primaryKey = 'user_product_and_service_linker_id';

    /**
     * The attributes that should be mutated to dates.
     *
     * @var array
     */
    protected $dates = ['deleted_at'];
    
    protected $fillable = [
        'user_id',
        'product_information_id',
        'service_information_id'
    ];

    protected $guarded = ['user_product_and_service_linker_id'];
}