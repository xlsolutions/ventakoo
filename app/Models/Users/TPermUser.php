<?php

namespace App\Models\Users;

use Illuminate\Database\Eloquent\Model;

/**
 * Class TPermUser
 */
class TPermUser extends Model
{
    protected $table = 't_perm_user';

    protected $primaryKey = 'perm_user_id';

    protected $fillable = [
        'user_id',
        'perm_id'
    ];

    protected $guarded = [];
}