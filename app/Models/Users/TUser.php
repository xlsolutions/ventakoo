<?php

namespace App\Models\Users;

use Illuminate\Database\Eloquent\Model;

/**
 * Class TUser
 */
class TUser extends Model
{
    protected $table = 't_user';

    protected $primaryKey = 'user_id';
    
    protected $guarded = ['user_id'];

    public $timestamps = false;

    public function select()
    {
        return $this->defaultJoins()
            ->select(
                't_user.user_id',
                't_user_information.created_at',
                't_user_information.firstname',
                't_user_information.lastname',
                't_user_information.last_login',
                't_user_contact.email_address',
                'uca1.value AS mobile',
                'uca2.value AS phone_number',
                'uca3.value AS address',
                'c_country.country_code',
                'c_country.country_name',
                'c_state.state_id',
                'c_state.state_name',
                'c_city.city_id',
                'c_city.city_name'
            );
    }

    public function selectWithoutContact()
    {
        return $this->defaultJoins()
            ->select(
                't_user.user_id',
                't_user_information.created_at',
                't_user_information.firstname',
                't_user_information.lastname',
                't_user_information.last_login',
                'c_country.country_code',
                'c_country.country_name',
                'c_state.state_name',
                'c_city.city_name'
            );
    }

    private function defaultJoins()
    {
        return self::query()
            ->join('t_user_information', 't_user.user_id', '=', 't_user_information.user_id')
            ->join('t_user_contact', 't_user_information.user_information_id', '=', 't_user_contact.user_information_id')
            ->leftJoin('t_user_contact_attribute as uca1', 'uca1.user_contact_id', '=', 't_user_contact.user_contact_id')
            ->join('c_user_contact_information as uci1', function($objJoin) {
                $objJoin->on('uci1.user_contact_information_id', '=', 'uci1.user_contact_information_id')
                    ->where('uci1.handle', '=', 'MOBILE_NUMBER');
            })
            ->leftJoin('t_user_contact_attribute as uca2', 'uca2.user_contact_id', '=', 't_user_contact.user_contact_id')
            ->join('c_user_contact_information as uci2', function($objJoin) {
                $objJoin->on('uci2.user_contact_information_id', '=', 'uca2.user_contact_information_id')
                    ->where('uci2.handle', '=', 'PHONE_NUMBER');
            })
            ->leftJoin('t_user_contact_attribute as uca3', 'uca3.user_contact_id', '=', 't_user_contact.user_contact_id')
            ->join('c_user_contact_information as uci3', function($objJoin) {
                $objJoin->on('uci3.user_contact_information_id', '=', 'uca3.user_contact_information_id')
                    ->where('uci3.handle', '=', 'ADDRESS');
            })
            ->leftJoin('t_user_location', 't_user_contact.user_location_id', '=', 't_user_location.user_location_id')
            ->leftJoin('c_country', 't_user_location.country_id', '=', 'c_country.country_id')
            ->leftJoin('c_state', 't_user_location.state_id', '=', 'c_state.state_id')
            ->leftJoin('c_city', 't_user_location.city_id', '=', 'c_city.city_id');
    }
}