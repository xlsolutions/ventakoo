<?php

namespace App\Models\Users;

use Illuminate\Database\Eloquent\Model;
use Illuminate\Database\Eloquent\SoftDeletes;

/**
 * Class TUserActivation
 */
class TUserActivation extends Model
{
    use SoftDeletes;

    protected $table = 't_user_activation';

    protected $primaryKey = 'user_activation_id';

    /**
     * The attributes that should be mutated to dates.
     *
     * @var array
     */
    protected $dates = ['deleted_at'];

    protected $fillable = [
        'activation_code',
        'activation_type_id',
        'user_contact_id'
    ];

    protected $guarded = ['user_activation_id'];

    public function select()
    {
        return self::query()
            ->select(
                't_user_activation.activation_code',
                'c_activation_type.handle'
            )
            ->join('c_activation_type', 'c_activation_type.activation_type_id', '=', 't_user_activation.activation_type_id')
            ->join('t_user_contact', 't_user_contact.user_contact_id', '=', 't_user_activation.user_contact_id')
            ->whereNull('deleted_at');
    }
}