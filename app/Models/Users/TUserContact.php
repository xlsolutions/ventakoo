<?php

namespace App\Models\Users;

use Illuminate\Database\Eloquent\Model;

/**
 * Class TUserContact
 */
class TUserContact extends Model
{
    protected $table = 't_user_contact';

    protected $primaryKey = 'user_contact_id';

    protected $fillable = [
        'user_information_id',
        'email_address',
        'user_location_id',
        'date_updated'
    ];

    protected $guarded = [];

    public function userContactAttr()
    {
        return $this->hasMany('App\Models\Users\TUserContactAttribute','user_contact_id');
    }

    public function userContactInfo()
    {
        return $this->belongsTo('App\Models\Constants\CUserContactInformation','user_contact_id');
    }
}