<?php

namespace App\Models\Users;

use Illuminate\Database\Eloquent\Model;

/**
 * Class TUserInformation
 */
class TUserInformation extends Model
{
    protected $table = 't_user_information';

    protected $primaryKey = 'user_information_id';
    
    protected $fillable = [
        'user_id',
        'firstname',
        'lastname',
        'password',
        'last_login'
    ];

    protected $guarded = [];

    public function userContact()
    {
        return $this->belongsTo('App\Models\Users\TUserContact','user_information_id');
    }
}