<?php

namespace App\Models\Users;

use Illuminate\Database\Eloquent\Model;

/**
 * Class TUserContactAttribute
 */
class TUserContactAttribute extends Model
{
    protected $table = 't_user_contact_attribute';

    protected $primaryKey = 'user_contact_attribute_id';

    protected $fillable = [
        'user_contact_id',
        'user_contact_information_id',
        'value'
    ];

    protected $guarded = [];

    public function userContactInfo()
    {
        return $this->belongsTo('App\Models\Constants\CUserContactInformation','user_contact_attribute_id');
    }
}