<?php

namespace App\Models\Users;

use Illuminate\Database\Eloquent\Model;

/**
 * Class TAccountBalance
 */
class TAccountBalance extends Model
{
    protected $table = 't_account_balance';

    protected $primaryKey = 'account_balance_id';

    protected $fillable = [
        'user_id',
        'amount'
    ];

    public $timestamps = false;
}