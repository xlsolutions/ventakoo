<?php

namespace App\Models\Users;

use Illuminate\Database\Eloquent\Model;

/**
 * Class TUsersLocation
 */
class TUserLocation extends Model
{
    protected $table = 't_user_location';

    protected $primaryKey = 'user_location_id';

    protected $fillable = [
        'country_id',
        'state_id',
        'city_id'
    ];

    protected $guarded = [];

        
}