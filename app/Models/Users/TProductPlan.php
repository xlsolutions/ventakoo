<?php

namespace App\Models\Users;

use Illuminate\Database\Eloquent\Model;

/**
 * Class TProductPlan
 */
class TProductPlan extends Model
{
    protected $table = 't_product_plan';

    protected $primaryKey = 'plan_id';

    protected $fillable = [
        'product_plan_id',
        'user_id',
        'price'
    ];
}