<?php

namespace App\Models\Users;

use Illuminate\Support\Facades\Crypt;
use Illuminate\Support\Facades\Session;

/**
 * Class UserRole
 */
class UserRole extends TPermUser
{
    public $timestamps = false;
    
    protected $guarded = [];
    
    /**
     * Used to load all perms of the user
     *
     * @param $intUserId integer
     * @return void
     *
     * */
    static public function loadPermsByUserId($intUserId, $bolRefresh = false)
    {
        $strKey = __CLASS__ . '::' . $intUserId;

        $strEncryptedPerm = Session::get($strKey);

        if (!$strEncryptedPerm || $bolRefresh)
        {
            $arrPerms = self::query()
                ->select('c_perm.handle')
                ->join('c_perm', 'c_perm.perm_id', '=', 't_perm_user.perm_id')
                ->where('t_perm_user.user_id', $intUserId)
                ->pluck('handle');

            Session::put($strKey, Crypt::encrypt(json_encode($arrPerms)));
            Session::save();
        }
    }

    /**
     * Used to get the role by user id
     *
     * @param $intUserId integer
     * @return string
     *
     * */
    static public function getRoleByUserId($intUserId)
    {
        $strKey = __CLASS__ . '::' . $intUserId;

        $strEncryptedPerm = Session::get($strKey);
        
        if ($strEncryptedPerm)
        {
            $strPerm = Crypt::decrypt($strEncryptedPerm);

            $arrPerms = json_decode($strPerm, true);

            return in_array('user.product', $arrPerms) ? 'SELLER' : 'BUYER';
        }
        else
        {
            return 'UNKNOWN';
        }
    }
}