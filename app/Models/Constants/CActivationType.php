<?php

namespace App\Models\Constants;

use Illuminate\Database\Eloquent\Model;
use Illuminate\Database\Eloquent\SoftDeletes;

/**
 * Class CActivationType
 */
class CActivationType extends Model
{
    use SoftDeletes;
    
    protected $table = 'c_activation_type';

    protected $primaryKey = 'activation_type_id';
    
    protected $guarded = [];
    
    protected $dates = ['deleted_at'];
    
    public $timestamps = false;
}