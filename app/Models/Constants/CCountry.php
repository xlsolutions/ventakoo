<?php

namespace App\Models\Constants;

use Illuminate\Database\Eloquent\Model;

/**
 * Class CCountry
 */
class CCountry extends Model
{
    protected $table = 'c_country';

    protected $primaryKey = 'country_id';
    
    protected $fillable = [
        'country_name'
    ];

    protected $guarded = [];

    public function states()
    {
        return $this->hasMany('App\Models\Constants\CState','country_id');
    }
}