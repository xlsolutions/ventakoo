<?php

namespace App\Models\Constants;

use Illuminate\Database\Eloquent\Model;

/**
 * Class CProductPlan
 */
class CProductPlan extends Model
{
    const STARTER_PER_POST = 50; //php per post

    const PRO_PER_POST = 45; //php per post

    const BUSINESS_PER_POST = 40; //php per post
    
    protected $table = 'c_product_plan';

    protected $primaryKey = 'product_plan_id';
    
    protected $fillable = [
        'handle',
        'name',
        'description',
        'is_available',
        'price'
    ];
}