<?php

namespace App\Models\Constants;

use Illuminate\Database\Eloquent\Model;

/**
 * Class CCity
 */
class CCity extends Model
{
    protected $table = 'c_city';

    protected $primaryKey = 'city_id';

	public $timestamps = true;

    protected $fillable = [
        'state_id',
        'city_name'
    ];

    protected $guarded = [];

    public function state()
    {
        return $this->belongsTo('App\Models\Constants\CState','state_id');
    }
}