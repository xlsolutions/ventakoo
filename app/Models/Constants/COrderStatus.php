<?php

namespace App\Models\Constants;

use Illuminate\Database\Eloquent\Model;

/**
 * Class COrderStatus
 */
class COrderStatus extends Model
{
    protected $table = 'c_order_status';

    protected $primaryKey = 'order_status_id';

    protected $fillable = [
        'handle',
        'name'
    ];

    public $timestamps = false;
}