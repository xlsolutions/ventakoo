<?php

namespace App\Models\Constants;

use Illuminate\Database\Eloquent\Model;

/**
 * Class CUserContactInformation
 */
class CUserContactInformation extends Model
{
    protected $table = 'c_user_contact_information';

    protected $primaryKey = 'user_contact_information_id';
    
    protected $fillable = [
        'user_contact_attribute_type',
        'handle',
        'status'
    ];

    protected $guarded = [];

        
}