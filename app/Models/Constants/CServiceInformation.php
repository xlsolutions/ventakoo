<?php

namespace App\Models\Constants;

use Illuminate\Database\Eloquent\Model;

/**
 * Class CServiceInformation
 */
class CServiceInformation extends Model
{
    protected $table = 'c_service_information';

    protected $primaryKey = 'service_information_id';
    
    protected $fillable = [
        'service_attribute_type',
        'handle',
        'status'
    ];

    protected $guarded = [];

        
}