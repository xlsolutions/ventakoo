<?php

namespace App\Models\Constants;

use Illuminate\Database\Eloquent\Model;

/**
 * Class CAdPostStatus
 */
class CAdPostStatus extends Model
{
    protected $table = 'c_ad_post_status';

    protected $primaryKey = 'ad_post_status_id';
    
    protected $guarded = [];
}