<?php

namespace App\Models\Constants;

use Illuminate\Database\Eloquent\Model;

/**
 * Class CImageInformation
 */
class CImageInformation extends Model
{
    protected $table = 'c_image_information';

    protected $primaryKey = 'image_information_id';
    
    protected $fillable = [
        'image_attribute_type',
        'handle',
        'status'
    ];

    protected $guarded = [];

        
}