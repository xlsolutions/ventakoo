<?php

namespace App\Models\Constants;

use Illuminate\Database\Eloquent\Model;

/**
 * Class CProductInformation
 */
class CProductInformation extends Model
{
    protected $table = 'c_product_information';

    protected $primaryKey = 'product_information_id';
    
    protected $fillable = [
        'product_attribute_type',
        'handle',
        'status'
    ];

    protected $guarded = [];

    protected $hidden = ['pivot'];
}