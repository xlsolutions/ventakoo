<?php

namespace App\Models\Constants;

use Illuminate\Database\Eloquent\Model;

/**
 * Class CStatus
 */
class CStatus extends Model
{
    protected $table = 'c_status';

    protected $primaryKey = 'status_id';
    
    protected $fillable = [
        'handle',
        'description'
    ];

    protected $guarded = [];

        
}