<?php

namespace App\Models\Constants;

use Illuminate\Database\Eloquent\Model;

/**
 * Class CState
 */
class CState extends Model
{
    protected $table = 'c_state';

    protected $primaryKey = 'state_id';
    
    protected $fillable = [
        'country_id',
        'state_name'
    ];

    protected $guarded = [];

    public function country()
    {
        return $this->belongsTo('App\Models\Constants\CCountry','country_id');
    }

    public function cities()
    {
        return $this->hasMany('App\Models\Constants\CCity','state_id');
    }
        
}