<?php

namespace App\Models\Constants;

use Illuminate\Database\Eloquent\Model;

/**
 * Class CPerm
 */
class CPerm extends Model
{
    protected $table = 'c_perm';

    protected $primaryKey = 'perm_id';
    
    protected $fillable = [
        'handle',
        'description'
    ];

    protected $guarded = [];


}