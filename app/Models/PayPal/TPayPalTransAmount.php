<?php

namespace App\Models\PayPal;

use Illuminate\Database\Eloquent\Model;

/**
 * Class TPayPalTransAmount
 */
class TPayPalTransAmount extends Model
{
    protected $table = 't_paypal_trans_amount';

    protected $primaryKey = 'paypal_trans_amount_id';

    protected $fillable = [
        'paypal_trans_id',
        'mc_currency',
        'mc_gross',
        'exchange_rate',
        'settle_amount',
        'settle_currency',
        'mc_fee'
    ];
    
    public $timestamps = false;
}