<?php

namespace App\Models\PayPal;

use Illuminate\Database\Eloquent\Model;

/**
 * Class TPayPalTrans
 */
class TPayPalTrans extends Model
{
    protected $table = 't_paypal_trans';

    protected $primaryKey = 'paypal_trans_id';

    protected $fillable = [
        'txn_id',
        'order_id',
        'ipn_track_id',
        'protection_eligibility',
        'payment_date',
        'payment_status',
        'pending_reason',
        'reason_code',
        'verify_sign',
        'payment_type',
        'txn_type',
        'quantity'
    ];

    public function selectOne()
    {
        return self::query()->select(
            'txn_id AS transactionId',
            'payment_date AS paymentDate',
            'payment_status AS paymentStatus',
            'pending_reason AS pendingReason',
            'reason_code AS reasonCode',
            'payment_type AS paymentType',
            'txn_type AS transactionType',
            'quantity',

            'mc_currency AS currency',
            'mc_gross AS gross',
            'mc_fee AS transactionFee',

            'item_name AS itemName',
            'item_number AS itemNumber',

            'address_status AS payerAddressStatus',
            'address_street AS payerAddressStreet',
            'address_zip AS payerAddressZip',
            'address_country_code AS payerAddressCountryCode',
            'address_name AS payerAddressName',
            'address_country AS payerAddressCountry',
            'address_city AS payerAddressCity',
            'address_state AS payerAddressState',

            'first_name AS payerFirstName',
            'last_name AS payerLastName',
            'payer_status AS payerStatus',
            'residence_country AS payerResidenceCountry'
        )
            ->join('t_order', 't_order.order_id', '=', 't_paypal_trans.order_id')
            ->join('t_paypal_trans_amount', 't_paypal_trans_amount.paypal_trans_id', '=', 't_paypal_trans.paypal_trans_id')
            ->join('t_paypal_trans_item', 't_paypal_trans_item.paypal_trans_id', '=', 't_paypal_trans.paypal_trans_id')
            ->join('t_paypal_trans_address', 't_paypal_trans_address.paypal_trans_id', '=', 't_paypal_trans.paypal_trans_id')
            ->join('t_paypal_buyer', 't_paypal_buyer.user_id', '=', 't_order.user_id')
            ->join('t_paypal_buyer_info', 't_paypal_buyer_info.buyer_id', '=', 't_paypal_buyer.buyer_id')
            ->join('t_paypal_trans_payer_info', 't_paypal_trans_payer_info.paypal_trans_id', '=', 't_paypal_trans.paypal_trans_id');
    }

    public function selectOneForAdmin()
    {
        return self::query()->select(
            'txn_id AS transactionId',
            'ipn_track_id AS ipnTrackId',
            'protection_eligibility AS eligibility',
            'payment_date AS paymentDate',
            'payment_status AS paymentStatus',
            'pendingReason AS pendingReason',
            'reason_code AS reasonCode',
            'verify_sign AS verifySign',
            'payment_type AS paymentType',
            'txn_type AS transactionType',
            'quantity',

            'mc_currency AS currency',
            'mc_gross AS gross',
            'exchange_rate AS xchangeRate',
            'mc_fee AS transactionFee',

            'item_name AS itemName',
            'item_number AS itemNumber',

            'address_status AS payerAddressStatus',
            'address_street AS payerAddressStreet',
            'address_zip AS payerAddressZip',
            'address_country_code AS payerAddressCountryCode',
            'address_name AS payerAddressName',
            'address_country AS payerAddressCountry',
            'address_city AS payerAddressCity',
            'address_state AS payerAddressState',

            'payer_email AS payerEmail',
            'payer_id AS payerId',

            'first_name AS payerFirstName',
            'last_name AS payerLastName',
            'payer_status AS payerStatus',
            'residence_country AS payerResidenceCountry'
        )
            ->join('t_order', 't_order.order_id', '=', 't_paypal_trans.order_id')
            ->join('t_paypal_trans_amount', 't_paypal_trans_amount.paypal_trans_id', '=', 't_paypal_trans.paypal_trans_id')
            ->join('t_paypal_trans_item', 't_paypal_trans_item.paypal_trans_id', '=', 't_paypal_trans.paypal_trans_id')
            ->join('t_paypal_trans_address', 't_paypal_trans_address.paypal_trans_id', '=', 't_paypal_trans.paypal_trans_id')
            ->join('t_paypal_buyer_info', 't_paypal_buyer_info.paypal_trans_id', '=', 't_paypal_trans.paypal_trans_id')
            ->join('t_paypal_trans_payer_info', 't_paypal_trans_payer_info.paypal_trans_id', '=', 't_paypal_trans.paypal_trans_id');
    }
}