<?php

namespace App\Models\PayPal;

use Illuminate\Database\Eloquent\Model;

/**
 * Class TPayPalTransItem
 */
class TPayPalTransItem extends Model
{
    protected $table = 't_paypal_trans_item';

    protected $primaryKey = 'paypal_trans_item_id';

    protected $fillable = [
        'paypal_trans_id',
        'item_name',
        'item_number'
    ];
    
    public $timestamps = false;
}