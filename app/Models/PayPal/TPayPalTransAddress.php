<?php

namespace App\Models\PayPal;

use Illuminate\Database\Eloquent\Model;

/**
 * Class TPayPalTransAddress
 */
class TPayPalTransAddress extends Model
{
    protected $table = 't_paypal_trans_address';

    protected $primaryKey = 'paypal_trans_address_id';

    protected $fillable = [
        'paypal_trans_id',
        'address_status',
        'address_street',
        'address_zip',
        'address_country_code',
        'address_name',
        'address_country',
        'address_city',
        'address_state'
    ];
    
    public $timestamps = false;
}