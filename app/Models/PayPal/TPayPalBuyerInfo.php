<?php

namespace App\Models\PayPal;

use Illuminate\Database\Eloquent\Model;

/**
 * Class TPayPalBuyerInfo
 */
class TPayPalBuyerInfo extends Model
{
    protected $table = 't_paypal_buyer_info';

    protected $primaryKey = 'buyer_info_id';

    protected $fillable = [
        'buyer_id',
        'payer_email',
        'payer_id'
    ];
}