<?php

namespace App\Models\PayPal;

use Illuminate\Database\Eloquent\Model;

/**
 * Class TPayPalBuyer
 */
class TPayPalBuyer extends Model
{
    protected $table = 't_paypal_buyer';

    protected $primaryKey = 'buyer_id';
    
    protected $fillable = [
        'user_id'
    ];

    public $timestamps = false;
}