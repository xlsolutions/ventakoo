<?php

namespace App\Models\PayPal;

use Illuminate\Database\Eloquent\Model;

/**
 * Class TPayPalTransPayerInfo
 */
class TPayPalTransPayerInfo extends Model
{
    protected $table = 't_paypal_trans_payer_info';

    protected $primaryKey = 'paypal_trans_payer_info_id';

    protected $fillable = [
        'paypal_trans_id',
        'first_name',
        'last_name',
        'payer_status',
        'residence_country'
    ];

    public $timestamps = false;
}