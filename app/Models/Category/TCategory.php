<?php

namespace App\Models\Category;

use Illuminate\Database\Eloquent\Model;
use Illuminate\Database\Eloquent\SoftDeletes;

/**
 * Class TCategory
 */
class TCategory extends Model
{
    use SoftDeletes;
    
    protected $table = 't_category';

    protected $primaryKey = 'category_id';
    
    protected $fillable = [
        'parent_id',
        'category_name',
        'handle'
    ];

    /**
     * The attributes that should be mutated to dates.
     *
     * @var array
     */
    protected $dates = ['deleted_at'];

    protected $guarded = ['category_id'];
    
    public function select()
    {
        return self::query()
            ->select(
                't_category.handle',
                't_category.category_name AS category',
                'parent_category.handle AS parentHandle'
            )
            ->leftJoin('t_category AS parent_category', 'parent_category.category_id', '=', 't_category.parent_id');
    }
}