<?php

namespace App\Models\Finance;

use Illuminate\Database\Eloquent\Model;

/**
 * Class TFinanceTransaction
 */
class TFinanceTransaction extends Model
{
    protected $table = 't_finance_transaction';

    protected $primaryKey = 'finance_transaction_id';
    
    protected $fillable = [
        'order_id',
        'note'
    ];
}