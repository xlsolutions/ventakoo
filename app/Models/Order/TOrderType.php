<?php

namespace App\Models\Order;

use Illuminate\Database\Eloquent\Model;

/**
 * Class TOrderType
 */
class TOrderType extends Model
{
    protected $table = 't_order_type';

    protected $primaryKey = 'order_type_id';

    protected $fillable = [
        'ad_post_id',
        'plan_id',
        'service_id'
    ];

    public $timestamps = false;
}