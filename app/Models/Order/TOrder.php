<?php

namespace App\Models\Order;

use Illuminate\Database\Eloquent\Model;
use DB;

/**
 * Class TOrder
 */
class TOrder extends Model
{
    protected $table = 't_order';

    protected $primaryKey = 'order_id';

    protected $fillable = [
        'user_id',
        'order_status_id',
        'price'
    ];
    
    public function selectOne()
    {
        return self::query()->select(
            't_order.order_id AS orderId',
            DB::raw("IF(t_paypal_trans.order_id IS NOT NULL, 1, 0) AS hasPayPalTransaction"),
            't_order.price',
            'c_order_status.name AS orderStatus',
            't_order.created_at AS dateOrdered'
        )
            ->join('c_order_status', 'c_order_status.order_status_id', '=', 't_order.order_status_id')
            ->leftJoin('t_paypal_trans', 't_paypal_trans.order_id', '=', 't_order.order_id');
    }
    
    public function selectAll()
    {
        return $this->selectOne();
    }
}