<?php

namespace App\Models\Order;

use Illuminate\Database\Eloquent\Model;
use DB;

/**
 * Class TOrderItem
 */
class TOrderItem extends Model
{
    protected $table = 't_order_item';

    protected $primaryKey = 'order_item_id';

    protected $fillable = [
        'order_id',
        'order_type_id',
        'price'
    ];

    public $timestamps = false;

    public function selectAll()
    {
        return self::query()->select(
            't_order_item.price',
            DB::raw("IF(t_order_type.ad_post_id IS NOT NULL, 'Campaign Product', IF(t_order_type.plan_id IS NOT NULL, 'Campaign Plan', IF(t_order_type.service_id IS NOT NULL, 'Service', 'Unknown Type'))) AS itemType")
        )
            ->join('t_order', 't_order.order_id', '=', 't_order_item.order_id')
            ->join('t_order_type', 't_order_type.order_type_id', '=', 't_order_item.order_type_id');
    }
}