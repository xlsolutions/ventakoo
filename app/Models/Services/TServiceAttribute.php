<?php

namespace App\Models\Services;

use Illuminate\Database\Eloquent\Model;

/**
 * Class TServiceAttribute
 */
class TServiceAttribute extends Model
{
    protected $table = 't_service_attribute';

    protected $primaryKey = 'service_attribute_id';

    protected $fillable = [
        'service_id',
        'service_information_id',
        'value'
    ];

    protected $guarded = [];

    public function service()
    {
        return $this->hasMany('App\Models\Services\TService','service_id');
    }

    public function serviceInformation()
    {
        return $this->belongsTo('App\Models\Constants\CServiceInformation','service_information_id');
    }
        
}