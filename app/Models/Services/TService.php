<?php

namespace App\Models\Services;

use Illuminate\Database\Eloquent\Model;

/**
 * Class TService
 */
class TService extends Model
{
    protected $table = 't_service';

    protected $primaryKey = 'service_id';

    protected $fillable = [
        'category_id',
        'duration',
        'price',
        'status_id'
    ];

    protected $guarded = [];

    public function user()
    {
        return $this->belongsTo('App\Models\Users\TUser','user_id');
    }

    public function serviceAttribute()
    {
        return $this->hasMany('App\Models\Services\TServiceAttribute','service_id');
    }

    public function category()
    {
        return $this->belongsTo('App\Models\Category\TCategory','category_id');
    }

    public function status()
    {
        return $this->belongsTo('App\Models\Constants\CStatus', 'status_id');
    }

        
}