<?php

namespace App\Providers;

use Illuminate\Support\ServiceProvider;

class FlashMessageServiceProvider extends ServiceProvider
{
    private static $arrFlashData = [];

    /**
     * Bootstrap the application services.
     *
     * @return void
     */
    public function boot()
    {
        if (isset($_COOKIE['app_flash_message']))
        {
            self::$arrFlashData = json_decode($_COOKIE['app_flash_message'], true);
        }
    }

    /**
     * Register the application services.
     *
     * @return void
     */
    public function register()
    {
        //
    }

    /**
     * @param string $strValue
     *
     * @return void
     */
    public static function info($strValue)
    {
        self::$arrFlashData['info'][] = $strValue;

        setcookie('app_flash_message', json_encode(self::$arrFlashData));
    }

    /**
     * @param string $strValue
     *
     * @return void
     */
    public static function success($strValue)
    {
        self::$arrFlashData['success'][] = $strValue;

        setcookie('app_flash_message', json_encode(self::$arrFlashData));
    }

    /**
     * @param string $strValue
     *
     * @return void
     */
    public static function warning($strValue)
    {
        self::$arrFlashData['warning'][] = $strValue;

        setcookie('app_flash_message', json_encode(self::$arrFlashData));
    }

    /**
     * @param string $strValue
     *
     * @return void
     */
    public static function danger($strValue)
    {
        self::$arrFlashData['danger'][] = $strValue;

        setcookie('app_flash_message', json_encode(self::$arrFlashData));
    }

    /**
     * @param string $strValue
     *
     * @return void
     */
    public static function show($strType = 'all')
    {
        //@todo: not working        
        setcookie('app_flash_message', json_encode([]));

        $strOutput = '<div class="app-flash-message">';
        
        if ($strType != 'all')
        {
            if (isset(self::$arrFlashData[$strType]))
            {
                $strOutput .= '<div class="alert alert-' . $strType . '">';
                $strOutput .= '<ul class="list-unstyled">';

                foreach (self::$arrFlashData[$strType] as $strValue)
                {
                    $strOutput .= "<li>{$strValue}</li>";
                }

                $strOutput .= '</ul>';
                $strOutput .= '</div>';
            }
        }
        else
        {
            foreach(self::$arrFlashData as $strType => $arrType)
            {
                $strOutput .= '<div class="alert alert-' . $strType . '">';
                $strOutput .= '<ul class="list-unstyled">';

                foreach ($arrType as $strValue)
                {
                    $strOutput .= "<li>{$strValue}</li>";
                }

                $strOutput .= '</ul>';
                $strOutput .= '</div>';
            }
        }

        return $strOutput . '</div>';
    }
}
