<?php

namespace App\Providers;

use Illuminate\Support\ServiceProvider;
use Mockery\Exception\RuntimeException;

class ThemeLayoutServiceProvider extends ServiceProvider
{    
    private static $arrThemeConfig;
    /**
     * Bootstrap the application services.
     *
     * @return void
     */
    public function boot()
    {
        try 
        {
            if (!($strJson = @file_get_contents(env('APP_THEME_JSON'))))
            {
                throw new RuntimeException("Invalid JSON Theme");
            }

            self::$arrThemeConfig = json_decode($strJson, true);

            if (!isset(self::$arrThemeConfig[env('APP_THEME')], self::$arrThemeConfig[env('APP_THEME')][env('APP_THEME_VERSION')]))
            {
                throw new RuntimeException("Invalid combination of Theme and Version");
            }

            switch(env('APP_ENV'))
            {
                case "production":
                    self::$arrThemeConfig = self::$arrThemeConfig[env('APP_THEME')][env('APP_THEME_VERSION')]["live"];
                    break;
                
                default:
                    self::$arrThemeConfig = self::$arrThemeConfig[env('APP_THEME')][env('APP_THEME_VERSION')]["dev"];
            }
        } 
        catch (RuntimeException $objRuntimeException) 
        {
            syslog(LOG_CRIT, __FILE__ . '::' . __METHOD__ . '::' . __LINE__ . ' => ' . $objRuntimeException->getMessage());
        }
    }

    /**
     * Register the application services.
     *
     * @return void
     */
    public function register()
    {
        //
    }
    
    public static function loadCSS()
    {
        if (self::$arrThemeConfig)
        {
            foreach(self::$arrThemeConfig['css'] as $strFile)
            {
                echo '<link href="' . $strFile . '" rel="stylesheet" type="text/css" />';
            }
        }
    }
    
    public static function loadJS()
    {
        if (self::$arrThemeConfig['js'])
        {
            foreach(self::$arrThemeConfig['js'] as $strFile)
            {
                echo '<script src="' . $strFile . '"></script>';
            }
        }
    }
}
