<?php

namespace App\Providers;

use App\Classes\IUserAuth;
use App\Models\Users\TAccountBalance;
use App\Models\Users\TProductPlan;
use Illuminate\Contracts\Auth\Access\Gate as GateContract;
use Illuminate\Foundation\Support\Providers\AuthServiceProvider as ServiceProvider;
use Illuminate\Support\Facades\Crypt;
use Illuminate\Support\Facades\Session;

class AuthServiceProvider extends ServiceProvider
{
    /**
     * The policy mappings for the application.
     *
     * @var array
     */
    protected $policies = [
        'App\Model' => 'App\Policies\ModelPolicy',
        //@todo: buyer shouldn't be able to create a product campaign
    ];

    /**
     * Register any application authentication / authorization services.
     *
     * @param  \Illuminate\Contracts\Auth\Access\Gate $gate
     * @return void
     */
    public function boot(GateContract $gate)
    {
        $this->registerPolicies($gate);

        //
    }

    /**
     * Used to check if someone is still login
     *
     * @return boolean
     *
     * */
    static public function bolLogin()
    {
        try
        {
            return self::getUserAuth()->getUserId() > 0;
        }
        catch(\Exception $objError)
        {
            return false;
        }
    }

    /**
     * Used to get instance of IUserAuth
     *
     * @throws \Exception
     * @return IUserAuth
     *
     * */
    static public function getUserAuth()
    {
        static $objUserAuth;

        $strSecretAuth = Session::get('SECRET_AUTH');

        if ($strSecretAuth)
        {
            if (is_null($objUserAuth))
            {
                $objUserAuth = unserialize(Crypt::decrypt($strSecretAuth));
            }

            if ($objUserAuth instanceof IUserAuth)
            {
                $strHashUser = Session::get('user_hash_id');

                if ($strHashUser)
                {
                    $intUserId = Crypt::decrypt($strHashUser);

                    if ($objUserAuth->getUserId() == $intUserId)
                    {
                        return $objUserAuth;
                    }
                    else
                    {
                        syslog(LOG_CRIT, __CLASS__ . ' > ' . __METHOD__ . ' > ' . $objUserAuth->getUserId() . '---' . $intUserId);

                        throw new \Exception("Secret auth user hash id does not match session user hash id");
                    }
                }
                else
                {
                    throw new \Exception("No user hash id session found");
                }
            }
            else
            {
                syslog(LOG_CRIT, __CLASS__ . ' > ' . __METHOD__ . ' > ' . 'failed to unserialize IUserAuth');

                throw new \Exception("Not an instance of App\\Classes\\IUserAuth, user is messing with the app.");
            }
        }
        else
        {
            throw new \Exception("No secret auth session found");
        }
    }

    /**
     * Used to get the profile photo by user id
     *
     * @param $intUserId integer
     * 
     * @throws \Exception
     * @return string
     *
     * */
    static public function getProfilePhotoByUserId($intUserId)
    {
        $intUserId = (int)$intUserId;
        
        if(@file_exists("../resources/sysclient/images/user/{$intUserId}.jpg"))
        {
            $strPhoto = "data:image/jpeg;base64," . base64_encode(file_get_contents("../resources/sysclient/images/user/{$intUserId}.jpg"));
        }
        else
        {
            $strPhoto = null;
        }

        return $strPhoto;
    }

    /**
     * Used to get the account balance by User Id
     *
     * @param $intUserId integer
     * 
     * @return float
     *
     * */
    static public function getAccountBalanceByUserId($intUserId)
    {
        $intUserId = (int)$intUserId;

        $objTAccountBalance = TAccountBalance::where('user_id', $intUserId)->first();

        if ($objTAccountBalance)
        {
            return (float)$objTAccountBalance['amount'];
        }
        else
        {
            return 0;
        }
    }

    /**
     * Used to get the campaign plan by User Id
     *
     * @param $intUserId integer
     *
     * @return string
     *
     * */
    static public function getCampaignPlanByUserId($intUserId)
    {
        $intUserId = (int)$intUserId;
        
        $objTProductPlan = TProductPlan::join('c_product_plan', 'c_product_plan.product_plan_id', '=', 't_product_plan.product_plan_id')
            ->where('t_product_plan.user_id', $intUserId)
            ->orderBy('plan_id', 'desc')
            ->limit(1)
            ->first();

        return $objTProductPlan['handle'];
    }

    /**
     * Used to get the campaign plan model by User Id
     *
     * @param $intUserId integer
     *
     * @return string
     *
     * */
    static public function getCampaignPlanModelByUserId($intUserId)
    {
        $intUserId = (int)$intUserId;

        $objTProductPlan = TProductPlan::join('c_product_plan', 'c_product_plan.product_plan_id', '=', 't_product_plan.product_plan_id')
            ->where('t_product_plan.user_id', $intUserId)
            ->orderBy('plan_id', 'desc')
            ->limit(1)
            ->first();
        
        return [
            'handle' => $objTProductPlan['handle'],
            'name' => $objTProductPlan['name']
        ];
    }
}
