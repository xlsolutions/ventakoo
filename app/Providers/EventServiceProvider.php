<?php

namespace App\Providers;

use Illuminate\Contracts\Events\Dispatcher as DispatcherContract;
use Illuminate\Foundation\Support\Providers\EventServiceProvider as ServiceProvider;

class EventServiceProvider extends ServiceProvider
{
    /**
     * The event listener mappings for the application.
     *
     * @var array
     */
    protected $listen = [
        'App\Events\SomeEvent' => [
            'App\Listeners\EventListener',
        ]
    ];

    /**
     * The subscriber classes to register.
     *
     * @var array
     */
    protected $subscribe = [
        'App\Events\VerifyRegistrationEmailEvent',
        'App\Events\ForgotPasswordEmailEvent',
        'App\Events\SetupBuyerAccountEvent',
        'App\Events\SetupSellerAccountEvent',
        'App\Events\UserLoginEvent',
        'App\Events\UserLogoutEvent',
        'App\Events\AdPostReviewEvent',
        'App\Events\AdPostPublishEvent',
        'App\Events\PayPal\PayPalIPNEvent',
        'App\Events\PayPal\PayPalTransactionEvent',
        'App\Events\OrderStateEvent',
        'App\Events\AccountBalanceEvent',
        'App\Events\CancelRecentOrderEvent',
        'App\Events\CancelAbandonedOrderEvent',
        'App\Events\ExpireCampaignProductEvent'
    ];

    /**
     * Register any other events for your application.
     *
     * @return void
     */
    public function boot()
    {
        parent::boot();

        //
    }
}
