<?php

namespace App\Providers;

use Illuminate\Support\ServiceProvider;

class ViewDataServiceProvider extends ServiceProvider
{
    private static $arrViewData;
    
    /**
     * Bootstrap the application services.
     *
     * @return void
     */
    public function boot()
    {
        self::$arrViewData['company'] = 'Ventakoo';
        self::$arrViewData['page_title'] = 'Welcome to Ventakoo!';
        
        self::$arrViewData['meta_description'] = '';
        self::$arrViewData['meta_keyword'] = 'buy, sell, gadgets, realty, services';
        self::$arrViewData['meta_author'] = 'project-r';
        
        self::$arrViewData['app_url'] = env('APP_URL');
    }

    /**
     * Register the application services.
     *
     * @return void
     */
    public function register()
    {
        //
    }

    public static function add($strKey, $strValue)
    {
        self::$arrViewData[$strKey] = $strValue;
    }

    public static function get($strKey)
    {
        if (!isset(self::$arrViewData[$strKey]))
        {
            return null;
        }
        
        return self::$arrViewData[$strKey];
    }
}
