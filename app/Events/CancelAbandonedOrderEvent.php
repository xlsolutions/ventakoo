<?php

namespace App\Events;

use Illuminate\Events\Dispatcher;

class CancelAbandonedOrderEvent extends UserEvent
{
    /**
     * Register the listeners for the subscriber.
     *
     * @param Dispatcher $objDispatcher
     */
    public function subscribe(Dispatcher $objDispatcher)
    {
        $objDispatcher->listen(
            'App\Events\CancelAbandonedOrderEvent',
            'App\Listeners\CancelAbandonedOrderListener'
        );
    }
}