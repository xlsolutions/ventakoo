<?php

namespace App\Events;

use App\Models\Users\TUserInformation;
use Illuminate\Events\Dispatcher;

class UserLoginEvent extends UserEvent
{
    private $objTUserInformation;
    private $bolRefreshAuth = false;
    
    public function __construct(TUserInformation $objTUserInformation)
    {
        $this->objTUserInformation = $objTUserInformation;
    }
    
    public function getTUserInformation()
    {
        return $this->objTUserInformation;
    }
    
    public function setRefreshAuth($bolRefresh)
    {
        $this->bolRefreshAuth = (boolean)$bolRefresh;
    }
    
    public function getRefreshAuth()
    {
        return $this->bolRefreshAuth;
    }

    /**
     * Register the listeners for the subscriber.
     *
     * @param Dispatcher $objDispatcher
     */
    public function subscribe(Dispatcher $objDispatcher)
    {
        $objDispatcher->listen(
            'App\Events\UserLoginEvent',
            'App\Listeners\UserLoginListener'
        );
    }
}