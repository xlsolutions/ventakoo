<?php

namespace App\Events;

use App\Models\AdPost\TAdPost;
use Illuminate\Events\Dispatcher;

abstract class AdPostReviewStatus {
    
    const DRAFT = 'DRAFT';
    const FOR_REVIEW = 'FOR_REVIEW';
    const REVIEWING = 'REVIEWING';
    const CANCELLED = 'CANCELLED';
    const DECLINED = 'DECLINED';
    const APPROVED = 'APPROVED';
    const START = 'START';
    const PAUSE = 'PAUSED';
    const COMPLETED = 'COMPLETED';
}

class AdPostReviewEvent extends UserEvent
{    
    private $intAdPostId;
    private $strAdPostReviewStatus;

    public function __construct($intAdPostId = 0)
    {
        $this->intAdPostId = $intAdPostId;
    }

    public function getAdPostId()
    {
        return $this->intAdPostId;
    }
    
    public function setReviewStatus($strAdPostReviewStatus)
    {
        $this->strAdPostReviewStatus = $strAdPostReviewStatus;
    }
    
    public function getReviewStatus()
    {
        return $this->strAdPostReviewStatus;
    }

    public function getUserIdByAdPostId()
    {
        $objTAdPost = TAdPost::query()
            ->select('t_product.user_id')
            ->join('t_product', 't_product.product_id', '=', 't_ad_post.product_id')
            ->find($this->intAdPostId);

        return $objTAdPost->user_id;
    }

    /**
     * Register the listeners for the subscriber.
     *
     * @param Dispatcher $objDispatcher
     */
    public function subscribe(Dispatcher $objDispatcher)
    {
        $objDispatcher->listen(
            'App\Events\AdPostReviewEvent',
            'App\Listeners\AdPostReviewListener'
        );
    }
}