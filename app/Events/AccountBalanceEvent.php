<?php

namespace App\Events;

use Illuminate\Events\Dispatcher;

class AccountBalanceEvent extends UserEvent
{
    private $intBalance;

    private $bolCredit = false;
    private $bolDebit = false;
    
    public function setBalance($intBalance)
    {
        $this->intBalance = (int)$intBalance;
    }
    
    public function getBalance()
    {
        return $this->intBalance;
    }

    /**
     * @param boolean $bolCredit
     */
    public function setCredit($bolCredit)
    {
        $this->bolCredit = (boolean)$bolCredit;
    }

    /**
     * @return boolean
     */
    public function getCredit()
    {
        return $this->bolCredit;
    }

    /**
     * @param boolean $bolDebit
     */
    public function setDebit($bolDebit)
    {
        $this->bolDebit = (boolean)$bolDebit;
    }

    /**
     * @return boolean
     */
    public function getDebit()
    {
        return $this->bolDebit;
    }
    
    /**
     * Register the listeners for the subscriber.
     *
     * @param Dispatcher $objDispatcher
     */
    public function subscribe(Dispatcher $objDispatcher)
    {
        $objDispatcher->listen(
            'App\Events\AccountBalanceEvent',
            'App\Listeners\AccountBalanceListener'
        );
    }
}