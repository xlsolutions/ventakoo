<?php

namespace App\Events;

use App\Classes\UserAuth;
use Illuminate\Events\Dispatcher;

class UserLogoutEvent extends UserEvent
{
    private $objUserAuth;

    public function __construct(UserAuth $objUserAuth)
    {
        $this->objUserAuth = $objUserAuth;
    }

    public function getUserAuth()
    {
        return $this->objUserAuth;
    }

    /**
     * Register the listeners for the subscriber.
     *
     * @param Dispatcher $objDispatcher
     */
    public function subscribe(Dispatcher $objDispatcher)
    {
        $objDispatcher->listen(
            'App\Events\UserLogoutEvent',
            'App\Listeners\UserLogoutListener'
        );
    }
}