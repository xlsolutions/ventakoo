<?php

namespace App\Events;

use App\Models\Constants\COrderStatus;
use Illuminate\Events\Dispatcher;

abstract class OrderState
{
    const NEW_PAYMENT = 'NEW';
    const INITIATING_PAYMENT = 'INITIATING';
    const AWAITING_PAYMENT = 'AWAITING_PAYMENT';
    const ABANDONED = 'ABANDONED';
    const CHARGING = 'CHARGING';
    const PROCESSING = 'PROCESSING';
    const CANCELLED = 'CANCELLED';
    const DECLINED = 'DECLINED';
    const COMPLETED = 'COMPLETED';
    const REVIEW = 'REVIEW';
}

class OrderStateEvent extends UserEvent
{
    private $strState;
    private $strCurrentState;
    private $intOrderId;

    private $objCOrderStatus;

    public function __construct($intOrderId = null, $strCurrentState = null, $strNewState = null)
    {
        if (is_null($intOrderId) || is_null($strCurrentState) || is_null($strNewState))
        {
            return false;
        }

        $this->strCurrentState = $strCurrentState;
        $this->intOrderId = (int)$intOrderId;

        $strError = null;

        switch ($strNewState)
        {
            case OrderState::INITIATING_PAYMENT:
                if (!in_array($strCurrentState, [OrderState::NEW_PAYMENT, OrderState::REVIEW]))
                {
                    $strError = "ERR_1";
                }
                
                break;
            
            case OrderState::PROCESSING:
                if (!in_array($strCurrentState, [OrderState::INITIATING_PAYMENT]))
                {
                    $strError = "ERR_2";
                }
            
                break;
            
            case OrderState::ABANDONED:
                if (!in_array($strCurrentState, [OrderState::NEW_PAYMENT]))
                {
                    $strError = "ERR_3";
                }
                
                break;
            
            case OrderState::CANCELLED:
                if (!in_array($strCurrentState, [OrderState::REVIEW, OrderState::NEW_PAYMENT]))
                {
                    $strError = "ERR_4";
                }
                
                break;
            
            case OrderState::REVIEW:
                if (!in_array($strCurrentState, [OrderState::NEW_PAYMENT, OrderState::INITIATING_PAYMENT, OrderState::PROCESSING]))
                {
                    $strError = "ERR_5";
                }
            
                break;
            
            case OrderState::DECLINED:
                if (!in_array($strCurrentState, [OrderState::REVIEW, OrderState::PROCESSING]))
                {
                    $strError = "ERR_6";
                }
            
                break;
        }
        
        if (!is_null($strError))
        {
            $strMessage = __CLASS__ . ':' . __METHOD__ . ": Changing Order #: {$this->intOrderId} state from {$strCurrentState} to {$strNewState}, Error: {$strError}";
            
            syslog(LOG_CRIT, $strMessage);
            
            throw new \Exception($strMessage);
        }

        $this->strState = $strNewState;

        $this->objCOrderStatus = COrderStatus::where('handle', $this->strState)->first();
        
        if (!$this->objCOrderStatus)
        {
            $strMessage = __CLASS__ . ':' . __METHOD__ . ": Changing Order #: {$this->intOrderId} state from {$strCurrentState} to {$strNewState}, Error: State handle not found.";

            syslog(LOG_CRIT, $strMessage);

            throw new \Exception($strMessage);
        }
    }
    
    public function getOrderId()
    {
        return $this->intOrderId;
    }
    
    public function getCurrentState()
    {
        return $this->strCurrentState;
    }
    
    public function getNewState()
    {
        return $this->strState;
    }

    public function getOrderStatusId()
    {
        return $this->objCOrderStatus['order_status_id'];
    }

    /**
     * Register the listeners for the subscriber.
     *
     * @param Dispatcher $objDispatcher
     */
    public function subscribe(Dispatcher $objDispatcher)
    {
        $objDispatcher->listen(
            'App\Events\OrderStateEvent',
            'App\Listeners\OrderStateListener'
        );
    }
}