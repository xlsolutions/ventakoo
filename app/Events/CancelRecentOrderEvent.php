<?php

namespace App\Events;

use Illuminate\Events\Dispatcher;

class CancelRecentOrderEvent extends UserEvent
{
    /**
     * Register the listeners for the subscriber.
     *
     * @param Dispatcher $objDispatcher
     */
    public function subscribe(Dispatcher $objDispatcher)
    {
        $objDispatcher->listen(
            'App\Events\CancelRecentOrderEvent',
            'App\Listeners\CancelRecentOrderListener'
        );
    }
}