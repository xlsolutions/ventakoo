<?php

namespace App\Events;

use Illuminate\Events\Dispatcher;

class ExpireCampaignProductEvent extends Event
{
    private $intCampaignProductId;

    public function setCampaignProductId($intCampaignProductId)
    {
        $this->intCampaignProductId = (int)$intCampaignProductId;
    }

    public function getCampaignProductId()
    {
        return $this->intCampaignProductId;
    }

    /**
     * Register the listeners for the subscriber.
     *
     * @param Dispatcher $objDispatcher
     */
    public function subscribe(Dispatcher $objDispatcher)
    {
        $objDispatcher->listen(
            'App\Events\ExpireCampaignProductEvent',
            'App\Listeners\ExpireCampaignProductListener'
        );
    }
}