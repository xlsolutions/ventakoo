<?php

namespace App\Events;

use Illuminate\Events\Dispatcher;

class ForgotPasswordEmailEvent extends EmailEvent
{
    /**
     * Register the listeners for the subscriber.
     *
     * @param Dispatcher $objDispatcher
     */
    public function subscribe(Dispatcher $objDispatcher)
    {
        $objDispatcher->listen(
            'App\Events\ForgotPasswordEmailEvent',
            'App\Listeners\ForgotPasswordEmailListener'
        );
    }
}