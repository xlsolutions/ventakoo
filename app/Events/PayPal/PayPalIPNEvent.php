<?php

namespace App\Events\PayPal;

use Illuminate\Events\Dispatcher;

class PayPalIPNEvent extends PayPalEvent
{
    private $intOrderId;
    private $decOrderGrossAmount;
    
    public function setOrderId($intOrderId)
    {
        $this->intOrderId = (int)$intOrderId;
    }
    
    public function getOrderId()
    {
        return $this->intOrderId;
    }
    
    public function setOrderGrossAmount($decOrderGrossAmount)
    {
        $this->decOrderGrossAmount = (float)$decOrderGrossAmount;
    }
    
    public function getOrderGrossAmount()
    {
        return $this->decOrderGrossAmount;
    }
    
    /**
     * Register the listeners for the subscriber.
     *
     * @param Dispatcher $objDispatcher
     */
    public function subscribe(Dispatcher $objDispatcher)
    {
        $objDispatcher->listen(
            'App\Events\PayPal\PayPalIPNEvent',
            'App\Listeners\PayPal\PayPalIPNListener'
        );
    }
}