<?php

namespace App\Events\PayPal;

use App\Events\UserEvent;

abstract class PayPalEvent extends UserEvent
{
    private $arrRequest;
    private $strRequestParam;
    
    final public function __construct()
    {
        $strStreamPostData = file_get_contents('php://input');

        $bolGetMagicQuotesGpc = function_exists('get_magic_quotes_gpc');

        if ($this->getIntUserId())
        {
            syslog(LOG_DEBUG, __CLASS__ . ':' . __METHOD__ . ' - raw ipn: ' . $strStreamPostData);
        }

        $arrPostData = explode('&', $strStreamPostData);
        
        foreach ($arrPostData as $intIndex => $strValue) 
        {
            $arrValue = explode ('=', $strValue);
            
            if (count($arrValue) === 2)
            {
                if ($bolGetMagicQuotesGpc === true && get_magic_quotes_gpc() === 1)
                {
                    $arrPostData[$arrValue[0]] = urldecode(stripslashes($arrValue[1]));
                }
                else
                {
                    $arrPostData[$arrValue[0]] = urldecode($arrValue[1]);
                }
            }
            
            unset($arrPostData[$intIndex]);
        }

        //@readme: make sure cmd=_notify-validate is in front
        $this->strRequestParam = 'cmd=_notify-validate&' . http_build_query($arrPostData);

        $this->arrRequest = $arrPostData;
    }
    
    public function getRequest()
    {
        return $this->arrRequest;
    }
    
    public function getRequestParam()
    {
        return $this->strRequestParam;
    }
}