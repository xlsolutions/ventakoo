<?php

namespace App\Events\PayPal;

use App\Classes\PayPal\IPayPalBuyer;
use App\Classes\PayPal\IPayPalBuyerInfo;
use App\Classes\PayPal\IPayPalTrans;
use App\Classes\PayPal\IPayPalTransAddress;
use App\Classes\PayPal\IPayPalTransAmount;
use App\Classes\PayPal\IPayPalTransItem;
use App\Classes\PayPal\IPayPalTransPayerInfo;
use Illuminate\Events\Dispatcher;

class PayPalTransactionEvent extends PayPalEvent
{
    private $objPayPalTrans;
    private $objPayPalTransItem;
    private $objPayPalTransAmount;
    private $objPayPalTransPayerInfo;
    private $objPayPalTransAddress;
    private $objPayPalTransBuyerInfo;
    
    public function setPayPalTrans(IPayPalTrans $objPayPalTrans)
    {
        $this->objPayPalTrans = $objPayPalTrans;
    }
    
    public function getPayPalTrans()
    {
        return $this->objPayPalTrans;
    }
    
    public function setPayPalTransItem(IPayPalTransItem $objPayPalTransItem)
    {
        $this->objPayPalTransItem = $objPayPalTransItem;        
    }
    
    public function getPayPalTransItem()
    {
        return $this->objPayPalTransItem;
    }
    
    public function setPayPalTransAmount(IPayPalTransAmount $objPayPalTransAmount)
    {
        $this->objPayPalTransAmount = $objPayPalTransAmount;
    }
    
    public function getPayPalTransAmount()
    {
        return $this->objPayPalTransAmount;
    }
    
    public function setPayPalTransPayerInfo(IPayPalTransPayerInfo $objPayPalTransPayerInfo)
    {
        $this->objPayPalTransPayerInfo = $objPayPalTransPayerInfo;
    }
    
    public function getPayPalTransPayerInfo()
    {
        return $this->objPayPalTransPayerInfo;
    }
    
    public function setPayPalTransAddress(IPayPalTransAddress $objPayPalTransAddress)
    {
        $this->objPayPalTransAddress = $objPayPalTransAddress;
    }
    
    public function getPayPalTransAddress()
    {
        return $this->objPayPalTransAddress;
    }
    
    public function setPayPalBuyerInfo(IPayPalBuyerInfo $objPayPalBuyerInfo)
    {
        $this->objPayPalTransBuyerInfo = $objPayPalBuyerInfo;
    }

    public function getPayPalBuyerInfo()
    {
        return $this->objPayPalTransBuyerInfo;
    }
    
    /**
     * Register the listeners for the subscriber.
     *
     * @param Dispatcher $objDispatcher
     */
    public function subscribe(Dispatcher $objDispatcher)
    {
        $objDispatcher->listen(
            'App\Events\PayPal\PayPalTransactionEvent',
            'App\Listeners\PayPal\PayPalTransactionListener'
        );
    }
}