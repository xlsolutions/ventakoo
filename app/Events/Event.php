<?php

namespace App\Events;

use Illuminate\Events\Dispatcher;

abstract class Event
{    
    abstract public function subscribe(Dispatcher $objDispatcher);
}
