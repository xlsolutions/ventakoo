<?php

namespace App\Events;

use Illuminate\Events\Dispatcher;

interface IEmailEvent {

    public function setCode($strCode);
    public function getCode();

    public function setEmail($strEmail);
    public function getEmail();
    
    public function setFrom($strFrom);
    public function getFrom();
    
    public function setFromAddress($strFromAddress);
    public function getFromAddress();
}

abstract class EmailEvent extends Event implements IEmailEvent
{
    private $strEmail;
    private $strCode;
    private $strFrom = 'Ventakoo';
    private $strFromAddress = 'noreply@ventakoo.com';
    
    public function setCode($strCode)
    {
        $this->strCode = $strCode;
    }

    public function getCode()
    {
        return $this->strCode;
    }

    public function setEmail($strEmail)
    {
        $this->strEmail = $strEmail;
    }

    public function getEmail()
    {
        return $this->strEmail;
    }
    
    public function setFrom($strFrom)
    {
        $this->strFrom = $strFrom;
    }
    
    public function getFrom()
    {
        return $this->strFrom;
    }

    public function setFromAddress($strFromAddress)
    {
        $this->strFromAddress = $strFromAddress;
    }
    
    public function getFromAddress()
    {
        return $this->strFromAddress;
    }

    /**
     * Register the listeners for the subscriber.
     *
     * @param Dispatcher $objDispatcher
     */
    public function subscribe(Dispatcher $objDispatcher)
    {
        // do nothing
    }
}