<?php

namespace App\Events;

use Illuminate\Events\Dispatcher;

class SetupSellerAccountEvent extends SetupAccountEvent
{    
    /**
     * Register the listeners for the subscriber.
     *
     * @param Dispatcher $objDispatcher
     */
    public function subscribe(Dispatcher $objDispatcher)
    {
        $objDispatcher->listen(
            'App\Events\SetupSellerAccountEvent',
            'App\Listeners\SetupSellerAccountListener'
        );
    }
}