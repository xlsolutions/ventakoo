<?php

namespace App\Events;

use Illuminate\Events\Dispatcher;

class AdPostPublishEvent extends Event
{
    const DEFAULT_EXPIRY_BY_SECONDS = 2678400; //2678400 = 31 DAYS | 86400 SECS = 1 DAY
    
    private $intAdPostId;
    private $bolPause = false;
    private $bolStart = false;
    private $bolForceExpire = false;
    private $intExpiration;

    public function __construct($intAdPostId = 0)
    {
        $this->intAdPostId = $intAdPostId;
    }

    public function getAdPostId()
    {
        return $this->intAdPostId;
    }
    
    public function setPause($bolPause)
    {
        $this->bolPause = (boolean)$bolPause;
    }
    
    public function getPause()
    {
        return $this->bolPause;
    }
    
    public function setStart($bolStart)
    {
        $this->bolStart = (boolean)$bolStart;
    }
    
    public function getStart()
    {
        return $this->bolStart;
    }

    public function setExpiration($intExpiration)
    {
        $this->intExpiration = (int)$intExpiration;
    }

    public function getExpiration()
    {
        return $this->intExpiration;
    }

    public function setForceExpireCampaign($bolForceExpire)
    {
        $this->bolForceExpire = (boolean)$bolForceExpire;
    }

    public function getForceExpireCampaign()
    {
        return $this->bolForceExpire;
    }
    
    /**
     * Register the listeners for the subscriber.
     *
     * @param Dispatcher $objDispatcher
     */
    public function subscribe(Dispatcher $objDispatcher)
    {
        $objDispatcher->listen(
            'App\Events\AdPostPublishEvent',
            'App\Listeners\AdPostPublishListener'
        );
    }
}