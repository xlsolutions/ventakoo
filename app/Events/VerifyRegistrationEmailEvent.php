<?php

namespace App\Events;

use Illuminate\Events\Dispatcher;

class VerifyRegistrationEmailEvent extends EmailEvent
{
    private $bolPricingPageSession = false;
    
    public function setPricingPageSession($bolPricingPageSession)
    {
        $this->bolPricingPageSession = $bolPricingPageSession;
    }
    
    public function getPricingPageSession()
    {
        return $this->bolPricingPageSession;
    }
    
    /**
     * Register the listeners for the subscriber.
     *
     * @param Dispatcher $objDispatcher
     */
    public function subscribe(Dispatcher $objDispatcher)
    {
        $objDispatcher->listen(
            'App\Events\VerifyRegistrationEmailEvent',
            'App\Listeners\VerifyRegistrationEmailListener'
        );
    }
}