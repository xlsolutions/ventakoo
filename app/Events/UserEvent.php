<?php

namespace App\Events;

interface IUser {
    
    function setIntUserId($intUserId);
    
    function getIntUserId();
}

abstract class UserEvent extends Event implements IUser
{
    private $intUserId;

    /**
     * @param integer $intUserId
     */
    public function setIntUserId($intUserId)
    {
        $this->intUserId = (int)$intUserId;
    }

    /**
     * @return integer
     */
    public function getIntUserId()
    {
        return $this->intUserId;
    }
}