<?php

use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class AddVerifyAccountEmail extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        /**
         * @table t_user_information
         *
         * Add new column validated data type unsigned tinyint
         */
        if (Schema::hasTable('t_user_information'))
        {
            Schema::table('t_user_information', function (Blueprint $objBlueprint)
            {
                $objBlueprint->unsignedTinyInteger('validated')->default(0);
            });
        }

        if (!Schema::hasTable('c_activation_type'))
        {
            Schema::create('c_activation_type', function (Blueprint $objBlueprint) {
                $objBlueprint->smallIncrements('activation_type_id');

                $objBlueprint->string('handle', 50);

                $objBlueprint->unique('handle');

                $objBlueprint->string('description', 100);

                $objBlueprint->softDeletes();
            });
        }

        /**
         * @table t_user_activation
         * @primary_key user_activation_id
         * @foreign_key user_contact_id,activation_type_id
         *
         * Table Description : This is used to activate an account using activation_code and activation_type_id
         */
        if (!Schema::hasTable('t_user_activation'))
        {
            Schema::create('t_user_activation', function (Blueprint $objBlueprint) {
                $objBlueprint->increments('user_activation_id');

                $objBlueprint->integer('user_contact_id', false, true);
                $objBlueprint->foreign('user_contact_id','t_user_activation_ibfk1')->references('user_contact_id')->on('t_user_contact');

                $objBlueprint->unique(['user_contact_id', 'activation_type_id'], 'user_activation_key');

                $objBlueprint->string('activation_code',10);

                $objBlueprint->smallInteger('activation_type_id',false,true);
                
                $objBlueprint->foreign('activation_type_id','t_user_activation_ibfk2')->references('activation_type_id')->on('c_activation_type');

                $objBlueprint->timestamps();
                $objBlueprint->softDeletes();
            });
        }
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::dropIfExists('t_user_activation');
        Schema::dropIfExists('c_activation_type');
    }
}
