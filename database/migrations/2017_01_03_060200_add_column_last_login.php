<?php

use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class AddColumnLastLogin extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        /**
         * @table t_user_information
         *
         * Add new column last_login data type unsigned tinyint
         */
        if (Schema::hasTable('t_user_information'))
        {
            Schema::table('t_user_information', function (Blueprint $table)
            {
                $table->dateTime('last_login')->nullable();
            });
        }
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {

    }
}
