<?php

use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class UserProductPlanAndOrderTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        $this->createCOrderStatus();

        $this->createTOrder();

        $this->createTFinanceTransaction();

        $this->createCProductPlan();

        $this->createTProductPlan();

        $this->createTOrderType();

        $this->createTOrderItem();

        $this->createTAccountBalance();
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::disableForeignKeyConstraints();

        Schema::dropIfExists('t_order_item');
        Schema::dropIfExists('t_order');
        Schema::dropIfExists('t_order_type');
        Schema::dropIfExists('c_order_status');
        Schema::dropIfExists('t_finance_transaction');

        Schema::dropIfExists('t_account_balance');

        Schema::dropIfExists('t_product_plan');
        Schema::dropIfExists('c_product_plan');

        Schema::enableForeignKeyConstraints();
    }

    private function createCOrderStatus()
    {
        /**
         * @table c_order_status
         * @primary_key order_status_id
         * @unique_key handle
         *
         * Table Description : Used to know the current status of an order
         *
         * Definitions:
         * NEW - new order in queue
         * INITIATING - order is being process
         * AWAITING_PAYMENT - order is waiting for the payment
         * ABANDONED - order was abandoned
         * CHARGING - order is now charging
         * PROCESSING - order is being process
         * CANCELLED - order was cancelled
         * DECLINED - order was declined by the system or admin
         * COMPLETED - order was completed
         */
        if (!Schema::hasTable('c_order_status'))
        {
            Schema::create('c_order_status', function (Blueprint $objBlueprint) {
                $objBlueprint->tinyInteger('order_status_id', true, true);

                $objBlueprint->string('handle', 50);
                $objBlueprint->unique('handle', 'handle');

                $objBlueprint->string('name', 50);
            });
        }
    }

    private function createTOrder()
    {
        /**
         * @table t_order
         * @primary_key order_id
         * @foreign_key user_id
         * @foreign_key order_status_id
         *
         * Table Description : Orders of user
         */
        if (!Schema::hasTable('t_order'))
        {
            Schema::create('t_order', function (Blueprint $objBlueprint) {
                $objBlueprint->integer('order_id', true, true);

                $objBlueprint->integer('user_id', false, true);
                $objBlueprint->foreign('user_id','t_order_ibfk1')->references('user_id')->on('t_user');

                $objBlueprint->tinyInteger('order_status_id', false, true);
                $objBlueprint->foreign('order_status_id','t_order_ibfk2')->references('order_status_id')->on('c_order_status');

                $objBlueprint->unsignedInteger('price');

                $objBlueprint->timestamps();
            });
        }
    }

    private function createTFinanceTransaction()
    {
        /**
         * @table t_finance_transaction
         * @primary_key product_plan_id
         * @unique_key handle
         *
         * Table Description : Use to separate the query usage of t_order
         */
        if (!Schema::hasTable('t_finance_transaction'))
        {
            Schema::create('t_finance_transaction', function (Blueprint $objBlueprint) {
                $objBlueprint->integer('finance_transaction_id', true, true);

                $objBlueprint->integer('order_id', false, true);
                $objBlueprint->foreign('order_id','t_finance_transaction_ibfk1')->references('order_id')->on('t_order');

                $objBlueprint->text('note');
                $objBlueprint->timestamps();
            });
        }
    }

    private function createCProductPlan()
    {
        /**
         * @table c_product_plan
         * @primary_key product_plan_id
         * @unique_key handle
         *
         * Table Description : Used to store product plans
         */
        if (!Schema::hasTable('c_product_plan'))
        {
            Schema::create('c_product_plan', function (Blueprint $objBlueprint) {
                $objBlueprint->tinyInteger('product_plan_id', true, true);

                $objBlueprint->string('handle', 50);
                $objBlueprint->unique('handle', 'handle');

                $objBlueprint->string('name', 50);

                $objBlueprint->text('description');

                $objBlueprint->unsignedTinyInteger('is_available');

                $objBlueprint->unsignedInteger('price');

                $objBlueprint->timestamps();
            });
        }
    }

    private function createTProductPlan()
    {
        /**
         * @table t_product_plan
         * @primary_key product_plan_id
         * @foreign_key user_id
         *
         * Table Description : Used to store the product plans of a user
         */
        if (!Schema::hasTable('t_product_plan'))
        {
            Schema::create('t_product_plan', function (Blueprint $objBlueprint) {
                $objBlueprint->integer('plan_id', true, true);

                $objBlueprint->tinyInteger('product_plan_id', false, true);
                $objBlueprint->foreign('product_plan_id','t_product_plan_ibfk1')->references('product_plan_id')->on('c_product_plan');

                $objBlueprint->integer('user_id', false, true);
                $objBlueprint->foreign('user_id','t_product_plan_ibfk2')->references('user_id')->on('t_user');

                $objBlueprint->unique(['product_plan_id', 'user_id'], 't_product_plan_key');
                
                $objBlueprint->timestamps();
            });
        }
    }

    private function createTOrderType()
    {
        /**
         * @table t_order_type
         * @primary_key order_type_id
         * @foreign_key ad_post_id - nullable
         * @foreign_key service_id - nullable
         * @foreign_key product_plan_id - nullable
         *
         * Table Description : Item type of an ordered item
         */
        if (!Schema::hasTable('t_order_type'))
        {
            Schema::create('t_order_type', function (Blueprint $objBlueprint) {
                $objBlueprint->integer('order_type_id', true, true);

                $objBlueprint->integer('ad_post_id', false, true)->nullable();
                $objBlueprint->foreign('ad_post_id','t_order_type_ibfk1')->references('ad_post_id')->on('t_ad_post');

                $objBlueprint->unsignedTinyInteger('plan_id')->nullable();
                $objBlueprint->foreign('plan_id','t_order_type_ibfk2')->references('product_plan_id')->on('c_product_plan');

                $objBlueprint->integer('service_id', false, true)->nullable();
                $objBlueprint->foreign('service_id','t_order_type_ibfk3')->references('service_id')->on('t_service');
            });
        }
    }

    private function createTOrderItem()
    {
        /**
         * @table t_order_item
         * @primary_key order_item_id
         * @foreign_key order_id
         * @foreign_key order_type_id
         *
         * Table Description : Items of an order
         */
        if (!Schema::hasTable('t_order_item'))
        {
            Schema::create('t_order_item', function (Blueprint $objBlueprint) {
                $objBlueprint->integer('order_item_id', true, true);

                $objBlueprint->integer('order_id', false, true);
                $objBlueprint->foreign('order_id','t_order_item_ibfk1')->references('order_id')->on('t_order');

                $objBlueprint->integer('order_type_id', false, true);
                $objBlueprint->foreign('order_type_id','t_order_item_ibfk2')->references('order_type_id')->on('t_order_type');

                $objBlueprint->unsignedInteger('price');
            });
        }
    }

    private function createTAccountBalance()
    {
        /**
         * @table t_account_balance
         * @primary_key account_balance_id
         * @foreign_key user_id
         *
         * Table Description : Used to update the account balance of a user
         */
        if (!Schema::hasTable('t_account_balance'))
        {
            Schema::create('t_account_balance', function (Blueprint $objBlueprint) {
                $objBlueprint->integer('account_balance_id', true, true);

                $objBlueprint->integer('user_id', false, true);
                $objBlueprint->unique('user_id', 'user_id');
                $objBlueprint->foreign('user_id','t_account_balance_ibfk1')->references('user_id')->on('t_user');

                $objBlueprint->unsignedInteger('amount');
            });
        }
    }
}