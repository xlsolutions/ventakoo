<?php

use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class PayPalTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        /**
         * @table t_paypal_trans
         * @primary_key paypal_trans_id
         * @unique_key txn_id
         * @foreign_key order_id
         * @key ipn_track_id
         * @key payment_date
         * @key payment_status
         * @key verify_sign
         * @key payment_type
         * @key txn_type
         *
         */
        if (!Schema::hasTable('t_paypal_trans'))
        {
            Schema::create('t_paypal_trans', function (Blueprint $objBlueprint) {
                $objBlueprint->unsignedInteger('paypal_trans_id', true);

                $objBlueprint->string('txn_id', 30);
                $objBlueprint->unique('txn_id', 'txn_id');

                $objBlueprint->unsignedInteger('order_id');
                $objBlueprint->index('order_id', 'order_id');
                $objBlueprint->foreign('order_id', 't_paypal_trans_ibfk1')->references('order_id')->on('t_order');

                $objBlueprint->string('ipn_track_id', 50)->nullable();
                
                $objBlueprint->string('protection_eligibility', 50)->nullable();
                
                $objBlueprint->string('payment_date', 35);
                $objBlueprint->index('payment_date', 'payment_date');

                $objBlueprint->string('payment_status', 15);
                $objBlueprint->index('payment_status', 'payment_status');

                $objBlueprint->string('pending_reason', 40);
                $objBlueprint->index('pending_reason', 'pending_reason');

                $objBlueprint->string('reason_code', 40);
                $objBlueprint->index('reason_code', 'reason_code');

                $objBlueprint->string('verify_sign', 100);
                $objBlueprint->index('verify_sign', 'verify_sign');

                $objBlueprint->string('payment_type', 10);
                $objBlueprint->index('payment_type', 'payment_type');

                $objBlueprint->string('txn_type', 20);
                $objBlueprint->index('txn_type', 'txn_type');
                
                $objBlueprint->unsignedInteger('quantity');

                $objBlueprint->timestamps();
            });
        }

        /**
         * @table t_paypal_trans_item
         * @primary_key paypal_trans_item_id
         * @foreign_key paypal_trans_id
         */
        if (!Schema::hasTable('t_paypal_trans_item'))
        {
            Schema::create('t_paypal_trans_item', function (Blueprint $objBlueprint) {
                $objBlueprint->unsignedInteger('paypal_trans_item_id', true);

                $objBlueprint->unsignedInteger('paypal_trans_id');
                $objBlueprint->index('paypal_trans_id', 'paypal_trans_id');
                $objBlueprint->foreign('paypal_trans_id', 't_paypal_trans_item_ibfk1')->references('paypal_trans_id')->on('t_paypal_trans');

                $objBlueprint->string('item_name', 127);
                $objBlueprint->index('item_name', 'item_name');

                $objBlueprint->string('item_number', 127);
                $objBlueprint->index('item_number', 'item_number');
            });
        }

        /**
         * @table t_paypal_trans_amount
         * @primary_key paypal_trans_amount_id
         * @foreign_key paypal_trans_id
         */
        if (!Schema::hasTable('t_paypal_trans_amount'))
        {
            Schema::create('t_paypal_trans_amount', function (Blueprint $objBlueprint) {
                $objBlueprint->unsignedInteger('paypal_trans_amount_id', true);

                $objBlueprint->unsignedInteger('paypal_trans_id');
                $objBlueprint->index('paypal_trans_id', 'paypal_trans_id');
                $objBlueprint->foreign('paypal_trans_id', 't_paypal_trans_amount_ibfk1')->references('paypal_trans_id')->on('t_paypal_trans');

                //@readme: currency of this transaction
                $objBlueprint->string('mc_currency', 3);
                $objBlueprint->index('mc_currency', 'mc_currency');

                //@readme: total amount of this transaction
                $objBlueprint->decimal('mc_gross', 20, 10);

                //@readme: exchange rate from paypal, based in the primary currency of your account
                $objBlueprint->decimal('exchange_rate', 20, 10)->nullable();

                //@readme: mc_gross amount converted based in primary currency of your account
                $objBlueprint->decimal('settle_amount', 20, 10)->nullable();

                //@readme: primary currency of your account
                $objBlueprint->string('settle_currency', 3)->nullable();
                $objBlueprint->index('settle_currency', 'settle_currency');

                //@readme: paypal transaction fee from mc_gross
                $objBlueprint->decimal('mc_fee', 20, 10);
            });
        }

        /**
         * @table t_paypal_trans_payer_info
         * @primary_key paypal_trans_payer_info_id
         * @foreign_key paypal_trans_id
         */
        if (!Schema::hasTable('t_paypal_trans_payer_info'))
        {
            Schema::create('t_paypal_trans_payer_info', function (Blueprint $objBlueprint) {
                $objBlueprint->unsignedInteger('paypal_trans_payer_info_id', true);

                $objBlueprint->unsignedInteger('paypal_trans_id');
                $objBlueprint->index('paypal_trans_id', 'paypal_trans_id');
                $objBlueprint->foreign('paypal_trans_id', 't_paypal_trans_payer_info_ibfk1')->references('paypal_trans_id')->on('t_paypal_trans');

                $objBlueprint->string('first_name', 70);

                $objBlueprint->string('last_name', 70);

                $objBlueprint->string('payer_status', 15);
                $objBlueprint->index('payer_status', 'payer_status');

                $objBlueprint->string('residence_country', 30)->nullable();
                $objBlueprint->index('residence_country', 'residence_country');
            });
        }

        /**
         * @table t_paypal_trans_address
         * @primary_key paypal_trans_address_id
         * @foreign_key paypal_trans_id
         */
        if (!Schema::hasTable('t_paypal_trans_address'))
        {
            Schema::create('t_paypal_trans_address', function (Blueprint $objBlueprint) {
                $objBlueprint->unsignedInteger('paypal_trans_address_id', true);

                $objBlueprint->unsignedInteger('paypal_trans_id');
                $objBlueprint->index('paypal_trans_id', 'paypal_trans_id');
                $objBlueprint->foreign('paypal_trans_id', 't_paypal_trans_address_ibfk1')->references('paypal_trans_id')->on('t_paypal_trans');

                $objBlueprint->string('address_status', 20)->nullable();
                $objBlueprint->index('address_status', 'address_status');

                $objBlueprint->string('address_street', 210)->nullable();
                $objBlueprint->index('address_street', 'address_street');

                $objBlueprint->string('address_zip', 30)->nullable();
                $objBlueprint->index('address_zip', 'address_zip');

                $objBlueprint->string('address_country_code', 3)->nullable();
                $objBlueprint->index('address_country_code', 'address_country_code');

                $objBlueprint->string('address_name', 140)->nullable();

                $objBlueprint->string('address_country', 80)->nullable();
                $objBlueprint->index('address_country', 'address_country');

                $objBlueprint->string('address_city', 50)->nullable();
                $objBlueprint->index('address_city', 'address_city');

                $objBlueprint->string('address_state', 50)->nullable();
                $objBlueprint->index('address_state', 'address_state');
            });
        }

        /**
         * @table t_paypal_buyer
         * @primary_key buyer_id
         * @foreign_key user_id
         */
        if (!Schema::hasTable('t_paypal_buyer'))
        {
            Schema::create('t_paypal_buyer', function (Blueprint $objBlueprint) {
                $objBlueprint->unsignedInteger('buyer_id', true);

                $objBlueprint->unsignedInteger('user_id');
                $objBlueprint->index('user_id', 'user_id');
                $objBlueprint->foreign('user_id', 't_paypal_buyer_ibfk1')->references('user_id')->on('t_user');
            });
        }

        /**
         * @table t_paypal_buyer_info
         * @primary_key buyer_info_id
         * @foreign_key buyer_id
         */
        if (!Schema::hasTable('t_paypal_buyer_info'))
        {
            Schema::create('t_paypal_buyer_info', function (Blueprint $objBlueprint) {
                $objBlueprint->unsignedInteger('buyer_info_id', true);

                $objBlueprint->unsignedInteger('buyer_id');
                $objBlueprint->index('buyer_id', 'buyer_id');
                $objBlueprint->foreign('buyer_id', 't_paypal_buyer_info_ibfk1')->references('buyer_id')->on('t_paypal_buyer');

                $objBlueprint->string('payer_email', 130);
                $objBlueprint->unique('payer_email', 'payer_email');
                
                $objBlueprint->string('payer_id', 20);
                $objBlueprint->unique('payer_id', 'payer_id');

                $objBlueprint->timestamps();
            });
        }
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::dropIfExists('t_paypal_trans_item');
        Schema::dropIfExists('t_paypal_trans_amount');
        Schema::dropIfExists('t_paypal_trans_payer_info');
        Schema::dropIfExists('t_paypal_trans_address');
        Schema::dropIfExists('t_paypal_trans');
        Schema::dropIfExists('t_paypal_buyer_info');
        Schema::dropIfExists('t_paypal_buyer');
    }
}
