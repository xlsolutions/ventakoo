<?php

use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class AdPostTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        /**
         * @table c_ad_post_status
         * @primary_key ad_post_status_id
         * @unique_key handle
         *
         */
        if (!Schema::hasTable('c_ad_post_status'))
        {
            Schema::create('c_ad_post_status', function (Blueprint $objBlueprint) {
                $objBlueprint->unsignedTinyInteger('ad_post_status_id', true);

                $objBlueprint->string('name', 50);
                
                $objBlueprint->string('handle', 50);
                $objBlueprint->unique('handle');
                
                $objBlueprint->string('description', 255);
            });
        }

        /**
         * @table t_ad_post
         * @primary_key ad_post_id
         * @foreign_key ad_post_status_id, product_id
         *
         * Table Description : Used to publish a product
         */
        if (!Schema::hasTable('t_ad_post'))
        {
            Schema::create('t_ad_post', function (Blueprint $objBlueprint) {
                $objBlueprint->integer('ad_post_id', true, true);
                $objBlueprint->unsignedInteger('product_id');
                $objBlueprint->unsignedTinyInteger('auto_publish');
                $objBlueprint->unsignedTinyInteger('cancel_ad_post');
                
                $objBlueprint->unique('product_id');
                
                $objBlueprint->foreign('product_id','t_ad_post_ibfk1')->references('product_id')->on('t_product');
            });
        }

        /**
         * @table t_ad_post_note
         * @primary_key ad_post_note_id
         * @foreign_key ad_post_id, ad_post_status_id
         *
         * Table Description : Used to log the ad post
         */
        if (!Schema::hasTable('t_ad_post_note'))
        {
            Schema::create('t_ad_post_note', function (Blueprint $objBlueprint) {
                $objBlueprint->integer('ad_post_note_id', true, true);
                $objBlueprint->unsignedInteger('ad_post_id');
                $objBlueprint->unsignedTinyInteger('ad_post_status_id');
                $objBlueprint->unsignedInteger('user_id')->nullable();
                $objBlueprint->text('note');
                $objBlueprint->timestamps();

                $objBlueprint->foreign('ad_post_id','t_ad_post_note_ibfk1')->references('ad_post_id')->on('t_ad_post');
                $objBlueprint->foreign('ad_post_status_id','t_ad_post_note_ibfk2')->references('ad_post_status_id')->on('c_ad_post_status');
                $objBlueprint->foreign('user_id','t_ad_post_note_ibfk3')->references('user_id')->on('t_user');
            });
        }

        /**
         * @table t_ad_post_expiration
         * @primary_key ad_post_expiration_id
         * @foreign_key ad_post_id
         *
         * Table Description : Used to update duration or expiration of ad post
         */
        if (!Schema::hasTable('t_ad_post_expiration'))
        {
            Schema::create('t_ad_post_expiration', function (Blueprint $objBlueprint) {
                $objBlueprint->integer('ad_post_expiration_id', true, true);
                $objBlueprint->unsignedInteger('ad_post_id');
                $objBlueprint->timestamp('time_start');
                $objBlueprint->timestamp('time_end')->nullable();
                $objBlueprint->unsignedInteger('time_duration')->nullable();
                $objBlueprint->timestamps();

                $objBlueprint->foreign('ad_post_id','t_ad_post_expiration_ibfk1')->references('ad_post_id')->on('t_ad_post');
            });
        }
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::dropIfExists('t_ad_post_note');
        Schema::dropIfExists('t_ad_post_expiration');
        Schema::dropIfExists('t_order_type');
        Schema::dropIfExists('t_ad_post');
        Schema::dropIfExists('c_ad_post_status');
    }
}
