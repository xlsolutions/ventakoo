<?php

use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class CreateTableTUserProductsAndServicesLinker extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        /**
         * @table t_user_product_and_service_linker
         * @primary_key user_product_and_service_linker_id
         * @foreign_keys user_id,category_id,product_information_id,service_information_id
         *
         * Table Description : This is used to reference this tables (t_user,t_category,c_product_information,c_service_information) by their primary keys,
         * and to serve as linker between user products and services.
         */
        Schema::disableForeignKeyConstraints();

        Schema::create('t_user_product_and_service_linker', function (Blueprint $table) {
            $table->increments('user_product_and_service_linker_id');
            
            $table->integer('user_id', false, true);
            $table->foreign('user_id','t_user_products_and_services_linker_ibfk1')->references('user_id')->on('t_user');
            
            $table->integer('product_information_id', false, true)->nullable();
            $table->unique(['product_information_id', 'user_id'], 'user_product_key');
            $table->foreign('product_information_id','t_user_products_and_services_linker_ibfk3')->references('product_information_id')->on('c_product_information')->onDelete('cascade');
            
            $table->integer('service_information_id', false, true)->nullable();
            $table->unique(['service_information_id', 'user_id'], 'user_service_key');
            
            $table->foreign('service_information_id','t_user_products_and_services_linker_ibfk4')->references('service_information_id')->on('c_service_information')->onDelete('cascade');
            
            $table->timestamps();
            
            $table->softDeletes();
        });

        Schema::enableForeignKeyConstraints();
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::dropIfExists('t_user_products_and_services_linker');
        Schema::dropIfExists('t_user_product_and_service_linker');
    }
}
