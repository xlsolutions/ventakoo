<?php

use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class CreateUserPermissionTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::disableForeignKeyConstraints();

        /**
         * @table c_perm
         * @primary_key perm_id
         *
         * Table Description : This is used for system define permissions
         */
        if (!Schema::hasTable('c_perm'))
        {
            Schema::create('c_perm', function (Blueprint $objBlueprint) {
                $objBlueprint->smallIncrements('perm_id');
                $objBlueprint->string('handle', 50);
                $objBlueprint->unique('handle');
                $objBlueprint->string('description', 100);
                $objBlueprint->timestamps();
            });
        }

        /**
         * @table t_perm_user
         * @primary_key perm_user_id
         * @foreign_key user_id,perm_id
         *
         * Table Description : This is used to give different users system permissions
         */
        if (!Schema::hasTable('t_perm_user'))
        {
            Schema::create('t_perm_user', function (Blueprint $objBlueprint) {
                $objBlueprint->increments('perm_user_id');
                $objBlueprint->integer('user_id',false,true);
                $objBlueprint->foreign('user_id','t_perm_user_ibfk1')->references('user_id')->on('t_user');
                $objBlueprint->smallInteger('perm_id',false,true);
                $objBlueprint->foreign('perm_id','t_perm_user_ibfk2')->references('perm_id')->on('c_perm');
                $objBlueprint->timestamps();
            });
        }

        Schema::enableForeignKeyConstraints();
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::dropIfExists('t_perm_user');
        Schema::dropIfExists('c_perm');
    }
}
