<?php

use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class MakeHandleUnique extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        /**
         * @tables c_product_information,c_service_information
         *
         * Make column handle unique in c_product_information and c_service_information tables
         */
        if (Schema::hasTable('c_product_information'))
        {
            Schema::table('c_product_information', function (Blueprint $table)
            {
                $table->string('handle',60)->unique()->change();
            });
        }

        if (Schema::hasTable('c_service_information'))
        {
            Schema::table('c_service_information', function (Blueprint $table)
            {
                $table->string('handle',60)->unique()->change();
            });
        }
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {

    }
}
