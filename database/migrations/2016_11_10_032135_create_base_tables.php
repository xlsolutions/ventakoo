<?php
    /**
     * Migraton for initial database tables for Ecoreal
     * Author : Dana Ardee Ayes <shinayes@gmail.com>
     * Date : October 11, 2016
     */
    use Illuminate\Database\Schema\Blueprint;
    use Illuminate\Database\Migrations\Migration;

    class CreateBaseTables extends Migration
    {
        /**
         * Run the migrations.
         *
         * @return void
         */
        public function up()
        {
            /**
             * @table t_category
             * @primary_key category_id
             *
             * Table Description : The category_id column of this table is referenced in this tables (t_product,t_service)
             * This is used to categorize each record from both tables by its primary key with parent categories(parent_id -> 0 = 'Products', parent_id -> 1 = 'Services')
             */
            Schema::create('t_category', function (Blueprint $table) {
                $table->increments('category_id');
                $table->smallInteger('parent_id',false,true)->nullable();
                $table->string('handle',100);
                $table->string('category_name',100);
                $table->timestamps();
                $table->softDeletes();
            });

            /**
             * @table c_status
             * @primary_key status_id
             *
             * Table Description : The status_id column of this table is referenced in this tables (t_product,t_service)
             * This is used to set statuses for each records from both tables by its primary key
             */
            Schema::create('c_status', function (Blueprint $table) {
                $table->increments('status_id');
                $table->string('handle',20);
                $table->string('description',100);
                $table->timestamps();
            });

            /**
             * @table t_user
             * @primary_key user_id
             *
             * Table Description : The user_id column of this table is referenced in this table (t_user_information)
             * This is used to store user ID records.
             */
            Schema::create('t_user', function (Blueprint $table) {
                $table->increments('user_id');
            });

            /**
             * @table t_user_information
             * @primary_key user_information_id
             * @foreign_key user_id
             *
             * Table Description : The user_information_id column of this table is referenced in this table (t_user_contact)
             * This is used to store User Information records and to reference this table (t_user) by its primary key.
             */
            Schema::create('t_user_information', function (Blueprint $table) {
                $table->increments('user_information_id');
                $table->integer('user_id',false,true);
                $table->foreign('user_id','t_user_information_ibfk1')->references('user_id')->on('t_user');
                $table->string('firstname',64)->nullable();
                $table->string('lastname',64)->nullable();
                $table->string('password',255);
                $table->timestamps();
                $table->softDeletes();
            });

            /**
             * @table t_product
             * @primary_key product_id
             * @foreign_key category_id,status_id
             *
             * Table Description : The product_id column of this table is referenced in this table (t_product_attribute)
             * and to reference this table (t_user) by their primary keys,
             * This is used to store Product records.
             */
            Schema::create('t_product', function (Blueprint $table) {
                $table->increments('product_id');
                $table->string('handle',60);
                $table->integer('user_id',false,true);
                $table->foreign('user_id','t_product_ibfk1')->references('user_id')->on('t_user');
                $table->integer('category_id',false,true);
                $table->foreign('category_id','t_product_ibfk2')->references('category_id')->on('t_category');
                $table->integer('quantity',false,true);
                $table->decimal('price',20,6);
                $table->integer('status_id',false,true);
                $table->foreign('status_id','t_product_ibfk3')->references('status_id')->on('c_status');
                $table->timestamps();
                $table->softDeletes();
            });

            /**
             * @table c_product_information
             * @primary_key product_information_id
             *
             * Table Description : The product_information_id column of this table is referenced in this table (t_product_attribute)
             * This is used to store Product Information records by type and handle, and for the visibility of the information.
             */
            Schema::create('c_product_information', function (Blueprint $table) {
                $table->increments('product_information_id');
                $table->string('product_attribute_type',20);
                $table->string('handle',20);
                $table->tinyInteger('status',false,true)->default(1);
                $table->timestamps();
            });

            /**
             * @table t_product_attribute
             * @primary_key product_attribute_id
             * @foreign_key product_id,product_information_id
             *
             * Table Description : This is used to reference this tables (t_product,c_product_information) by their primary keys,
             * and to store product attributes value records.
             */
            Schema::create('t_product_attribute', function (Blueprint $table) {
                $table->increments('product_attribute_id');
                $table->integer('product_id',false,true);
                $table->foreign('product_id','t_product_attribute_ibfk1')->references('product_id')->on('t_product');
                $table->integer('product_information_id',false,true);
                $table->foreign('product_information_id','t_product_attribute_ibfk2')->references('product_information_id')->on('c_product_information');
                $table->string('value');
                $table->timestamps();
                $table->softDeletes();
            });

            /**
             * @table t_service
             * @primary_key service_id
             *
             * Table Description : The service_id column of this table is referenced in this table (t_service_attributes)
             * and to reference this table (t_user) by their primary keys,
             * This is used to store Service records.
             */
            Schema::create('t_service', function (Blueprint $table) {
                $table->increments('service_id');
                $table->string('handle',60);
                $table->integer('user_id',false,true);
                $table->foreign('user_id','t_service_ibfk1')->references('user_id')->on('t_user');
                $table->integer('category_id',false,true);
                $table->foreign('category_id','t_service_ibfk2')->references('category_id')->on('t_category');
                $table->timestamp('duration');
                $table->decimal('price',8,6);
                $table->integer('status_id',false,true);
                $table->foreign('status_id','t_service_ibfk3')->references('status_id')->on('c_status');
                $table->timestamps();
                $table->softDeletes();
            });

            /**
             * @table c_service_information
             * @primary_key service_information_id
             *
             * Table Description : The service_information_id column of this table is referenced in this table (t_service_attribute)
             * This is used to store Service Information records by type and handle, and for the visibility of the information.
             */
            Schema::create('c_service_information', function (Blueprint $table) {
                $table->increments('service_information_id');
                $table->string('service_attribute_type',20);
                $table->string('handle',20);
                $table->tinyInteger('status',false,true)->default(1);
                $table->timestamps();
            });

            /**
             * @table t_service_attribute
             * @primary_key service_attribute_id
             * @foreign_key service_id,service_information_id
             *
             * Table Description : This is used to reference this tables (t_service,c_service_information) by their primary keys,
             * and to store service attributes value records.
             */
            Schema::create('t_service_attribute', function (Blueprint $table) {
                $table->increments('service_attribute_id');
                $table->integer('service_id',false,true);
                $table->foreign('service_id','t_service_attribute_ibfk1')->references('service_id')->on('t_service');
                $table->integer('service_information_id',false,true);
                $table->foreign('service_information_id','t_service_attribute_ibfk2')->references('service_information_id')->on('c_service_information');
                $table->string('value');
                $table->timestamps();
                $table->softDeletes();
            });

            /**
             * @table c_country
             * @primary_key country_id
             *
             * Table Description : The country_id column of this table is referenced in this tables (c_state,t_user_location)
             * This is used to store Country records.
             */
            Schema::create('c_country', function (Blueprint $table) {
                $table->increments('country_id');
                $table->string('country_code',3);
                $table->string('country_name',100);
                $table->timestamps();
            });

            /**
             * @table c_state
             * @primary_key state_id
             * @foreign_key state_name
             *
             * Table Description : The state_id column of this table is referenced in this tables (c_city,t_user_location)
             * This is used to store State records and to reference this table (c_country) by its primary key.
             */
            Schema::create('c_state', function (Blueprint $table) {
                $table->increments('state_id');
                $table->integer('country_id',false,true);
                $table->foreign('country_id','c_state_ibfk1')->references('country_id')->on('c_country');
                $table->string('state_name',100);
                $table->timestamps();
            });

            /**
             * @table c_city
             * @primary_key city_id
             * @foreign_key state_id
             *
             * Table Description : The state_id column of this table is referenced in this table (t_user_location)
             * This is used to store City records and to reference this table (c_state) by its primary key.
             */
            Schema::create('c_city', function (Blueprint $table) {
                $table->increments('city_id');
                $table->integer('state_id',false,true);
                $table->foreign('state_id','c_city_ibfk1')->references('state_id')->on('c_state');
                $table->string('city_name',100);
                $table->timestamps();
            });

            /**
             * @table t_user_location
             * @primary_key user_location_id
             * @foreign_key country_id,state_id,city_id
             *
             * Table Description : The user_location_id column of this table is referenced in this table (t_user_contact)
             * This is used for the complete address of the user and to reference this tables (c_country,c_state,c_city) by their primary keys.
             */
            Schema::create('t_user_location', function (Blueprint $table) {
                $table->increments('user_location_id');
                $table->integer('country_id',false,true)->nullable();
                $table->foreign('country_id','t_user_location_ibfk1')->references('country_id')->on('c_country');
                $table->integer('state_id',false,true)->nullable();
                $table->foreign('state_id','t_user_location_ibfk2')->references('state_id')->on('c_state');
                $table->integer('city_id',false,true)->nullable();
                $table->foreign('city_id','t_user_location_ibfk3')->references('city_id')->on('c_city');
                $table->timestamps();
                $table->softDeletes();
            });

            /**
             * @table t_user_contact
             * @primary_key user_contact_id
             *
             * Table Description : The category_id column of this table is referenced in this table (t_user_contact_attribute)
             * This is used to store User Contact records and to reference this table (t_user_information) by its primary key.
             */
            Schema::create('t_user_contact', function (Blueprint $table) {
                $table->increments('user_contact_id');
                $table->integer('user_information_id',false,true);
                $table->foreign('user_information_id','t_user_contact_ibfk1')->references('user_information_id')->on('t_user_information');
                $table->string('email_address',100);
                $table->unique('email_address');
                $table->integer('user_location_id',false,true);
                $table->foreign('user_location_id','t_user_contact_ibfk2')->references('user_location_id')->on('t_user_location');
                $table->timestamps();
                $table->softDeletes();
            });

            /**
             * @table c_user_contact_information
             * @primary_key user_contact_information_id
             *
             * Table Description : The user_contact_information_id column of this table is referenced in this tables (t_user_contact_attribute)
             * This is used to store User Contact Information records by type and handle, and for the visibility of the information.
             */
            Schema::create('c_user_contact_information', function (Blueprint $table) {
                $table->increments('user_contact_information_id');
                $table->string('user_contact_attribute_type',20);
                $table->string('handle',20);
                $table->tinyInteger('status',false,true);
                $table->timestamps();
            });

            /**
             * @table t_user_contact_attribute
             * @primary_key user_contact_attribute_id
             * @foreign_key user_contact_id,user_contact_information_id
             *
             * Table Description : This is used to reference this tables (t_user_contact,c_user_contact_information) by their primary keys,
             * and to store user contact attributes value records.
             */
            Schema::create('t_user_contact_attribute', function (Blueprint $table) {
                $table->increments('user_contact_attribute_id');
                $table->integer('user_contact_id',false,true);
                $table->foreign('user_contact_id','t_user_contact_attribute_ibfk1')->references('user_contact_id')->on('t_user_contact');
                $table->integer('user_contact_information_id',false,true);
                $table->foreign('user_contact_information_id','t_users_contact_attributes_ibfk2')->references('user_contact_information_id')->on('c_user_contact_information');
                $table->string('value')->nullable();
                $table->timestamps();
                $table->softDeletes();
            });

        }

        /**
         * Reverse the migrations.
         *
         * @return void
         */
        public function down()
        {
            /**
             * This is for dev server environment dropping of tables
             * no need to be in sequence for faster development.
             */
            Schema::disableForeignKeyConstraints();

            Schema::dropIfExists('t_category');
            Schema::dropIfExists('c_status');
            Schema::dropIfExists('t_product');
            Schema::dropIfExists('c_product_information');
            Schema::dropIfExists('t_product_attribute');
            Schema::dropIfExists('t_service');
            Schema::dropIfExists('c_service_information');
            Schema::dropIfExists('t_service_attribute');
            Schema::dropIfExists('t_user');
            Schema::dropIfExists('t_user_information');
            Schema::dropIfExists('t_user_contact');
            Schema::dropIfExists('c_user_contact_information');
            Schema::dropIfExists('t_user_contact_attribute');
            Schema::dropIfExists('c_country');
            Schema::dropIfExists('c_state');
            Schema::dropIfExists('c_city');
            Schema::dropIfExists('t_user_location');

            Schema::enableForeignKeyConstraints();

            /**
             * This is for production server environment sequential drop of tables
             * no need to disable the foreign key constraints before dropping tables.
             *
             * Schema::dropIfExists('t_product_attribute');
             * Schema::dropIfExists('t_product');
             * Schema::dropIfExists('c_product_information');
             * Schema::dropIfExists('t_service_attribute');
             * Schema::dropIfExists('t_service');
             * Schema::dropIfExists('c_service_information');
             * Schema::dropIfExists('t_category');
             * Schema::dropIfExists('c_status');
             * Schema::dropIfExists('t_user_contact_attribute');
             * Schema::dropIfExists('c_user_contact_information');
             * Schema::dropIfExists('t_user_contact');
             * Schema::dropIfExists('t_user_information');
             * Schema::dropIfExists('t_user');
             * Schema::dropIfExists('t_user_location');
             * Schema::dropIfExists('c_city');
             * Schema::dropIfExists('c_state');
             * Schema::dropIfExists('c_country');
             *
             */

        }

    }
