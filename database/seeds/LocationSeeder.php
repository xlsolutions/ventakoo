<?php

use Illuminate\Database\Seeder;

class LocationSeeder extends Seeder
{
    /**
     * Run the database seeds.
     *
     * @return void
     */
    public function run()
    {
        $this->call('CountryTableSeeder');
        $this->call('StateTableSeeder');
        $this->call('CityTableSeeder');
    }
}

class CountryTableSeeder extends Seeder
{
    public function run()
    {
        try
        {
            Eloquent::unguard();

            DB::transaction(function ()
            {

                Excel::filter('chunk')->load(storage_path('csv/location/country_data.csv'))->chunk(250, function ($results)
                {

                    $countryModel = app('App\Models\Constants\CCountry');

                    foreach ($results as $key => $row)
                    {
                        $resultCountry = $countryModel->where('country_id', $row['country_id'])->first();

                        if (!$resultCountry)
                        {
                            $data = [
                                'country_code' => $row->country_code,
                                'country_name' => $row->country_name
                            ];

                            $countryModel->create($data);
                        }
                    }
                });

                $this->command->info('Country table initial records successfully seeded!');
            });

            DB::commit();
        }
        catch(Exception $objException)
        {
            DB::rollBack();

            syslog(LOG_CRIT,__CLASS__.":".__METHOD__."-{$objException->getMessage()}");

            $this->command->info("Country table seeding failed with error - {$objException->getMessage()}");
        }
    }
}

class StateTableSeeder extends Seeder
{
    public function run()
    {
        try
        {
            Eloquent::unguard();

            DB::transaction(function ()
            {
                Excel::filter('chunk')->load(storage_path('csv/location/state_data.csv'))->chunk(250, function ($results)
                {
                    $stateModel = app('App\Models\Constants\CState');

                    foreach ($results as $key => $row)
                    {
                        $resultState = $stateModel->where('state_id', $row['state_id'])->first();

                        if (!$resultState)
                        {
                            $data = [
                                'country_id' => $row->country_id,
                                'state_name' => $row->state_name
                            ];

                            $stateModel->create($data);
                        }
                    }

                });

                $this->command->info('State table initial records successfully seeded!');
            });

            DB::commit();
        }
        catch(Exception $objException)
        {
            DB::rollBack();

            syslog(LOG_CRIT,__CLASS__.":".__METHOD__."-{$objException->getMessage()}");

            $this->command->info("State table seeding failed with error - {$objException->getMessage()}");
        }
    }
}

class CityTableSeeder extends Seeder
{
    public function run()
    {
        try
        {
            Eloquent::unguard();

            DB::transaction(function ()
            {
                Excel::filter('chunk')->load(storage_path('csv/location/city_data.csv'))->chunk(500, function ($results)
                {

                    $cityModel = app('App\Models\Constants\CCity');

                    foreach ($results as $key => $row)
                    {
                        $resultCity = $cityModel->where('city_id', $row['city_id'])->first();

                        if (!$resultCity)
                        {
                            $data = [
                                'state_id'  => $row->state_id,
                                'city_name' => $row->city_name
                            ];

                            $cityModel->create($data);
                        }
                    }
                });

                $this->command->info('City table initial records successfully seeded!');
            });

            DB::commit();
        }
        catch(Exception $objException)
        {
            DB::rollBack();

            syslog(LOG_CRIT,__CLASS__.":".__METHOD__."-{$objException->getMessage()}");

            $this->command->info("City table seeding failed with error - {$objException->getMessage()}");
        }
    }
}
