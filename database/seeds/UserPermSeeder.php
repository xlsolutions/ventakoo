<?php

use Illuminate\Database\Seeder;

class UserPermSeeder extends Seeder
{
    public function run()
    {
        try
        {
            Eloquent::unguard();

            DB::transaction(function ()
            {
                Schema::disableForeignKeyConstraints();

                $strDateTime =  date("Y-m-d H:i:s",time());

                $arrAdminData = [
                    [
                        'handle' => 'admin',
                        'description' => 'Admin permission on all system functions',
                        'created_at' => $strDateTime,
                        'updated_at' => $strDateTime
                    ],
                    [
                        'handle' => 'admin.user.account',
                        'description' => 'Admin permission for the account',
                        'created_at' => $strDateTime,
                        'updated_at' => $strDateTime
                    ],
                    [
                        'handle' => 'admin.user.account.block',
                        'description' => 'Admin permission for user blocking',
                        'created_at' => $strDateTime,
                        'updated_at' => $strDateTime
                    ],
                    [
                        'handle' => 'admin.user.account.reset_password',
                        'description' => 'Admin permission for  resetting password of users',
                        'created_at' => $strDateTime,
                        'updated_at' => $strDateTime
                    ],
                    [
                        'handle' => 'admin.order',
                        'description' => 'Admin permission on all order system functions',
                        'created_at' => $strDateTime,
                        'updated_at' => $strDateTime
                    ],
                    [
                        'handle' => 'admin.order.charge',
                        'description' => 'Admin permission for charging orders',
                        'created_at' => $strDateTime,
                        'updated_at' => $strDateTime
                    ],
                    [
                        'handle' => 'admin.order.cancel',
                        'description' => 'Admin permission for cancelling orders',
                        'created_at' => $strDateTime,
                        'updated_at' => $strDateTime
                    ],
                    [
                        'handle' => 'admin.order.deny',
                        'description' => 'Admin permission for denying orders',
                        'created_at' => $strDateTime,
                        'updated_at' => $strDateTime
                    ]
                ];

                $arrUserData = [
                    [
                        'handle' => 'user.account',
                        'description' => 'User permission for all user system functions',
                        'created_at' => $strDateTime,
                        'updated_at' => $strDateTime
                    ],
                    [
                        'handle' => 'user.account.view',
                        'description' => 'User permission for viewing data',
                        'created_at' => $strDateTime,
                        'updated_at' => $strDateTime
                    ],
                    [
                        'handle' => 'user.account.add',
                        'description' => 'User permission for inserting data',
                        'created_at' => $strDateTime,
                        'updated_at' => $strDateTime
                    ],
                    [
                        'handle' => 'user.account.edit',
                        'description' => 'User permission for updating data',
                        'created_at' => $strDateTime,
                        'updated_at' => $strDateTime
                    ],
                    [
                        'handle' => 'user.account.delete',
                        'description' => 'User permission for removing data',
                        'created_at' => $strDateTime,
                        'updated_at' => $strDateTime
                    ],
                    [
                        'handle' => 'user.product',
                        'description' => 'User permission on all product system functions',
                        'created_at' => $strDateTime,
                        'updated_at' => $strDateTime
                    ],
                    [
                        'handle' => 'user.product.view',
                        'description' => 'User permission for viewing product data',
                        'created_at' => $strDateTime,
                        'updated_at' => $strDateTime
                    ],
                    [
                        'handle' => 'user.product.add',
                        'description' => 'User permission for inserting product data',
                        'created_at' => $strDateTime,
                        'updated_at' => $strDateTime
                    ],
                    [
                        'handle' => 'user.product.edit',
                        'description' => 'User permission for updating product data',
                        'created_at' => $strDateTime,
                        'updated_at' => $strDateTime
                    ],
                    [
                        'handle' => 'user.product.delete',
                        'description' => 'User permission for removing product data',
                        'created_at' => $strDateTime,
                        'updated_at' => $strDateTime
                    ],
                    [
                        'handle' => 'user.product.report',
                        'description' => 'User permission for product report',
                        'created_at' => $strDateTime,
                        'updated_at' => $strDateTime
                    ],
                    [
                        'handle' => 'user.message',
                        'description' => 'User permission on all message system functions',
                        'created_at' => $strDateTime,
                        'updated_at' => $strDateTime
                    ],
                    [
                        'handle' => 'user.message.view',
                        'description' => 'User permission for viewing message data',
                        'created_at' => $strDateTime,
                        'updated_at' => $strDateTime
                    ],
                    [
                        'handle' => 'user.message.add',
                        'description' => 'User permission for inserting message data',
                        'created_at' => $strDateTime,
                        'updated_at' => $strDateTime
                    ],
                    [
                        'handle' => 'user.message.edit',
                        'description' => 'User permission for updating message data',
                        'created_at' => $strDateTime,
                        'updated_at' => $strDateTime
                    ],
                    [
                        'handle' => 'user.message.delete',
                        'description' => 'User permission for removing message data',
                        'created_at' => $strDateTime,
                        'updated_at' => $strDateTime

                    ],
                    [
                        'handle' => 'user.order',
                        'description' => 'User permission for order functions',
                        'created_at' => $strDateTime,
                        'updated_at' => $strDateTime
                    ],
                    [
                        'handle' => 'user.service',
                        'description' => 'User permission on all service system functions',
                        'created_at' => $strDateTime,
                        'updated_at' => $strDateTime
                    ],
                    [
                        'handle' => 'user.service.view',
                        'description' => 'User permission for viewing service data',
                        'created_at' => $strDateTime,
                        'updated_at' => $strDateTime
                    ],
                    [
                        'handle' => 'user.service.add',
                        'description' => 'User permission for inserting service data',
                        'created_at' => $strDateTime,
                        'updated_at' => $strDateTime
                    ],
                    [
                        'handle' => 'user.service.edit',
                        'description' => 'User permission for updating service data',
                        'created_at' => $strDateTime,
                        'updated_at' => $strDateTime
                    ],
                    [
                        'handle' => 'user.service.delete',
                        'description' => 'User permission for removing service data',
                        'created_at' => $strDateTime,
                        'updated_at' => $strDateTime
                    ],
                    [
                        'handle' => 'user.service.report',
                        'description' => 'User permission for service report',
                        'created_at' => $strDateTime,
                        'updated_at' => $strDateTime
                    ]
                ];

                $objModel = app('App\Models\Constants\CPerm');

                foreach ($arrAdminData as $key => $row)
                {
                    $objModel->insert($row);

                    $arrAdminHandle[] = $row['handle'];
                }

                $arrHandle = array();

                foreach ($arrUserData as $key => $row)
                {
                    $objModel->insert($row);

                    $arrHandle[] = $row['handle'];
                }

                $objPermModel = app('App\Models\Constants\CPerm');

                $objPerm = $objPermModel->whereIn('handle',$arrHandle)->get();

                $objUserModel = app('App\Models\Users\TUser');

                $objUser = $objUserModel->get();

                $objPermUserModel = app('App\Models\Users\TPermUser');

                foreach ($objUser as $objUserRow)
                {
                    foreach ($objPerm as $objPermRow)
                    {
                        $objPermUserModel->create(
                            [
                                'user_id' => $objUserRow->user_id,
                                'perm_id' => $objPermRow->perm_id
                            ]
                        );
                    }
                }

                if (env('APP_ENV', 'local') != 'local')
                {
                    $objAdminPerm = $objPermModel->whereIn('handle', $arrAdminHandle)->get();

                    foreach ($objAdminPerm as $objAdminPermRow)
                    {
                        $objPermUserModel->create(
                            [
                                'user_id' => 3,
                                'perm_id' => $objAdminPermRow->perm_id
                            ]
                        );
                    }
                }

                Schema::enableForeignKeyConstraints();

                $this->command->info(__CLASS__ . ' class successfully seeded!');
            });

            DB::commit();
        }
        catch(Exception $objException)
        {
            DB::rollBack();

            $strErrorMsg = __CLASS__ . ":" . __METHOD__ . " - {$objException->getMessage()}";

            syslog(LOG_CRIT, $strErrorMsg);

            $this->command->info("Failed seeding: {$strErrorMsg}");
        }
    }
}
