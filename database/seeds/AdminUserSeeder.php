<?php

use Illuminate\Database\Seeder;

class AdminUserSeeder extends Seeder
{
    /**
     * Run the database seeds.
     *
     * @return void
     */
    public function run()
    {
        $this->call('AdminUserTableSeeder');
        $this->call('AdminUserInformationTableSeeder');
        $this->call('AdminUserLocationTableSeeder');
        $this->call('AdminUserContactTableSeeder');
        $this->call('AdminUserContactInformationTableSeeder');
        $this->call('AdminUserContactAttributeTableSeeder');
    }
}

class AdminUserTableSeeder extends Seeder
{
    public function run()
    {
        try
        {
            Eloquent::unguard();

            DB::transaction(function ()
            {
                Schema::disableForeignKeyConstraints();

                $data = [
                    [
                        'user_id' => 3
                    ]
                ];

                $userModel = app('App\Models\Users\TUser');

                foreach ($data as $key => $row)
                {
                    $filterUser = array_except($row, ['user_id']);

                    $objUser = $userModel->updateOrCreate(['user_id' => $row['user_id']], $filterUser);

                    if (isset($row['handle']))
                    {
                        $resultUser = $userModel->where('user_id', $row['user_id'])->first();

                        if ($resultUser)
                        {
                            $objUser->user_id = $resultUser->user_id;
                            $objUser->save();
                        }
                    }

                }


                Schema::enableForeignKeyConstraints();

                $this->command->info('User table initial records successfully seeded!');

            });

            DB::commit();
        }
        catch(Exception $objException)
        {
            DB::rollBack();

            syslog(LOG_CRIT,__CLASS__.":".__METHOD__."-{$objException->getMessage()}");

            $this->command->info("User table seeding failed with error - {$objException->getMessage()}");
        }
    }
}

class AdminUserInformationTableSeeder extends Seeder
{
    public function run()
    {
        try
        {
            Eloquent::unguard();

            DB::transaction(function ()
            {
                Schema::disableForeignKeyConstraints();

                $strHash = env('AUTH_HASH_PASSWORD');

                $strPassword = Hash::make($strHash.'ventakoo20170530');

                $data = [
                    [
                        'user_id'   => 3,
                        'firstname' => 'Ventakoo',
                        'lastname'  => 'Ventakoo',
                        'password'  => $strPassword
                    ]
                ];

                $userInfoModel = app('App\Models\Users\TUserInformation');

                foreach ($data as $key => $row)
                {
                    $filterUserInformation = array_except($row, ['user_id']);

                    $objUserInformation = $userInfoModel->updateOrCreate(['user_id' => $row['user_id']], $filterUserInformation);

                    if (isset($row['handle']))
                    {
                        $resultUserInformation = $userInfoModel->where('user_id', $row['user_id'])->first();

                        if ($resultUserInformation)
                        {
                            $objUserInformation->user_id = $resultUserInformation->user_id;
                            $objUserInformation->save();
                        }
                    }

                }

                Schema::enableForeignKeyConstraints();

                $this->command->info('User Information table initial records successfully seeded!');

            });

            DB::commit();
        }
        catch(Exception $objException)
        {
            DB::rollBack();

            syslog(LOG_CRIT,__CLASS__.":".__METHOD__."-{$objException->getMessage()}");

            $this->command->info("User Information table seeding failed with error - {$objException->getMessage()}");
        }
    }
}

class AdminUserLocationTableSeeder extends Seeder
{
    public function run()
    {
        try
        {
            Eloquent::unguard();

            DB::transaction(function ()
            {
                Schema::disableForeignKeyConstraints();

                $data = [
                    [
                        'user_location_id' => 3,
                        'country_id'       => 173,
                        'state_id'         => 4167,
                        'city_id'          => 49280
                    ]
                ];

                $userLocationModel = app('App\Models\Users\TUserLocation');

                foreach ($data as $key => $row)
                {
                    $filterUserLocation = array_except($row, ['user_location_id']);

                    $objUserLocation = $userLocationModel->updateOrCreate(['user_location_id' => $row['user_location_id']], $filterUserLocation);

                    if (isset($row['user_location_id']))
                    {
                        $resultUserLocation = $userLocationModel->where('user_location_id', $row['user_location_id'])->first();

                        if ($resultUserLocation)
                        {
                            $objUserLocation->user_location_id = $resultUserLocation->user_location_id;
                            $objUserLocation->save();
                        }
                    }

                }

                Schema::enableForeignKeyConstraints();

                $this->command->info('User Location table initial records successfully seeded!');
            });

            DB::commit();
        }
        catch(Exception $objException)
        {
            DB::rollBack();

            syslog(LOG_CRIT,__CLASS__.":".__METHOD__."-{$objException->getMessage()}");

            $this->command->info("User Location table seeding failed with error - {$objException->getMessage()}");
        }
    }
}

class AdminUserContactInformationTableSeeder extends Seeder
{
    public function run()
    {
        try
        {
            Eloquent::unguard();

            DB::transaction(function ()
            {
                Schema::disableForeignKeyConstraints();

                $userContactInformationModel = app('App\Models\Constants\CUserContactInformation');

                $data = [
                    [
                        'handle'                      => 'MOBILE_NUMBER',
                        'user_contact_attribute_type' => 'mobile_number',
                        'status'                      => 1
                    ],
                    [
                        'handle'                      => 'PHONE_NUMBER',
                        'user_contact_attribute_type' => 'phone_number',
                        'status'                      => 1
                    ],
                    [
                        'handle'                      => 'ADDRESS',
                        'user_contact_attribute_type' => 'address',
                        'status'                      => 1
                    ],
                    [
                        'handle'                      => 'LAST_LOGIN_TIME',
                        'user_contact_attribute_type' => 'last_login_time',
                        'status'                      => 1
                    ],
                    [
                        'handle'                      => 'SECURITY_QUESTION_1',
                        'user_contact_attribute_type' => 'security_question_1',
                        'status'                      => 1
                    ],
                    [
                        'handle'                      => 'SECURITY_QUESTION_2',
                        'user_contact_attribute_type' => 'security_question_2',
                        'status'                      => 1
                    ],
                    [
                        'handle'                      => 'SECURITY_ANSWER_1',
                        'user_contact_attribute_type' => 'security_answer_1',
                        'status'                      => 1
                    ],
                    [
                        'handle'                      => 'SECURITY_ANSWER_2',
                        'user_contact_attribute_type' => 'security_answer_2',
                        'status'                      => 1
                    ],
                ];

                foreach ($data as $key => $productInfo)
                {

                    $filterUserContactInfo = array_except($productInfo, ['handle']);

                    $userContactInfoObj = $userContactInformationModel->updateOrCreate(['handle' => $productInfo['handle']], $filterUserContactInfo);

                    if (isset($userContactInfoObj['handle']))
                    {
                        $objUserContactInfo = $userContactInformationModel->where('handle', $userContactInfoObj['handle'])->first();

                        if ($objUserContactInfo)
                        {
                            $userContactInfoObj->user_contact_information_id = $objUserContactInfo->user_contact_information_id;
                            $userContactInfoObj->save();
                        }
                    }
                }

                Schema::enableForeignKeyConstraints();

                $this->command->info('User Contact Information table initial records successfully seeded!');
            });

            DB::commit();
        }
        catch(Exception $objException)
        {
            DB::rollBack();

            syslog(LOG_CRIT,__CLASS__.":".__METHOD__."-{$objException->getMessage()}");

            $this->command->info("User Contact Information table seeding failed with error - {$objException->getMessage()}");
        }
    }
}

class AdminUserContactTableSeeder extends Seeder
{
    public function run()
    {
        try
        {
            Eloquent::unguard();

            DB::transaction(function ()
            {
                Schema::disableForeignKeyConstraints();

                $data = [
                    [
                        'user_information_id' => 3,
                        'user_location_id'    => 3,
                        'email_address'       => 'admin@ventakoo.com'
                    ]
                ];

                $userContactModel = app('App\Models\Users\TUserContact');

                foreach ($data as $key => $row)
                {
                    $filterUserContact = array_except($row, ['email_address']);

                    $objUseContact = $userContactModel->updateOrCreate(['email_address' => $row['email_address']], $filterUserContact);

                    if (isset($row['email_address']))
                    {
                        $resultUserLocation = $userContactModel->where('email_address', $row['email_address'])->first();

                        if ($resultUserLocation)
                        {
                            $objUseContact->user_location_id = $resultUserLocation->user_location_id;
                            $objUseContact->save();
                        }
                    }
                }

                Schema::enableForeignKeyConstraints();

                $this->command->info('User Contact table initial records successfully seeded!');
            });

            DB::commit();
        }
        catch(Exception $objException)
        {
            DB::rollBack();

            syslog(LOG_CRIT,__CLASS__.":".__METHOD__."-{$objException->getMessage()}");

            $this->command->info("User Contact table seeding failed with error - {$objException->getMessage()}");
        }
    }
}

class AdminUserContactAttributeTableSeeder extends Seeder
{
    public function run()
    {
        try
        {
            Eloquent::unguard();

            DB::transaction(function ()
            {
                $arrSecurityQuestions1 = explode(",","What was the house number and street name you lived in as a child?,".
                    "What were the last four digits of your childhood telephone number?,".
                    "What primary school did you attend?,".
                    "In what town or city was your first full time job?,".
                    "In what town or city did you meet your spouse/partner?,".
                    "What is the middle name of your oldest child?");
                $arrSecurityQuestions2 = explode(",","What are the last five digits of your driver's licence number?,".
                    "What is your grandmother's (on your mother's side) maiden name?,".
                    "What is your spouse or partner's mother's maiden name?,".
                    "In what town or city did your mother and father meet?,".
                    "What time of the day were you born? (hh:mm),".
                    "What time of the day was your first child born? (hh:mm)");

                $userContactModel = app('App\Models\Users\TUserContact');

                $userContactInformationModel = app('App\Models\Constants\CUserContactInformation');

                $userContactAttributeModel = app('App\Models\Users\TUserContactAttribute');

                //delete users table records
                DB::table('t_user_contact_attribute')->delete();

                $objUserContact = $userContactModel->get();

                $objUserContactInfo = $userContactInformationModel->get();

                foreach ($objUserContact as $row)
                {
                    foreach ($objUserContactInfo as $key => $cRow)
                    {
                        $fakerFactory = app('Faker\Factory');

                        $data = [
                            'user_contact_id'             => $row->user_contact_id,
                            'user_contact_information_id' => $cRow->user_contact_information_id
                        ];

                        $fake = $fakerFactory->create('en_PH');

                        $fake2 = $fakerFactory->create('Text');

                        $strQuestion1 = $arrSecurityQuestions1[array_rand($arrSecurityQuestions1)];
                        $strQuestion2 = $arrSecurityQuestions2[array_rand($arrSecurityQuestions2)];

                        switch ($cRow->handle)
                        {
                            case 'MOBILE_NUMBER':
                                $data['value'] = $fake->mobileNumber;
                                break;
                            case 'PHONE_NUMBER':
                                $data['value'] = $fake->phoneNumber;
                                break;
                            case 'ADDRESS':
                                $data['value'] = $fake->address;
                                break;
                            case 'LAST_LOGIN_TIME':
                                $data['value'] = date("Y-m-d H:i:s",time());
                                break;
                            case 'SECURITY_QUESTION_1':
                                $data['value'] = $strQuestion1;
                                break;
                            case 'SECURITY_QUESTION_2':
                                $data['value'] = $strQuestion2;
                                break;
                            case 'SECURITY_ANSWER_1':
                                $data['value'] = $fake2->realText(10,3);
                                break;
                            case 'SECURITY_ANSWER_2':
                                $data['value'] = $fake2->realText(10,4);
                                break;
                        }

                        $userContactAttributeModel->create($data);
                    }
                }

                $this->command->info('User Contact Attribute table initial records successfully seeded!');
            });

            DB::commit();
        }
        catch(Exception $objException)
        {
            DB::rollBack();

            syslog(LOG_CRIT,__CLASS__.":".__METHOD__."-{$objException->getMessage()}");

            $this->command->info("User Contact Attribute table seeding failed with error - {$objException->getMessage()}");
        }
    }
}