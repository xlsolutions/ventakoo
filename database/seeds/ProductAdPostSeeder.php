<?php

use Illuminate\Database\Seeder;

class ProductAdPostSeeder extends Seeder
{
    public function run()
    {
        try
        {
            Eloquent::unguard();

            DB::transaction(function ()
            {
                $arrData = [
                    [
                        'name' => 'Completed',
                        'handle' => 'COMPLETED',
                        'description' => 'Ad post ended'
                    ],
                    [
                        'name' => 'Paused',
                        'handle' => 'PAUSED',
                        'description' => 'Ad post has been paused by seller'
                    ],
                    [
                        'name' => 'For Review',
                        'handle' => 'FOR_REVIEW',
                        'description' => 'Ad post submitted for review by seller'
                    ],
                    [
                        'name' => 'Reviewing',
                        'handle' => 'REVIEWING',
                        'description' => 'Ad post currently being reviewed by admin'
                    ],
                    [
                        'name' => 'Approved',
                        'handle' => 'APPROVED',
                        'description' => 'Ad post has been approved by admin'
                    ],
                    [
                        'name' => 'Start',
                        'handle' => 'START',
                        'description' => 'Ad post has been published by system or seller'
                    ],
                    [
                        'name' => 'Declined',
                        'handle' => 'DECLINED',
                        'description' => 'Ad post review result has been declined by admin'
                    ],
                    [
                        'name' => 'Cancelled',
                        'handle' => 'CANCELLED',
                        'description' => 'Ad post has been cancelled by seller'
                    ]
                ];

                $objModel = app('App\Models\Constants\CAdPostStatus');

                foreach ($arrData as $key => $row)
                {
                    $objModel->insert($row);
                }
                
                $this->command->info(__CLASS__ . ' class successfully seeded!');
            });

            DB::commit();
        }
        catch(Exception $objException)
        {
            DB::rollBack();

            $strErrorMsg = __CLASS__ . ":" . __METHOD__ . " - {$objException->getMessage()}";

            syslog(LOG_CRIT, $strErrorMsg);

            $this->command->info("Failed seeding: {$strErrorMsg}");
        }
    }
}