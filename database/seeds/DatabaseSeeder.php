<?php
/**
 * Seeder for initial database records for Ventakoo
 * Author : Dana Ardee Ayes <shinayes@gmail.com>
 * Date : December 6, 2016
 */
use Illuminate\Database\Seeder;

class DatabaseSeeder extends Seeder
{
    /**
     * Run the database seeds.
     *
     * @return void
     */
    public function run()
    {
        $this->call('LocationSeeder');
        
        if (env('APP_ENV', 'local') === 'local')
        {
            $this->call('UserSeeder');
        }

        $this->call('AdminUserSeeder');
        
        $this->call('CategoryTableSeeder');
        $this->call('ProductServiceSeeder');
        $this->call('UpdatePhilippinesStatesAndCitiesSeeder');
        $this->call('UserActivationTypeSeeder');
        $this->call('UserPermSeeder');
        
        if (env('APP_ENV', 'local') === 'local')
        {
            $this->call('UserProductAndServiceLinkerSeeder');
        }
        
        $this->call('ProductAdPostSeeder');
        $this->call('UserPlanAndOrderSeeder');
    }
}

class CategoryTableSeeder extends Seeder
{
    public function run()
    {
        try
        {
            Eloquent::unguard();

            DB::transaction(function ()
            {
                Schema::disableForeignKeyConstraints();

                //delete users table records
                DB::table('t_category')->delete();

                $arrMainCategory = explode(',', "Products,Services");


                $arrProductCategories = explode(',', "Real Estate,Vehicle,Electronics,Fashion,Sports,Furniture,Pets,Others");

                $arrServiceCategories = explode(',', "Graphics and Design,Digital Marketing,Writing Translation,Video and Animation,Music and Audio,Programming and Tech,Advertsing,Business,Fun and Life Style");


                $arrProductRealStateSubCategory = explode(',', "Real Estate For Sale,Real Estate For Rent,House and Lot or Townhouses and Subdivisions,Apartment and Condominium,Beach and Resort,Commercial and Industrial Properties");

                $arrProductVehicleSubCategory = explode(',', "Motorcycle,Cars,Accessories,Tools and Equipment");

                $arrProductElectronicsSubCategory = explode(',', "Mobile Devices,Computer,Camera,Mobile Accessories,Home Appliances");

                $arrProductFashionSubCategory = explode(',', "Mens Fashion,Womens Fashion,Kids Fasion");

                //$arrProductSubFashionSubCategory = explode(',', "Clothes,Shoes,Watch,Accesories");//to be added as subcategory of each fashion sub category

                $arrProductSportsSubCategory = explode(',', "Indoor,Outdoor,Team Sports");

                $arrProductFurnitureSubCategory = explode(',', "Bedroom,Living Room,Office,Dining,Hallway,Bathroom");

                $arrProductPetsSubCategory = explode(',', "Accessories,Food");


                $arrServiceGraphicsSubCategory = explode(',', "Logo Design,Business Cards and Stationery,Illustration,Cartoons and Caricatures,Flyers and Posters,Book Covers And Packaging,Web and Mobile Design,Banner Ads,Photoshop Editing,3D and 2D Models,T-Shirts,Presentation - Design,Infographics,Vector Tracing,Invitations,Other");

                $arrServiceDigitalSubCategory = explode(',', "Social Media Marketing,Search Engine Optimization,Web Traffic,Content Marketing,Social Video Marketing,Email Marketing,Search Engine Marketing,Marketing Strategy,Web Analytics,Influencer Marketing,Local Listings,Domain Research,Mobile Advertising,Other");

                $arrServiceWritingSubCategory = explode(',', "Resumes and Cover Letters,Proofreading and Editing,Translation,Creative Writing,Business Copywriting,Research and Summaries,Articles and Blog Posts,Press Release,Transcription,Legal Writing,Thesis,Other");

                $arrServiceVideoSubCategory = explode(',', "Whiteboard and Explainer Videos,Intros and Animated Logos,Promotional and Brand Videos,Editing and Post Production,Lyric and Music Videos,Spokespersons and Testimonials,Animated Characters and Modeling,Other");

                $arrServiceMusicSubCategory = explode(',', "Voice Over,Mixing and Mastering,Producers and Composers,Singer-Songwriters,Session Musicians and Singers,Jingles and Drops,Sound Effects,Other");

                $arrServiceProgrammingSubCategory = explode(',', "WordPress,Website Builders and CMS,Web Programming,Ecommerce,Mobile Apps and Web,Desktop applications,Support and IT,Data Analysis and Reports,Convert Files,DatabasesUser Testing,Quality Assurance,Other");

                $arrServiceAdvertisingSubCategory = explode(',', "Music Promotion,Radio,Banner Advertising,Flyers and Handouts,Pet Models,Other");

                $arrServiceBusinessSubCategory = explode(',', "Virtual Assistant,Accounting services,Market Research,Business Plans,Branding Services,Legal Consulting,Financial Consulting,Business Tips,Presentations,Career Advice,Other");

                $arrServiceFunSubCategory = explode(',', "Online Lessons,Arts and Crafts,Relationship Advice,Health,Nutrition and Fitness,Astrology and Readings,Spiritual and Healing,Family and Genealogy,Collectibles,Greeting Cards and Videos,Your Message On,Viral Videos,Pranks and Stunts,Celebrity Impersonators,Gaming,Global Culture,Other");


                $arrInsertData = array();

                $intMainCount = 1;

                $intProductCount = count($arrMainCategory) + 1;

                $intProductSubCount = $intProductCount + count($arrProductCategories);

                $intServiceCount = $intProductSubCount + count($arrProductRealStateSubCategory) + count($arrProductVehicleSubCategory) + count($arrProductElectronicsSubCategory) + count($arrProductFashionSubCategory) + count($arrProductSportsSubCategory) + count($arrProductFurnitureSubCategory) + count($arrProductPetsSubCategory) + 1;

                $intServiceSubCount = $intServiceCount + count($arrServiceCategories);

                $dtCreate = date("Y-m-d H:i:s", strtotime("now"));

                foreach ($arrMainCategory as $strMainCategory)
                {
                    $arrInsertData[] = array('category_id' => $intMainCount, 'parent_id' => null, 'handle' => 'MAIN_' . str_replace(' ', '_', strtoupper($strMainCategory)), 'category_name' => $strMainCategory, 'created_at' => $dtCreate, 'updated_at' => $dtCreate);

                    if ($strMainCategory == 'Products')
                    {
                        foreach ($arrProductCategories as $strProductCategory)
                        {
                            $arrInsertData[] = array('category_id' => $intProductCount, 'parent_id' => $intMainCount, 'handle' => 'CAT_' . str_replace(' ', '_', strtoupper($strProductCategory)), 'category_name' => $strProductCategory, 'created_at' => $dtCreate, 'updated_at' => $dtCreate);

                            if ($strProductCategory == 'Real Estate')
                            {
                                foreach ($arrProductRealStateSubCategory as $strProductRealStateSubCategory)
                                {
                                    $arrInsertData[] = array('category_id' => $intProductSubCount, 'parent_id' => $intProductCount, 'handle' => 'SUB_' . str_replace(' ', '_', strtoupper($strProductRealStateSubCategory)), 'category_name' => $strProductRealStateSubCategory, 'created_at' => $dtCreate, 'updated_at' => $dtCreate);

                                    $intProductSubCount++;
                                }
                            }

                            if ($strProductCategory == 'Vehicle')
                            {
                                foreach ($arrProductVehicleSubCategory as $strProductVehicleSubCategory)
                                {
                                    $arrInsertData[] = array('category_id' => $intProductSubCount, 'parent_id' => $intProductCount, 'handle' => 'SUB_' . str_replace(' ', '_', strtoupper($strProductVehicleSubCategory)), 'category_name' => $strProductVehicleSubCategory, 'created_at' => $dtCreate, 'updated_at' => $dtCreate);

                                    $intProductSubCount++;
                                }
                            }

                            if ($strProductCategory == 'Electronics')
                            {
                                foreach ($arrProductElectronicsSubCategory as $strProductElectronicsSubCategory)
                                {
                                    $arrInsertData[] = array('category_id' => $intProductSubCount, 'parent_id' => $intProductCount, 'handle' => 'SUB_' . str_replace(' ', '_', strtoupper($strProductElectronicsSubCategory)), 'category_name' => $strProductElectronicsSubCategory, 'created_at' => $dtCreate, 'updated_at' => $dtCreate);

                                    $intProductSubCount++;
                                }
                            }

                            if ($strProductCategory == 'Fashion')
                            {
                                foreach ($arrProductFashionSubCategory as $strProductFashionSubCategory)
                                {
                                    $arrInsertData[] = array('category_id' => $intProductSubCount, 'parent_id' => $intProductCount, 'handle' => 'SUB_' . str_replace(' ', '_', strtoupper($strProductFashionSubCategory)), 'category_name' => $strProductFashionSubCategory, 'created_at' => $dtCreate, 'updated_at' => $dtCreate);

                                    $intProductSubCount++;
                                }
                            }

                            if ($strProductCategory == 'Sports')
                            {
                                foreach ($arrProductSportsSubCategory as $strProductSportsSubCategory)
                                {
                                    $arrInsertData[] = array('category_id' => $intProductSubCount, 'parent_id' => $intProductCount, 'handle' => 'SUB_' . str_replace(' ', '_', strtoupper($strProductSportsSubCategory)), 'category_name' => $strProductSportsSubCategory, 'created_at' => $dtCreate, 'updated_at' => $dtCreate);

                                    $intProductSubCount++;
                                }
                            }

                            if ($strProductCategory == 'Furniture')
                            {
                                foreach ($arrProductFurnitureSubCategory as $strProductFurnitureSubCategory)
                                {
                                    $arrInsertData[] = array('category_id' => $intProductSubCount, 'parent_id' => $intProductCount, 'handle' => 'SUB_' . str_replace(' ', '_', strtoupper($strProductFurnitureSubCategory)), 'category_name' => $strProductFurnitureSubCategory, 'created_at' => $dtCreate, 'updated_at' => $dtCreate);

                                    $intProductSubCount++;
                                }
                            }

                            if ($strProductCategory == 'Pets')
                            {
                                foreach ($arrProductPetsSubCategory as $strProductPetsSubCategory)
                                {
                                    $arrInsertData[] = array('category_id' => $intProductSubCount, 'parent_id' => $intProductCount, 'handle' => 'SUB_' . str_replace(' ', '_', strtoupper($strProductPetsSubCategory)), 'category_name' => $strProductPetsSubCategory, 'created_at' => $dtCreate, 'updated_at' => $dtCreate);

                                    $intProductSubCount++;
                                }
                            }

                            $intProductCount++;
                        }
                    }
                    else
                    {
                        foreach ($arrServiceCategories as $strServiceCategory)
                        {
                            $arrInsertData[] = array('category_id' => $intServiceCount, 'parent_id' => $intMainCount, 'handle' => 'CAT_' . str_replace(' ', '_', strtoupper($strServiceCategory)), 'category_name' => $strServiceCategory, 'created_at' => $dtCreate, 'updated_at' => $dtCreate);

                            if ($strServiceCategory == 'Graphics and Design')
                            {
                                foreach ($arrServiceGraphicsSubCategory as $strServiceGraphicsSubCategory)
                                {
                                    if ($strServiceGraphicsSubCategory != 'Other')
                                    {
                                        $arrInsertData[] = array('category_id' => $intServiceSubCount, 'parent_id' => $intServiceCount, 'handle' => 'SUB_' . str_replace(' ', '_', strtoupper($strServiceGraphicsSubCategory)), 'category_name' => $strServiceGraphicsSubCategory, 'created_at' => $dtCreate, 'updated_at' => $dtCreate);
                                    }
                                    else
                                    {
                                        $arrInsertData[] = array('category_id' => $intServiceSubCount, 'parent_id' => $intServiceCount, 'handle' => 'SUB_' . str_replace(' ', '_', strtoupper($strServiceCategory . '_' . $strServiceGraphicsSubCategory)), 'category_name' => $strServiceGraphicsSubCategory, 'created_at' => $dtCreate, 'updated_at' => $dtCreate);
                                    }

                                    $intServiceSubCount++;
                                }
                            }

                            if ($strServiceCategory == 'Digital Marketing')
                            {
                                foreach ($arrServiceDigitalSubCategory as $strServiceDigitalSubCategory)
                                {
                                    if ($strServiceDigitalSubCategory != 'Other')
                                    {
                                        $arrInsertData[] = array('category_id' => $intServiceSubCount, 'parent_id' => $intServiceCount, 'handle' => 'SUB_' . str_replace(' ', '_', strtoupper($strServiceDigitalSubCategory)), 'category_name' => $strServiceDigitalSubCategory, 'created_at' => $dtCreate, 'updated_at' => $dtCreate);
                                    }
                                    else
                                    {
                                        $arrInsertData[] = array('category_id' => $intServiceSubCount, 'parent_id' => $intServiceCount, 'handle' => 'SUB_' . str_replace(' ', '_', strtoupper($strServiceCategory . '_' . $strServiceDigitalSubCategory)), 'category_name' => $strServiceDigitalSubCategory, 'created_at' => $dtCreate, 'updated_at' => $dtCreate);
                                    }

                                    $intServiceSubCount++;
                                }
                            }

                            if ($strServiceCategory == 'Writing Translation')
                            {
                                foreach ($arrServiceWritingSubCategory as $strServiceWritingSubCategory)
                                {
                                    if ($strServiceWritingSubCategory != 'Other')
                                    {
                                        $arrInsertData[] = array('category_id' => $intServiceSubCount, 'parent_id' => $intServiceCount, 'handle' => 'SUB_' . str_replace(' ', '_', strtoupper($strServiceWritingSubCategory)), 'category_name' => $strServiceWritingSubCategory, 'created_at' => $dtCreate, 'updated_at' => $dtCreate);
                                    }
                                    else
                                    {
                                        $arrInsertData[] = array('category_id' => $intServiceSubCount, 'parent_id' => $intServiceCount, 'handle' => 'SUB_' . str_replace(' ', '_', strtoupper($strServiceCategory . '_' . $strServiceWritingSubCategory)), 'category_name' => $strServiceWritingSubCategory, 'created_at' => $dtCreate, 'updated_at' => $dtCreate);
                                    }

                                    $intServiceSubCount++;
                                }
                            }

                            if ($strServiceCategory == 'Video and Animation')
                            {
                                foreach ($arrServiceVideoSubCategory as $strServiceVideoSubCategory)
                                {
                                    if ($strServiceVideoSubCategory != 'Other')
                                    {
                                        $arrInsertData[] = array('category_id' => $intServiceSubCount, 'parent_id' => $intServiceCount, 'handle' => 'SUB_' . str_replace(' ', '_', strtoupper($strServiceVideoSubCategory)), 'category_name' => $strServiceVideoSubCategory, 'created_at' => $dtCreate, 'updated_at' => $dtCreate);
                                    }
                                    else
                                    {
                                        $arrInsertData[] = array('category_id' => $intServiceSubCount, 'parent_id' => $intServiceCount, 'handle' => 'SUB_' . str_replace(' ', '_', strtoupper($strServiceCategory . '_' . $strServiceVideoSubCategory)), 'category_name' => $strServiceVideoSubCategory, 'created_at' => $dtCreate, 'updated_at' => $dtCreate);
                                    }

                                    $intServiceSubCount++;
                                }
                            }

                            if ($strServiceCategory == 'Music and Audio')
                            {
                                foreach ($arrServiceMusicSubCategory as $strServiceMusicSubCategory)
                                {
                                    if ($strServiceMusicSubCategory != 'Other')
                                    {
                                        $arrInsertData[] = array('category_id' => $intServiceSubCount, 'parent_id' => $intServiceCount, 'handle' => 'SUB_' . str_replace(' ', '_', strtoupper($strServiceMusicSubCategory)), 'category_name' => $strServiceMusicSubCategory, 'created_at' => $dtCreate, 'updated_at' => $dtCreate);
                                    }
                                    else
                                    {
                                        $arrInsertData[] = array('category_id' => $intServiceSubCount, 'parent_id' => $intServiceCount, 'handle' => 'SUB_' . str_replace(' ', '_', strtoupper($strServiceCategory . '_' . $strServiceMusicSubCategory)), 'category_name' => $strServiceMusicSubCategory, 'created_at' => $dtCreate, 'updated_at' => $dtCreate);
                                    }

                                    $intServiceSubCount++;
                                }
                            }

                            if ($strServiceCategory == 'Programming and Tech')
                            {
                                foreach ($arrServiceProgrammingSubCategory as $strServiceProgrammingSubCategory)
                                {
                                    if ($strServiceProgrammingSubCategory != 'Other')
                                    {
                                        $arrInsertData[] = array('category_id' => $intServiceSubCount, 'parent_id' => $intServiceCount, 'handle' => 'SUB_' . str_replace(' ', '_', strtoupper($strServiceProgrammingSubCategory)), 'category_name' => $strServiceProgrammingSubCategory, 'created_at' => $dtCreate, 'updated_at' => $dtCreate);
                                    }
                                    else
                                    {
                                        $arrInsertData[] = array('category_id' => $intServiceSubCount, 'parent_id' => $intServiceCount, 'handle' => 'SUB_' . str_replace(' ', '_', strtoupper($strServiceCategory . '_' . $strServiceProgrammingSubCategory)), 'category_name' => $strServiceProgrammingSubCategory, 'created_at' => $dtCreate, 'updated_at' => $dtCreate);
                                    }

                                    $intServiceSubCount++;
                                }
                            }

                            if ($strServiceCategory == 'Advertsing')
                            {
                                foreach ($arrServiceAdvertisingSubCategory as $strServiceAdvertisingSubCategory)
                                {
                                    if ($strServiceAdvertisingSubCategory != 'Other')
                                    {
                                        $arrInsertData[] = array('category_id' => $intServiceSubCount, 'parent_id' => $intServiceCount, 'handle' => 'SUB_' . str_replace(' ', '_', strtoupper($strServiceAdvertisingSubCategory)), 'category_name' => $strServiceAdvertisingSubCategory, 'created_at' => $dtCreate, 'updated_at' => $dtCreate);
                                    }
                                    else
                                    {
                                        $arrInsertData[] = array('category_id' => $intServiceSubCount, 'parent_id' => $intServiceCount, 'handle' => 'SUB_' . str_replace(' ', '_', strtoupper($strServiceCategory . '_' . $strServiceAdvertisingSubCategory)), 'category_name' => $strServiceAdvertisingSubCategory, 'created_at' => $dtCreate, 'updated_at' => $dtCreate);
                                    }

                                    $intServiceSubCount++;
                                }
                            }

                            if ($strServiceCategory == 'Business')
                            {
                                foreach ($arrServiceBusinessSubCategory as $strServiceBusinessSubCategory)
                                {
                                    if ($strServiceBusinessSubCategory != 'Other')
                                    {
                                        $arrInsertData[] = array('category_id' => $intServiceSubCount, 'parent_id' => $intServiceCount, 'handle' => 'SUB_' . str_replace(' ', '_', strtoupper($strServiceBusinessSubCategory)), 'category_name' => $strServiceBusinessSubCategory, 'created_at' => $dtCreate, 'updated_at' => $dtCreate);
                                    }
                                    else
                                    {
                                        $arrInsertData[] = array('category_id' => $intServiceSubCount, 'parent_id' => $intServiceCount, 'handle' => 'SUB_' . str_replace(' ', '_', strtoupper($strServiceCategory . '_' . $strServiceBusinessSubCategory)), 'category_name' => $strServiceBusinessSubCategory, 'created_at' => $dtCreate, 'updated_at' => $dtCreate);
                                    }

                                    $intServiceSubCount++;
                                }
                            }

                            if ($strServiceCategory == 'Fun and Life Style')
                            {
                                foreach ($arrServiceFunSubCategory as $strServiceFunSubCategory)
                                {
                                    if ($strServiceFunSubCategory != 'Other')
                                    {
                                        $arrInsertData[] = array('category_id' => $intServiceSubCount, 'parent_id' => $intServiceCount, 'handle' => 'SUB_' . str_replace(' ', '_', strtoupper($strServiceFunSubCategory)), 'category_name' => $strServiceFunSubCategory, 'created_at' => $dtCreate, 'updated_at' => $dtCreate);
                                    }
                                    else
                                    {
                                        $arrInsertData[] = array('category_id' => $intServiceSubCount, 'parent_id' => $intServiceCount, 'handle' => 'SUB_' . str_replace(' ', '_', strtoupper($strServiceCategory . '_' . $strServiceFunSubCategory)), 'category_name' => $strServiceGraphicsSubCategory, 'created_at' => $dtCreate, 'updated_at' => $dtCreate);
                                    }

                                    $intServiceSubCount++;
                                }
                            }

                            $intServiceCount++;
                        }
                    }
                    $intMainCount++;
                }

                DB::table('t_category')->insert($arrInsertData);

                Schema::enableForeignKeyConstraints();
            });

            $this->command->info('Category table initial records successfully seeded!');

            DB::commit();
        }
        catch(Exception $objException)
        {
            DB::rollBack();

            syslog(LOG_CRIT,__CLASS__.":".__METHOD__."-{$objException->getMessage()}");

            $this->command->info("City table seeding failed with error - {$objException->getMessage()}");
        }
    }
}
