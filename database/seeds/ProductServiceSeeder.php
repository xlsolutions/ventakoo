<?php

use Illuminate\Database\Seeder;
use Illuminate\Http\UploadedFile;
use Maatwebsite\Excel\Facades\Excel;

class ProductServiceSeeder extends Seeder
{
    /**
     * Run the database seeds.
     *
     * @return void
     */
    public function run()
    {
        //constants
        $this->call('StatusTableSeeder');
        $this->call('ProductInformationTableSeeder');
        $this->call('ServiceInformationTableSeeder');

        //non-contants
        if (env('APP_ENV', 'local') === 'local')
        {
            $this->call('ProductServiceTableSeeder');
        }
    }

}

class StatusTableSeeder extends Seeder
{
    public function run()
    {
        try
        {
            Eloquent::unguard();

            DB::transaction(function ()
            {
                Schema::disableForeignKeyConstraints();

                $statusModel = app('App\Models\Constants\CStatus');

                $data = [
                    [
                        'handle'      => 'PUBLIC',
                        'description' => 'Public'
                    ],
                    [
                        'handle'      => 'HIDDEN',
                        'description' => 'Hidden'
                    ]
                ];

                foreach ($data as $key => $status)
                {

                    $filterStatus = array_except($status, ['handle']);

                    $statusObj = $statusModel->updateOrCreate(['handle' => $status['handle']], $filterStatus);

                    if (isset($status['handle']))
                    {
                        $objStatus = $statusModel->where('handle', $status['handle'])->first();

                        if ($objStatus)
                        {
                            $statusObj->status_id = $objStatus->status_id;
                            $statusObj->save();
                        }

                    }
                }

                $this->command->info('Status table initial records successfully seeded!');

                Schema::enableForeignKeyConstraints();
            });

            DB::commit();
        }
        catch(Exception $objException)
        {
            DB::rollBack();

            syslog(LOG_CRIT,__CLASS__.":".__METHOD__."-{$objException->getMessage()}");

            $this->command->info("Status table seeding failed with error - {$objException->getMessage()}");
        }
    }
}

class ProductInformationTableSeeder extends Seeder
{
    public function run()
    {
        try
        {
            Eloquent::unguard();

            DB::transaction(function ()
            {
                Schema::disableForeignKeyConstraints();

                $productInfoModel = app('App\Models\Constants\CProductInformation');

                $data = [
                    [
                        'handle'                 => 'COLOR',
                        'product_attribute_type' => 'color'
                    ],
                    [
                        'handle'                 => 'DESCRIPTION',
                        'product_attribute_type' => 'description'
                    ],
                    [
                        'handle'                 => 'NAME',
                        'product_attribute_type' => 'name'
                    ],
                    [
                        'handle'                 => 'SIZE',
                        'product_attribute_type' => 'size'
                    ],
                    [
                        'handle'                 => 'IMAGE_THUMBNAIL',
                        'product_attribute_type' => 'image_thumbnail'
                    ],
                    [
                        'handle'                 => 'IMAGE_ORIGINAL',
                        'product_attribute_type' => 'image_original'
                    ],
                    [
                        'handle'                 => 'IMAGE_GALLERY',
                        'product_attribute_type' => 'image_gallery'
                    ]
                ];

                foreach ($data as $key => $productInfo)
                {
                    $filterProductInfo = array_except($productInfo, ['handle']);

                    $productInfoObj = $productInfoModel->updateOrCreate(['handle' => $productInfo['handle']], $filterProductInfo);

                    if (isset($productInfo['handle']))
                    {
                        $objProductInfo = $productInfoModel->where('handle', $productInfo['handle'])->first();

                        if ($objProductInfo)
                        {
                            $productInfoObj->product_information_id = $objProductInfo->product_information_id;
                            $productInfoObj->save();
                        }
                    }
                }

                Schema::enableForeignKeyConstraints();

                $this->command->info('Product Information table initial records successfully seeded!');
            });

            DB::commit();
        }
        catch(Exception $objException)
        {
            DB::rollBack();

            syslog(LOG_CRIT,__CLASS__.":".__METHOD__."-{$objException->getMessage()}");

            $this->command->info("Product Information table seeding failed with error - {$objException->getMessage()}");
        }
    }
}

class ServiceInformationTableSeeder extends Seeder
{
    public function run()
    {
        try
        {
            Eloquent::unguard();

            DB::transaction(function ()
            {
                Schema::disableForeignKeyConstraints();

                $serviceInfoModel = app('App\Models\Constants\CServiceInformation');

                $data = [
                    [
                        'handle'                 => 'COLOR',
                        'service_attribute_type' => 'color'
                    ],
                    [
                        'handle'                 => 'DESCRIPTION',
                        'service_attribute_type' => 'description'
                    ],
                    [
                        'handle'                 => 'NAME',
                        'service_attribute_type' => 'name'
                    ],
                    [
                        'handle'                 => 'SIZE',
                        'service_attribute_type' => 'size'
                    ],                [
                        'handle'                 => 'IMAGE_THUMBNAIL',
                        'service_attribute_type' => 'image_thumbnail'
                    ],
                    [
                        'handle'                 => 'IMAGE_ORIGINAL',
                        'service_attribute_type' => 'image_original'
                    ],
                    [
                        'handle'                 => 'IMAGE_GALLERY',
                        'service_attribute_type' => 'image_gallery'
                    ]
                ];

                foreach ($data as $key => $productInfo)
                {

                    $filterServicetInfo = array_except($productInfo, ['handle']);

                    $serviceInfoObj = $serviceInfoModel->updateOrCreate(['handle' => $productInfo['handle']], $filterServicetInfo);

                    if (isset($serviceInfoObj['handle']))
                    {
                        $objServiceInfo = $serviceInfoModel->where('handle', $serviceInfoObj['handle'])->first();

                        if ($objServiceInfo)
                        {
                            $serviceInfoObj->service_information_id = $objServiceInfo->service_information_id;
                            $serviceInfoObj->save();
                        }
                    }
                }

                Schema::enableForeignKeyConstraints();

                $this->command->info('Service Information table initial records successfully seeded!');
            });

            DB::commit();
        }
        catch(Exception $objException)
        {
            DB::rollBack();

            syslog(LOG_CRIT,__CLASS__.":".__METHOD__."-{$objException->getMessage()}");

            $this->command->info("Service Information table seeding failed with error - {$objException->getMessage()}");
        }
    }
}

class ProductServiceTableSeeder extends Seeder
{
    public function run()
    {
        try
        {
            Eloquent::unguard();

            $strSysClientDirectory = "resources/sysclient";

            if(!file_exists($strSysClientDirectory))Storage::makeDirectory($strSysClientDirectory);
            if(!file_exists($strSysClientDirectory."/images"))Storage::makeDirectory($strSysClientDirectory."/images");

            if(!file_exists($strSysClientDirectory.'/images/product'))Storage::makeDirectory($strSysClientDirectory.'/images/product');
            if(!file_exists($strSysClientDirectory.'/images/service'))Storage::makeDirectory($strSysClientDirectory.'/images/service');

            $strProductThumbnailPath = '/product/thumbnail';
            $strProductDefaultPath = '/product/default';
            $strProductGalleryPath = '/product/gallery';
            $strServiceThumbnailPath = '/service/thumbnail';
            $strServiceDefaultPath = '/service/default';
            $strServiceGalleryPath = '/service/gallery';

            if(!file_exists($strSysClientDirectory."/images".$strProductThumbnailPath))Storage::disk('sysclient-images')->makeDirectory($strProductThumbnailPath);
            if(!file_exists($strSysClientDirectory."/images".$strProductDefaultPath))Storage::disk('sysclient-images')->makeDirectory($strProductDefaultPath);
            if(!file_exists($strSysClientDirectory."/images".$strProductGalleryPath))Storage::disk('sysclient-images')->makeDirectory($strProductGalleryPath);
            if(!file_exists($strSysClientDirectory."/images".$strServiceThumbnailPath))Storage::disk('sysclient-images')->makeDirectory($strServiceThumbnailPath);
            if(!file_exists($strSysClientDirectory."/images".$strServiceDefaultPath))Storage::disk('sysclient-images')->makeDirectory($strServiceDefaultPath);
            if(!file_exists($strSysClientDirectory."/images".$strServiceGalleryPath))Storage::disk('sysclient-images')->makeDirectory($strServiceGalleryPath);

            if(file_exists($strSysClientDirectory."/images".$strProductThumbnailPath))
            {
                $allFiles = Storage::disk('sysclient-images')->allFiles($strProductThumbnailPath);
                Storage::disk('sysclient-images')->delete($allFiles);
            }

            if(file_exists($strSysClientDirectory."/images".$strProductDefaultPath))
            {
                $allFiles =  Storage::disk('sysclient-images')->allFiles($strProductDefaultPath);
                Storage::disk('sysclient-images')->delete($allFiles);
            }

            if(file_exists($strSysClientDirectory."/images".$strProductGalleryPath))
            {
                $allFiles = Storage::disk('sysclient-images')->allFiles($strProductGalleryPath);
                Storage::disk('sysclient-images')->delete($allFiles);
            }

            if(file_exists($strSysClientDirectory."/images".$strServiceThumbnailPath))
            {
                $allFiles = Storage::disk('sysclient-images')->allFiles($strServiceThumbnailPath);
                Storage::disk('sysclient-images')->delete($allFiles);
            }

            if(file_exists($strSysClientDirectory."/images".$strServiceDefaultPath))
            {
                $allFiles =  Storage::disk('sysclient-images')->allFiles($strServiceDefaultPath);
                Storage::disk('sysclient-images')->delete($allFiles);
            }

            if(file_exists($strSysClientDirectory."/images".$strServiceGalleryPath))
            {
                $allFiles = Storage::disk('sysclient-images')->allFiles($strServiceGalleryPath);
                Storage::disk('sysclient-images')->delete($allFiles);
            }

            DB::transaction(function ()
            {
                Schema::disableForeignKeyConstraints();

                Excel::filter('chunk')->load(storage_path('csv/product_service_data.csv'))->chunk(100, function ($results)
                {

                    $categoryModel = app('App\Models\Category\TCategory');

                    $productModel = app('App\Models\Products\TProduct');

                    $serviceModel = app('App\Models\Services\TService');

                    $productInfoModel = app('App\Models\Constants\CProductInformation');

                    $serviceInfoModel = app('App\Models\Constants\CServiceInformation');

                    $productAttribtuteModel = app('App\Models\Products\TProductAttribute');

                    $serviceAttribtuteModel = app('App\Models\Services\TServiceAttribute');

                    foreach ($results as $key => $row)
                    {
                        $objCategory = $categoryModel->orderByRaw("RAND()")->first();

                        $data = [
                            'handle'      => sha1(date("Y-m-d H:i:s")).uniqid(),
                            'quantity'    => $row->quantity,
                            'price'       => $row->price,
                            'category_id' => $objCategory->category_id,
                            'status_id'   => 1,
                            'user_id'     => rand(1, 2)
                        ];

                        $strSysAdminDirectory = "resources/sysadmin";

                        $resultProduct = $productModel->where('product_id', $row['id'])->first();

                        if (!$resultProduct)
                        {
                            $objProduct = $productModel->create($data);

                            $objProductInfo = $productInfoModel->get();

                            foreach ($objProductInfo as $key => $pRow)
                            {
                                $fakerFactory = app('Faker\Factory');

                                $data = [
                                    'product_id'             => $objProduct->product_id,
                                    'product_information_id' => $pRow->product_information_id
                                ];

                                $fake = $fakerFactory->create();

                                switch ($pRow->handle)
                                {
                                    case 'COLOR':
                                        $data['value'] = $fake->hexcolor;
                                        break;
                                    case 'DESCRIPTION':
                                        $data['value'] = $fake->text;
                                        break;
                                    case 'NAME':
                                        $data['value'] = 'Product ' . $objProduct->product_id;
                                        break;
                                    case 'SIZE':
                                        $data['value'] = mt_rand(1, 1000);
                                        break;
                                    case 'IMAGE_THUMBNAIL':
                                        $files = glob($strSysAdminDirectory."/sample_images/thumbnail" . '/*.*');
                                        $file = array_rand($files);
                                        $filePath = "resources/sysclient/images/product/thumbnail/product_".$objProduct->product_id."_".date('YmdHis').".jpg";
                                        file_put_contents($filePath, fopen($files[$file], 'r'));
                                        $data['value'] = "product_".$objProduct->product_id."_".date('YmdHis').".jpg";
                                        break;
                                    case 'IMAGE_ORIGINAL':
                                        $files = glob($strSysAdminDirectory."/sample_images/default" . '/*.*');
                                        $file = array_rand($files);
                                        $filePath = "resources/sysclient/images/product/default/product_".$objProduct->product_id."_".date('YmdHis').".jpg";
                                        file_put_contents($filePath, fopen($files[$file], 'r'));
                                        $data['value'] = "product_".$objProduct->product_id."_".date('YmdHis').".jpg";
                                        break;
                                    case 'IMAGE_GALLERY':
                                        $files = glob($strSysAdminDirectory."/sample_images/gallery" . '/*.*');
                                        $file = array_rand($files);
                                        $filePath = "resources/sysclient/images/product/gallery/product_".$objProduct->product_id."_".date('YmdHis').".jpg";
                                        file_put_contents($filePath, fopen($files[$file], 'r'));
                                        $data['value'] = "product_".$objProduct->product_id."_".date('YmdHis').".jpg";
                                        break;
                                }

                                $objProductAttribute = $productAttribtuteModel->updateOrCreate($data);

                                $resultProductAtrribute = $productAttribtuteModel->where('product_information_id', $objProductAttribute['product_information_id'])->first();

                                if ($resultProductAtrribute)
                                {
                                    $objProductAttribute->product_information_id = $resultProductAtrribute->product_information_id;
                                    $objProductAttribute->save();
                                }
                            }
                        }

                        $data = [
                            'handle'      => sha1(date("Y-m-d H:i:s")).uniqid(),
                            'duration'    => $row->duration,
                            'price'       => $row->price,
                            'category_id' => $objCategory->category_id,
                            'status_id'   => 1,
                            'user_id'     => rand(1, 2)
                        ];

                        $resultService = $serviceModel->where('service_id', $row['id'])->first();

                        if (!$resultService)
                        {
                            $objService = $serviceModel->create($data);

                            $objServiceInfo = $serviceInfoModel->get();

                            foreach ($objServiceInfo as $sRow)
                            {
                                $fakerFactory = app('Faker\Factory');

                                $data = [
                                    'service_id'             => $objService->service_id,
                                    'service_information_id' => $sRow->service_information_id
                                ];

                                $fake = $fakerFactory->create();

                                switch ($sRow->handle)
                                {
                                    case 'COLOR':
                                        $data['value'] = $fake->hexcolor;
                                        break;
                                    case 'DESCRIPTION':
                                        $data['value'] = $fake->text;
                                        break;
                                    case 'NAME':
                                        $data['value'] = 'Service ' . $objService->service_id;
                                        break;
                                    case 'SIZE':
                                        $data['value'] = mt_rand(1, 1000);
                                        break;
                                    case 'IMAGE_THUMBNAIL':
                                        $files = glob($strSysAdminDirectory."/sample_images/thumbnail" . '/*.*');
                                        $file = array_rand($files);
                                        $filePath = "resources/sysclient/images/service/thumbnail/service_".$objService->service_id."_".date('YmdHis').".jpg";
                                        file_put_contents($filePath, fopen($files[$file], 'r'));
                                        $data['value'] = "service_".$objService->service_id."_".date('YmdHis').".jpg";
                                        break;
                                    case 'IMAGE_ORIGINAL':
                                        $files = glob($strSysAdminDirectory."/sample_images/default" . '/*.*');
                                        $file = array_rand($files);
                                        $filePath = "resources/sysclient/images/service/default/service_".$objService->service_id."_".date('YmdHis').".jpg";
                                        file_put_contents($filePath, fopen($files[$file], 'r'));
                                        $data['value'] = "service_".$objService->service_id."_".date('YmdHis').".jpg";
                                        break;
                                    case 'IMAGE_GALLERY':
                                        $files = glob($strSysAdminDirectory."/sample_images/gallery" . '/*.*');
                                        $file = array_rand($files);
                                        $filePath = "resources/sysclient/images/service/gallery/service_".$objService->service_id."_".date('YmdHis').".jpg";
                                        file_put_contents($filePath, fopen($files[$file], 'r'));
                                        $data['value'] = "service_".$objService->service_id."_".date('YmdHis').".jpg";
                                        break;
                                }

                                $objServiceAttribute = $serviceAttribtuteModel->updateOrCreate($data);

                                $resultServiceAtrribute = $serviceAttribtuteModel->where('service_information_id', $objServiceAttribute['service_information_id'])->first();

                                if ($resultServiceAtrribute)
                                {
                                    $objServiceAttribute->service_information_id = $resultServiceAtrribute->service_information_id;
                                    $objServiceAttribute->save();
                                }
                            }
                        }
                    }
                });

                Schema::enableForeignKeyConstraints();

                $this->command->info('Product and Service table initial records successfully seeded!');
            });

            DB::commit();
        }
        catch(Exception $objException)
        {
            DB::rollBack();

            syslog(LOG_CRIT,__CLASS__.":".__METHOD__."-{$objException->getMessage()}");

            $this->command->info("Product and Service table seeding failed with error - {$objException->getMessage()}");
        }
    }
}