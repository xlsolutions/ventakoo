<?php

use Illuminate\Database\Seeder;

class UpdatePhilippinesStatesAndCitiesSeeder extends Seeder
{
    /**
     * Run the database seeds.
     *
     * @return void
     */
    public function run()
    {
        $this->call('UpdatePhilippinesCityTableSeeder');
        $this->call('UpdatePhilippinesStateTableSeeder');
    }
}

class UpdatePhilippinesCityTableSeeder extends Seeder
{
    public function run()
    {
        try
        {
            Eloquent::unguard();

            DB::transaction(function ()
            {
                Schema::disableForeignKeyConstraints();

                Excel::filter('chunk')->load(storage_path('csv/location/ph_update_city_data.csv'))->chunk(500, function ($results) {

                    $countryModel = app('App\Models\Constants\CCountry');

                    $resultCountry = $countryModel->where('country_code','PH')->first();

                    if(!$resultCountry)
                    {
                        throw new Exception("Country with country code 'PH' does not exist!");
                    }

                    $stateModel = app('App\Models\Constants\CState');

                    $stateModel->where('country_id',$resultCountry['country_id'])->get();

                    $cityModel = app('App\Models\Constants\CCity');

                    foreach ($stateModel as $itemState)
                    {
                        $cityModel->where('state_id',$itemState['state_id'])->delete();
                    }

                    foreach ($results as $key => $row)
                    {
                        $resultCity = $cityModel->where('city_id', $row['city_id'])->first();

                        if (!$resultCity)
                        {
                            $data = [
                                'state_id' => $row->state_id,
                                'city_name' => $row->city_name
                            ];

                            $cityModel->create($data);
                        }
                    }
                });

                $this->command->info('Updates for Philippines City records successfully seeded!');

                Schema::enableForeignKeyConstraints();
            });

            DB::commit();
        }
        catch (Exception $objException)
        {
            DB::rollBack();

            syslog(LOG_CRIT, __CLASS__ . ":" . __METHOD__ . "-{$objException->getMessage()}");

            $this->command->info("Updates for Philippines City records failed with error - {$objException->getMessage()}");
        }
    }
}

class UpdatePhilippinesStateTableSeeder extends Seeder
{
    public function run()
    {
        try
        {
            Eloquent::unguard();

            DB::transaction(function ()
            {
                Schema::disableForeignKeyConstraints();

                Excel::filter('chunk')->load(storage_path('csv/location/ph_update_state_data.csv'))->chunk(250, function ($results)
                {
                    $countryModel = app('App\Models\Constants\CCountry');

                    $resultCountry = $countryModel->where('country_code','PH')->first();

                    if(!$resultCountry)
                    {
                        throw new Exception("Country with country code 'PH' does not exist!");
                    }

                    $stateModel = app('App\Models\Constants\CState');

                    $stateModel->where('country_id',$resultCountry['country_id'])->delete();

                    foreach ($results as $key => $row)
                    {
                        $resultState = $stateModel->where('state_id', $row['state_id'])->first();

                        if (!$resultState)
                        {
                            $data = [
                                'country_id' => $row->country_id,
                                'state_name' => $row->state_name
                            ];

                            $stateModel->create($data);
                        }
                    }

                });

                Schema::enableForeignKeyConstraints();

                $this->command->info('Updates for Philippines State records successfully seeded!');
            });

            DB::commit();
        }
        catch(Exception $objException)
        {
            DB::rollBack();

            syslog(LOG_CRIT,__CLASS__.":".__METHOD__."-{$objException->getMessage()}");

            $this->command->info("Updates for Philippines State records failed with error - {$objException->getMessage()}");
        }
    }
}
