<?php

use Illuminate\Database\Seeder;

class UserProductAndServiceLinkerSeeder extends Seeder
{
    /**
     * Run seeder.
     *
     * @return void
     */
    public function run()
    {
        try
        {
            Eloquent::unguard();

            DB::transaction(function ()
            {
                $arrData = [];

                $objCProductInformation = app('App\Models\Constants\CProductInformation');

                $objCProductInformationSet = $objCProductInformation::all();

                $dtCreated = date('Y-m-d H:i:s');

                foreach ($objCProductInformationSet as $objCProductInformation)
                {
                    $arrData[] = [
                        'product_information_id' => $objCProductInformation->product_information_id,
                        'user_id' => 1,
                        'created_at' => $dtCreated
                    ];

                    $arrData[] = [
                        'product_information_id' => $objCProductInformation->product_information_id,
                        'user_id' => 2,
                        'created_at' => $dtCreated
                    ];
                }

                $objCServiceInformation = app('App\Models\Constants\CServiceInformation');

                $objCServiceInformationSet = $objCServiceInformation::all();

                foreach ($objCServiceInformationSet as $objCServiceInformation)
                {
                    $arrData[] = [
                        'service_information_id' => $objCServiceInformation->service_information_id,
                        'user_id' => 1,
                        'created_at' => $dtCreated
                    ];

                    $arrData[] = [
                        'service_information_id' => $objCServiceInformation->service_information_id,
                        'user_id' => 2,
                        'created_at' => $dtCreated
                    ];
                }

                $objTUserProductAndServiceLinker = app('App\Models\Users\TUserProductAndServiceLinker');
                
                foreach ($arrData as $data)
                {
                    $objTUserProductAndServiceLinker->updateOrCreate($data);
                }

                $this->command->info('User product and service linker table initial records successfully seeded!');

            });

            DB::commit();
        }
        catch(Exception $objException)
        {
            DB::rollBack();

            syslog(LOG_CRIT,__CLASS__.":".__METHOD__."-{$objException->getMessage()}");

            $this->command->info("User product and service linker table seeding failed with error - {$objException->getMessage()}");
        }
    }
}