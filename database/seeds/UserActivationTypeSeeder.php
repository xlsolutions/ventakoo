<?php

use Illuminate\Database\Seeder;

class UserActivationTypeSeeder extends Seeder
{
    public function run()
    {
        try
        {
            Eloquent::unguard();

            DB::transaction(function () 
            {
                Schema::disableForeignKeyConstraints();

                $arrData = [
                    [
                        'handle' => 'VERIFY_ACCOUNT_EMAIL',
                        'description' => 'Token / code used to verify an account after registration'
                    ],
                    [
                        'handle' => 'VERIFY_FORGOT_EMAIL',
                        'description' => 'Token / code used to verify an account using forgot password'
                    ]
                ];

                $objModel = app('App\Models\Constants\CActivationType');

                foreach ($arrData as $key => $row)
                {
                    $objModel->insert($row);
                }

                Schema::enableForeignKeyConstraints();

                $this->command->info(__CLASS__ . ' class successfully seeded!');
            });

            DB::commit();
        }
        catch(Exception $objException)
        {
            DB::rollBack();

            $strErrorMsg = __CLASS__ . ":" . __METHOD__ . " - {$objException->getMessage()}";
            
            syslog(LOG_CRIT, $strErrorMsg);

            $this->command->info("Failed seeding: {$strErrorMsg}");
        }
    }
}