<?php

use Illuminate\Database\Seeder;

class UserPlanAndOrderSeeder extends Seeder
{
    /**
     * Run seeder.
     *
     * @return void
     */
    public function run()
    {
        try
        {
            Eloquent::unguard();

            DB::transaction(function ()
            {
                $objCProductPlan = app('App\Models\Constants\CProductPlan');

                $objCProductPlan->updateOrCreate([
                    'handle' => 'FREE_AD_POST_CAMPAIGN',
                    'name' => 'Free',
                    'description' => 'Ad post campaign (free plan account) but first 5 posts are only free.',
                    'is_available' => 1,
                    'price' => 0
                ]);

                $objCProductPlan->updateOrCreate([
                    'handle' => 'STARTER_AD_POST_CAMPAIGN',
                    'name' => 'Starter',
                    'description' => 'Ad post campaign (starter plan account); price 1000Php; no expiration of ad post campaign but 50Php per post',
                    'is_available' => 1,
                    'price' => 1000
                ]);

                $objCProductPlan->updateOrCreate([
                    'handle' => 'PRO_AD_POST_CAMPAIGN',
                    'name' => 'Professional',
                    'description' => 'Ad post campaign (pro plan account); price 2000Php; no expiration of ad post campaign but 45Php per post',
                    'is_available' => 1,
                    'price' => 2000
                ]);

                $objCProductPlan->updateOrCreate([
                    'handle' => 'BUSINESS_AD_POST_CAMPAIGN',
                    'name' => 'Business',
                    'description' => 'Ad post campaign (business plan account); price 3000Php; no expiration of ad post campaign but 40Php per post',
                    'is_available' => 1,
                    'price' => 3000
                ]);

                $this->command->info('Product plans successfully seeded!');

                $objCOrderStatus = app('App\Models\Constants\COrderStatus');

                $objCOrderStatus->updateOrCreate([
                    'handle' => 'NEW',
                    'name' => 'New Payment'
                ]);

                $objCOrderStatus->updateOrCreate([
                    'handle' => 'INITIATING',
                    'name' => 'Initiating Payment'
                ]);

                $objCOrderStatus->updateOrCreate([
                    'handle' => 'AWAITING_PAYMENT',
                    'name' => 'Awaiting Payment'
                ]);

                $objCOrderStatus->updateOrCreate([
                    'handle' => 'ABANDONED',
                    'name' => 'Abandoned Payment'
                ]);

                $objCOrderStatus->updateOrCreate([
                    'handle' => 'CHARGING',
                    'name' => 'Charging'
                ]);

                $objCOrderStatus->updateOrCreate([
                    'handle' => 'PROCESSING',
                    'name' => 'Processing Payment'
                ]);

                $objCOrderStatus->updateOrCreate([
                    'handle' => 'CANCELLED',
                    'name' => 'Payment Cancelled'
                ]);

                $objCOrderStatus->updateOrCreate([
                    'handle' => 'DECLINED',
                    'name' => 'Payment Declined'
                ]);

                $objCOrderStatus->updateOrCreate([
                    'handle' => 'COMPLETED',
                    'name' => 'Payment Completed'
                ]);

                $objCOrderStatus->updateOrCreate([
                    'handle' => 'REVIEW',
                    'name' => 'Reviewing Payment'
                ]);

                $objCOrderStatus->updateOrCreate([
                    'handle' => 'REFUND',
                    'name' => 'Refund Payment'
                ]);

                $this->command->info('Order statuses successfully seeded!');

                if (env('APP_ENV', 'local') === 'local')
                {
                    $objTProductPlan = app('App\Models\Users\TProductPlan');

                    $objCProductPlan = $objCProductPlan::where('handle', 'FREE_AD_POST_CAMPAIGN')->first();

                    $objTProductPlan->updateOrCreate([
                        'product_plan_id' => $objCProductPlan['product_plan_id'],
                        'user_id' => 1
                    ]);

                    $objTProductPlan->updateOrCreate([
                        'product_plan_id' => $objCProductPlan['product_plan_id'],
                        'user_id' => 2
                    ]);

                    $this->command->info('User default product plan successfully seeded!');   
                }
            });

            DB::commit();
        }
        catch(Exception $objException)
        {
            DB::rollBack();

            syslog(LOG_CRIT,__CLASS__.":".__METHOD__."-{$objException->getMessage()}");

            $this->command->info("Product plan, order status and default user plan table seeding failed with error - {$objException->getMessage()}");
        }
    }
}