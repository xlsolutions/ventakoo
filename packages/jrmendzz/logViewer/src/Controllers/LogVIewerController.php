<?php namespace jrmendzz\logViewer\Controllers;
/**
 * Created by PhpStorm.
 * User: jr
 * Date: 11/11/16
 * Time: 6:15 PM
 */

if (class_exists("\\Illuminate\\Routing\\Controller")) {
    class BaseController extends \Illuminate\Routing\Controller {}
} else if (class_exists("Laravel\\Lumen\\Routing\\Controller")) {
    class BaseController extends \Laravel\Lumen\Routing\Controller {}
}

use Illuminate\Support\Facades\File;
use Illuminate\Support\Facades\View;
use Illuminate\Support\Facades\Redirect;
use Illuminate\Support\Facades\Request;
use Illuminate\Support\Facades\Response;
use jrmendzz\logViewer\LaravelLogViewer;
use jrmendzz\logViewer\LogViewer;

class LogViewerController extends BaseController
{

    public function index()
    {

        if (Request::input('l')) {
            LogViewer::setFile(base64_decode(Request::input('l')));
        }

        if (Request::input('dl')) {
            return Response::download(LogViewer::pathToLogFile(base64_decode(Request::input('dl'))));
        } elseif (Request::has('del')) {
            File::delete(LogViewer::pathToLogFile(base64_decode(Request::input('del'))));
            return Redirect::to(Request::url());
        }

        $logs = LogViewer::all();

        return View::make('logViewer::log', [
            'logs' => $logs,
            'files' => LogViewer::getFiles(true),
            'current_file' => LogViewer::getFileName()
        ]);
    }
}
