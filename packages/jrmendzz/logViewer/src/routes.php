<?php

Route::group(['namespace' => 'jrmendzz\logViewer\Controllers', 'prefix'=>'admin'], function() {
    Route::get('logs', 'LogViewerController@index');
});